#!/bin/bash

#
# Run blue-green deployment strategy
#
# Requirements:
# - make
# - jq
# - docker
# - docker compose
# - A run_app_besides task in a Makefile
#   Example:
#   DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up -d --no-deps $(SCALE_COMMAND) --no-recreate
# - A run_app_standalone task in a Makefile
#   Example:
#   DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up $(CONTAINER_NAME) -d
# - A run_app_stack task in a Makefile
#   Example:
#   DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up -d
# - A pull_app in a Makefile
#   Example:
#   DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} docker compose pull

set -e

HEALTHCHECK_TIMEOUT=${DEPLOYMENT_HEALTHCHECK_TIMEOUT:-"10 seconds"} # Maximum timeout waiting for a new container to become healthy before it fails
DEPLOY_FOLDER="deploy" # Folder containing this script and the load balancer's docker compos

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-h] [--no-maintenance] [CONTAINERS]=[SCALE]...
Deploy the list of containers according to the blue-green deployment strategy.
The script deploys each container one per one in the order of the CONTAINERS list.
SCALE, the number of instances of the same app which can run simultaneously, can be either 1 or 2.
By default, a maintenance page is always displayed before switching apps.

    -h                display this help and exit
    --no-maintenance  disable maintenance when switching containers
EOF
}

stdout_logger() {
  # $1=message
  printf "$(date)\t%s\n" "$1"
}

# Declare command line options
LONGOPTS=help,no-maintenance
OPTIONS=h
# Parse options with getopt
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
# Read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# Initiate options
maintenance=y
# Map options to args
while true; do
    case "$1" in
        -h|--help)
            show_help
            exit 0
            ;;
        --no-maintenance)
            maintenance=n
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

{
  # Get current deployment color (blue/green)
  current_deployment=$(docker inspect --format='{{range .Config.Env}}{{println .}}{{end}}' $DEPLOY_FOLDER-docker_gen-1 | grep -P "DEPLOYMENT_COLOR=" | sed 's/[^=]*=//')

  # Calculate next deployment type
  next_deployment='blue'
  if [[ "$next_deployment" == "$current_deployment" ]]; then
    next_deployment='green'
  fi

  # Get Compose Project name
  project_name=$(docker compose config --format json 2&> /dev/null | jq -r .name)

  # Divide the argument string through spaces
  args=$@
  IFS=' ' read -r -a containers_array <<< "$args"

  old_containers_array=()

## 1. PULL DOCKER IMAGES
echo '## 1. PULL DOCKER IMAGES'
DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} make --no-print-directory pull_app

  if [[ $maintenance == 'y' ]]; then
    ## 2. RUN MAINTENANCE MODE
    echo '## 2. RUN MAINTENANCE MODE'
    # Enable maintenance mode on load balancer
    pushd $DEPLOY_FOLDER > /dev/null
    make --no-print-directory switch_lb_maintenance
    popd > /dev/null
  else
    echo '## 2. DEPLOYING WITHOUT MAINTENANCE'
  fi

  ## 3. LAUNCH NEW CONTAINERS
  echo '## 3. LAUNCH NEW CONTAINERS'

  check_running_containers="$(docker compose ps -q)"

  # If no containers are running at all, simply run them at once.
  if [[ -z "$check_running_containers" ]]; then
    DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$next_deployment make --no-print-directory run_app_stack
  # Otherwise, deploy following blue-green strategy
  else
    scale_command_string=""

    for container in "${containers_array[@]}"
    do
      # Read container params
      IFS='=' read -r -a container_params <<< "$container"
      container_name=${container_params[0]}
      container_scale=${container_params[1]}

      # Get current running container
      old_container_id=$(docker ps -f name=$project_name-$container_name -q | tail -n1)

      # If the app was already running
      if [[ -n "$old_container_id" ]] && [[ $container_scale = '2' ]]; then
        # Add running container to the list of all old containers
        old_containers_array+=( $old_container_id )

        # Deploy the new app besides the old one
        scale_command_string+=" --scale $container_name=2"
        DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$next_deployment SCALE_COMMAND=$scale_command_string make --no-print-directory run_app_besides
      else
        # Otherwise deploy the new app alone
        scale_command_string+=" --scale $container_name=1"
        DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$next_deployment CONTAINER_NAME=$container_name make --no-print-directory run_app_standalone
      fi

      ## Docker-gen will already filter on healthy apps when we are going to switch, but
      ## we want to be sure there is at least one healthy container. Otherwise the Nginx
      ## configuration will result with an empty upstream servers list

      # Get new app container_id
      new_container_id=$(docker ps -f name=$project_name-$container_name -q | head -n1)

      # Check if the container has a healthcheck setup
      has_healthcheck=$(docker inspect -f '{{.State.Health}}' $new_container_id)
      if [ "$has_healthcheck" != '<nil>' ]; then
        # Wait for the new app to have a healthy status
        healthy=$(docker inspect -f '{{.State.Health.Status}}' $new_container_id)
        endtime=$(date -ud "$HEALTHCHECK_TIMEOUT" +%s)
        while [ $healthy != 'healthy' ]
        do
            sleep 0.5
            if [[ $(date -u +%s) -ge $endtime ]]; then
              echo "ERROR: Deployed $container_name container is unhealthy after a timeout of $HEALTHCHECK_TIMEOUT. Maintenance page is kept displayed. Please check the issue."
              echo "ERROR: $container_name container healthcheck logs:"
              docker inspect -f '{{range .State.Health.Log}}{{json .}}{{end}}' $new_container_id
              echo "ERROR: $container_name container logs (can also be accessible through the 'journalctl -r CONTAINER_ID=$new_container_id' command):"
              docker logs $new_container_id
              echo "ERROR: Removing failed container..."
              docker stop $new_container_id
              docker rm $new_container_id
              exit 1
            fi
            healthy=$(docker inspect -f '{{.State.Health.Status}}' $new_container_id)
        done
      fi
    done
  fi

  ## 4. SWITCH TO THE NEW CONTAINERS
  echo '## 4. SWITCH TO THE NEW CONTAINERS'

  # Switch load balancer to new app
  pushd $DEPLOY_FOLDER > /dev/null
  DEPLOYMENT_COLOR=$next_deployment make --no-print-directory switch_lb_color
  popd > /dev/null

  ## 5. REMOVE OLD CONTAINERS
  echo '## 5. REMOVE OLD CONTAINERS'

  # Stop old containers if they existed
  for container_id in "${old_containers_array[@]}"
  do
    if [[ -n "$container_id" ]]; then
      docker stop $container_id
      docker rm $container_id
    fi
  done

  echo '## 6. FINISHED DEPLOYMENT'
} > >(while read -r line; do stdout_logger "$line"; done)
