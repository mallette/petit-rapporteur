---
title: "Explorer un rapport"
---

# Explorer un rapport

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et d'écriture (<i class="fa fa-font" title="Droit d'écrire dans le rapport"></i>/`report:write`) associés
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le nom du rapport à compléter
3. Déplier les conteneurs existants avec la flèche de liste déroulante à gauche de chaque ligne
