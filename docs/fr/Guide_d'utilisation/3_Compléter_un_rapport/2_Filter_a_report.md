---
title: "Filtrer un rapport"
---

# Filtrer un rapport

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et d'écriture (<i class="fa fa-font" title="Droit d'écrire dans le rapport"></i>/`report:write`) associés
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le nom du rapport à compléter
3. Dans le bloc « Filtrer les données » en haut de l'écran, choisir dans les menus déroulants de chaque type de catégorie la valeur sur laquelle filtrer. Les menus déroulants se mettent à jour en fonction des valeurs disponibles avec les filtres déjà appliqués. Pour annuler un filtre, choisir la valeur « - ».

<figure>
  <img src="Screenshot_Petit-Rapporteur_filter_report.png" alt="" />
  <figcaption>Seule la catégorie « Académie » avec pour valeur « Aix Marseille » s'affiche dans la catégorie « Priorité » avec pour valeur « 1.2 Orientation et Lutte décrochage ».</figcaption>
</figure>
