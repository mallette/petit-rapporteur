---
title: "Rechercher à travers un rapport"
---

# Rechercher à travers un rapport

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et d'écriture (<i class="fa fa-font" title="Droit d'écrire dans le rapport"></i>/`report:write`) associés
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le nom du rapport à compléter
3. Dans le champ de recherche « Recherchez des contenus dans ce rapport » en haut de l'écran, écrire des termes de recherche. Ces termes seront recherchés dans les noms et valeurs de tous les champs
4. Cliquer sur « Rechercher » ou appuyer sur la touche « Entrée »

<figure>
  <img src="Screenshot_Petit-Rapporteur_search_report.png" alt="" />
  <figcaption>Seul les champs (et leurs conteneurs parents) contenant le terme « 12 » apparaissent. Ici, c'est le champ quantitatif-addition avec la valeur « 12 » qui fait apparaitre la <code>Promo</code> et ses conteneurs parents.</figcaption>
</figure>

!!! info "Note"
    Effectuer une recherche est équivalent à appliquer un filtre comme vu dans [« Filtrer un rapport »](2_Filter_a_report.md).
    La différence c'est qu'il n'est pas possible de cumuler des termes de recherche.
    En revanche, la recherche est libre et peut s'effectuer sur plusieurs niveaux à la fois.
