---
title: "Importer un fichier CSV dans un rapport"
---

# Importer un fichier CSV dans un rapport

Cette fonctionnalité permet d'importer des lignes de données en masse dans un rapport soit depuis un autre logiciel, soit depuis son propre tableur.

Pour utiliser cette fonctionnalité, il est nécessaire de s'assurer que le fichier à importer soit au format CSV et construit comme attendu par le Petit Rapporteur.

## Préparer le fichier CSV pour l'import

Pour importer des lignes de données dans un rapport, le fichier CSV doit être construit de cette manière :

1. Il doit comporter une colonne `id` qui contient un identifiant unique pour chaque ligne
2. Chaque ligne correspond à un `container_promo` (Promo) à importer (une ligne dans l'interface du Petit Rapporteur)
3. Chaque nom de colonne doit correspondre **exactement** à un nom du conteneur du rapport (ainsi que le nom du `container_promo` et des champs à remplir)
4. Le Petit Rapporteur lit chaque colonne de gauche à droite et s'attend à ce que chaque colonne soit contenu dans la colonne précédente
  - Les colonnes vides sont ignorées pour passer à la colonne suivante

!!! info "Import/export du Petit Rapporteur"
    [L'export CSV](../Exploiter_les_données_d'un_rapport/CSV_export_via_interface.md) proposé par le Petit Rapporteur est compatible avec sa propre fonction d'import.
    Il est donc possible d'exporter un rapport au format CSV et de le réimporter dans le même rapport ou un autre rapport après avoir ajouté des nouvelles lignes. Seules les nouvelles lignes avec un identifiant unique qui n'existe pas encore dans le Petit Rapporteur seront importées.
    Il ne s'agit cependant pas de la méthode la plus performante pour réaliser cette opération. Il vaut mieux priviléger les méthodes d'[import et export JSON (usage avancé)](../../../fr_2/Guide_d'administration_expert/Exploiter_les_données_d'un_rapport/3_JSON_import_and_export.md).

### Exemple

Voici un [exemple de fichier CSV](example_assymetric_import.csv) pour importer dans un rapport avec des conteneurs assymétriques tel qu'illustré ci-dessous.

<figure>
  <img src="Screenshot_Petit Rapporteur_import-assymetric-example.png" alt="" />
  <figcaption>Dans cet exemple, les deux conteneurs à la racine ont des noms de catégorie différents (« Colonne1 » et « ColonneDifferente »). Il est quand même possible de former un fichier CSV pour effectuer un import.</figcaption>
</figure>

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et d'import (<i class="fa fa-font" title="Droit d'importer dans le rapport"></i>/`report:import`) associés
2. Depuis la page d'accueil, ouvrir le rapport dans lequel vous souhaitez importer un fichier.
3. Sur la page du rapport, cliquer sur le bouton « Importer un fichier CSV ».
4. Dans la fenêtre modale qui s'est ouverte, choisir le fichier CSV à importer puis cliquer sur « Importer ».
5. Si nécessaire, déplier les options avancées et indiquer l'identifiant de source si les données que vous souhaitez importer proviennent d'un autre système que le Petit Rapporteur (voir la note « Plus de détails sur identifiant de la source »).
7. Des notifications vont s'afficher informant l'état de l'import en cours.
8. Une fois la notification indiquant l'import terminé, cliquer sur le bouton « Voir le résultat ».
9. La fenêtre modale d'import s'ouvre à nouveau avec le résultat de l'import ligne par ligne (« importé » ou « ignoré » si la ligne était déjà présente dans le rapport par exemple).

!!! info "Plus de détails sur identifiant de la source"
    Le champ « Identifiant de la source » est facultatif et est automatiquement complété avec l'identifiant du rapport courant si laissé vide. Il s'agit d'une chaînes de caractères libre dont vous choisissez vous-même la nomenclature.
    Il permet de distinguer les sources de données et d'éviter le réimport de données si elles sont déjà présentes dans le rapport avec le même identifiant de source et le même identifiant de ligne.
    Vous pouvez par exemple importer un fichier CSV provenant du logiciel `Graf` avec une ligne avec pour ID `2`, puis importer un autre fichier CSV que vous avez écrit vous-même avec aussi une ligne avec pour ID `2`.
    Mais si vous réimporter deux fois le même fichier CSV provenant du même logiciel `Graf`, les lignes en doublon ne seront pas réimportées.

<figure>
  <img src="Screenshot_Petit Rapporteur_import-button.png" alt="" />
  <figcaption>Le bouton d'import se trouve sous la barre de recherche.</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_import-modal.png" alt="" />
  <figcaption>La fenêtre modale permet de choisir le fichier à téléverser et de préciser l'identifiant de la source si nécessaire.</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_import-notifications.png" alt="" />
  <figcaption>Lorsque l'import est lancé, les notifications apparaissent en haut à droite pour informer de l'état de la copie. La notification de fin affiche un bouton « Voir le résultat ».</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_import-result.png" alt="" />
  <figcaption>La fenêtre modale affiche les résultats de l'import pour chaque ligne</figcaption>.
</figure>
