---
title: "Copier une ligne dans un rapport"
---

# Copier une ligne dans un rapport

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et d'écriture (<i class="fa fa-font" title="Droit d'écrire dans le rapport"></i>/`report:write`) associés
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le nom du rapport
3. Dérouler complètement les conteneurs existants avec la flèche de liste déroulante à gauche de chaque ligne
4. Sur la ligne à copier, cliquer sur le bouton à droite représentant deux carrés superposés
5. Une fois cliqué sur le bouton, celui-ci change d'apparence en une croix pour annuler l'action en cours si nécessaire
6. Les destinations possibles de la copie apparaissent sous forme de boutons bleus avec une icône « cible ». Cliquer sur le bouton cible situé sur le nom de la catégorie vers laquelle vous souhaitez effectuer la copie.
7. Des notifications vont s'afficher informant l'état de la copie de la ligne en cours
8. Une fois la notification indiquant la copie terminée, rafraichir l'affichage de la catégorie ayant reçu la copie en cliquant dessus deux fois pour plier et déplier si elle était déjà dépliée

<figure>
  <img src="Screenshot_Petit Rapporteur_copy_line_button.png" alt="" />
  <figcaption>Le bouton de copie d'une ligne se situe à droite, avant le bouton de suppression.</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_copy_line_destination.png" alt="" />
  <figcaption>Une fois cliqué sur le bouton de copie, les destinations possibles apparaissent avec un bouton bleu</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_copy_line_notifications.png" alt="" />
  <figcaption>Lorsque la copie est lancée, les notifications apparaissent en haut à droite pour informer de l'état de la copie.</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_copy_line_refresh.png" alt="" />
  <figcaption>Une fois la copie terminée, cliquer sur la catégorie de destination pour afficher le résultat de la copie</figcaption>
</figure>
