---
title: "Écrire dans un rapport"
---

# Écrire dans un rapport

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et d'écriture (<i class="fa fa-font" title="Droit d'écrire dans le rapport"></i>/`report:write`) associés
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le nom du rapport à compléter
3. Dérouler complètement les conteneurs existants avec la flèche de liste déroulante à gauche de chaque ligne
4. Cliquer sur le bouton « Nouvelle ligne »
5. Dans la nouvelle ligne du tableau qui est apparue, compléter les cases correspondantes. L'enregistrement des données se fait automatiquement lorsqu'une case est déselectionnée.
6. Naviguer entre les cases du tableau avec la souris ou avec la touche tabulation du clavier

<figure>
  <img src="Screenshot_Petit-Rapporteur_write_report.png" alt="" />
  <figcaption>Plusieurs lignes peuvent être ajoutées dans un même tableau.</figcaption>
</figure>
