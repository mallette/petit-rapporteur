---
title: "Supprimer une ligne dans un rapport"
---

# Supprimer une ligne dans un rapport

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins les droits de lecture (<i class="fa fa-eye" title="Droit de lire le rapport"></i>/`report:read`) et de suppression (<i class="fa fa-trash" title="Droit de supprimer le rapport"></i>/`report:delete`) associés
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le nom du rapport
3. Dérouler complètement les conteneurs existants avec la flèche de liste déroulante à gauche de chaque ligne
4. Sur la ligne à supprimer, cliquer sur le bouton à droite correspondant à une poubelle pour la supprimer
5. Valider la modale vous demandant de confirmer la suppression de la ligne

<figure>
  <img src="Screenshot_Petit Rapporteur_delete_line.png" alt="" />
  <figcaption>Le bouton de suppression se situe tout à droite de la ligne. Il faut parfois faire défiler l'écran horizontalement pour l'atteindre.</figcaption>
</figure>
