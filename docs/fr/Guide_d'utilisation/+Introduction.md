# Introduction

## Contexte

Le Petit Rapporteur est un logiciel libre développé par l'[association nationale des CEMÉA](https://cemea.asso.fr/).
Il s'agit d'une application Web permettant à une organisation de générer des rapports d'activité manière semi-automatique en facilitant le remplissage de données par les personnes.
Ce mini-site web présente l'outil Petit Rapporteur et fournit la documentation pour son installation et son usage.

## But de ce guide

Ce guide d'utilisation sert cet objectif :

- Expliquer comment utiliser le logiciel pour les personnes qui vont remplir les données des rapports

Dans la barre latérale à gauche sont disponibles les explications pour effectuer chacunes des actions prévues. Pour certaines actions il sera précisé quel type de droit vous devez avoir pour pouvoir les effectuer.
