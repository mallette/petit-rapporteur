# Requirements

## Hardware

Configuration used in production by the development team:

- 4 CPU cores
- 9GB RAM
- 20GB storage

!!! info "Note"
    The app can work with much less resources.
    The most resource-hungry operations are the copy and the automated generation of reports.
    If the server behaves normally after having tested a copy, then it should be okay to let it open for report completion.

## Software

- Docker (tested with v20.10.17)
- Docker Compose (tested with v2.6.0)
- Make (tested with GNU Make 4.3)
