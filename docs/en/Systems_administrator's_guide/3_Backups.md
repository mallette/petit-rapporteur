# Backups

The most important data to be backed up is the PostgreSQL database.

You can create a dump from the PostgreSQL container with this command:

```
docker exec -t petit-rapporteur-db-1 pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
```
