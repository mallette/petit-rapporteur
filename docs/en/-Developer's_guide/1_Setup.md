# Development environment setup

The software is running Python FastAPI on the backend and VueJS 3 on the frontend (TypeScript is installed but not used).
PostgreSQL is used for the database and Redis to store task queues.

Docker Compose is being used to deploy the software on all platforms, whether locally for development purpose or on production servers.

## Launch the app locally

1. First check the [software requirements](../Systems_administrator's_guide/Requirements.md)
2. Clone the git repository onto your computer
3. Once you have cloned the repository locally, you are only four steps away from running the app:

```
make dev_config
make build
make start
make alembic_migrate # Run database migration
```

The app will be available at `http://localhost:8080/`.

If you need to restart the app for any reason (for example a new VueJS component file is not automatically detected):

```
make restart
```

!!! info "Note"
    All available administration/development commands are visible by running `make` and are also described on the ["Available commands" page](Available_commands.md)
