---
title: "Available commands"
---

# Available commands

To help you with the development process, there are commands available in the `Makefile`.

They are all displayed when you run `make`:

```
% make
Available targets:
alembic_downgrade    Downgrade Alembic migration by one version
alembic_migrate      Run all Alembic migrations
alembic_reset        Revert all Alembic migrations
alembic_revision     Create a new Alembic revision
build                Build containers
changelog            Display changelog since last merge
ci_config            Configure app to be run on GitlabCI
clear                Stop containers and delete associated volumes
deploy               Run all commands to deploy app. You need to set APP_ENV and PUBLIC_DOMAIN variables before.
dev_config           Configure app to be run locally with dev tools
empty_task_runner_queue For development purposes, empty task queue
generate_docs_website Generate mini-website for documentation with Daux.io
lint_js              Run JS linter
lint_python          Run Python linter
lint_tjs             Run TypeScript linter
logs                 Show logs of running containers
postgres             Open Postgres console
prod_config          Configure app to be run on GitlabCI as for production
ps                   List running containers
restart              Stop and start containers
review_app_config    Configure app to be run on GitlabCI
start_maintenance_app Run maintenance app. You need to set PUBLIC_DOMAIN variable before.
start                Start containers
stop_maintenance_app Stop maintenance app
stop                 Stop containers
test_config          Configure app to be run locally with dev and test configurations.
test_js_unit         Run JS unit tests
test_python_dev      Run Python unit tests in dev environment
test_python_partly_dev Run Python unit tests partly according to given test name in dev environment
test_python_partly   Run Python unit tests partly according to given test name
test_python          Run Python unit tests
test                 Run all tests
```
