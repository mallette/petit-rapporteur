---
title: "Files organization"
---

# Files organization

## Main files

Main files of the project are organized the following way:

```
\_ backend/ # All Python files for the FastAPI backend
\_ db/ # Docker and configuration files for the PostgreSQL database
\_ docs/ # All files of this documentation website
\_ frontend/ # All Javascript files for the VueJS frontend
\_ maintenance_app/ # Nginx configuration and HTML page for the mini-maintenance website
\_ nginx/ # Docker and configuration files for the Nginx reverse-proxy
\_ docker-compose.base.yml # Base Docker Compose configuration for Petit Rapporteur
\_ docker-compose.dev.yml # Development override for the base Docker Compose configuration
\_ docker-compose.maintenance_app.yml # Independent Docker Compose configuration for the maintenance website
\_ docker-compose.prod.yml # Production override for the base Docker Compose configuration
\_ docker-compose.test.yml # Test override for the base Docker Compose configuration
\_ docker-compose.traefik.yml # Traefik override for the base Docker Compose configuration allowing for routing
```

## Backend

```
\_ backend/
  \_ app/
    \_ api/
      \_ dependencies/ # [FastAPI dependencies](https://fastapi.tiangolo.com/tutorial/dependencies/) especially used for authenticating API routes
      \_ routes/ # REST API routes: `/reports`,`/shares` and `/users`
    \_ core/ # Basic app configuration
    \_ db/
      \_ migrations/ # Database migrations run with Alembic
      \_ repositories/ # The most important: backend logic to manipulate data
    \_ lang/ # Backend's strings translations
    \_ models/ # Pydantic models to define each object used in the app
    \_ services/ # Generic functions like token creation for authentication
  \_ tests/ # All Python tests go here
  \_ openapi.json # Not used. Just a backup of the OpenAPI schema
```

## Frontend

```
\_ frontend/
  \_ petit-rapporteur/
    \_ cypress/ # Cypress files for End-to-end testing
    \_ public/ # Fonts and image assets served as-is by the reverse-proxy
    \_ src/ # The frontend app source code
      \_ assets/ # Image and CSS preprocessed assets
      \_ components/ # VueJS components files
        \_ __tests__/ # VueJS component test files for unit testing
      \_ locales/ # Frontend's strings translations
      \_ router/ # App router file
      \_ stores/ # VueJS Store definition
      \_ views/ # VueJS views (technically same as components, but here corresponds to an app page)
      \_ App.vue # Main app component
      \_ main.ts # Main Javascript file to load main app component
      \_ utils.ts # Generic Javascript functions shared among several components
```
