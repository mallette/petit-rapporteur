---
title: "Code style and practices"
---

# Code style and practices

[TOC]

## Automation philosophy

During the development process we try to automate every operation to avoid special commands
to be run by system administrators.

At the best, when doing a software upgrade, system administrators should only need to fetch
the latest Git tag and then run `APP_ENV=prod make deploy`. It allows to implement a simple
Continuous Deployment process like it currently exists for the project with GitlabCI.

For instance, database migrations should be automatically run.

Notice that today the server app runs automatic tasks at start, like deleting all PostgreSQL views.
Because PostgreSQL views don't allow to be replaced with less columns, it has been chosen to delete them
automatically and to be recreated on the fly. It implies that after an app deployment, each first request on a report takes a longer time,
but the drawback is not too bad.

It is encouraged to write idempotent functions which are only there to consolidate data after a database migration
if there are not too costy in performance. They can then be later removed in some future versions afterwards.
You can also add steps in the Makefile's `deploy` target if you need to do things outside the Python FastAPI app.

The philosphy to follow is to encourage automation and avoid as best as possible any human operation when upgrading and deploying the app.
Still, you may sometimes prefer to be pragmatic and chose to make one human operation instead of a very complex function which needs to be released through several version upgrades.

## Git practice

This repository follows the Conventional Commits as described on this [website](https://www.conventionalcommits.org/en/v1.0.0/).

You may use following types of commits, but are not limited to it:

- feat: (new feature for the user, not a new feature for build script)
- fix: (bug fix for the user, not a fix to a build script)
- docs: (changes to the documentation)
- style: (formatting, missing semi colons, etc; no production code change)
- refactor: (refactoring production code, eg. renaming a variable)
- test: (adding missing tests, refactoring tests; no production code change)
- chore: (updating dependencies, everything else which doesn't fit in above types; no production code change)

## Tests

There are currently three types of tests you must think of updating when needed:

- Python unit tests (`backend/tests/` folder)
- Javascript unit tests (`frontend/petit-rapporteur/src/components/__tests__/` folder)
- Cypress end-to-end tests (`frontend/petit-rapporteur/cypress/e2e/` folder)

You can run all tests with this command:

```
make test
```

To run only Python tests, while having setup the development environment:
```
make test_python_dev # It will temporarily setup testing connections to DB and remove them afterwards, so you can work again on your development app.
```

To run only some Python tests, while having setup the development environment:
```
make test_python_partly_dev TEST_NAME="string-matching-test-name" # You can match with part of test names, classes or filenames
```

## Linters

Linters allow to follow strict rules for code styling and to have a consistent codebase.
[Black](https://black.readthedocs.io/en/stable/) is being used for Python and [ESLint](https://eslint.org/) for Javascript.

```
make lint_js # Run linter on JavaScript code
make lint_ts # Run linter on JavaScript code with TypeScript type checker
make lint_python # Run linter on Python code
```

!!! info "Note"
    TypeScript has been installed in the project, but has been afterwards disabled.
    As for now, you don't have to take it into account.
