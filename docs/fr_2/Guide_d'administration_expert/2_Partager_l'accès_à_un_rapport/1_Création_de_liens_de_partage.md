# Création de liens de partage

!!! warning "Avertissement"
    Un lien de partage peut aujourd'hui seulement être créé via l'API.

## Via l'API

1. Aller sur la page Web de l'API `https://petit-rapporteur.mondomaine.example/api/docs/`
2. Cliquer sur le bouton en haut à droite `Authorize` et entrer les identifiants `root` dans les champs `username` et `password` puis cliquer sur le bouton `Authorize` en bas pour s'authentifier
3. Déplier la route `POST /v1/shares` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/shares/shares_create_share_v1_shares__post`
4. Cliquer sur le bouton `Try it out` pour éditer les paramètres de la requête
5. Dans le champ `Request body` copier le contenu suivant en remplaçant `[id-du-rapport]` par l'ID du rapport
    ```
    {
        "comment": "Nom affiché en haut à droite",
        "policies": [
          {
            "report_id": "[id-du-rapport-1]",
            "field_id": "*",
            "actions": [
              "report:list",
              "report:read",
              "report:write",
              "report:create",
              "report:share",
              "report:edit",
              "report:delete"
            ],
            "filters": null,
          },
          {
            "report_id": "[id-du-rapport-2]",
            "field_id": "*",
            "actions": [
              "report:read",
              "report:write"
            ],
            "filters": [
              {
                "field_name": "AT",
                "field_data": "01. PACA"
              }
            ],
          }
        ]
      },
    ```
6. Cliquer sur le bouton `Execute` pour lancer la création du rapport
7. Utiliser la chaîne de caractère appelée TOKEN qui a été obtenue afin de former un lien de partage de la forme suivante (penser à bien ajouter le `/s/` après le nom de domaine et avant le TOKEN) : `https://petit-rapporteur.mondomaine.example/s/TOKEN`

Exemple de JSON pour simplement autoriser la modification d'un rapport :

```
{
    "comment": "Nom qui sera affiché en haut à droite de l'interface",
    "policies": [
      {
        "report_id": "[id-du-rapport]",
        "field_id": "*",
        "actions": [
          "report:read",
          "report:write"
        ]
      }
    ]
  }
```

!!! info "Note"
    On peut supprimer ou rajouter des filtres en modifiant le JSON (supprimer les "," finales si inutiles).

!!! info "Note"
    Par défaut seuls les rapports qui ont la permission `report:read` sont affichées dans la liste des rapports sur la page d'accueil.
    Pour afficher la liste entière de tous les rapports, il faut ajouter la permission `report:list`.
    Cependant cette permission n'accorde pas le droit de lecture aux autres rapports lorsque l'on clique dessus dans la liste.
