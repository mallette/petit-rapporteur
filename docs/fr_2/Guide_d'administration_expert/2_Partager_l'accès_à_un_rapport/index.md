# Liens de partage

Pour l'instant, Le Petit Rapporteur n'a pas de système de gestion des comptes.
En revanche, pour donner un accès spécifique à certains rapports, vous pouvez utiliser des liens de partage.
Chaque lien de partage dispose d'un champ de commentaires que vous pouvez utiliser pour les organiser.
Vous pouvez avoir un lien de partage pour une personne ou un groupe de personnes comme vous le souhaitez.

Un lien de partage est composé d'une ou plusieurs politiques.
Chaque politique définit les permissions autorisées pour un seul rapport.
Il existe un identifiant spécial `report_id : "*"` pour définir une politique pour tous les rapports.

Une politique est composée des paramètres suivants :

- `report_id` : (nombre ou `*`. Par défaut : `*`) correspond à l'identifiant du rapport concerné par la politique.
- `field_id` : (nombre ou `*`. Par défaut : `*`) agit comme un filtre à l'intérieur de ce rapport. La politique définit les autorisations pour les sous-rapports sous `field_id`.
  **Remarque :** pour l'instant, cette fonctionnalité est limitée et ne permet en fait de définir que pour le `field_id` spécifique, sans ses enfants. Cette fonctionnalité pourrait être supprimée dans les prochaines versions si les filtres ci-dessous semblent suffisants.
- `actions` (obligatoire et ne peut être vide) : voir [les types de permissions](#types-de-permissions) pour les valeurs possibles
- `filters` : (`null` ou une liste de filtres) applique automatiquement des filtres sur les demandes de lecture de rapport afin que seuls certains champs soient accessibles. Modèle de filtre :
    ```
          {
            "field_name": "AT",
            "field_data": "01. PACA"
          }
    ```

## Types de permissions

| Nom technique | Icône | Explicatif |
|---|---|---|
| `report:list` | <i class="fa fa-list" title="Droit de lister tous les rapports"></i> | Permet de lister tous les rapports, même si le lien de partage n'a pas l'autorisation `report:read`. Cette action doit être utilisée avec `report_id : "*"` pour fonctionner.
| `report:read` | <i class="fa fa-eye" title="Droit de lire le rapport"></i> | Permet de lire le contenu du rapport.
| `report:write` | <i class="fa fa-font" title="Droit d'écrire dans le rapport"></i> | Permet d'ajouter du contenu et de modifier le contenu d'un rapport.
| `report:create` | <i class="fa fa-file-text-o" title="Droit de créer un rapport"></i> | Permet de créer un nouveau rapport. Cette action doit être utilisée avec `report_id : "*"` pour fonctionner.
| `report:share` | <i class="fa fa-share" title="Droit de partager un lien vers le rapport"></i> | Permet d'utiliser les fonctionnalités des liens de partage (créer, lister et modifier les liens de partage).
| `report:edit` | <i class="fa fa-pencil" title="Droit d'éditer le nom d'un rapport"></i> | Permet de modifier le nom d'un rapport.
| `report:delete` | <i class="fa fa-trash" title="Droit de supprimer le rapport"></i> | Permet de supprimer un rapport et ses champs.

## Exemple

Voici un exemple de configuration complète d'un lien de partage prêt à être créé :

```
{
    "comment": "Mon lien de partage",
    "policies": [
      {
        "report_id": "*",
        "field_id": "*",
        "actions": [
          "report:list",
          "report:read",
          "report:write",
          "report:create",
          "report:share",
          "report:edit",
          "report:delete"
        ],
        "filters": null,
      },
      {
        "report_id": "1",
        "field_id": "*",
        "actions": [
          "report:read",
          "report:write"
        ],
        "filters": [
          {
            "field_name": "AT",
            "field_data": "01. PACA"
          }
        ],
      }
    ]
  },
```

Quelques règles :
- Le commentaire d'un lien de partage doit être unique
- Une seule politique par rapport est autorisée dans un lien de partage

## Utilisation des liens de partage

1. Tout d'abord, connectez-vous en tant qu'utilisateur `root` sur `/api/docs` via le bouton `Authorize` en haut à droite. Demandez à l'administrateur systèmes si vous ne connaissez pas le mot de passe. Le mot de passe a été généré lors du premier déploiement et son hash a été défini comme `ROOT_PASSWORD_HASH`.
2. Sur la page `/api/docs` vous pouvez :
  - [créer un lien de partage](1_Création_de_liens_de_partage.md) et obtenir son jeton en envoyant la configuration du lien de partage à `POST /v1/shares`
  - lister les liens de partage existants à `GET /v1/shares`
  - [modifier un lien de partage](2_Modification_de_liens_de_partage.md) existant, en récupérant son ID et en utilisant la route `PUT /v1/shares/{share_id}`.
  - [obtenir le jeton d'un lien de partage](3_Open_a_sharing_link.md) existant, en récupérant son ID et en utilisant la route `GET /v1/shares/{share_id}/token`.
3. Une fois que vous avez obtenu le jeton d'accès au lien de partage, vous pouvez l'utiliser de la manière suivante : `https://url-du-petit-rapporteur/s/ACCESS-TOKEN`. Lorsque vous accédez à cette URL, l'application charge le lien de partage et vous donne l'accès correspondant.
