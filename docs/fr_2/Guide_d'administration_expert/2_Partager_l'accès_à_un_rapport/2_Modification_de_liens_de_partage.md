# Modification de liens de partage

!!! warning "Avertissement"
    Un lien de partage peut aujourd'hui seulement être modifié via l'API.

## Via l'API

1. Aller sur la page Web de l'API `https://petit-rapporteur.mondomaine.example/api/docs/`
2. Cliquer sur le bouton en haut à droite `Authorize` et entrer les identifiants `root` dans les champs `username` et `password` puis cliquer sur le bouton `Authorize` en bas pour s'authentifier
3. Déplier la route `GET /v1/shares` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/shares/shares_get_shares_v1_shares__get`
4. Cliquer sur le bouton `Try it out`
5. Cliquer sur le bouton `Execute` pour lister les partages existants
6. Identifier l'id du partage à modifier (à ne pas confondre avec l'id de la policy qui peut être différent) : il s'agit de l'id sous le `"comment"`.
7. Déplier la route `GET /v1/shares/{share_id}` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/shares/shares_update_share_by_id_v1_shares__share_id__get`
8. Cliquer sur le bouton `Try it out` pour éditer les paramètres de la requête
9. Indiquer dans le champ `share_id` des paramètres l'id précédemment identifié
10. Cliquer sur le bouton `Execute` pour afficher le détail du partage existant
11. Déplier la route `PUT /v1/shares/{share_id}` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/shares/shares_update_share_by_id_v1_shares__share_id__put`
12. Cliquer sur le bouton `Try it out` pour éditer les paramètres de la requête
13. Indiquer dans le champ `share_id` des paramètres l'id précédemment identifié
14. Dans le champ `Request body` copier le contenu du partage existant précédemment consulté et le modifier à sa convenance
15. Cliquer sur le bouton `Execute` pour mettre à jour le partage existant

Exemple de contenu à copier :
```
{
    "comment": "CPO EN 2023 final - Acces AT PACA",
    "policies": [
      {
        "report_id": "69774",
        "field_id": "*",
        "actions": [
          "report:read"
        ],
        "filters": [
          {
            "field_name": "AT",
            "field_data": "01. PACA"
          }
        ]
      }
    ]
  }
```
