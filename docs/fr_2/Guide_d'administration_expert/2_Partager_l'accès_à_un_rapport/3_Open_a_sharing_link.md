---
title: "Consulter les permissions d'un lien de partage"
---

# Consulter les permissions d'un lien de partage

!!! warning "Avertissement"
    Un lien de partage peut aujourd'hui seulement être généré via l'API.

!!! info "Note"
    Si vous possédez déjà le lien de partage que vous souhaitez consulter, vous pouvez avancer à l'étape 11 et éviter l'utilisation de l'API.

## Via l'API

1. Aller sur la page Web de l'API `https://petit-rapporteur.mondomaine.example/api/docs/`
2. Cliquer sur le bouton en haut à droite `Authorize` et entrer les identifiants `root` dans les champs `username` et `password` puis cliquer sur le bouton `Authorize` en bas pour s'authentifier
3. Déplier la route `GET /v1/shares` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/shares/shares_get_shares_v1_shares__get`
4. Cliquer sur le bouton `Try it out`
5. Cliquer sur le bouton `Execute` pour lister les partages existants
6. Identifier l'id du partage à modifier (à ne pas confondre avec l'id de la policy qui peut être différent) : il s'agit de l'id sous le `"comment"`.
7. Déplier la route `GET /v1/shares/{share_id}/token` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/shares/shares_get_share_token_by_id_v1_shares__share_id__token_get`
8. Cliquer sur le bouton `Try it out` pour éditer les paramètres de la requête
9. Indiquer dans le champ `share_id` des paramètres l'id précédemment identifié
10. Cliquer sur le bouton `Execute` pour obtenir le jeton d'accès du lien de partage
11. Accéder à l'interface Web du Petit Rapporteur via le lien de partage (sous la forme `https://petit-rapporteur.mondomaine.example/s/[jeton-d'accès]`)
12. En haut à droite, vérifier le nom associé au lien de partage
13. En haut à droite, à droite du cadenas, vérifier les permisions associées au lien de partage

<figure>
  <img src="Screenshot_Petit-Rapporteur_check_permissions.png" alt="" />
  <figcaption>La barre du haut affiche le nom du lien de partage et ses permissions associées. Ici le nom du lien de partage est « All actions » et les sept permissions lui sont associées.</figcaption>
</figure>

!!! info "Note"
    Voir les [types de permissions](index.md#types-de-permissions) pour connaître la signification des icônes.
