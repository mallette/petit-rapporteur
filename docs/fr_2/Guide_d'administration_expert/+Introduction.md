# Introduction

## Contexte

Le Petit Rapporteur est un logiciel libre développé par l'[association nationale des CEMÉA](https://cemea.asso.fr/).
Il s'agit d'une application Web permettant de collecter des données dans des tableaux afin de faciliter la génération de rapports d'activités.

Cet outil a été développé afin de répondre à un besoin où les organisations n'ont pas pleinement adopté d'outils de type ERP et qu'il n'est pas possible de générer automatiquement un rapport d'activités.
L'outil rend possible la complétion simultanée d'un tableau et un accès contrôlé à des parties du tableau.
Le but à terme de l'outil est de permettre d'accélérer la complétion d'un rapport d'activités en le connectant à des outils informatiques existants et d'ainsi permettre la complétion semi-automatique des données tout en permettant l'édition manuelle.

## But de ce guide

Ce guide d'utilisation sert cet objectif :

- Expliquer comment utiliser le logiciel pour les personnes avec les droits d'administration

Ci-dessous sont disponibles une présentation générale de l'usage de l'outil et les manières de l'utiliser.

Dans la barre latérale à gauche sont disponibles les explications pour effectuer chacunes des actions prévues. Pour certaines actions il sera précisé quel type de droit vous devez avoir pour pouvoir les effectuer.

## Description d'un usage habituel

1. Création d'un nouveau rapport dans le Petit Rapporteur par une personne ayant les droits d'administration
    1. Définition des catégories du rapport
    2. Définition des champs textes, nombres et booléen (oui/non) pour collecter les données
    3. Génération automatique de sous-catégories du rapport pour chaque entité qui devra remplir le rapport (par exemple « Association Nord », « Association Est », etc.)
2. Création de liens de partage par une personne ayant les droits de partage
    1. Définition des droits et des filtres attribués à chaque lien de partage (par exemple un filtre sur « Association Nord » pour le lien à destination de l'Association Nord)
    2. Partage des liens aux personnes concernées
3. Complétion du rapport
    1. Les personnes ayant reçu les liens de partage accèdent à l'interface du Petit Rapporteur avec les droits attribués
    2. Les personnes complètent les champs demandés
4. Exploitation des données
    1. Toute personne ayant un accès en lecture à un rapport peut télécharger son contenu au format CSV avec les filtres associés à ses accès

## Interfaces disponibles

Il est possible d'interagir de plusieurs manières avec Le Petit Rapporteur.

1. La manière la plus accessible et la plus populaire est d'accéder à son interface Web via un lien de partage fourni par une personne avec les droits d'administration (par exemple l'[interface Web pour les CEMÉA](https://petit-rapporteur.cemea.org/))
2. Il est aussi possible d'accéder à l'interface Web de son API (Application Programming Interface), via `https://petit-rapporteur.mondomaine.example/api/docs/` (par exemple l'[interface API pour les CEMÉA](https://petit-rapporteur.cemea.org/api/docs)) qui permet d'utiliser directement des routes techniques lorsque l'on connaît les paramètres à indiquer. Cette manière d'utiliser nécessite de connaître les identifiants `root` qui ont été définis au moment de l'installation du logiciel sur le serveur.
3. Enfin il est possible d'utiliser l'API directement avec un outil de requêtes HTTP comme `curl` avec les mêmes modalités que pour la méthode précédente. Il faut dans ce cas utiliser une URL de type `https://petit-rapporteur.mondomaine.example/api/v1/`. Cette manière d'utiliser l'API nécessite d'avoir un jeton d'accès qui peut se trouver dans un lien de partage.
