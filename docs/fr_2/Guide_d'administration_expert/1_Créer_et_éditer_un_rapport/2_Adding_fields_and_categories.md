---
title: "Ajout de catégories et de champs"
---

!!! info "Plus d\'information"
    Voir le [détail explicatif](index.md) pour comprendre le fonctionnement les types de champs et l'importance bien les nommer pour leur affichage.

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins le droit d'édition (<i class="fa fa-pencil" title="Droit d'éditer le nom d'un rapport"></i>/`report:edit`) associé
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le bouton à droite qui a pour signification « Éditer »
3. Déplier les conteneurs existants avec la flèche de liste déroulante à gauche de chaque ligne
4. Dans le conteneur où vous voulez ajouter un champ, cliquer sur le bouton `+` pour déplier la ligne de création de champ
5. Choisir dans la première liste déroulante le type de champ à ajouter
6. Compléter le nom et/ou la valeur du champ lorsque pertinent
7. Cliquer sur le bouton `Créer champ`. L'opération peut durer longtemps s'il y a beaucoup de champs enfants.

<figure>
  <img src="Screenshot_Petit-Rapporteur_add-field.png" alt="" />
  <figcaption>Pour un conteneur, faire attention à bien orthographier son nom comme les autres du même niveau d'arborescence afin qu'il soit reconnu comme étant du même type.</figcaption>
</figure>
