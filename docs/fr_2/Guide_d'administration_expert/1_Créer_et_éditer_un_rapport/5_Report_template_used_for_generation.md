---
title: "Utilisation d'un modèle de rapport pour générer des sous-catégories"
---

# Utilisation d'un modèle de rapport pour générer des sous-catégories

!!! info "Information"
    Seul un modèle de rapport peut être utilisé dans la génération de sous-catégories d'un autre rapport.
    Voir le [détail explicatif](index.md#utilisation-d-un-modele-de-rapport-pour-generer-des-sous-categories) pour comprendre le fonctionnement.

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins le droit d'édition (<i class="fa fa-pencil" title="Droit d'éditer le nom d'un rapport"></i>/`report:edit`) associé
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le bouton à droite qui a pour signification « Éditer »
3. Repérer le bloc « Générer automatiquement des catégories » en haut à droite
4. Sélectionner dans la première liste déroulante le modèle de rapport qui sera dupliqué
5. Sélectionner dans la seconde liste déroulante le nom des catégories qui vont recevoir une copie du modèle de rapport. Toutes les catégories avec ce nom recevra une copie, quel que soit leur position dans le rapport.
6. Cliquer sur `Générer`. L'opération peut être longue selon la taille du rapport, de l'ordre de 30 minutes.

<figure>
  <img src="Screenshot_Petit-Rapporteur_generation.png" alt="" />
  <figcaption>Le bloc permettant de générer automatiquement les sous-catégories</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit-Rapporteur_generation_filled.png" alt="" />
  <figcaption>Le bloc de génération de sous-catégories avec les champs modèle de rapport et de catégories cibles complétées</figcaption>
</figure>
