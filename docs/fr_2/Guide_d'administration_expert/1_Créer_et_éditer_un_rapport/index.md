# Architecture d'un rapport

[TOC]

Un rapport est une arborescence de champs dont les champs parents ont des effets sur les champs enfants. Les champs modèles définis en amont de l'arborescence, créeront des champs parmi les champs `Promo` enfants du `Conteneur` auquel ils sont rattachés.

## Exemple d'arborescence de rapport

```mermaid
graph TD
R([Rapport 'Rapport d'activités 2023']):::container --- A([Conteneur 'Axe 1']):::container
R --- B([Conteneur 'Axe 2']):::container
R --- MP{{Modèle 'Événement'}}:::promo_model
R --- MCT{{Modèle  'Description'}}:::qualitative_field_model
B --- MCA{{Modèle 'Nombre de participants'}}:::quantitative_addition_field_model
A --- N1([Conteneur 'Association Nord']):::container
N1 --- P1([Promo 'Événement']):::promo
P1 --- CT1[Champ 'Description']:::qualitative_field_model
B --- E1([Conteneur 'Association Est']):::container
E1 --- P2([Promo 'Événement']):::promo
P2 --- CT2[Champ 'Description']:::qualitative_field_model
P2 --- CA1[Champ 'Nombre de participants']:::quantitative_addition_field_model

classDef container fill:#f96
classDef promo fill:#ffffe0
classDef promo_model fill:#ffffe0
classDef qualitative_field_model fill:#8fbc8f
classDef quantitative_addition_field_model fill:#6495ed
```

## Légende

```mermaid
graph LR
T1([Conteneur]):::container
T2{{Modèle de promo}}:::promo_model
T3{{Modèle de champ de type qualitatif-texte}}:::qualitative_field_model
T4{{Modèle de champ de type quantitatif-addition}}:::quantitative_addition_field_model
T5(["Promo (créé par les personnes qui complètent un rapport)"]):::promo
T6["Champ (généré automatiquement)"]:::field

classDef container fill:#f96
classDef promo_model fill:#ffffe0
classDef promo fill:#ffffe0
classDef qualitative_field_model fill:#8fbc8f
classDef quantitative_addition_field_model fill:#6495ed
```

## Types de champs

Plusieurs éléments existent afin de concevoir l'architecture d'un rapport. Voici les types existants :

| Type de champ | Explicatif |
|---|---|
| `Conteneur` | Il s'agit d'un élément, aussi appelé « Catégorie », qui permet d'organiser un rapport. Il peut y avoir autant de conteneurs dans d'autres conteneurs que souhaité. |
| `Promo` | Correspond à une ligne de donnée dans un rapport. |
| `Modèle de promo` | Définit le nom par défaut de tous les champs `Promo` qui sont parmi ses enfants. |
| `Modèle pour champ de type qualitatif-texte` | Crée un champ de type qualitatif-texte dans tous les champs `Promo` enfants. Permet de recueillir une donnée de type textuel. |
| `Modèle pour champ de type qualitatif-booléen` | Crée un champ de type qualitatif-booléen dans tous les champs `Promo` enfants. Permet de recueillir une donnée de type booléen (oui/non). |
| `Modèle pour champ de type quantitatif-addition` | Crée un champ de type quantitatif-addition dans tous les champs `Promo` enfants. Permet de recueillir une donnée de type nombre.<br>Dans une future évolution, ce champ permettra d'afficher la somme des données des mêmes champs de type `quantitatif-addition` au niveau de la catégorie parente. |
| `Modèle pour champ de type quantitatif-moyenne` | Crée un champ de type quantitatif-moyenne dans tous les champs `Promo` enfants. Permet de recueillir une donnée de type nombre.<br>Dans une future évolution, ce champ permettra d'afficher la moyenne des données des mêmes champs de type `quantitatif-moyenne` au niveau de la catégorie parente. |

!!! info "Note"
    Les champs dont le nom commencent par « Modèle » sont nécessairement enfants d'un conteneur.
    Les champs de données comme `qualitatif-texte` ne peuvent pas être créés manuellement, et sont générés automatiquement comme enfant des champs `Promo`.

!!! info "Note"
    Les champs `quantitatif-addition` et `quantitatif-moyenne` ont pour l'instant aujourd'hui un comportement identique.
    Il est prévu dans une évolution futur de permettre des calculs automatisés qui permettra d'afficher la somme des données des mêmes champs de type `quantitatif-addition` au niveau de la catégorie parente
    et d'afficher la moyenne des données des mêmes champs de type `quantitatif-moyenne` au niveau de la catégorie parente.

## Noms des champs dans le tableau

Lorsqu'ils sont affichés dans un tableau sur l'interface Web, les champs sont classés par ordre alphabétique.
Pour modifier l'ordre des en-têtes de colonne, vous pouvez faire précéder le nom des champs d'un nombre dans ce format : `XX. `.
Le préfixe et le nom du champ doivent être séparés par une espace. Par exemple, l'en-tête `01. Description` sera affiché comme `Description` dans le tableau du rapport.

<figure>
  <img src="Screenshot_Petit-Rapporteur-table.png" alt="" />
  <figcaption>Les colonnes du tableau sont affichées par ordre alphabétique. Si « Description » et « Note interne » apparaissent ici avant les autres colonnes, c'est parce que leurs véritables noms sont « 00. Description » et « 01. Note interne ». « Événement » est la première colonne car il s'agit du nom de la <code>Promo</code>.</figcaption>
</figure>

## Utilisation d'un modèle de rapport pour générer des sous-catégories

Il est possible de définir un modèle de rapport qui sera ensuite utilisé pour être dupliqué comme sous-catégories d'un autre rapport.
Par exemple, si pour un rapport vous avez 3 grandes catégories définies et que pour chacune vous souhaitez définir autant de sous-catégories
qu'il y a d'association territoriales, vous pouvez définir un modèle de rapport listant toutes les associations territoriales,
puis générer automatiquement ce modèle de rapport comme sous-catégories de chacune des 3 catégories principales.

Pour illustrer, il est possible de générer en boucle le modèle de rapport suivant :

```mermaid
graph TD
R[Modèle de rapport 'Associations territoriales'] --- A[Association Nord]
R --- B[Association Est]
R --- C[Association Sud]
```

Dans le rapport suivant :

```mermaid
graph TD
R[Rapport 'Rapport d'activités 2023'] --- A[Axe 1]
R --- B[Axe 2]
R --- C[Axe 3]
```

Afin d'obtenir ce rapport :

```mermaid
graph TD
R[Rapport 'Rapport d'activités 2023'] --- A[Axe 1]
R --- B[Axe 2]
R --- C[Axe 3]
A --- N1[Association Nord]
A --- E1[Association Est]
A --- S1[Association Sud]
B --- N2[Association Nord]
B --- E2[Association Est]
B --- S2[Association Sud]
C --- N3[Association Nord]
C --- E3[Association Est]
C --- S3[Association Sud]
```

Voir [la page décrivant les instructions](Report_template_used_for_generation.md) pour copier un modèle de rapport dans un autre rapport.
