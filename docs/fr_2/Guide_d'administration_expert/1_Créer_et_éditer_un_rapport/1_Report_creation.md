---
title: "Création d'un rapport"
---

# Création d'un rapport

## Via l'interface web (recommandé)

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins le droit de création de rapport (<i class="fa fa-file-text-o" title="Droit de créer un rapport"></i>/`report:create`) associé
2. Sur la page d'accueil, en bas de l'écran, écrire le nom du rapport
3. Cliquer sur le bouton « Créer rapport »

Votre nouveau rapport va apparaître dans La liste des rapports.


<figure>
  <img src="Screenshot_Petit-Rapporteur_create_report.png" alt="" />
  <figcaption>Le champ de création de rapport se trouve en bas de la liste des rapports</figcaption>
</figure>

## Via l'API

1. Aller sur la page Web de l'API `https://petit-rapporteur.mondomaine.example/api/docs/`
2. Cliquer sur le bouton en haut à droite `Authorize` et entrer les identifiants `root` dans les champs `username` et `password` puis cliquer sur le bouton `Authorize` en bas pour s'authentifier
3. Déplier la route `POST /v1/reports` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/reports/reports_create_report_v1_reports__post`
4. Cliquer sur le bouton `Try it out` pour éditer les paramètres de la requête
5. Dans le champ `Request body` copier le contenu suivant en remplaçant `[nom-du-rapport]` par le nom que vous souhaitez
    ```
    {
      "new_report": {
        "name": "Report",
        "field_type": "container",
        "data": "[nom-du-rapport]"
      }
    }
    ```
6. Cliquer sur le bouton `Execute` pour lancer la création du rapport
