---
title: "Définition de champs par défaut"
---

Un champ par défaut est un modèle de champ qui est directement enfant du champ `Rapport`
et dont les effets s'appliquent à tous les champs enfants.

Son comportement est donc le même que pour les autres champs modèles si ce n'est qu'il est
appliqué à l'ensemble d'un rapport.

!!! info "Plus d\'information"
    Voir le [détail explicatif](index.md) pour comprendre le fonctionnement des champs modèle et l'importance bien les nommer pour leur affichage.

!!! info "Note"
    Il est possible d'appliquer un champ par défaut dans tous le rapport, puis d'ajouter des modèles de champ
    dans certaines sous-catégories. Il n'est en revanche pas possible d'appliquer un champ par défaut partout sauf
    dans certaines sous-catégories. Il faut donc bien réfléchir à quels champs doivent être par défaut et lesquels
    seront à ajouter manuellement.

## Via l'interface Web

1. Accéder à l'interface Web du Petit Rapporteur via un lien de partage avec au moins le droit d'édition (<i class="fa fa-pencil" title="Droit d'éditer le nom d'un rapport"></i>/`report:edit`) associé
2. Sur la page d'accueil, sur la ligne du rapport à modifier, cliquer sur le bouton à droite qui a pour signification « Éditer »
3. Repérer le bloc « Champs par défaut » en bas à droite
3. Déplier les conteneurs existants avec la flèche de liste déroulante à gauche de chaque ligne
4. Dans le conteneur où vous voulez ajouter un champ, cliquer sur le bouton `+` pour déplier la ligne de création de champ
5. Choisir dans la première liste déroulante le type de champ à ajouter
6. Compléter le nom et/ou la valeur du champ lorsque pertinent
7. Cliquer sur le bouton `Créer champ`. L'opération peut durer longtemps s'il y a beaucoup de champs enfants.

<figure>
  <img src="Screenshot_Petit-Rapporteur_default_fields.png" alt="" />
  <figcaption>Le bloc « Champs par défaut » permet de voir plus facilement les champs concernés par rapport à la liste de champs à gauche.</figcaption>
</figure>

!!! warning "Précaution"
    [Un bogue connu](https://gitlab.cemea.org/mallette/petit-rapporteur/-/issues/21) empêche la visualisation de champs par défaut lorsqu'ils sont imbriqués dans des sous-catégories qui elles ne contiennent pas de champ par défaut.
    Le bloc « Champs par défaut » déplie seulement les conteneurs qui ont des champs par défaut comme enfants directs.
    Il est possible de contourner cette limitation en naviguant dans les champs de la colonne de gauche.

!!! info "Note"
    Toutes ces opérations peuvent être effectuées aussi via la méthode classique décrite dans [« Ajout de catégories et de champs »](2_Adding_fields_and_categories.md).
    La vue est simplement simplifiée et plus lisible dans le bloc « Champs par défaut ».
