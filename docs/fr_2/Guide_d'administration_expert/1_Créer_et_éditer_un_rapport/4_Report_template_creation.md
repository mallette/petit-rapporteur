---
title: "Création d'un modèle de rapport"
---

# Création d'un modèle de rapport

Un modèle de rapport est un rapport qu'il est possible de réutiliser comme base d'édition pour un autre rapport.
Il est notamment utile pour la génération automatique de sous-catégories telle que détaillée dans la page suivante.
Il n'y a aucune autre différence technique avec un autre rapport.

!!! warning "Avertissement"
    Un modèle de rapport peut aujourd'hui seulement être créé via l'API.

## Via l'API

1. Aller sur la page Web de l'API `https://petit-rapporteur.mondomaine.example/api/docs/`
2. Cliquer sur le bouton en haut à droite `Authorize` et entrer les identifiants `root` dans les champs `username` et `password` puis cliquer sur le bouton `Authorize` en bas pour s'authentifier
3. Déplier la route `POST /v1/reports` (exemple `https://petit-rapporteur.mondomaine.example/api/docs#/reports/reports_create_report_v1_reports__post`
4. Cliquer sur le bouton `Try it out` pour éditer les paramètres de la requête
5. Dans le champ `Request body` copier le contenu suivant en remplaçant `[nom-du-rapport]` par le nom que vous souhaitez
    ```
    {
      "new_report": {
        "name": "ReportTemplate",
        "field_type": "container",
        "data": "[nom-du-rapport]"
      }
    }
    ```
6. Cliquer sur le bouton `Execute` pour lancer la création du rapport

<figure>
  <img src="Screenshot_Petit-Rapporteur_create_report_template.png" alt="" />
  <figcaption>Seul le champ `Request body` a besoin d'être modifié pour lancer la création du modèle de rapport</figcaption>
</figure>
