# Concevoir un nouveau rapport CPO

Voici le récapitulatif des étapes pour concevoir un nouveau rapport CPO pour être ensuite diffusé

1. Créer un nouveau rapport sur [l'interface Web](https://petit-rapporteur.cemea.org) (il vous faut pour cela un lien de partage avec les droits de création correspondant (<i class="fa fa-file-text-o" title="Droit de créer un rapport"></i>/`report:create`)).
    - Créer un modele de promo à la racine appelé `événement`
    - On peut créer/renommer des champs complémentaires qui seront appliqués à tous
    - Puis récupérer l'identifiant du rapport sur [/api/docs#/reports/reports_get_reports_v1_reports__get](https://petit-rapporteur.cemea.org/api/docs#/reports/reports_get_reports_v1_reports__get)
    - Récupérer aussi l'identifiant du rapport « **Modèle de CPO complet**  » par la même occasion
2. Copie du rapport « Modèle de CPO » dans le nouveau rapport
    - Utiliser la route [/api/docs#/reports/reports_copy_fields_to_report_v1_reports__report_id__fields__field_id__post](https://petit-rapporteur.cemea.org/api/docs#/reports/reports_copy_fields_to_report_v1_reports__report_id__fields__field_id__post)
    - Remplir report_id et field_id avec le même identifiant du nouveau rapport créé
    - Dans le JSON `field_copy` inscrire dans `source_id` l'identifiant du rapport « **Modèle de CPO complet**  » (qui contient le champ Action : *Z. Autres*)
    - Lancer la requête, qui peut durer quelques secondes
3. Génération des champs « AT > Académies » dans les catégories « Action » du rapport
    - Sur l'interface Web, ouvrir le nouveau rapport en mode édition
    - Dans le bloc de génération, choisir de générer les champs « AT > Académies » dans les catégories « Action » du rapport
    - Lancer l'action, qui peut durer presque 2 minutes avec les performances actuelles. Il n'y aura pas de retour utilisateur alors il est conseillé de ne pas interagir avec l'application pendant ce temps. Elle fera un timeout côté navigateur au bout d'une minute mais l'application continuera le traitement. Si une requête fait crasher l'application, le rapport sera dans un état incohérent.
4. Créer les champs-modèle manquants dans le nouveau rapport
    - Sur l'interface Web, avec le rapport en mode édition, vous pouvez créer des nouveaux champs modèles à la racine et chaque promo enfant héritera de ces champs
