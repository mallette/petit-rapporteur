# Concevoir un exemple simplifié de rapport d'activité

Voici le récapitulatif des étapes pour concevoir un simple rapport.

1. Créer un nouveau rapport sur [la page d'accueil de l'interface Web](https://petit-rapporteur.cemea.org) (il vous faut pour cela un lien de partage avec les droits de création correspondant (<i class="fa fa-file-text-o" title="Droit de créer un rapport"></i>/`report:create`)).
2. Cliquer sur le bouton d'édition à droite sur la ligne correspondante au rapport nouvellement créé
3. Une fois sur la page d'édition du rapport, à partir du bloc de création de champs en bas à gauche, créer un champ « Modèle de promo » avec pour nom « activité »
4. Créer deux champs de type conteneur pour structurer le rapport avec pour catégorie « Mois » et pour nom « Janvier » et « Février » respectivement
5. Créer un champ-modèle qualitatif-texte avec pour nom « Description »
6. Créer un champ-modèle quantitatif-addition avec pour nom « Nombre de bénéficiaires »
7. Pour tester le rapport, retourner sur la page d'accueil en cliquant sur le lien « Rapports » en haut à gauche, puis cliquer sur le nom du nouveau rapport dans la liste et afficher la page de complétion du rapport
