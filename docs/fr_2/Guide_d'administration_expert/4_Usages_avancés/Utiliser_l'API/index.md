# Utilisation de l'API

L'API intégrée au Petit Rapporteur est complétée avec l'usage de [Swagger](https://swagger.io/) qui propose une interface « simplifiée » sur [/api/docs/](https://petit-rapporteur.preprod.cemea.org/api/docs/)

## Récupérer les ID de chaque rapport

GET [/v1/reports/](https://petit-rapporteur.preprod.cemea.org/api/docs#/reports/reports_get_reports_v1_reports__get)

Reports:Get-Reports → `Try it out` + `Execute`

pour obtenir les ID de chaque rapport dans

Response body (utile si on veut en supprimer/copier)

## Effacer un rapport

DELETE [/v1/reports/{id}/](https://petit-rapporteur.preprod.cemea.org/api/docs#/reports/reports_delete_report_by_id_v1_reports__id___delete)

Reports:Delete-Report-By-Id → `Try it out`

Ajouter l'ID du rapport à supprimer et `Execute`

## Copier un rapport vers un autre

Il est possible de copier un rapport entier dans un rapport vierge (ex : pour dupliquer une CPO) ou dans une branche/catégorie d'un autre rapport.

POST [/v1/reports/{id}/fields/{field_id}](https://petit-rapporteur.preprod.cemea.org/api/docs#/reports/reports_copy_fields_to_report_v1_reports__report_id__fields__field_id__post)

Reports:Copy-Fields-To-Report → `Try-it out`

Remplir :

- `id` = ID du rapport destinataire
- `field_id` = ID du rapport destinataire aussi

### Dupliquer le contenu d'un rapport dans un autre rapport

Dans le request body, ajouter :

```
{
  "field_copy": {
    "source_id": [ID du modèle à copier],
    "recursive": true,
    "loop": false,
    "field_name": "string",
    "run_side_effects": false
  }
}
```

### Dupliquer le contenu d'un rapport dans chaque branche/catégorie du même nom d'un autre rapport
Dans le request body, ajouter :

```
{
  "field_copy": {
    "source_id": [ID du modèle à copier],
    "field_name": "[chaine de caractères de la branche où copier]",
    "recursive": true,
    "loop": true,
    "run_side_effects": true
  }
}
```
