<img src="logo.png" alt="" />

<p class="HeroText">
	<strong>Le Petit Rapporteur</strong> est une application Web permettant à une organisation de générer des rapports d'activité manière semi-automatique en facilitant le remplissage de données par les personnes.
</p>

---

Ce mini-site web présente l'outil Petit Rapporteur et fournit la documentation pour son installation et son usage.

Les documentations [à l'intention des usagers](fr/Guide_d'utilisation/Introduction.md) et des [personnes en charge de l'administration](fr_2/Guide_d'administration_expert/Introduction.md) sont rédigées en français et la [documentation technique](en/Systems_administrator's_guide/Requirements.md) pour l'installation et le développement est rédigée en anglais.

<figure>
  <img src="Screenshot_Petit Rapporteur.png" alt="" />
  <figcaption>Page d'accueil listant les rapports disponibles</figcaption>
</figure>

<figure>
  <img src="Screenshot_Petit Rapporteur_completion.png" alt="" />
  <figcaption>Page de complétion d'un rapport</figcaption>
</figure>

---

### Fonctionnalités

<div class="Row">
<div class="Row__third">

#### Pour compléter un rapport

- Enregistrement des données à la volée
- Filtrer un rapport en fonction des noms de catégories
- Filtrer un rapport à partir d'une recherche de mots-clés
- Liens de partage avec filtres intégrés

</div>
<div class="Row__third">

#### Pour éditer un rapport

- Modèles de rapports pouvant être réutilisés
- Génération automatiques de sous-catégories dans un rapport en utilisant un modèle de rapport qui sera dupliqué
- Types de champs possibles : texte, numérique, booléen (oui/non)

</div>
<div class="Row__third">

#### Pour exploiter les données d'un rapport

- Téléchargement du contenu au format CSV
- Export d'un rapport au format JSON pour être réimporté dans une autre instance du Petit Rapporteur
- Application des filtres et de la recherche au moment de l'export


</div>
</div>

---

### Cas d'usage auquel l'outil répond

Une association nationale souhaite générer un rapport d'activités pour un réseau d'association.
Les activités de ces associations sont pas ou peu enregistrées dans des systèmes informatiques et il est nécessaire que les personnes de chaque association déclarent manuellement toutes les activités réalisées dans l'année.
L'association nationale doit présenter un rapport d'activité répondant à une Convention Pluriannuelle d'Objectifs (CPO) signée avec l'État dont chaque axe d'activité a été précisément définie.

Afin de faciliter la complétion de ce rapport d'activité dans un temps restreint, de manière simultanée par toutes les associations territoriales et tout en limitant les risques d'écrasement de données entre les associations,
le Petit Rapporteur permet de générer un lien de partage spécifique pour chaque association afin que chacune d'elles puisse compléter sa part du tableau préalablement défini par l'association nationale.
Le risque de pertes de données est limitée car l'enregistrement des données se fait à la volée, les vues du rapport sont filtrées pour chaque association afin qu'elle ne puissent modifier que leurs parties et le reste du rapport ne peut être vu qu'en lecture seule.

L'association nationale peut ensuite extraire du Petit Rapporteur un fichier CSV contenant toutes les données enregistrées pour le mettre en forme.
