# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.12.3](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.12.2...v3.12.3) (2025-02-11)

### Summary

Make JSON fields import more resilient

### Bug Fixes

- 5fc9e1d fix(api): Add failed fields import status
- 13ce9ff fix(api): Retry failed fields to import

## [3.12.2](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.12.1...v3.12.2) (2025-02-04)

### Summary

Improve some errors display

### Bug Fixes

- 4b44a2c fix(front): Normalize modal layouts
- e29fae0 fix(front): Add generic error modal

## [3.12.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.12.0...v3.12.1) (2025-01-28)

### Summary

Fixes some interface layout issues and the filters.

### Bug Fixes

- 81aad71 fix(front): Pull down fields consistently in edition mode
- e597bd4 fix(front): Correct layout after Bulma CSS update
- 96b9f7e fix(api): Refresh also column views
- 79cbe5b fix(api): Check complete update params before update query

## [3.12.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.11.2...v3.12.0) (XXXX-XX-XX)

### Summary

Adds CSV file import feature in the API and on the interface.
Display the import results through the button on the job end notification.

Adds a new `report:import` permission to allow to import CSV files.

Fixes important regression included in v3.11.3: Websockets and, as a result, notifications were broken
due to the proxy change with the new deployment system.

### Features

- 8fc0196 feat(api): Add route to import CSV
- 93acac4 feat(front): Add import CSV button

### Bug Fixes

- 09e85a1 fix(api): Check complete update params before update query

## [3.11.2](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.11.1...v3.11.2) (2024-02-06)

### Features

- 9dffbc8 feat(front): Rewrite line deletion message

## [3.11.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.11.0...v3.11.1) (2024-02-05)

### Bug Fixes

- bac35da fix(front): Display missing delete button when container_promo:delete

## [3.11.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.10.0...v3.11.0) (2024-01-31)

### Bug Fixes

- ae1d112 fix(front): Keep already displayed field children

## [3.10.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.9.0...v3.10.0) (2024-01-24)

### Summary

Add line suppression for basic user and remove the need to have at least one container_promo in containers.

Improve performance, usability and reliability by running some tasks in background.

### Upgrade notes

You need to add and fill these environment variable sin `backend/.env` to allow the frontend to connect to the backend with websockets:

```
VITE_APP_SERVER_HOST='localhost:8080'
VITE_APP_SERVER_WS_PROTOCOL='ws' # Should be 'ws' or 'wss' for security
```

### Features

- 5ead1d2 feat(front): Add search and filters reset button
- bcbc95f feat(database): Add indexes on foreign keys to optimize deletion
- 5ff0bde feat(front): Reduce width of utils cells when displaying lines
- 84822b9 feat(front): Add CopyFieldButton in ReportCompletionView
- e910a93 feat(api): Add a field_copy parameter to copy whole field container
- 6535f43 feat(front): Get notification when deleting template field
- f6a6b72 feat(api): Add more readable field deletion message
- f13965b feat(api): Delete fields in background
- 73171cd feat(api): Run report deletion in background
- 3c3fa42 feat(front): Update field move locally on edition page
- dc17786 feat(front): Add move field button
- b728143 feat(api): Run batch field import in task queue
- 2d43214 feat(front): Mutualize fields loading on edition page
- a82412c feat(front): Edit fields faster by applying locally
- aabc07e feat(front): Avoid reloading after generating fields
- 1adb883 feat(api): Add REST /jobs/{job_id}
- 5d63e9a feat(front): Connect app to jobs websocket
- 34042a1 feat(api): Add WS /ws/jobs route
- 446c3f6 feat(api): Add better description to field copy job
- 25d3125 feat(frontend): Make notifications persistent between pages
- 53141bb feat(front): Display notification when job is run
- 90e5a34 feat(api): Run copy_field task with task runner
- c9454bd feat(api): Filter columns by leaf container rows instead of promos
- c5093c2 feat(api): Get empty container even when filters are enabled
- 736eb4f feat(api): Add container_promo:delete permission to delete container_promo fields
- d87375f feat(front): Only display the delete button when there is the permission
- 3ffbb04 feat(front): Add delete button for each line on report completion page

### Bug Fixes

- 9bd206b fix(front): Display all columns in lines even if they not exist
- fac00fb fix(front): Hide line creation button when not write permission
- 33b0f65 fix(front): Remove broken moveFieldLocally call
- fe18fca fix(api): Add maintenance task to recreate policies' views
- c50dd75 fix(api): Copy new template_ids when copying fields
- 9c1c900 fix(front): Apply filters also when loading report root
- 386cfae fix(api): Increase task timeout
- b9b3c45 fix(front): Fix and allow local field move to report root
- baedf57 fix(front): Recreate fetchFieldsList function which was missing
- f73b978 fix(api): Restrict one info field per parent
- 253901d fix(front): Avoid creating empty info fields
- 8b25796 fix(api): Ignore race condition when creating a field tree view
- 5fd79f0 fix(api): Avoid duplicates in policy view creation
- fc7b89f fix(front): Display button to add line even when container is empty

## [3.9.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.8.0...v3.9.0) (2023-04-25)

### Features

- 463dee2 feat(api): Take into account order by param when getting nested fields
- f7b7b84 feat(api): Order fields by generation_number and name

### Bug Fixes

- 7f9559e fix(api): Rewrite policy view creation
- 4fe9e98 fix(front): Catch missing error case when editing field
- 8155430 fix(front): Manage fetch api error upfront
- be6888f fix(front): Better handle request errors
- 26f1a1a fix(api): Add more precise distinct on ID

## [3.8.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.7.0...v3.8.0) (2023-02-16)

### Summary

Setup analytics integration for Matomo.

### Features

- 1550546 feat(front): Enable heartBeatTimer for Matomo
- e2981bf feat(front): Enable Matomo's page tracking

## [3.7.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.6.0...v3.7.0) (2023-02-15)

### Features

- 99e6e98 feat(front): Add CSV download button

## [3.6.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.5.2...v3.6.0) (2023-02-15)

### Features

- 51c5c45 feat(api): Return also policy group at share creation

## [3.5.2](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.5.1...v3.5.2) (2023-02-14)

### Summary

No new feature or fix. Only app deployment tooling.

## [3.5.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.5.0...v3.5.1) (2023-02-14)

### Bug Fixes

- 13749f3 fix(api): Only consolidate containers wich are empty
- 28051cf fix(api): Get only parent's direct fields for container's effect
- 846519d fix(api): Drop instead of refreshing materialized views
- 4756bd2 fix(api): Add consolidation step after fields copy

## [3.5.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.4.0...v3.5.0) (2023-02-09)

### Summary

Add user interface finition and add JSON and CSV exports.

### Features

- b23bd80 feat(front): Display checkboxes at the end of the line
- 73091ee feat(front): Hide checkboxes behind a button
- 0471eff feat(front): Remove ifo field creation from creation form
- 7bde844 feat(front): Allow to edit and create info field
- 2ee1924 feat(front): Display info field on edition page
- 46ce3cc feat(front): Display info button on completion page
- 823581b feat(api): Allow to import flat fields in JSON
- 4e8bf2d feat(front): Increase height of tooltip hovering for consistency
- fcb7d90 feat(front): Display headers in multiple lines
- 2df68e7 feat(FieldsTableCell): Make a more dynamic calculation of rows in the textarea elements
- 6212e66 feat(ReportFormItem): Implement dynamic styling regardless of the number of levels in a report
- 0fb7b33 feat(api): Add export folder cleaning task
- 54b4834 feat(api): Add CSV export option
- 47c044d feat(front): Style template block similarly to the other fields list
- d550bf9 feat(front): Display report title to be dragged on in templates block
- 0327cf0 feat(api): Allow to move template field's parent
- 98378e0 feat(front): Display create button at right place for templates
- 0779241 feat(front): Lazy load template edition panel
- e35afb7 feat(front): Clean display of nested template
- fddf52a feat(front): Propagate update events to direct parent and refresh only partly
- 154e97b feat(front): Display dedicated icons for each field type
- bab40d9 feat(front): Add edit button on fields
- 612d0af feat(front): Add delete buttons to summaries
- 851324f feat(front): Enable delete buttons on sub-fields
- 19405ab feat(front): Add loading spinner on field creation button
- 208d702 feat(front): Add refresh on edition page
- 4c91f83 feat(front): Display field creation button on edition page
- c6eb677 feat(ReportEditionView): Add default report fields block with its styles
- cc00ff6 feat(ReportEditionView): Remove template fields from default report fields
- 60c23b5 feat(ReportEditionView): Add new components to ReportEditionView
- d113cee feat(ReportFormItem): Add edition-mode to templates and add CreateFieldButton to it
- 2a47857 feat(ReportForm): Add an edition-mode for the report edition view
- b028eec feat(CSS): Change .card-container styling
- cb27556 feat(CSS): Change layout of main parts of the report edition page

### Bug Fixes

- a5cd089 fix(api): Add view refresh after fields batch copy
- 2f4907d fix(api): Avoid duplicate category name for column filters
- 943d267 fix(front): Use generic button name for new line
- c44959c fix(api): Add view refresh after import
- dc4f512 fix(api): Refresh view after promo creation
- 751f9f7 fix(front): Use lower z-index for labels
- ba36e1a fix(front): Avoid unnecessary updates when cell out of focus
- 853917a fix(front): Reuse translated strings for template fields list
- 5315866 fix(front): Translate default fields string
- e398af1 fix(api): Break refresh loop while updating template
- 918e4a1 fix(front): Remove duplicate createFieldButton from edition page
- 81e7d74 fix(front): Improve deletion button alignment
- 21b8bf6 fix(front): Use lazy loaded route for displaying default templates
- cf57951 fix(front): Remove duplicate import
- 1bdfe4d fix(CreateFieldButton): Fix CSS of CreateFieldButton.vue in edition mode
- a1ff1de fix(CSS): Fix width of select element in the edition mode of CreateFieldButton.vue
- 8d94414 fix(api): Refresh materialized view after deletion
- bcc9d1f fix(api): Refresh materialized view after field creation
- d3dfbe0 fix(api): Missing correct exception name
- c698895 fix(api): Use correct exception name
- c96ec06 fix(api): Implement materialized views refresh at update
- b84cc8d fix(api): Add missing HTTP code import
- 1e15d76 fix(front): Add missing locale for filters string
- 2e57e12 fix(front): Add loading spinner for filters form
- 9024dd2 fix(api): Ignore unique constraints when creating materialized views
- dd755b4 fix(api): Use more precise hash param for preprocessed view name
- cbb2715 fix(api): Use category_sql for crosstab

## [3.4.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.3.0...v3.4.0) (2023-01-17)

### Summary

Entire redesign of the homepage and the completion page.

### Features
- 656ad11 feat(api): Implement export param for fields
- b110f33 feat(api): Implement import fields route
- 47a301e Merge branch 'hide-field-name-order' into 'main'
- 2f4fa0f feat(front): Hide number prefix on column's headers
- b478a56 feat(front): Load fields when opening details element
- 039ec9b feat(front): Hide filters button when no columns has been received
- 270fbf3 feat(front): Set search string for filter's columns
- a385d42 feat(api): Take into account search string for columns
- b4d0c75 feat(front): Set filters in requests
- 1c2da3f feat(front): Add filters form
- ab43f43 feat(api): Sort column values alphabetically
- ac2e7fe feat(api): Implement get-columns route
- acc7d13 feat(api): Create ColumnPublic model
- 90fdab2 feat(promo): Use template as default promo data
- 98183c9 feat(front): Add loading spinner on report completion page
- e61e85a feat(front): Add search bar component
- 1941ef8 feat(api): Implement search through report
- b5a793c Merge branch 'style-permissions-icons' into 'main'
- b6866a7 feat(api): Add view cleaning task at server startup
- 355c4d8 feat(front): Use new lazy loading route
- e623616 feat(api): Implement lazy_loading_from
- a43bfc7 feat(front): Add titles to permissions icons
- 8c49b05 feat(CSS): Change layout to organise icons and logout button
- 9cd6337 feat(CSS): Revert position of permissions icons for more consistent layout
- 8708a01 feat(CSS): Make permissions icons style not look like buttons
- ecae36a feat(api): Setup policy's filters by default on getting nested fields
- 0a9b185 feat(api): Allow to add filters on same column
- ff1d806 feat(api): Implement filters on lazy loading
- efa0712 feat(api): Implement multiple filters on nested fields
- c230190 feat(front): Change sentence for forbidden report's list access
- 0fee613 feat(front): Translate strings in FR and EN
- fd26ec8 feat(front): Add LangSwitcher
- 58b6f85 feat(api): Translate strings in English and French
- 7b3ffc7 feat(api): Setup i18n and translation system
- 564dbbf feat(CSS): Apply styling to the buttons in report editing page
- 41257f1 feat(CSS): Improve layout on Home view
- 557d281 feat(CSS): Style the "create report" block
- bb3b678 feat(front): Add loader to cells after user input
- 4ecaff8 feat(front): Added button for new event and improved styling for tables in reports
- baab91d feat(CSS): Improved cells styling in FieldsTableCell vue file
- e7998ce feat(front): Enable promo creation button
- 864c23b feat(front): Display table rows vertically
- a2e44fe feat(CSS): style table elements in report completion views
- 756719f feat(front): Enhance performance when editing FieldCell
- 493d5e2 feat(front): Implement lazy-loading nested fields
- 5c5de33 feat(api): Add lazy_loading option to get nested fields
- f998b54 feat(CSS): Improved styles of headings
- ac2e607 feat(CSS): Apply style on the Home page
- a452b9a feat(CSS): Reinstall Bulma CSS in the project
- 56ca0b6 feat(ReportFormn): Setup a secondary view to work on integration.
- d4aecbe feat(CSS): Restyle page header
- f11af8f feat(ReportForm): Display promo fields table
- 297ce37 feat(front): WIP Create ReportForm component
- 2765006 feat(ReportFormn): Setup a secondary view to work on integration.
- d5c4aae feat(front): Display report creation form only if permission
- 5a4afd5 feat(front): Allow to add promo template
- 4be8138 feat(api): Implement template_container_promo field type
- d160f8b feat(front): Display checkboxes
- 6b843ed feat(front): Allow to add qualitative boolean template field
- 2598843 feat(api): Implement basic qualitative_bool field type
- 9006850 Merge branch 'quantitative-field' into 'main'
- a53eb46 feat(front): Display error when writing data in a bad format
- 9b2cfee feat(front): Allow to add quantitative template fields
- f31d4f5 feat(api): Implement basically quantitative field types
- 8f44f16 feat(front): Refresh token before each route
- ecf80a1 feat(api): Add /shares/me route to return access token

### Bug Fixes
- a963daa fix(api): Manage optional accept-language header
- c2aeedb Merge branch 'fix-loading-sub-fields' into 'main'
- 8885a31 fix(front): Manage better async requests to control order
- e91df1e fix(api): Don't store empty values for column values
- de99e19 fix(api): Add explicit distinct on column's name
- 89ec50d fix(api): Concat search string properly
- a3bb889 fix(front): Reset filters correctly
- 788fa47 fix(front): Manage default search string
- 3159508 fix(api): Set maintenance task to fix promo's empty names
- 4dabd34 fix(api): Set default name to container_promo
- 836db20 fix(api): Use correct filter attribute to overload filters with policy's ones
- 9c102a2 fix(api): Allow lazy loading to work on not filtered response
- f518d93 fix(api): Simplify view columns declaration
- f151abd fix(api): Use promo rows processing only when filters can work
- ef47770 fix(api): Set unprocessed nested reports route as default
- 593f595 fix(front): Display promos correctly when there are template field
- e3f5a6b fix(front): Hide template fields in completion view
- e5b2ae2 fix(front): Rename 'Mes CPOs' to 'Rapports' in header
- 25adae6 fix(css): Add margin above report creation form
- af31008 fix(css): Remove underline and padding from sharing icons
- 82ccb2e fix(front): Add disabled attribute on unauthorized cells & styling
- 3c5441c fix(front): Make cell error more reactive
- ec247d8 fix(front): Improve feedback when error on cell
- 6beb4c3 fix(api): Allow float number in quantitative field type
- 28b7046 fix(api): Fix concurrency when getting nested fields
- 3d649cf fix(front): Display table only once when several promo lines
- 078b569 fix(api): Allow to fetch non-report nested fields
- dcba0ee Merge branch 'token-expiration' into 'main'
- 9dbfc40 fix(api): Remove expiration date from report tokens
- 4aff47c Merge branch 'front-load-token' into 'main'
- c23edcc fix(front): Avoid checking current token when loading new one
- 1ff19ff fix(api): Consolidate also other field types than qualitative_text
- 74072cb Merge branch 'fix-share-link' into 'main'
- 30c6686 fix(api): Escape quotes in filter's names

## [3.3.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.2.1...v3.3.0) (2022-12-01)

### Summary

Add new sharing links features with policies for each report. See [documentation](docs/USAGE.md#sharing-links)

### Upgrade notes

1. You must generate the root password, its salt, and the secret key used for the JSON Web token as described in the [documentation](docs/DEPLOYMENT.md#initialize-the-secrets).
2. You must run the database migration with `make alembic_migrate`

### Features

- 969ee0c feat(front): Display edition button only if has permission
- ccf350c feat(api): Forbid specifying several times same report in policies group
- c073a02 feat(front): Disable filter buttons when filters in token
- b6d28a0 feat(front): Display disconnect button
- 23bcf24 feat(api): Forbid duplicated reports
- 861d78b feat(front): Display report creation form only if permission
- 236eca8 feat(front): Display current report permissions
- d1606da feat(front): Reset active report when on home view
- 69e4fee feat(front): Persist state across refresh
- 219eee1 feat(front): Display active share link name in navbar
- 970c693 feat(api): Limit displayed list of reports according to policies group
- 70f1117 feat(api): Manage complementary filters for table route
- f673f66 feat(authorization): Allow to specify multiple filters in one policy
- 2a059eb feat(api): Add PUT /shares/{share-id}
- a799592 feat(api): Add GET /shares/{share-id}/token
- 4a533b6 feat(api): Add GET /shares/{share-id}
- d237fce feat(api): Add GET /shares/
- 859277c feat(front): Manage forbidden access
- 5e2a501 feat(authorization): Override with filters from policy
- 949c7bc feat(front): Import share link
- 84ed6e4 feat(api): Implement authorization with policies_groups
- c34a6a5 feat(api): Implement /report/share/token route
- 08da209 feat(api): Create and delete dedicated views for report
- 598bddd feat(api): Configure minimal 'me' scope
- c3a8235 feat(api): Add /me protected route
- 69da68a feat(api): Add login route for root user
- cd9f89e feat(api): Add include_children param for flatten fields
- b468d6f feat(api): Add filter on data for fields in flatten return

### Bug Fixes

- c701fd2 fix(front): Manage error when editing field in FieldsList
- 3abbac9 fix(front): Change table filter request to adapt to API parameters
- 64885a7 fix(authorization): Include fields children when filtered for authorization check
- 0c67aa3 fix(authorization): Return policies from policies group
- d3a3724 fix(authorization): Restrict access to enforce rules without tolerance
- ffa70b1 fix(authorization): Manage edge case when report_id is used as field_id
- 1b66780 fix(filters): Filter on report before requesting
- 4c6a27c fix(authorization): Return 404 when field doesn't exist
- c955201 fix(api): Return first policy match when no field_id
- b2ba958 fix(api): Return filetered report when authorized
- 1b08964 fix(python): Fix code after database dependencies upgrade

## [3.2.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.2.0...v3.2.1) (2022-10-25)

### Bug Fixes
- bca3657 fix(front): Forbid creating report with empty name
- 283efbc fix(api): Forbid empty data strings for reports

## [3.2.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.1.0...v3.2.0) (2022-10-25)

### Features

- 7b14ce8 feat(front): Add loader on report completion page
- ee56429 feat(front): Add loader on report edition page

### Bug Fixes

- 2bd6f65 fix(front/table): Enable again sticky header.

## [3.1.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.0.1...v3.1.0) (2022-10-24)

### Summary

Fix bugs related with the new promo field constraint in reports.
Add an icon for ReportTemplate to distinguish them from others as they don't need, and shouldn't have promo fields.

### Features

- 22063c1 feat(api): Render ReportTemplate as table even without promo
- 115468e feat(front/report-edition): Display template icon for ReportTemplate
- d810593 feat(front/reports-list): Display template icon for ReportTemplate
- 51bddbe feat(api): Avoid creating promo fields in ReportTemplate

### Bug Fixes

- 7a44b2a fix(api): Add side effects when copying fields
- 5855a8e fix(api): Run consolidation on current report only
- fdbffd7 fix(api): Create promo children only if container has no promo children
- 5245dc0 fix(api): Isolate container side effects and fix duplicates

## [3.0.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v3.0.0...v3.0.1) (2022-10-20)

### Bug Fixes

- c66c8405 fix(front): Allow filter buttons to work again

## [3.0.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v2.1.0...v3.0.0) (2022-10-19)

### Summary

Improve support of promo fields by making it easier to create them and displays them better.
Warning: this release automatically runs an upgrade on all existing reports when a new field is being created.

### Features

- 3597fc7 feat!(api): Migrate automatically leaf containers on POST
- de8841f feat(api): Hide promo fields from table route
- 4715970 feat(api): Inherit of parent templates at promo field creation
- 6c631c0 feat(api): Move existing promos when creating new container
- f0397a8 feat(api): Automatically generate promo when container added

### Bug Fixes

- b19a73b fix(front/table): Display cells in the right column
- 4263912 fix(api): Use correct query to get container_promo at template creation
- 328aac3 fix(api/table): Return rows even if no promo children

## [2.1.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v2.0.0...v2.1.0) (2022-10-11)

### Features

- 52bc224 feat(front): Add getReportsTable to utils
- 8a7d3a3 feat(api): Add GET report/id/table route

## [2.0.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v1.0.1...v2.0.0) (2022-10-13)

### Features

- 9fa57e5 feat(front): Allow to create promo fields
- a6c216b feat(api): Generate linked fields on promo instead of containers
- 0083395 feat(api): Implement container_promo field_type

## [1.0.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v1.0.0...v1.0.1) (2022-10-13)

### Summary

Fix a bug on the Report edition page where fields weren't able to be dragged and dropped around.

### Bug Fixes

- fa1a4ff fix(front): Allow to change parent fields in report edition page

## [1.0.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.9.0...v1.0.0) (2022-10-05)

### Summary

Adds the API route to delete a whole report.
By the same time, it means also that now fields can be deleted on the interface ALSO when they still have children.
It may be an unattended change of behaviour if the user assumes there is a kind of security preventing to delete everything.

### Features

- a1bb89e feat(api)!: Add DELETE on /reports/id


## [0.9.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.8.0...v0.9.0) (2022-09-29)

### Summary
- Various stuff on the spreadsheet view, a fix on the field generator module and a new favicon!

### Features

- ec16e27 feat(spreadsheet): Set sticky table header when scrolling
- 3c1441b feat(JS): Add getReportsContainerFields function in utils
- c15b004 feat(img): Replace VueJS favicon by Petit Rapporteur

### Bug Fixes

- 9d279f0 fix(JS/generator): Display only containers in target dropdown

## [0.8.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.7.0...v0.8.0) (2022-09-29)

### Summary

- The application appearance has been completely restyled
- The spreadsheet view should be more comfortable to use

### Features

- 66f5e30 feat(spreadsheet): Forbid cell's textarea resize
- 0189917 feat(spreadsheet): Allow line return in cell on ctrl+enter
- 334096f feat(spreadsheet): Save cell on blur
- e689eff feat(spreadsheet): Allow to edit table cell on focus
- 0ddd70d feat(spreadsheet): Set categories as readonly in table view
- 6eff506 feat(front): Enhance loading button for generator
- 075fff4 feat(front): Restyle CreateFieldButton
- 406f469 feat(front): Change style of create button in fields list
- 37c1e32 feat(front): Style FieldsGenerator component
- 87f5525 feat(front): Improve title of fields list
- a9fca4b feat(front): Manage escape key on cell to cancel
- e0804dd feat(Front): Add title to homepage
- 37e0162 feat(CSS): Display modified state for cell at rest
- a298f42 feat(CSS): Restyle spreadsheet view
- e2a0690 feat(CSS): Style report list view
- b73ecd4 feat(CSS): Style CreateReportButton
- 759278b feat(CSS): Add container style to main view
- af18ec9 feat(CSS): Style navbar
- a076145 feat(CSS): Add class to app container
- ac42410 feat(CSS): Enable Bulma CSS framework

### Bug Fixes
- 6a37e22 fix(CSS): Force th bottom border

## [0.7.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.6.0...v0.7.0) (2022-09-27)

### Features

- baa774b feat(Front): Add Fields generator to report edition view
- 4c60de5 feat(JS): Add copyReportToFields to utils
- 03e8221 feat(JS): Add getReportsFilterByName to utils
- 2cc6b82 feat(api): Add deep copy on loop
- ce6ec4b feat(api): Add filter on name in flatten response
- 0606ce0 feat(api): Allow filter in flatten response
- e38a2ae feat(API): Allow to copy to report as target
- 0ef19d7 feat(API): Add fields copy route
- c3ce9a2 feat(API): Allow field_type filter to GET fields
- 02cf121 feat(API): Allow to get report templates
- e97a511 feat(API): Add ReportTemplate name for report

### Bug Fixes
- f708892 fix(API): Filter type also in children

## [0.6.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.5.1...v0.6.0) (2022-09-26)

### Features

- 06af7a7 feat(Spreadsheet): Manage the case there is an empty container
- e5c5161 feat(Spreadsheet): Add modified state to cell
- 9000379 feat(FieldsList): Add list toggle button
- 71d5fd9 feat(Spreadsheet): Sort rows according to first column
- 029c773 feat(Front): Ensure to only display container and editable fields in spreadsheet
- 73d7b6e feat(Front): Use API field headers instead of front-side calculated ones
- 804c09d feat(API): Add route to get report's headers
- e96ee2d feat(API): Add HeaderFieldPublic model
- b386e5e feat(Front): Allow to edit on spreadsheet view
- 1bf6a42 feat(Front): Display fields as table
- d1a21b2 feat(API): Add leaf container fields route
- d8a4cfc feat(Front): Add empty report completion view

### Bug Fixes

- dda5606 fix(API): Add explicit order by to query for tests
- e6055e4 fix(spreadsheet): Disallow editing disabled cell
- ede5105 fix(Spreadsheet): Set table width

## [0.5.1](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.5.0...v0.5.1) (2022-09-14)

### Features

- b3c5a1b feat(API): Add filter option on containers for leaf fields

### Bug Fixes

- d07382e fix(API): Ensure a field is always created as a child of a container
- 0eff8cd fix(API): Generate fields from template only in containers

## [0.5.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.4.0...v0.5.0) (2022-09-14)

### Features

- c19a116 feat(Front): Hide data edition field for templates
- ce69576 feat(Front): Display fields icons
- 206786a feat(Front): Add template_qualitative_text option at field creation
- a687b59 feat(API): Update to a template updates a linked field too
- 99ac708 feat(API): Delete fields linked to a deleted template
- 1685842 feat(Front): Add order_by option to fields list query
- 83625e2 feat(API): Add order_by name as option
- 81f534c feat(API): Generate text fields on leaf fields
- b7f1f4b feat(API): Add get leaf function in utils
- fef54d5 feat(API): Manage template_id property for fields
- 40da7c4 feat(API): Add template_qualitative_text type
- b399ce4 feat(API): Add qualitative_text field_type

### Bug Fixes

- 0286f97 fix(Front): Minimize field update params

## [0.4.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.3.0...v0.4.0) (2022-09-06)

### Features

- 216aa1c feat(JS): Allow to drop dragged item on report name
- 0313aee feat(JS): Change report's edition page title
- 2449bcd feat(JS): Display error when trying to move parent under child
- b679f1b feat(Python): Prevent invalid field movement
- ea78a1c feat(Python): Add flatten children result for a parent
- a20206b feat(JS): Add highlight on drop zones
- aeff344 feat(JS): Allow drag-drop only on containers
- 50c2d99 feat(JS): Display an error modal when dragging a field on itself
- e47cd7e feat(JS): Allow fields to be draggable and to change their parents
- 67c0edc feat(JS): Add delete button
- 4fb327e feat: Sort fields alphabetically
- 307c8ec feat(JS): Add edit field button and modal
- 0fbbd10 feat(CSS): Use FA icon for item creation
- 0951969 feat(CSS): Add styling to container's names
- 041e23a feat(JS): Display field creation button in list
- 6a648ef feat: Render children fields in a tree
- 350c1c1 feat: Display report's fields
- 8dbfe69 feat: Add report's fields page display

## [0.3.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.2.0...v0.3.0) (2022-08-31)

### Features

- 134ecf8 feat(JS): Add button to create report
- 0ebc75d feat(JS): Display list of reports

## [0.2.0](https://gitlab.cemea.org/cemeainterne/petit-rapporteur/compare/v0.1.2...v0.2.0) (2022-08-29)

### Features

- 31e1124 feat: Add field's recursive children to data response
- 3349e88 feat(fields): Add direct children to fields
- 4bc4ab9 feat(fields): Add DELETE /reports/{id}/fields/{field_id}
- e8919c6 feat(field): Add GET /report/{id}/fields/{field_id}
- 7237503 feat(field): Add PUT /reports/{id}/fields{field_id}
- 3bdb59f feat(report): Add PUT /reports/{id}
- 9a13fc1 feat: Add GET /reports/{id}/fields
- 9d7e9de feat: Add POST /report/{id}/fields
- bf9c5f7 feat: Add GET /report/id

### Bug Fixes

- 8e5defa fix(routes): Add id validation in path
- 94bc147 fix(report): Only return reports on GET /reports/{id}
- 39e3eff fix(test): Remove useless data_id in GET /reports
