all: help

DOCKER_IMAGE_TAG ?= 'latest'

UNAME_S := $(shell uname -s)
# GNU/Linux configuration
ifeq ($(UNAME_S),Linux)
	NB_CORES=$(shell cat /proc/cpuinfo | grep processor | wc -l )
	DOCKER_COMPOSE = DOCKER_IMAGE_TAG=$(DOCKER_IMAGE_TAG) DOCKER_UID=$(shell id -u) DOCKER_GID=$(shell id -g) docker compose
endif
# Mac OS configuration
ifeq ($(UNAME_S),Darwin)
	NB_CORES=$(shell sysctl -n hw.physicalcpu)
	DOCKER_COMPOSE = DOCKER_IMAGE_TAG=$(DOCKER_IMAGE_TAG) DOCKER_UID=$(shell id -u) DOCKER_GID=3000 docker compose
endif
MSG=''
TEST_NAME='' # Used to run specific Python tests
PYTHON_NB_WORKERS=$(shell echo $$(( $(NB_CORES) * 2 + 1 )))

.PHONY: help
help:
	@echo 'Available targets:'
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
	@printf '\nAvailable variables:\n'
	@grep -E '^[a-zA-Z_-]+\?=.* ## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = "?=.* ## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

backend/.env:
	cp backend/.env.template backend/.env
	sed -i 's/PYTHON_NB_WORKERS=[0-9]/PYTHON_NB_WORKERS=$(PYTHON_NB_WORKERS)/' backend/.env

.PHONY: build
build: backend/.env ## Build containers
	$(DOCKER_COMPOSE) build

.PHONY: start
start: ## Start containers
	$(DOCKER_COMPOSE) up -d

.PHONY: restart
restart: stop start ## Stop and start containers

.PHONY: ps
ps: ## List running containers
	$(DOCKER_COMPOSE) ps

.PHONY: logs
logs: ## Show logs of running containers
	$(DOCKER_COMPOSE) logs -f

.PHONY: stop
stop: ## Stop containers
	$(DOCKER_COMPOSE) down

.PHONY: clear
clear: ## Stop containers and delete associated volumes
	$(DOCKER_COMPOSE) down -v

.PHONY: postgres
postgres: ## Open Postgres console
	$(DOCKER_COMPOSE) exec db psql -h localhost -U postgres --dbname=postgres

.PHONY: alembic_revision
alembic_revision: ## Create a new Alembic revision
	$(DOCKER_COMPOSE) run server alembic revision -m "$(MSG)"

.PHONY: alembic_migrate
alembic_migrate: ## Run all Alembic migrations
	$(DOCKER_COMPOSE) run server alembic upgrade head

.PHONY: alembic_downgrade
alembic_downgrade: ## Downgrade Alembic migration by one version
	$(DOCKER_COMPOSE) run server alembic downgrade -1

.PHONY: alembic_reset
alembic_reset: ## Revert all Alembic migrations
	$(DOCKER_COMPOSE) run server alembic downgrade base

.PHONY: set_testing_environment_for_dev
set_testing_environment_for_dev: # Setup testing variable for worker
	docker compose ls | grep 'docker.compose.dev.yml' \
		| grep 'docker.compose.test.yml' \
		|| echo "TESTING=1" \
		> backend/.testing.env \
		&& $(DOCKER_COMPOSE) down worker \
		&& $(DOCKER_COMPOSE) up -d worker # Run only if docker compose test config is not loaded

.PHONY: unset_testing_environment_for_dev
unset_testing_environment_for_dev: # Remove testing variable for worker
	docker compose ls | grep 'docker.compose.dev.yml' \
		| grep 'docker.compose.test.yml' \
		|| echo "" \
		> backend/.testing.env \
		&& $(DOCKER_COMPOSE) down worker \
		&& $(DOCKER_COMPOSE) up -d worker # Run only if docker compose test config is not loaded

.PHONY: test_python
test_python: ## Run Python unit tests
	$(DOCKER_COMPOSE) run server pytest -v

.PHONY: test_python_dev
test_python_dev: | set_testing_environment_for_dev test_python unset_testing_environment_for_dev ## Run Python unit tests in dev environment

.PHONY: test_python_partly
test_python_partly: ## Run Python unit tests partly according to given test name
	$(DOCKER_COMPOSE) run server pytest -vv -k "$(TEST_NAME)"

.PHONY: test_python_partly_dev
test_python_partly_dev: | set_testing_environment_for_dev test_python_partly unset_testing_environment_for_dev ## Run Python unit tests partly according to given test name in dev environment

.PHONY: test_js_unit
test_js_unit: ## Run JS unit tests
	$(DOCKER_COMPOSE) run frontend npm run test:unit:ci

.PHONY: test_js_e2e
test_js_e2e: ## Run JS e2e tests
	$(DOCKER_COMPOSE) run frontend npm run build && $(DOCKER_COMPOSE) run frontend npm run test:e2e:ci

.PHONY: test
test: start test_python test_js_unit test_js_e2e stop ## Run all tests. Stop services at the end for the CI runner to avoid run collisions.

.PHONY: dev_config
dev_config: backend/.env ## Configure app to be run locally with dev tools
	echo "COMPOSE_FILE='./docker-compose.base.yml:./docker-compose.build.yml:./docker-compose.dev.yml'" >> .env
	touch backend/.testing.env # Just create file to allow worker to run in development mode

.PHONY: test_config
test_config: backend/.env ## Configure app to be run locally with dev and test configurations.
	echo "COMPOSE_FILE='./docker-compose.base.yml:./docker-compose.build.yml:./docker-compose.dev.yml:./docker-compose.test.yml'" >> .env

.PHONY: ci_config
ci_config: backend/.env ## Configure app to be run on GitlabCI
	echo "COMPOSE_FILE='./docker-compose.base.yml:./docker-compose.build.yml:./docker-compose.test.yml'" >> .env
	echo "COMPOSE_PROJECT_NAME='${CI_COMMIT_REF_SLUG}'" >> .env

.PHONY: review_app_config
review_app_config: backend/.env ## Configure app to be run on GitlabCI
	echo "COMPOSE_FILE='./docker-compose.base.yml:./docker-compose.build.yml:./docker-compose.prod.yml:./docker-compose.lb.yml'" >> .env
	echo "COMPOSE_PROJECT_NAME='${CI_COMMIT_REF_SLUG}'" >> .env

.PHONY: build_prod_config
build_prod_config: backend/.env ## Configure app to be built on GitlabCI for production
	echo "COMPOSE_FILE='./docker-compose.base.yml:./docker-compose.build.yml:./docker-compose.prod.yml:./docker-compose.lb.yml'" >> .env

.PHONY: prod_config
prod_config: backend/.env ## Configure app to be run on GitlabCI as for production with already built images
	echo "COMPOSE_FILE='./docker-compose.base.yml:./docker-compose.prod.yml:./docker-compose.lb.yml'" >> .env

.PHONY: deploy
deploy: ${APP_ENV}_config ## Run all commands to deploy app. You need to set APP_ENV and PUBLIC_DOMAIN variables before.
	bash deploy/blue-green_deploy.sh redis=1 db=1 nginx=2 frontend=2 worker=2 server=2

.PHONY: changelog
changelog: ## Display changelog since last merge
	@echo "Features"
	@git log --oneline --no-merges --grep "^feat" $$(git describe --tags --abbrev=0)..HEAD
	@echo "Bug Fixes"
	@git log --oneline --no-merges --grep "^fix" $$(git describe --tags --abbrev=0)..HEAD

.PHONY: lint_python
lint_python: ## Run Python linter
	$(DOCKER_COMPOSE) run server black .

.PHONY: lint_js
lint_js: ## Run JS linter
	$(DOCKER_COMPOSE) run frontend npm run lint

.PHONY: lint_tjs
lint_tjs: ## Run TypeScript linter
	$(DOCKER_COMPOSE) run frontend npm run type-check

.PHONY: empty_task_runner_queue
empty_task_runner_queue: ## For development purposes, empty task queue
	$(DOCKER_COMPOSE) run worker rq empty -u redis://redis default

.PHONY: generate_docs_website
generate_docs_website: ## Generate mini-website for documentation with Daux.io
	docker run --rm -p 8085:8085 -w /build -u "$$(id -u):$$(id -g)" -v "$$PWD":/build -v /etc/timezone:/etc/timezone:ro -v /etc/localtime:/etc/localtime:ro daux/daux.io daux generate --themes=docs/themes --destination=public

.PHONY: pull_app
pull_app:## Pull app's Docker images. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} $(DOCKER_COMPOSE) pull

.PHONY: run_app_stack
run_app_stack:## Run app from scratch. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) $(DOCKER_COMPOSE) up -d

.PHONY: run_app_besides
run_app_besides: ## Run new container besides running one. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) $(DOCKER_COMPOSE) up -d --no-deps $(SCALE_COMMAND) --no-recreate

.PHONY: run_app_standalone
run_app_standalone: ## Run new container as standalone. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) $(DOCKER_COMPOSE) up $(CONTAINER_NAME) -d
