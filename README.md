<h1 align="center">
  <img src="frontend/petit-rapporteur/src/assets/logo.png " alt="Petit Rapporteur">
</h1>

Little tool to facilitate gathering activity data for an organization by allowing manual and semi-automated input to complete reports.

## Screenshot

<p align="center">
  <img src="docs/Screenshot_Petit Rapporteur.png" alt="">
</p>

## Documentation

The online documentation is available on [this website](https://docdupetit-rapporteur.cemea.org/).

The local documentation, which is the same content, but as original markdown files:

- [Systems administrator's guide (English)](docs/en/Systems_administrator's_guide/1_Requirements.md)
- [Developer's guide (English)](docs/en/-Developer's_guide/1_Setup.md)
- [User's guide (French)](docs/fr/Guide_d'utilisation/+Introduction.md)
- [Administrator's guide (French)](docs/fr_2/Guide_d'administration_expert/+Introduction.md)

## License

[AGPL 3.0](LICENSE)

The licenses of all the software dependencies are listed in the ["Software architecture" documentation page](docs/en/-Developer's_guide/Software_architecture.md).

## Credit

Designed and developed by [CEMÉA's National Association](https://cemea.asso.fr/)
