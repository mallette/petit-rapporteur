FROM node:18.8.0-buster-slim

ARG DOCKER_UID
ARG DOCKER_GID

# Set existing node user with same IDs as user on host
RUN groupmod -g ${DOCKER_GID} node && usermod -u ${DOCKER_UID} -g ${DOCKER_GID} node

USER node:node

ADD --chown=node:node . /app

WORKDIR /app/petit-rapporteur

RUN npm install

# Create folder before volume mount
RUN mkdir -p /app/petit-rapporteur/dist

COPY user-entrypoint.sh /usr/local/bin/

USER root:root

ENTRYPOINT ["user-entrypoint.sh"]
