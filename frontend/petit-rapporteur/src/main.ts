import mitt from "mitt";
import { createApp } from "vue";
import { createPinia } from "pinia";
import { createPersistedState } from "pinia-plugin-persistedstate";
import VueMatomo from 'vue-matomo';

import App from "./App.vue";
import router from "./router";
import { setupI18n } from "./utils";

import "./assets/main.css";


const pinia = createPinia();
pinia.use(createPersistedState());

const app = createApp(App);

const i18n = setupI18n();
app.use(i18n)

export const emitter = mitt();
app.config.globalProperties.emitter = emitter;

app.use(router);

// Enable Matomo if configured
if (import.meta.env.VITE_APP_MATOMO_DOMAIN && import.meta.env.VITE_APP_MATOMO_SITE_ID) {
  app.use(VueMatomo, {
    host: `https://${import.meta.env.VITE_APP_MATOMO_DOMAIN}`,
    siteId: import.meta.env.VITE_APP_MATOMO_SITE_ID,
    disableCookies: true,
    router: router,
    enableHeartBeatTimer: true,
  })
}

app.use(pinia);
app.mount("#app");
