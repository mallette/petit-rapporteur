import { defineStore } from "pinia";

export const useReportStore = defineStore("report", {
  state: () => ({
    id: 0,
    type: "",
    searchString: "",
    filters: [],
  }),
  persist: true,
});
