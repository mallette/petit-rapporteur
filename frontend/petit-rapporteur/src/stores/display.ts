import { defineStore } from "pinia";

export const useDisplayStore = defineStore("display", {
  state: () => ({
    notifications: [],
    clientId: '',
  }),
  persist: true,
});
