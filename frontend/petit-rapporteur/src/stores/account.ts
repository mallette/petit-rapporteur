import { defineStore } from "pinia";

export const useAccountStore = defineStore("account", {
  state: () => ({
    token: "",
    lang: ""
  }),
  persist: true,
});
