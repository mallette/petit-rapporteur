import { describe, it, expect, beforeEach } from "vitest";
import { setActivePinia, createPinia } from "pinia";

import { mount } from "@vue/test-utils";
import ReportsListItem from "../ReportsListItem.vue";

describe("ReportsListItem", () => {
  beforeEach(() => {
    // creates a fresh pinia and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia());
  });

  it("renders properly", () => {
    const wrapper = mount(ReportsListItem, { props: { name: "Hello Vitest" } });
    expect(wrapper.text()).toContain("Hello Vitest");
  });
});
