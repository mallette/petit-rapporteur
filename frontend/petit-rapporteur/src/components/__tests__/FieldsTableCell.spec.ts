import { describe, it, expect } from "vitest";
import "whatwg-fetch";

import { mount } from "@vue/test-utils";
import FieldsTableCell from "../FieldsTableCell.vue";

describe("FieldsTableCell", () => {
  it("renders properly", () => {
    const cell = {
      id: 1,
      name: "My name",
      data: "My data",
    };
    const wrapper = mount(FieldsTableCell, { props: { initialCell: cell } });
    const textarea = wrapper.find("textarea");
    expect(textarea.exists()).toBe(true);
    expect(textarea.attributes().readonly).not.toBeDefined();
    expect(textarea.attributes().disabled).not.toBeDefined();
    expect(textarea.classes()).not.toContain("editing");
    expect(wrapper.html()).toContain("My data");
  });

  it("transforms to editable textarea on click", async () => {
    const cell = {
      id: 1,
      name: "My name",
      data: "My data",
    };
    const wrapper = mount(FieldsTableCell, { props: { initialCell: cell } });
    const div = wrapper.find("div");
    // Trigger div to create textarea
    await div.trigger("click");
    const textarea = wrapper.find("textarea");
    expect(textarea.exists()).toBe(true);
    expect(textarea.attributes().readonly).not.toBeDefined();
    expect(textarea.attributes().disabled).not.toBeDefined();
    expect(textarea.classes()).toContain("editing");
    expect(wrapper.html()).toContain("My data");
  });

  it("doesn't update textarea after modification of props", async () => {
    const cell = {
      id: 1,
      name: "My name",
      data: "My data",
    };
    const wrapper = mount(FieldsTableCell, { props: { initialCell: cell } });
    // Trigger div to create textarea
    const div = wrapper.find("div");
    await div.trigger("click");
    // Change property from parent
    cell.data = "changed props data";
    const textarea = wrapper.find("textarea");
    expect(textarea.classes()).not.toContain("modified");
    expect(wrapper.html()).toContain("My data");
    expect(wrapper.html()).not.toContain("changed props data");
  });

  it("displays as modified after modification of data", async () => {
    const cell = {
      id: 1,
      name: "My name",
      data: "My data",
    };
    const wrapper = mount(FieldsTableCell, { props: { initialCell: cell } });
    const div = wrapper.find("div");
    // Trigger div to create textarea
    await div.trigger("click");
    // Change data of component
    const textarea = wrapper.find("textarea");
    await textarea.setValue("changing content");
    expect(textarea.classes()).toContain("modified");
  });

  it("displays as readonly when is a container", async () => {
    const cell = {
      id: 1,
      name: "My name",
      data: "My data",
      field_type: "container",
    };
    const wrapper = mount(FieldsTableCell, { props: { initialCell: cell } });
    const textarea = wrapper.find("textarea");
    expect(textarea.attributes().readonly).toBeDefined();
  });

  it("displays as disabled when is empty", async () => {
    const cell = {};
    const wrapper = mount(FieldsTableCell, { props: { initialCell: cell } });
    const textarea = wrapper.find("textarea");
    expect(textarea.attributes().disabled).toBeDefined();
  });
});
