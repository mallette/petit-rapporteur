import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ReportEditionView from "../views/ReportEditionView.vue";
import ReportCompletionView from "../views/ReportCompletionView.vue";
import ShareLinkImporter from "../components/ShareLinkImporter.vue";
import { getShareMe, setToken } from "@/utils.js";
import { useAccountStore } from "@/stores/account";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/edit/:reportName",
      name: "reportEdition",
      component: ReportEditionView,
    },
    {
      path: "/view/:reportName",
      name: "reportCompletion",
      component: ReportCompletionView,
    },
    {
      path: "/s/:shareToken",
      name: "shareLink",
      component: ShareLinkImporter,
    },
  ],
});

router.beforeEach(async (to, from) => {
  const accountStore = useAccountStore();
  if (accountStore.token != '' && to.name !== 'shareLink') {
    let token = await getShareMe();
    accountStore.token = token.access_token;
    setToken(token.access_token);
  }
  return true;
})

export default router;
