import { createI18n } from 'vue-i18n';
import { mande } from "mande";
import messages from '@intlify/unplugin-vue-i18n/messages';
export {
  getAnnotatedCSVFile,
  getReports,
  getReportsFilterByName,
  postReport,
  getReportsFields,
  getReportsColumns,
  getReportsContainerFields,
  postFieldToReport,
  postCSVFile,
  copyReportToFields,
  copyFieldToField,
  putField,
  deleteField,
  getShareMe,
  getURL
};

/* I18N */

export const SUPPORTED_LOCALES = ['en', 'fr']

export function setupI18n(options = { locale: 'en', fallbackLocale: 'en', messages: messages }) {
  // Use default language set in server's VITE_APP_DEFAULT_LANG environment variable.
  if(import.meta.env.VITE_APP_DEFAULT_LANG && SUPPORTED_LOCALES.includes(import.meta.env.VITE_APP_DEFAULT_LANG)) {
    options.locale = import.meta.env.VITE_APP_DEFAULT_LANG;
  }

  const i18n = createI18n(options)
  setI18nLanguage(i18n, options.locale)
  return i18n
}

export function setI18nLanguage(i18n, locale) {
  if (i18n.mode === 'legacy') {
    i18n.global.locale = locale
  } else {
    i18n.global.locale.value = locale
  }
}

/* TOKENS */

export function parseTokenDetails(accountStoreToken: string) {
  if (accountStoreToken && accountStoreToken != "") {
    const tokenData = accountStoreToken.split(".")[1];
    const parsedToken = JSON.parse(
      decodeURIComponent(escape(atob(tokenData)))
    );
    return parsedToken;
  }
  return null;
}

export function parseReportPermissions(reportStoreId: number, accountStoreToken: string) {
  if (reportStoreId != 0 && accountStoreToken) {
    let usedPolicy = null;
    for (const policy of parseTokenDetails(accountStoreToken).policies_group.policies) {
      if (policy.report_id === reportStoreId.toString()) {
        return policy;
      } else if (policy.report_id === "*") {
        usedPolicy = policy;
      }
    }
    return usedPolicy;
  }
  return null;
}

/* REQUESTS */

const baseUrl = "/api/v1/";
const reportsBaseUrl = `${baseUrl}reports/`;

// Check that it is safe to use the fetch function in mande initialization.
// Required for the test suite.
const reports = typeof fetch === "function" ? mande(reportsBaseUrl) : undefined;
const HEADERS = {}

export function setAcceptLanguage(locale) {
  reports.options.headers['Accept-Language'] = locale;
  shares.options.headers['Accept-Language'] = locale;
  HEADERS['Accept-Language'] = locale;
}

export function setToken(token) {
  reports.options.headers.Authorization = "Bearer " + token;
  shares.options.headers.Authorization = "Bearer " + token;
  HEADERS['Authorization'] = "Bearer " + token;
}

async function getURL(url: string, accept: string) {
  if (accept == null) {
    accept = 'json/application'
  }

  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Accept': accept,
      'Content-Type': 'application/json',
      'Authorization': HEADERS['Authorization'],
      'Accept-Language': HEADERS['Accept-Language']
    }
  });
  if (response.status === 503) {
    const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
    document.dispatchEvent(evt);
  }
  if (!response.ok) {
    const evt = new CustomEvent("errorMessage", {"detail": response.statusText, "bubbles": false, "cancelable": false});
    document.dispatchEvent(evt);
    return Promise.reject(new Error(response.statusText));
  }
  return Promise.resolve(response);
}

function getReports() {
  return reports.get()
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function getReportsFilterByName(name: string) {
  return reports.get("/", { query: { name: name } })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function postReport(name: string) {
  return reports.post({
    new_report: {
      data: name,
    },
  })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function stringifyQuery(query: any): string {
  let searchParams = Object.keys(query)
    .map((k) => [k, query[k]].map(encodeURIComponent).join('='))
    .join('&')
  return searchParams ? '?' + searchParams : ''
}

async function getReportsFields(reportId: number, search: string = null, filters: string = null, lazyLoading: boolean = false, lazyLoadingFrom: number = null, exportReport: boolean = null, exportFormat: string = 'csv') {
  // Default to reportId when doing lazy loading
  if(!lazyLoadingFrom) {
    lazyLoadingFrom = reportId
  }
  let query = { order_by: "name", lazy_loading: lazyLoading, lazy_loading_from: lazyLoadingFrom };
  let accept = 'json/application'

  if (search) {
    query.search = search;
  }
  if (filters) {
    query.filters = JSON.stringify({filters: filters});
  }
  if (exportReport) {
    query.export = exportReport;

    if (exportFormat === 'csv') {
      accept = 'text/csv'
    }
  }

  let url = `${reportsBaseUrl}/${reportId}/fields`
  url += stringifyQuery(query)

  return await getURL(url, accept)
}

function getReportsColumns(reportId: number, search: string = null, filters: string = null) {
  let query = {}

  if (search) {
    query.search = search;
  }
  if (filters) {
    query.filters = JSON.stringify({filters: filters});
  }

  return reports.get(`${reportId}/columns`, { query })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function getReportsContainerFields(reportId: number) {
  return reports.get(`${reportId}/fields`, {
    query: { order_by: "name", flatten: true, field_type: "container" },
  })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function postFieldToReport(
  reportId: number,
  parent_id: number,
  name: string,
  field_type: string,
  data: string
) {
  return reports.post(`${reportId}/fields`, {
    new_field: {
      name: name,
      field_type,
      parent_id,
      data: data,
    },
  })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function copyReportToFields(
  reportId: number,
  sourceId: number,
  fieldName: string,
  clientId: string
) {
  return reports.post(`${reportId}/fields/${reportId}?${clientId}`, {
    field_copy: {
      source_id: sourceId,
      recursive: "true",
      loop: "true",
      run_side_effects: "true",
      field_name: fieldName,
    },
  })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function copyFieldToField(
  reportId: number,
  sourceId: number,
  targetId: number,
) {
  return reports.post(`${reportId}/fields/${targetId}`, {
    field_copy: {
      source_id: sourceId,
      copy_type: 'whole_container'
    },
  })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function putField(reportId: number, id: number, field_update: object) {
  return reports.put(`${reportId}/fields/${id}`, {
    field_update: field_update,
  })
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

function postCSVFile(reportId: number, sourceId: string, file: object) {
  let formData = new FormData();
  if (sourceId != null) {
    formData.append('source_id', sourceId);
  }
  formData.append('file', file);
  return reports.post(`${reportId}/bulk_import`, formData,
    {
      headers: {
        'Accept': null,
        'Content-Type': null
      }
    }
  )
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

async function getAnnotatedCSVFile(reportId: number, filename: string) {
  let url = `${reportsBaseUrl}/${reportId}/bulk_import/${filename}`

  let accept = 'text/csv'
  return await getURL(url, accept)
}

function deleteField(reportId: number, id: number) {
  return reports.delete(`${reportId}/fields/${id}`)
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}

const sharesBaseUrl = `${baseUrl}shares/`;
// Check that it is safe to use the fetch function in mande initialization.
// Required for the test suite.
const shares = typeof fetch === "function" ? mande(sharesBaseUrl) : undefined;

function getShareMe() {
  return shares.get('/me/')
  .catch((e) => {
    if (e.response.status === 503) {
      const evt = new Event("maintenanceMode", {"bubbles": false, "cancelable": false});
      document.dispatchEvent(evt);
    }
    throw e;
  });
}
