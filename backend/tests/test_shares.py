from typing import List
import jwt
import pytest
from httpx import AsyncClient
from databases import Database
from fastapi import FastAPI
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_401_UNAUTHORIZED,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.core.config import (
    SECRET_KEY,
    JWT_ALGORITHM,
    JWT_AUDIENCE,
    JWT_TOKEN_PREFIX,
    ACCESS_TOKEN_EXPIRE_MINUTES,
)
from app.db.repositories.policies import PoliciesRepository
from app.models.report import ReportCreate, ReportPublic, ReportInDB
from app.models.field import FieldInDB
from app.models.policy import (
    AnonymousPoliciesGroupPublic,
    PoliciesGroupPublic,
    PoliciesGroupInDB,
    ReportShareCreate,
)
from app.services import auth_service

# decorate all tests with @pytest.mark.asyncio
pytestmark = pytest.mark.asyncio


@pytest.fixture
async def test_share(client: AsyncClient, db: Database) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Some share",
        policies=[
            {
                "actions": [
                    "report:list",
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ]
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    return created_policies_group


class TestGetReportsShare:
    async def test_get_all_reports_shares(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        all_scopes_policies_group: PoliciesGroupInDB,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for("shares:get-shares")
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        policies_groups = [
            PoliciesGroupPublic(**policies_group) for policies_group in res.json()
        ]
        assert (
            PoliciesGroupPublic(**all_scopes_policies_group.model_dump()) in policies_groups
        )

    async def test_get_share_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        all_scopes_policies_group: PoliciesGroupInDB,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "shares:get-share-by-id", share_id=all_scopes_policies_group.id
            )
        )
        assert res.status_code == HTTP_200_OK
        share = PoliciesGroupPublic(**res.json())
        assert PoliciesGroupPublic(**all_scopes_policies_group.model_dump()) == share

    async def test_get_share_token_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        all_scopes_policies_group: PoliciesGroupInDB,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "shares:get-share-token-by-id", share_id=all_scopes_policies_group.id
            )
        )
        assert res.status_code == HTTP_200_OK
        token = res.json().get("access_token")
        creds = jwt.decode(
            token, str(SECRET_KEY), audience=JWT_AUDIENCE, algorithms=[JWT_ALGORITHM]
        )
        assert "policies_group" in creds
        assert (
            AnonymousPoliciesGroupPublic(**creds["policies_group"]).model_dump()
            == AnonymousPoliciesGroupPublic(**all_scopes_policies_group.model_dump()).model_dump()
        )


class TestEditShare:
    @pytest.mark.parametrize(
        "comments, attrs_to_change, values",
        (
            ("Edited share 1", ["actions"], [["report:list"]]),
            (
                "Edited share 2",
                ["filters", "actions"],
                [[{"field_data": "01", "field_name": "AT"}], ["report:list"]],
            ),
        ),
    )
    async def test_update_share_with_valid_input(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_share: PoliciesGroupPublic,
        test_cpo_report: ReportInDB,
        comments: List[str],
        attrs_to_change: List[str],
        values: List[str],
    ) -> None:
        share_params = {
            "comment": comments,
            "policies": [
                {attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))}
            ],
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "shares:update-share-by-id",
                share_id=test_share.id,
            ),
            json=share_params,
        )
        assert res.status_code == HTTP_200_OK

        assert res.json()["id"] == test_share.id  # make sure it's the same share
        updated_share = AnonymousPoliciesGroupPublic(**res.json())
        test_share = AnonymousPoliciesGroupPublic(**test_share.model_dump())

        # make sure that any attribute we updated has changed to the correct value
        for i in range(len(attrs_to_change)):
            attr_to_change = updated_share.policies[0].model_dump().get(attrs_to_change[i])
            assert attr_to_change != test_share.policies[0].model_dump().get(attrs_to_change[i])
            assert attr_to_change == values[i]

        # make sure that no other attributes' values have changed
        for attr, value in updated_share.policies[0].model_dump().items():
            if attr not in attrs_to_change:
                assert test_share.policies[0].model_dump().get(attr) == value

    async def test_update_share_with_duplicate_report_id_fail(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_share: PoliciesGroupPublic,
        test_cpo_report: ReportInDB,
    ) -> None:
        """
        Forbid editing a policies group by declaring several times same report.
        """
        share_params = {
            "comment": "Edited policies group with duplicated report_id",
            "policies": [
                {
                    "report_id": str(test_cpo_report["0"].id),
                    "actions": ["report:read", "report:write"],
                    "filters": [
                        {
                            "field_name": test_cpo_report["1_1_1_1"].name,
                            "field_data": test_cpo_report["1_1_1_1"].data,
                        }
                    ],
                },
                {"report_id": str(test_cpo_report["0"].id), "actions": ["report:share"]},
            ],
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "shares:update-share-by-id",
                share_id=test_share.id,
            ),
            json=share_params,
        )
        assert res.status_code == HTTP_400_BAD_REQUEST


class TestShareReport:
    async def test_can_generate_share_token_for_one_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: ReportInDB,
    ) -> None:
        share_params = {
            "comment": "First policies group",
            "policies": [
                {
                    "report_id": str(test_cpo_report["0"].id),
                    "actions": ["report:read", "report:write"],
                    "filters": [
                        {
                            "field_name": test_cpo_report["1_1_1_1"].name,
                            "field_data": test_cpo_report["1_1_1_1"].data,
                        }
                    ],
                }
            ],
        }
        # Create a report share
        res = await authorized_client_all_scopes.post(
            app.url_path_for("shares:create-share"),
            json=share_params,
        )
        assert res.status_code == HTTP_201_CREATED

        token = res.json().get("access_token").get("access_token")
        creds = jwt.decode(
            token, str(SECRET_KEY), audience=JWT_AUDIENCE, algorithms=[JWT_ALGORITHM]
        )
        share_id = res.json().get("policies_group").get("id")
        assert share_id is not None

        assert "policies_group" in creds
        assert (
            AnonymousPoliciesGroupPublic(**creds["policies_group"]).model_dump()
            == AnonymousPoliciesGroupPublic(**share_params).model_dump()
        )

    async def test_can_generate_share_token_for_several_reports(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: ReportInDB,
    ) -> None:
        share_params = {
            "comment": "Second policies group",
            "policies": [
                {
                    "actions": ["report:read", "report:write"],
                    "filters": [
                        {
                            "field_name": test_cpo_report["1_1_1_1"].name,
                            "field_data": test_cpo_report["1_1_1_1"].data,
                        }
                    ],
                },
                {
                    "report_id": str(test_cpo_report["0"].id),
                    "actions": ["report:read", "report:write", "report:edit"],
                    "filters": [
                        {
                            "field_name": test_cpo_report["1_1_1_1"].name,
                            "field_data": test_cpo_report["1_1_1_1"].data,
                        }
                    ],
                    "field_id": str(test_cpo_report["2"].id),
                },
            ],
        }
        # Create a report share
        res = await authorized_client_all_scopes.post(
            app.url_path_for("shares:create-share"),
            json=share_params,
        )
        assert res.status_code == HTTP_201_CREATED

        token = res.json().get("access_token").get("access_token")
        creds = jwt.decode(
            token, str(SECRET_KEY), audience=JWT_AUDIENCE, algorithms=[JWT_ALGORITHM]
        )
        share_id = res.json().get("policies_group").get("id")
        assert share_id is not None

        assert "policies_group" in creds
        assert (
            AnonymousPoliciesGroupPublic(**creds["policies_group"]).model_dump()
            == AnonymousPoliciesGroupPublic(**share_params).model_dump()
        )

    async def test_can_not_generate_same_share_param_for_one_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: ReportInDB,
    ) -> None:
        """
        Run twice the same command. It should not allow to create same policies group.
        """
        share_params = {
            "comment": "Third policies group",
            "policies": [
                {
                    "report_id": str(test_cpo_report["0"].id),
                    "actions": ["report:read", "report:write"],
                    "filters": [
                        {
                            "field_name": test_cpo_report["1_1_1_1"].name,
                            "field_data": test_cpo_report["1_1_1_1"].data,
                        }
                    ],
                }
            ],
        }
        # Create a report share
        res = await authorized_client_all_scopes.post(
            app.url_path_for("shares:create-share"),
            json=share_params,
        )
        assert res.status_code == HTTP_201_CREATED

        token = res.json().get("access_token").get("access_token")
        creds = jwt.decode(
            token, str(SECRET_KEY), audience=JWT_AUDIENCE, algorithms=[JWT_ALGORITHM]
        )
        share_id = res.json().get("policies_group").get("id")
        assert share_id is not None

        assert "policies_group" in creds
        assert (
            AnonymousPoliciesGroupPublic(**creds["policies_group"]).model_dump()
            == AnonymousPoliciesGroupPublic(**share_params).model_dump()
        )

        # Do the same thing: create the same report share
        res = await authorized_client_all_scopes.post(
            app.url_path_for("shares:create-share"),
            json=share_params,
        )
        assert res.status_code == HTTP_400_BAD_REQUEST

    async def test_can_not_generate_share_param_for_same_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: ReportInDB,
    ) -> None:
        """
        Forbid having several policies concerning same report_id.
        """
        share_params = {
            "comment": "Fourth policies group",
            "policies": [
                {
                    "report_id": str(test_cpo_report["0"].id),
                    "actions": ["report:read", "report:write"],
                    "filters": [
                        {
                            "field_name": test_cpo_report["1_1_1_1"].name,
                            "field_data": test_cpo_report["1_1_1_1"].data,
                        }
                    ],
                },
                {"report_id": str(test_cpo_report["0"].id), "actions": ["report:share"]},
            ],
        }
        # Create a report share
        res = await authorized_client_all_scopes.post(
            app.url_path_for("shares:create-share"),
            json=share_params,
        )
        assert res.status_code == HTTP_400_BAD_REQUEST

class TestShareMe:
    async def test_authenticated_user_can_retrieve_own_data(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        all_scopes_policies_group: PoliciesGroupInDB,
    ) -> None:
        res = await authorized_client_all_scopes.get(app.url_path_for("shares:get-current-share"))
        assert res.status_code == HTTP_200_OK
        token = res.json().get("access_token")
        creds = jwt.decode(
            token, str(SECRET_KEY), audience=JWT_AUDIENCE, algorithms=[JWT_ALGORITHM]
        )
        assert "policies_group" in creds
        assert (
            PoliciesGroupPublic(**creds["policies_group"]).model_dump()
            == PoliciesGroupPublic(**all_scopes_policies_group.model_dump()).model_dump()
        )

    async def test_user_cannot_access_own_data_if_not_authenticated(
        self,
        app: FastAPI,
        client: AsyncClient,
    ) -> None:
        res = await client.get(app.url_path_for("shares:get-current-share"))
        assert res.status_code == HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize(
        "jwt_prefix",
        (
            ("",),
            ("value",),
            ("Token",),
            ("JWT",),
            ("Swearer",),
        ),
    )
    async def test_user_cannot_access_own_data_with_incorrect_jwt_prefix(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        all_scopes_policies_group: PoliciesGroupInDB,
        db: Database,
        jwt_prefix: str,
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        token = auth_service.create_access_token_for_report_share(
            policies_group=all_scopes_policies_group, secret_key=str(SECRET_KEY)
        )
        res = await authorized_client_all_scopes.get(
            app.url_path_for("shares:get-current-share"),
            headers={"Authorization": f"{jwt_prefix} {token}"},
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED
