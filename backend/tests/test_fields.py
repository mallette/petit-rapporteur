import json
import csv
import datetime
from typing import List
import io
import pytest
from httpx import AsyncClient
from databases import Database
from fastapi import FastAPI
from fastapi.testclient import TestClient
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND,
    HTTP_401_UNAUTHORIZED,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_400_BAD_REQUEST,
)
import requests
import i18n

from app.core.config import SECRET_KEY, JWT_TOKEN_PREFIX
from app.db.repositories.policies import PoliciesRepository
from app.models.report import ReportInDB
from app.models.field import (
    FieldBase,
    FieldPublic,
    FieldCreate,
    FieldInDB,
    FieldAnonymized,
    FieldAnonymizedNoChildren,
    ColumnPublic,
    FieldPublicNoChildren,
)
from app.models.policy import PoliciesGroupPublic, ReportShareCreate
from app.services import auth_service

# decorate all tests with @pytest.mark.asyncio
pytestmark = pytest.mark.asyncio


@pytest.fixture
def new_field(test_report: ReportInDB):
    return FieldCreate(
        name="Category",
        field_type="container",
        data="Description of the field",
        parent_id=test_report.id,
    )


@pytest.fixture
def new_template_field(test_cpo_report: List["FieldInDB"]):
    return FieldCreate(
        name="Description of the text field",
        field_type="template_qualitative_text",
        data="",
        parent_id=test_cpo_report["0"].id,
    )


@pytest.fixture
def new_template_promo_field(test_cpo_report: List["FieldInDB"]):
    return FieldCreate(
        name="Événement",
        field_type="template_container_promo",
        data="",
        parent_id=test_cpo_report["0"].id,
    )

@pytest.fixture
def field_no_parent():
    return FieldCreate(
        name="Category",
        field_type="container",
        data="Description of the field",
        parent_id=0,
    )


@pytest.fixture
def field_not_container_parent(test_fields_with_children: List[ReportInDB]):
    return FieldCreate(
        name="Category",
        field_type="container",
        data="Description of the field",
        parent_id=test_fields_with_children["1_2_2"].id,
    )

@pytest.fixture
async def authorized_client_filtered_fields(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Filtered fields",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "actions": [
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ],
                "filters": [{"field_name": "AT", "field_data": "01"}],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client

@pytest.fixture
async def authorized_client_filtered_parent_fields(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Filtered fields",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "actions": [
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ],
                "filters": [
                    {
                        "field_name": test_cpo_report["1_1"].name,
                        "field_data": test_cpo_report["1_1"].data
                    }
                ],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def authorized_client_delete_container_promo_fields(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Delete container promo",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "actions": [
                    "report:read",
                    "container_promo:delete"
                ],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client

class TestCreateField:
    async def test_valid_input_creates_field(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_field: FieldCreate,
        test_report: ReportInDB,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:add-field-to-report", report_id=test_report.id),
            json={"new_field": new_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldCreate(**field) for field in res.json()]
        created_field = created_fields[0]
        assert created_field == new_field

    async def test_container_moves_existing_promo(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List["FieldInDB"],
    ) -> None:
        new_field = FieldCreate(
            name="Académie",
            field_type="container",
            data="Description of the academy",
            parent_id=test_cpo_report["1_1_1_1"].id,
        )

        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_field = created_fields[0]

        # Get parent field
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1_1_1_1"].id,
            )
        )
        assert res.status_code == HTTP_200_OK
        field = FieldPublic(**res.json())

        assert len(field.children) > 0

        assert field.children[0].field_type == created_field.field_type
        assert field.children[0].name == created_field.name
        assert field.children[0].data == created_field.data
        assert (
            field.children[0].children[0].field_type
            == test_cpo_report["1_1_1_1_1"].field_type
        )
        assert field.children[0].children[0].name == test_cpo_report["1_1_1_1_1"].name
        assert field.children[0].children[0].data == test_cpo_report["1_1_1_1_1"].data

    @pytest.mark.parametrize(
        "valid_payload, status_code",
        (
            (
                {
                    "name": "test_name",
                    "field_type": "container",
                    "data": "test",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "qualitative_text",
                    "data": "test",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "template_qualitative_text",
                    "data": "test",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "quantitative_addition",
                    "data": "43.5",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "template_quantitative_addition",
                    "data": "23",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "quantitative_average",
                    "data": "43",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "template_quantitative_average",
                    "data": "23",
                    "parent_id": 1,
                },
                201,
            ),
            (
                {
                    "name": "Événement",
                    "field_type": "template_container_promo",
                    "data": "",
                    "parent_id": 1,
                },
                201,
            ),
        ),
    )
    async def test_multiple_valid_input_created_field(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        valid_payload: dict,
        status_code: int,
        test_report: ReportInDB,
    ) -> None:
        # Set parent_id here to use fixture variable which is inaccessible in parametrize
        valid_payload["parent_id"] = test_report.id
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:add-field-to-report", report_id=test_report.id),
            json={"new_field": valid_payload},
        )
        assert res.status_code == status_code

    async def test_template_fields_generates_child_fields(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_template_field: FieldCreate,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        # Create the field
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_template_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldCreate(**field) for field in res.json()]
        assert created_fields[0] == new_template_field

        # Change the field type to make comparisons later
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_field = created_fields[0]

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the post is in the response
        assert created_field in fields

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)
        # We look into child number '1', as the '0' is the one we created without children
        assert len(list({v.model_dump()["id"]: v for v in fields[1].children})) == len(
            fields[1].children
        )

        # Assert exact number of children
        # Level 1
        assert 3 == len(fields)
        assert fields[2].id == created_field.id
        assert fields[2].field_type == "template_qualitative_text"
        assert fields[0].field_type == "container"
        assert fields[1].field_type == "container"
        # Level 2
        assert 1 == len(fields[0].children)
        assert 1 == len(fields[1].children)
        level_2_first_container = fields[0].children[0]
        level_2_second_container = fields[1].children[0]
        assert level_2_first_container.field_type == "container"
        assert level_2_second_container.field_type == "container"
        # Level 3
        assert 1 == len(level_2_first_container.children)
        assert 1 == len(level_2_second_container.children)
        level_3_first_container = level_2_first_container.children[0]
        level_3_second_container = level_2_second_container.children[0]
        assert level_3_first_container.field_type == "container"
        assert level_3_second_container.field_type == "container"
        # Level 4
        assert 2 == len(level_3_first_container.children)
        assert 3 == len(level_3_second_container.children)
        level_4_first_container = level_3_first_container.children[0]
        level_4_second_container = level_3_second_container.children[0]
        assert level_4_first_container.field_type == "container"
        assert level_4_second_container.field_type == "container"
        # Level 5
        assert 1 == len(level_4_first_container.children)
        assert 1 == len(level_4_second_container.children)
        level_5_first_container = level_4_first_container.children[0]
        level_5_second_container = level_4_second_container.children[0]
        assert level_5_first_container.field_type == "container_promo"
        assert level_5_second_container.field_type == "container_promo"
        # Level 6
        assert 2 == len(level_5_first_container.children)
        assert 2 == len(level_5_second_container.children)
        level_5_first_promo_default_qualitative_text = level_5_first_container.children[
            0
        ]
        level_5_first_promo_qualitative_text = level_5_first_container.children[1]
        level_5_second_promo_default_qualitative_text = (
            level_5_second_container.children[0]
        )
        level_5_second_promo_qualitative_text = level_5_second_container.children[1]
        assert level_5_first_promo_qualitative_text.field_type == "qualitative_text"
        assert level_5_first_promo_qualitative_text.template_id == created_field.id
        assert level_5_second_promo_qualitative_text.field_type == "qualitative_text"
        assert level_5_second_promo_qualitative_text.template_id == created_field.id

    async def test_import_flat_fields_in_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_template_field: FieldCreate,
        test_report_3: FieldInDB,
        request,
    ) -> None:
        """
        You should be able to import flat fields by batch without running side effects and by
        recreating the field's ids.
        """
        # Create the field
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_report_3.id
            ),
            json={"exported_fields": [{"name":"Ambition","parent_id":16002,"field_type":"container","data":"1. La réussite scolaire de tous","template_id":None,"id":16003},
                                    {"name":"03. Réalisé - Nb Bénéficiaires 2022","parent_id":16002,"field_type":"template_quantitative_addition","data":"","template_id": None,"id":17029},
                                    {"name":"Priorité","parent_id":16003,"field_type":"container","data":"1.5 Dispositifs périscolaires et extra-scolaires","template_id":None,"id":16007},
                                    {"name":"Action","parent_id":16007,"field_type":"container","data":"Accueillir des publics scolaires en internat","template_id":None,"id":16042},
                                    {"name":"Promo","parent_id":16042,"field_type":"container_promo","data":"","template_id":None,"id":16129},
                                    {"name":"03. Réalisé - Nb Bénéficiaires 2022","parent_id":16129,"field_type":"quantitative_addition","data":"","template_id":17029,"id":17044},
                                ],
                  "exported_report_id": 16002,
                }
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields creation in batch to report "{test_report_3.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_report_3.id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        fields = sorted(fields, key=lambda x: x.name)

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)

        # Assert exact number of children
        # Level 1
        assert 2 == len(fields)
        assert fields[0].field_type == "template_quantitative_addition"
        assert fields[1].field_type == "container"
        assert fields[1].data == "1. La réussite scolaire de tous"
        # Level 2
        assert None == fields[0].children
        assert 1 == len(fields[1].children)
        level_2_container = fields[1].children[0]
        assert level_2_container.field_type == "container"
        assert level_2_container.data == "1.5 Dispositifs périscolaires et extra-scolaires"
        assert level_2_container.parent_id == fields[1].id

        # Assert the quantitative field at the last level has the right template_id
        level_5_quantitative_field = fields[1].children[0].children[0].children[0].children[0]
        assert level_5_quantitative_field.parent_id == fields[1].children[0].children[0].children[0].id
        assert level_5_quantitative_field.template_id == fields[0].id

    async def test_promo_field_inherits_parent_template_fields(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_template_field: FieldCreate,
        new_template_promo_field: FieldCreate,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        # Setup template fields
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_template_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_template_field = created_fields[0]

        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_template_promo_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED

        # Create promo field
        new_field = FieldCreate(
            name="Action",
            field_type="container_promo",
            data="Fake promo description",
            parent_id=test_cpo_report["1_1_1_1"].id,
        )
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_field = created_fields[0]
        assert created_field.name == new_template_promo_field.name
        assert created_field.data == new_field.data

        # Get field to check what has been automatically generated
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=created_field.id,
            )
        )
        assert res.status_code == HTTP_200_OK
        field = FieldPublic(**res.json())

        assert len(field.children) > 0
        assert field.children[0].name == created_template_field.name
        assert field.children[0].data == created_template_field.data
        assert field.children[0].template_id == created_template_field.id

    async def test_absent_report_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_field: FieldCreate,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:add-field-to-report", report_id=0),
            json={"new_field": new_field.model_dump()},
        )
        assert res.status_code == HTTP_404_NOT_FOUND

    async def test_absent_parent_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        field_no_parent: FieldCreate,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:add-field-to-report", report_id=test_report.id),
            json={"new_field": field_no_parent.model_dump()},
        )
        assert res.status_code == HTTP_404_NOT_FOUND

    async def test_not_container_parent_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        field_not_container_parent: FieldCreate,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:add-field-to-report", report_id=test_report.id),
            json={"new_field": field_not_container_parent.model_dump()},
        )
        assert res.status_code == HTTP_400_BAD_REQUEST

    @pytest.mark.parametrize(
        "invalid_payload, status_code",
        (
            (None, 422),
            ({}, 422),
            ({"name": "test_name"}, 422),
            ({"field_type": "container"}, 422),
            ({"name": "test_name", "description": "test"}, 422),
            (
                {
                    "name": "*",
                    "field_type": "container",
                    "data": "test",
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "Normal name",
                    "field_type": "container",
                    "data": "*",
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "Normal name",
                    "field_type": "quantitative_addition",
                    "data": "this is text",
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "Normal name",
                    "field_type": "quantitative_average",
                    "data": "this is text",
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "Normal name",
                    "field_type": "qualitative_bool",
                    "data": "this is text",
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "Normal name",
                    "field_type": "qualitative_bool",
                    "data": "22",
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "template_qualitative_bool",
                    "data": True,
                    "parent_id": 1,
                },
                422,
            ),
            (
                {
                    "name": "test_name",
                    "field_type": "qualitative_bool",
                    "data": False,
                    "parent_id": 1,
                },
                422,
            ),
        ),
    )
    async def test_invalid_input_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        invalid_payload: dict,
        status_code: int,
        test_report: ReportInDB,
    ) -> None:
        # Set parent_id only if needed
        if invalid_payload and "parent_id" in invalid_payload:
            invalid_payload["parent_id"] = test_report.id

        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:add-field-to-report", report_id=test_report.id),
            json={"new_field": invalid_payload},
        )
        assert res.status_code == status_code

    async def test_copy_creates_multiple_fields(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_fields_with_children: ReportInDB,
        test_report: ReportInDB,
        test_report_2: ReportInDB,
        test_field: ReportInDB,
        request,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:copy-fields-to-report",
                report_id=test_report.id,
                field_id=test_field.id,
            ),
            json={"field_copy": {"source_id": test_report_2.id, "recursive": True}},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == ('Fields copy from report "other fake report name" to field '
                                 '"Some field description"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check the copy has been made to target field
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report.id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert copied fields are in response
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields[0].children
        ]
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1_1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[0].children
        ]
        assert FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_1"].model_dump()
        ) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[0].children[1].children
        ]
        assert (FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_3"].model_dump()
        ) ==
            FieldAnonymizedNoChildren(**fields[0].children[0].children[1].children[2].model_dump())
        )
        assert (FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_1_1"].model_dump()
        ) ==
            FieldAnonymizedNoChildren(**fields[0].children[0].children[1].children[0].children[0].model_dump())
        )
        assert FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_2"].model_dump()
        ) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[0].children[1].children
        ]

        # Ensure template_ids are well copied
        assert fields[0].children[0].children[1].children[0].children[0].template_id == fields[0].children[0].children[1].children[2].id

    async def test_copy_report_creates_multiple_fields(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_fields_with_children: ReportInDB,
        test_report: ReportInDB,
        test_report_2: ReportInDB,
        test_field: ReportInDB,
        request,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:copy-fields-to-report",
                report_id=test_report.id,
                field_id=test_report.id,
            ),
            json={"field_copy": {"source_id": test_report_2.id, "recursive": True}},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == ('Fields copy from report "other fake report name" to field '
                                 '"fake report name"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check the copy has been made to target field
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report.id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldAnonymized(**field) for field in res.json()]

        # Assert existant field is still present. (Now containing a container_promo)
        assert FieldAnonymizedNoChildren(**test_field.model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields
        ]
        # Assert copied fields are in response
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields
        ]
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1_1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields[1].children
        ]
        assert FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_1"].model_dump()
        ) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[1].children[1].children
        ]
        assert FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_2"].model_dump()
        ) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[1].children[1].children
        ]

    async def test_copy_field_creates_multiple_fields(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List["FieldInDB"],
        test_fields_with_children: List["FieldInDB"],
        request,
    ) -> None:
        """
        Copy the whole field, the container and its children under another field.
        """
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:copy-fields-to-report",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1_1_1_1"].id,
            ),
            json={"field_copy": {"source_id": test_fields_with_children["1_2"].id, "copy_type": "whole_container"}},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == ('Fields copy from report "Another sub-field 2 description" to field '
                                 '"01"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check the copy has been made to target field
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldAnonymized(**field) for field in res.json()]

        # Check fields have been copied whith the whole container
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1_2"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields[1].children[0].children[0].children[0].children
        ]
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1_2_1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields[1].children[0].children[0].children[0].children[1].children
        ]
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1_2_2"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields[1].children[0].children[0].children[0].children[1].children
        ]
    async def test_copy_report_creates_multiple_fields_in_loop(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_fields_with_children: ReportInDB,
        test_report: ReportInDB,
        test_report_2: ReportInDB,
        test_field: ReportInDB,
        request,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:copy-fields-to-report",
                report_id=test_report_2.id,
                field_id=test_report_2.id,
            ),
            json={
                "field_copy": {
                    "source_id": test_report.id,
                    "recursive": True,
                    "loop": True,
                    "field_name": test_fields_with_children["1_1"].name,
                }
            },
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == ('Fields copy from report "fake report name" to field '
                                 '"other fake report name"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check the copy has been made to target field
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_2.id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        # Note: FieldAnonymizedNoChildren is used to compare cloned fields without having
        # conflicts about different ids and parent_ids. Also to to avoid different children as
        # some has been filtered out.
        fields = [FieldAnonymized(**field) for field in res.json()]

        # Assert existant field is still present
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields
        ]
        assert FieldAnonymizedNoChildren(**test_fields_with_children["1_1"].model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump()) for field in fields[0].children
        ]
        assert FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_1"].model_dump()
        ) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[1].children
        ]
        assert FieldAnonymizedNoChildren(
            **test_fields_with_children["1_2_2"].model_dump()
        ) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[1].children
        ]
        # Assert copied fields are in response
        assert FieldAnonymizedNoChildren(**test_field.model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[0].children
        ]
        assert FieldAnonymizedNoChildren(**test_field.model_dump()) in [
            FieldAnonymizedNoChildren(**field.model_dump())
            for field in fields[0].children[1].children
        ]


class TestGetFieldsFromReport:
    async def test_get_fields_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        test_field: FieldInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        """
        Get all nested fields from a report when there are no rows (which means with
        no `container_promo` fields). These fields can't be processed and be filtered
        with columns.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report.id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the repost is in the response
        assert FieldPublic(**test_field.model_dump()) in fields
        # Assert other fields wich are not children of the report are not in the response

    async def test_get_fields_from_report_by_id_order_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get fields orderered by id
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={"order_by": "id"},
        )
        assert res.status_code == HTTP_200_OK
        export = res.json()
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        assert fields[0] == FieldPublic(**test_cpo_report["2"].model_dump())
        assert fields[1] == FieldPublic(**test_cpo_report["1"].model_dump())
        # Check order of deep children
        assert fields[0].children[0].children[0].children[1].children[0].children[0] == FieldPublic(**test_cpo_report["2_1_1_2_1_1"].model_dump())
        assert fields[0].children[0].children[0].children[1].children[0].children[1] == FieldPublic(**test_cpo_report["2_1_1_2_1_2"].model_dump())

    async def test_get_fields_from_report_by_id_order_by_name(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get fields orderered by name
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        export = res.json()
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        assert fields[0] == FieldPublic(**test_cpo_report["1"].model_dump())
        assert FieldPublicNoChildren(**fields[1].model_dump()) == FieldPublicNoChildren(**test_cpo_report["2"].model_dump())
        # Check order of deep children
        assert fields[1].children[0].children[0].children[1].children[0].children[0] == FieldPublic(**test_cpo_report["2_1_1_2_1_2"].model_dump())
        assert fields[1].children[0].children[0].children[1].children[0].children[1] == FieldPublic(**test_cpo_report["2_1_1_2_1_1"].model_dump())

    async def test_get_fields_from_report_by_id_for_export_json(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get fields in a proper JSON format useful to reimport in another instance.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={
                "export": "true",
                "order_by": "name"
            },
        )
        assert res.status_code == HTTP_200_OK
        export = res.json()
        assert isinstance(export['exported_fields'], list)
        assert len(export['exported_fields']) == len(test_cpo_report) - 1

        assert export['exported_report_id'] == test_cpo_report["0"].id
        assert FieldPublicNoChildren(**export['exported_fields'][0]) == FieldPublicNoChildren(**test_cpo_report["1"].model_dump())

    async def test_get_fields_from_report_by_id_for_export_csv(
        self,
        app: FastAPI,
        authorized_csv_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get fields in a CSV file in order to be processed through a spreadsheet software.
        """
        res = await authorized_csv_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={"export": "true"},
        )
        assert res.status_code == HTTP_200_OK
        parsed_response = csv.reader(io.StringIO(res.text), delimiter= ',')

        first_row = next(parsed_response)
        assert len(first_row) == 8
        assert first_row[0] == 'id'
        assert first_row[1] == 'Ambition'
        assert first_row[2] == 'Priorité'
        assert first_row[3] == 'Action'
        assert first_row[4] == 'AT'
        assert first_row[5] == 'Promo'
        assert first_row[6] == 'Additional description'
        assert first_row[7] == 'Description'

        second_row = next(parsed_response)
        assert len(second_row) == 8
        assert second_row[0] == str(test_cpo_report["1_1_1_1_1"].id)
        assert second_row[1] == test_cpo_report["1"].data
        assert second_row[2] == test_cpo_report["1_1"].data
        assert second_row[3] == test_cpo_report["1_1_1"].data
        assert second_row[4] == test_cpo_report["1_1_1_1"].data
        assert second_row[5] == test_cpo_report["1_1_1_1_1"].data
        assert second_row[6] == ''
        assert second_row[7] == test_cpo_report["1_1_1_1_1_1"].data

    async def test_get_fields_from_report_by_id_with_lazy_loading(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={
                "lazy_loading": "true",
                "order_by": "name"
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the repost is in the response
        # Note: FieldBase is used in order to avoid to compare children as some has been filtered in the response
        assert FieldBase(**test_cpo_report["1"].model_dump()) in [
            FieldBase(**field.model_dump()) for field in fields
        ]
        assert FieldBase(**test_cpo_report["1_1"].model_dump()) in [
            FieldBase(**field.model_dump()) for field in fields[0].children
        ]

        # Assert there is not level 3
        assert fields[0].children[0].children is None

    async def test_get_fields_from_report_by_id_with_filter_type_info_flatten(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_field_info: FieldInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        """
        The info fields appear in the result because we asked for a flattened output.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_2.id
            ),
            params={"field_type": "info", "flatten": "true"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldBase(**field) for field in res.json()]
        # Assert the field with the right type is present
        # Note: FieldBase is used in order to avoid to compare children as some has been filtered out in the response
        assert FieldBase(**test_fields_with_children["1_2_2"].model_dump()) in fields
        # Assert other fields wich are not children of the report are not in the response
        assert FieldBase(**test_field.model_dump()) not in fields
        # Assert other fields wich are not of the right type are not in the response
        assert FieldBase(**test_fields_with_children["1"].model_dump()) not in fields
        assert FieldBase(**test_fields_with_children["1_1"].model_dump()) not in fields

    async def test_get_fields_from_report_by_id_with_filter_name_flatten(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_field_info: FieldInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_2.id
            ),
            params={"name": test_fields_with_children["1_1"].name, "flatten": "true"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        # Note: FieldBase is used in order to avoid to compare children as some has been filtered out in the response
        fields = [FieldBase(**field) for field in res.json()]
        # Assert there are exactly two filtered fields
        assert len(fields) == 2
        assert FieldBase(**test_fields_with_children["1_1"].model_dump()) in fields
        assert FieldBase(**test_fields_with_children["1_2"].model_dump()) in fields
        # Assert other fields wich are not children of the report are not in the response
        assert FieldBase(**test_fields_with_children["1"].model_dump()) not in fields
        # Assert other fields wich are not of the right type are not in the response
        assert FieldBase(**test_field_info.model_dump()) not in fields

    async def test_get_fields_from_report_by_id_with_filter_name_and_data_flatten(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
            params={
                "name": test_cpo_report["2_1_1_2"].name,
                "data": test_cpo_report["2_1_1_2"].data,
                "flatten": "true",
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        # Note: FieldBase is used in order to avoid to compare children as some has been filtered out in the response
        fields = [FieldBase(**field) for field in res.json()]
        # Assert there are exactly two filtered fields
        assert len(fields) == 2
        assert FieldBase(**test_cpo_report["1_1_1_2"].model_dump()) in fields
        assert FieldBase(**test_cpo_report["2_1_1_2"].model_dump()) in fields

    async def test_get_fields_from_report_by_id_with_filter_name_and_data_flatten_include_children(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
            params={
                "name": test_cpo_report["2_1_1_2"].name,
                "data": test_cpo_report["2_1_1_2"].data,
                "flatten": "true",
                "include_children": "true",
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        # Note: FieldBase is used in order to avoid to compare children as some has been filtered out in the response
        fields = [FieldBase(**field) for field in res.json()]

        assert len(fields) == 7
        assert FieldBase(**test_cpo_report["1_1_1_2"].model_dump()) in fields
        assert FieldBase(**test_cpo_report["2_1_1_2"].model_dump()) in fields

        assert FieldBase(**test_cpo_report["1_1_1_2_1"].model_dump()) in fields
        assert FieldBase(**test_cpo_report["2_1_1_2_1"].model_dump()) in fields

        assert FieldBase(**test_cpo_report["1_1_1_2_1_1"].model_dump()) in fields
        assert FieldBase(**test_cpo_report["2_1_1_2_1_1"].model_dump()) in fields

        assert FieldBase(**test_cpo_report["1_1_1_1_1"].model_dump()) not in fields
        assert FieldBase(**test_cpo_report["1_1_1_1_1_1"].model_dump()) not in fields

    async def test_get_fields_with_children_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get processable and nested fields from report when there are `container_promo` fields in it.
        Having `container_promo` fields allows to build rows and then to filter through their columns.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={"order_by": "name"}
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the repost is in the response
        assert FieldPublic(**test_cpo_report["1"].model_dump()) == fields[0]
        assert (
            FieldPublic(**test_cpo_report["1_1"].model_dump())
            == fields[0].children[0]
        )

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)
        assert len(list({v.model_dump()["id"]: v for v in fields[0].children})) == len(
            fields[0].children
        )

        # Assert exact number of children
        assert 2 == len(fields) # Two "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité"
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 2 == len(fields[0].children[0].children[0].children) # Two "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo"
        assert 1 == len(fields[0].children[0].children[0].children[0].children[0].children) # One "Description"
        assert None == fields[0].children[0].children[0].children[0].children[0].children[0].children # Nothing more in Description

    async def test_get_filtered_fields_with_children_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get a report tree with each branch (= a "promo" or a row of a table) having at least one field matching the filter.
        In our case, get the parent fields and the children fields of the fields on
        which we apply the filter, even if these fields don't match the filter.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id,
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1_1"].name, # "Priorité 1.1"
                                "field_data": test_cpo_report["1_1"].data,
                            }
                        ]
                    }
                )
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité"
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 2 == len(fields[0].children[0].children[0].children) # Two "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo" in an "AT"

        # Assert only "Ambition 1" is present and not "Ambition 2"
        assert FieldPublic(**test_cpo_report["1"].model_dump()) == fields[0]
        assert FieldPublic(**test_cpo_report["2"].model_dump()) not in fields

        # Assert "Priorité 1.1" is present and not "Priorité 2.1"
        assert (
            FieldPublic(**test_cpo_report["1_1"].model_dump())
            == fields[0].children[0]
        )
        assert (
            FieldPublic(**test_cpo_report["2_1"].model_dump())
            not in fields[0].children
        )

    async def test_get_filtered_fields_with_children_from_report_by_id_with_empty_container(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_field: FieldInDB,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get a report tree filtered on containers name with some
        containers being empty (which means without a container_promo field).
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id,
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["2_1"].name, # "Priorité 2.1"
                                "field_data": test_cpo_report["2_1"].data,
                            }
                        ]
                    }
                )
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité"
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 3 == len(fields[0].children[0].children[0].children) # Three "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo" in an "AT"

        # Assert only "Ambition 2" is present and not "Ambition 1"
        assert FieldPublic(**test_cpo_report["2"].model_dump()) == fields[0]
        assert FieldPublic(**test_cpo_report["1"].model_dump()) not in fields

        # Assert "Priorité 2.1" is present and not "Priorité 1.1"
        assert (
            FieldPublic(**test_cpo_report["1_1"].model_dump())
            not in fields[0].children
        )
        assert (
            FieldPublic(**test_cpo_report["2_1"].model_dump())
            == fields[0].children[0]
        )

    async def test_get_double_filtered_fields_with_children_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get a report with two cumulated filters on different columns.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id,
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1_1"].name, # "Priorité 1.1
                                "field_data": test_cpo_report["1_1"].data,
                            },
                            {
                                "field_name": test_cpo_report["1_1_1_1"].name, # "AT 01"
                                "field_data": test_cpo_report["1_1_1_1"].data,
                            }
                        ]
                    }
                )
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité"
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 1 == len(fields[0].children[0].children[0].children) # One "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo" in an "AT"

        # Assert only "Ambition 1" is present and not "Ambition 2"
        assert FieldPublicNoChildren(**test_cpo_report["1"].model_dump()) == FieldPublicNoChildren(**fields[0].model_dump())
        assert FieldPublicNoChildren(**test_cpo_report["2"].model_dump()) not in [FieldPublicNoChildren(**field.model_dump()) for field in fields]

        # Assert "Priorité 1.1" is present and not "Priorité 2.1"
        assert (
            FieldPublicNoChildren(**test_cpo_report["1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].model_dump())
        )
        assert FieldPublicNoChildren(**test_cpo_report["2_1"].model_dump()) not in [FieldPublicNoChildren(**field.model_dump()) for field in fields[0].children]

        # Assert "Promo" of "AT 1" is present
        assert (
            FieldPublicNoChildren(**test_cpo_report["1_1_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].children[0].children[0].children[0].model_dump())
        )

    async def test_get_filtered_parallel_fields_with_children_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get a report with filters on one column, but on which it may
        have several matching rows in different branches of the tree report.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id,
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1_1_1_1"].name, # "AT 01"
                                "field_data": test_cpo_report["1_1_1_1"].data,
                            }
                        ]
                    }
                ),
                "order_by": "name"
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 2 == len(fields) # Two "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité" in Ambition
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 1 == len(fields[0].children[0].children[0].children) # One "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo" in an "AT"

        # Assert "Ambition 1" and "Ambition 2" are present
        assert test_cpo_report["1"].model_dump()["id"] == fields[0].model_dump()["id"]
        assert test_cpo_report["2"].model_dump()["id"] == fields[1].model_dump()["id"]

        # Assert "Priorité 1.1" is present in "Ambition 1" and "Priorité 2.1" in "Ambition 2"
        assert (
            FieldPublicNoChildren(**test_cpo_report["1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].model_dump())
        )
        assert (
            FieldPublicNoChildren(**test_cpo_report["2_1"].model_dump())
            == FieldPublicNoChildren(**fields[1].children[0].model_dump())
        )

        # Assert "AT 1" from "Ambition 1" and "Ambition 2" are present
        assert (
            FieldPublicNoChildren(**test_cpo_report["1_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].children[0].children[0].model_dump())
        )
        assert (
            FieldPublicNoChildren(**test_cpo_report["2_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[1].children[0].children[0].children[0].model_dump())
        )

        # Assert "Promo" of "AT 1" is present
        assert (
             FieldPublicNoChildren(**test_cpo_report["1_1_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].children[0].children[0].children[0].model_dump())
        )

    async def test_get_filtered_parallel_fields_with_children_from_report_by_id_order_by_name(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get a report, ordered by name, with filters on one column, but on which it may
        have several matching rows in different branches of the tree report.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report["0"].id,
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1_1_1_1"].name, # "AT 01"
                                "field_data": test_cpo_report["1_1_1_1"].data,
                            }
                        ]
                    }
                ),
                "order_by": "name"
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 2 == len(fields) # Two "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité" in Ambition
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 1 == len(fields[0].children[0].children[0].children) # One "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo" in an "AT"

        # Assert "Ambition 1" and "Ambition 2" are present
        assert test_cpo_report["1"].model_dump()["id"] == fields[0].model_dump()["id"]
        assert test_cpo_report["2"].model_dump()["id"] == fields[1].model_dump()["id"]

        # Assert "Priorité 1.1" is present in "Ambition 1" and "Priorité 2.1" in "Ambition 2"
        assert (
            FieldPublicNoChildren(**test_cpo_report["1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].model_dump())
        )
        assert (
            FieldPublicNoChildren(**test_cpo_report["2_1"].model_dump())
            == FieldPublicNoChildren(**fields[1].children[0].model_dump())
        )

        # Assert "AT 1" from "Ambition 1" and "Ambition 2" are present
        assert (
            FieldPublicNoChildren(**test_cpo_report["1_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].children[0].children[0].model_dump())
        )
        assert (
            FieldPublicNoChildren(**test_cpo_report["2_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[1].children[0].children[0].children[0].model_dump())
        )

        # Assert "Promo" of "AT 1" is present
        assert (
             FieldPublicNoChildren(**test_cpo_report["1_1_1_1_1"].model_dump())
            == FieldPublicNoChildren(**fields[0].children[0].children[0].children[0].children[0].model_dump())
        )

    async def test_get_fields_by_report_id_lazy_loaded_full_access(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Assure we can access sub-fields with lazy-loading even if there are restrictions
        on parent categories.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id
            ),
            params={
                "lazy_loading": "true",
                "lazy_loading_from": test_cpo_report["1_1"].id
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Action"
        assert 2 == len(fields[0].children) # Two "AT"
        assert None == fields[0].children[0].children # No promo as lazy loading stops here

        # Assert "Action 1" is present
        assert test_cpo_report["1_1_1"].model_dump()["id"] == fields[0].model_dump()["id"]

        # Assert "AT 1" and "AT 2" are present
        assert test_cpo_report["1_1_1_1"].model_dump()["id"] == fields[0].children[0].model_dump()["id"]
        assert test_cpo_report["1_1_1_2"].model_dump()["id"] == fields[0].children[1].model_dump()["id"]

    async def test_get_fields_by_report_id_lazy_loaded_limited_access(
        self,
        app: FastAPI,
        authorized_client_filtered_fields: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Assure we can access sub-fields with lazy-loading even if there are restrictions
        on parent categories.
        """
        res = await authorized_client_filtered_fields.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id
            ),
            params={
                "lazy_loading": "true",
                "lazy_loading_from": test_cpo_report["1_1"].id
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Action"
        assert 1 == len(fields[0].children) # One "AT"
        assert None == fields[0].children[0].children # No promo as lazy loading stops here

        # Assert "Action 1" is present
        assert test_cpo_report["1_1_1"].model_dump()["id"] == fields[0].model_dump()["id"]

        # Assert "AT 1" is present
        assert test_cpo_report["1_1_1_1"].model_dump()["id"] == fields[0].children[0].model_dump()["id"]

    async def test_get_fields_by_report_id_lazy_loaded_limited_access_unauthorized_fails(
        self,
        app: FastAPI,
        authorized_client_filtered_parent_fields: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Assure that we can't have unauthorized access to sub-fields using lazy-loading.
        """
        res = await authorized_client_filtered_parent_fields.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id
            ),
            params={
                "lazy_loading": "true",
                "lazy_loading_from": test_cpo_report["2_1"].id
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) == 0

    async def test_get_fields_by_report_id_search(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Run search through report.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id
            ),
            params={
                "search": "something here 2",
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Ambition"
        assert 1 == len(fields[0].children) # One "Priorité"
        assert 1 == len(fields[0].children[0].children) # One "Action"
        assert 1 == len(fields[0].children[0].children[0].children) # One "AT"
        assert 1 == len(fields[0].children[0].children[0].children[0].children) # One "Promo"
        assert 1 == len(fields[0].children[0].children[0].children[0].children[0].children) # One "Description"

        # Assert "Action Another action" is present
        assert test_cpo_report["2_1_1"].model_dump()["id"] == fields[0].children[0].children[0].model_dump()["id"]

        # Assert "Promo with something 2" is present
        assert test_cpo_report["2_1_1_1_1"].model_dump()["id"] == fields[0].children[0].children[0].children[0].children[0].model_dump()["id"]

    async def test_get_fields_by_report_id_filters_limited_access_lazy_search(
        self,
        app: FastAPI,
        authorized_client_filtered_fields: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Run search through the report to which is cumulated with policy's and user's filters.
        """
        res = await authorized_client_filtered_fields.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id
            ),
            params={
                "search": "something here 2",
                "lazy_loading": "true",
                "lazy_loading_from": test_cpo_report["2_1"].id,
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["2"].name, # "AT 01"
                                "field_data": test_cpo_report["2"].data,
                            }
                        ]
                    }
                )
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert exact number of children
        assert 1 == len(fields) # One "Action"
        assert 1 == len(fields[0].children) # One "AT"
        assert None == fields[0].children[0].children # No promo as lazy loading stops here

        # Assert "Action Another action" is present
        assert test_cpo_report["2_1_1"].model_dump()["id"] == fields[0].model_dump()["id"]

        # Assert "AT 01" is present
        assert test_cpo_report["2_1_1_1"].model_dump()["id"] == fields[0].children[0].model_dump()["id"]

    async def test_get_fields_with_children_from_report_by_id_order_by_name(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_2.id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the children order is inversed
        assert (
            FieldPublic(**test_fields_with_children["1_1"].model_dump())
            == fields[0].children[0]
        )
        assert (
            FieldPublic(**test_fields_with_children["1_2"].model_dump())
            == fields[0].children[1]
        )

    async def test_get_leaf_container_fields_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_2.id
            ),
            params={"leaf_containers": True},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        assert FieldPublic(**test_fields_with_children["1_1"].model_dump()) in fields
        assert FieldPublic(**test_fields_with_children["1_2_1"].model_dump()) not in fields
        # Assert there are only containers
        assert FieldPublic(**test_fields_with_children["1_2_2"].model_dump()) not in fields

    async def test_get_fields_with_flatten_children_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_field: FieldInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_2.id
            ),
            params={"flatten": "true"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the repost is in the response
        assert FieldPublic(**test_fields_with_children["1"].model_dump()).id in [
            field.id for field in fields
        ]
        assert FieldPublic(**test_fields_with_children["1_1"].model_dump()).id in [
            field.id for field in fields
        ]

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)

        # Assert exact number of children
        assert 7 == len(fields)

        # Assert other fields wich are not children of the report are not in the response
        assert FieldPublic(**test_field.model_dump()) not in fields

    async def test_empty_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_3: ReportInDB,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report_3.id
            )
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) == 0

    @pytest.mark.parametrize(
        "invalid_id, status_code",
        (
            (None, 422),
            ({}, 422),
            (0, 404),
        ),
    )
    async def test_invalid_id_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        invalid_id: int,
        status_code: int,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=invalid_id
            )
        )
        assert res.status_code == status_code


class TestGetField:
    async def test_get_field_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_report_2.id,
                field_id=test_fields_with_children["1"].id,
            )
        )
        assert res.status_code == HTTP_200_OK
        field = FieldPublic(**res.json())
        assert field == FieldPublic(**test_fields_with_children["1"].model_dump())

        # Assert children are in response too
        assert FieldPublic(**test_fields_with_children["1_1"].model_dump()) in field.children
        assert (
            FieldPublic(**test_fields_with_children["1_2_1"].model_dump())
            in field.children[1].children
        )

    async def test_get_field_by_id_filters(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1"].id,
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1_1_1_1"].name, # "Priorité 1.1"
                                "field_data": test_cpo_report["1_1_1_1"].data,
                            }
                        ]
                    }
                )
            },
        )
        assert res.status_code == HTTP_200_OK
        field = FieldPublic(**res.json())
        assert FieldPublicNoChildren(**field.model_dump()) == FieldPublicNoChildren(**test_cpo_report["1"].model_dump())

        # Assert children are in response too
        assert FieldPublic(**test_cpo_report["1_1_1_1"].model_dump()) in field.children[0].children[0].children

        # Assert other fields have been filtered out
        assert (
            FieldPublic(**test_cpo_report["1_1_1_2"].model_dump())
            not in field.children[0].children[0].children
        )

class TestEditField:
    @pytest.mark.parametrize(
        "attrs_to_change, values",
        (
            (["data"], ["new description"]),
            (["field_type"], ["info"]),
            (["parent_id"], [1]),
            (["name"], ["new name"]),
            (["name", "field_type"], ["Change the field name", "info"]),
        ),
    )
    async def test_update_field_with_valid_input(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_field: FieldInDB,
        test_report: ReportInDB,
        attrs_to_change: List[str],
        values: List[str],
    ) -> None:
        field_update = {
            "field_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-field-by-id",
                report_id=test_report.id,
                field_id=test_field.id,
            ),
            json=field_update,
        )
        assert res.status_code == HTTP_200_OK

        updated_field = FieldPublic(**res.json())
        assert updated_field.id == test_field.id  # make sure it's the same field

        # make sure that any attribute we updated has changed to the correct value
        for i in range(len(attrs_to_change)):
            attr_to_change = getattr(updated_field, attrs_to_change[i])
            assert attr_to_change != getattr(test_field, attrs_to_change[i])
            assert attr_to_change == values[i]

        # make sure that no other attributes' values have changed
        for attr, value in updated_field.model_dump().items():
            if attr not in attrs_to_change:
                assert getattr(test_field, attr) == value

    @pytest.mark.parametrize(
        "attrs_to_change, values",
        (
            (["data"], ["new description"]),
            (["field_type"], ["info"]),
            (["parent_id"], [1]),
            (["name"], ["new name"]),
            (["name", "field_type"], ["Change the field name", "info"]),
        ),
    )
    async def test_update_field_with_children_with_valid_input(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_fields_with_children: List[FieldInDB],
        test_report: ReportInDB,
        attrs_to_change: List[str],
        values: List[str],
    ) -> None:
        field_update = {
            "field_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-field-by-id",
                report_id=test_report.id,
                field_id=test_fields_with_children["1"].id,
            ),
            json=field_update,
        )
        assert res.status_code == HTTP_200_OK

        updated_field = FieldPublic(**res.json())
        assert (
            updated_field.id == test_fields_with_children["1"].id
        )  # make sure it's the same field
        # Check field's children are present
        assert (
            FieldPublic(**test_fields_with_children["1_1"].model_dump())
            in updated_field.children
        )
        assert (
            FieldPublic(**test_fields_with_children["1_2_1"].model_dump())
            in updated_field.children[1].children
        )

        # make sure that any attribute we updated has changed to the correct value
        for i in range(len(attrs_to_change)):
            attr_to_change = getattr(updated_field, attrs_to_change[i])
            assert attr_to_change != getattr(
                FieldPublic(**test_fields_with_children["1"].model_dump()), attrs_to_change[i]
            )
            assert attr_to_change == values[i]

        # make sure that no other attributes' values have changed
        for attr, value in updated_field.model_dump().items():
            if attr not in attrs_to_change:
                assert (
                    FieldPublic(**test_fields_with_children["1"].model_dump()).model_dump().get(attr)
                    == value
                )

    @pytest.mark.parametrize(
        "attrs_to_change, values, status_code",
        (
            (["field_type"], [0], 422),
            (["id"], [0], 400),
            (["data_id"], [0], 400),
        ),
    )
    async def test_update_field_with_invalid_input_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_field: FieldInDB,
        test_report: ReportInDB,
        attrs_to_change: List[str],
        values: List[str],
        status_code: int,
    ) -> None:
        field_update = {
            "field_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-field-by-id",
                report_id=test_report.id,
                field_id=test_field.id,
            ),
            json=field_update,
        )
        assert res.status_code == status_code

    async def test_update_field_with_child_as_parent_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_fields_with_children: List[FieldInDB],
        test_report: ReportInDB,
    ) -> None:
        field_update = {
            "field_update": {"parent_id": test_fields_with_children["1_1"].id}
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-field-by-id",
                report_id=test_report.id,
                field_id=test_fields_with_children["1"].id,
            ),
            json=field_update,
        )
        assert res.status_code == HTTP_400_BAD_REQUEST

    @pytest.mark.parametrize(
        "attrs_to_change, values",
        (
            (["name"], ["A new name"]),
        ),
    )
    # test_fields_with_children parameter is required to generate children
    async def test_update_template_updates_linked_fields_too(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        attrs_to_change: List[str],
        values: List[str],
        test_cpo_report: List[FieldInDB],
        new_template_field: FieldCreate,
    ) -> None:

        # First part is a copy of test_template_fields_generates_child_fields to
        # generate all the fields

        # Create the field
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_template_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldCreate(**field) for field in res.json()]
        created_field = created_fields[0]
        assert created_field == new_template_field

        # Change the field type to make comparisons later
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_field = created_fields[0]

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the post is in the response
        assert created_field in fields

        ## The following second part about update is custom

        # update the field
        field_update = {
            "field_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=created_field.id,
            ),
            json=field_update,
        )
        assert res.status_code == HTTP_200_OK

        # Get again report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)
        assert len(list({v.model_dump()["id"]: v for v in fields[0].children})) == len(
            fields[0].children
        )

        # Create variables to make clear which are the linked fields which are being updated
        level_2_first_container = fields[0].children[0]
        level_2_second_container = fields[1].children[0]
        level_3_first_container = level_2_first_container.children[0]
        level_3_second_container = level_2_second_container.children[0]
        level_4_first_container = level_3_first_container.children[0]
        level_4_second_container = level_3_second_container.children[0]
        level_5_first_container = level_4_first_container.children[0]
        level_5_second_container = level_4_second_container.children[0]

        level_5_first_promo_qualitative_text = level_5_first_container.children[0]
        # This is an updated linked field
        level_5_first_promo_new_qualitative_text = level_5_first_container.children[1]
        level_5_second_promo_qualitative_text = level_5_second_container.children[0]
        # This is an updated linked field
        level_5_second_promo_new_qualitative_text = level_5_second_container.children[1]

        updated_fields = [
            fields[2],
            level_5_first_promo_new_qualitative_text,
            level_5_second_promo_new_qualitative_text,
        ]
        for updated_field in updated_fields:
            # make sure that any attribute we updated has changed to the correct value
            for i in range(len(attrs_to_change)):
                attr_to_change = getattr(updated_field, attrs_to_change[i])
                assert attr_to_change != getattr(
                    FieldPublic(**created_field.model_dump()), attrs_to_change[i]
                )
                assert attr_to_change == values[i]

            # make sure that no other attributes' values have changed
            for attr, value in updated_field.model_dump().items():
                # Ignore attributes which should always be different between a template
                # and its linked fields
                normal_different_attributes = [
                    "parent_id",
                    "field_type",
                    "template_id",
                    "id",
                ]
                if (
                    attr not in attrs_to_change
                    and attr not in normal_different_attributes
                ):
                    assert getattr(FieldPublic(**created_field.model_dump()), attr) == value

    @pytest.mark.parametrize(
        "attrs_to_change, values",
        (
            (["data"], ["new description"]),
            (["field_type"], ["info"]),
            (["name", "field_type"], ["A change the field name", "info"]),
        ),
    )
    # test_fields_with_children parameter is required to generate children
    async def test_update_template_restricted_properties_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        attrs_to_change: List[str],
        values: List[str],
        test_cpo_report: List[FieldInDB],
        new_template_field: FieldCreate,
    ) -> None:

        # First part is a copy of test_template_fields_generates_child_fields to
        # generate all the fields

        # Create the field
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_template_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldCreate(**field) for field in res.json()]
        created_field = created_fields[0]
        assert created_field == new_template_field

        # Change the field type to make comparisons later
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_field = created_fields[0]

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the post is in the response
        assert created_field in fields

        ## The following second part about update is custom

        # update the field
        field_update = {
            "field_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=created_field.id,
            ),
            json=field_update,
        )
        assert res.status_code == HTTP_400_BAD_REQUEST


class TestDeleteField:
    async def test_can_delete_field_successfully(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        test_field: FieldInDB,
        request,
    ) -> None:
        # delete the field
        res = await authorized_client_all_scopes.delete(
            app.url_path_for(
                "reports:delete-field-by-id",
                report_id=test_report.id,
                field_id=test_field.id,
            ),
        )
        assert res.status_code == HTTP_200_OK

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Field "{test_field.name}" with id {test_field.id} deletion')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # ensure that the field no longer exists
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_report.id,
                field_id=test_field.id,
            ),
        )
        assert res.status_code == HTTP_404_NOT_FOUND

    async def test_can_delete_container_promo_field_successfully_with_container_promo_permission(
        self,
        app: FastAPI,
        authorized_client_delete_container_promo_fields: AsyncClient,
        test_cpo_report: List["FieldInDB"],
        request,
    ) -> None:
        # delete the container_promo field
        res = await authorized_client_delete_container_promo_fields.delete(
            app.url_path_for(
                "reports:delete-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1_1_1_1_1"].id,
            ),
        )
        assert res.status_code == HTTP_200_OK

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Field "{test_cpo_report["1_1_1_1_1"].name}" with id {test_cpo_report["1_1_1_1_1"].id} deletion')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # ensure that the field no longer exists
        res = await authorized_client_delete_container_promo_fields.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1_1_1_1_1"].id,
            ),
        )
        assert res.status_code == HTTP_404_NOT_FOUND

    async def test_delete_container_fails_with_container_promo_permission(
        self,
        app: FastAPI,
        authorized_client_delete_container_promo_fields: AsyncClient,
        test_cpo_report: List["FieldInDB"]
    ) -> None:
        """
        Fields other than `container_promo` should not be deleted when only
        having the `container_promo:delete` permission.
        """
        # try to delete the field
        res = await authorized_client_delete_container_promo_fields.delete(
            app.url_path_for(
                "reports:delete-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1"].id,
            ),
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED

    async def test_delete_report_fails(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        test_field: FieldInDB,
    ) -> None:
        # delete the field
        res = await authorized_client_all_scopes.delete(
            app.url_path_for(
                "reports:delete-field-by-id",
                report_id=test_report.id,
                field_id=test_report.id,
            ),
        )
        assert res.status_code == HTTP_400_BAD_REQUEST

    async def test_delete_parent_field_succeeds(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report_2: ReportInDB,
        test_fields_with_children: List[FieldInDB],
        request,
    ) -> None:
        # delete the field
        res = await authorized_client_all_scopes.delete(
            app.url_path_for(
                "reports:delete-field-by-id",
                report_id=test_report_2.id,
                field_id=test_fields_with_children["1"].id,
            ),
        )
        assert res.status_code == HTTP_200_OK

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect("/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Field "{test_fields_with_children["1"].name}" with id {test_fields_with_children["1"].id} deletion')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # ensure that the field doesn't exist
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_report_2.id,
                field_id=test_fields_with_children["1"].id,
            ),
        )
        assert res.status_code == HTTP_404_NOT_FOUND

        # ensure that the child field doesn't exist
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_report_2.id,
                field_id=test_fields_with_children["1_2_1"].id,
            ),
        )
        assert res.status_code == HTTP_404_NOT_FOUND

    @pytest.mark.parametrize(
        "id, status_code",
        (
            (1000500, 404),
            (0, 404),
            (-1, 422),
            (None, 422),
        ),
    )
    async def test_delete_field_with_invalid_input_throws_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_field: FieldInDB,
        test_report: ReportInDB,
        id: int,
        status_code: int,
    ) -> None:
        res = await authorized_client_all_scopes.delete(
            app.url_path_for(
                "reports:delete-field-by-id", report_id=test_report.id, field_id=id
            ),
        )
        assert res.status_code == status_code

    async def test_delete_template_deletes_linked_fields_too(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_template_field: FieldCreate,
        test_cpo_report: List[FieldInDB],
        request
    ) -> None:

        # First part is a copy of test_template_fields_generates_child_fields to
        # generate all the fields

        # Create the field
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:add-field-to-report", report_id=test_cpo_report["0"].id
            ),
            json={"new_field": new_template_field.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        assert isinstance(res.json(), list)
        created_fields = [FieldCreate(**field) for field in res.json()]
        created_field = created_fields[0]
        assert created_field == new_template_field

        # Change the field type to make comparisons later
        created_fields = [FieldPublic(**field) for field in res.json()]
        created_field = created_fields[0]

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the child of the post is in the response
        assert created_field in fields

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)
        # We look into child number '1', as the '0' is the one we created without children
        assert len(list({v.model_dump()["id"]: v for v in fields[1].children})) == len(
            fields[1].children
        )

        ## The following second part about deletion is custom

        # delete the field
        res = await authorized_client_all_scopes.delete(
            app.url_path_for(
                "reports:delete-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=created_field.id,
            ),
        )
        assert res.status_code == HTTP_200_OK

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Field "{created_field.name}" with id {created_field.id} deletion')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Get again report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            )
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        # Assert the template field is really absent
        assert created_field not in fields

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)
        # We look into child number '0', as there are no more other children
        assert len(list({v.model_dump()["id"]: v for v in fields[0].children})) == len(
            fields[0].children
        )

        # Assert exact number of children with absent qualitative_text children
        # Level 1
        assert 2 == len(fields)
        assert fields[0].field_type == "container"
        assert fields[1].field_type == "container"
        # Level 2
        assert 1 == len(fields[0].children)
        assert 1 == len(fields[1].children)
        level_2_first_container = fields[0].children[0]
        level_2_second_container = fields[1].children[0]
        assert level_2_first_container.field_type == "container"
        assert level_2_second_container.field_type == "container"
        # Level 3
        assert 1 == len(level_2_first_container.children)
        assert 1 == len(level_2_second_container.children)
        level_3_first_container = level_2_first_container.children[0]
        level_3_second_container = level_2_second_container.children[0]
        assert level_3_first_container.field_type == "container"
        assert level_3_second_container.field_type == "container"
        # Level 4
        assert 3 == len(level_3_first_container.children)
        assert 2 == len(level_3_second_container.children)
        level_4_first_container = level_3_first_container.children[0]
        level_4_second_container = level_3_second_container.children[0]
        assert level_4_first_container.field_type == "container"
        assert level_4_second_container.field_type == "container"
        # Level 5
        assert 1 == len(level_4_first_container.children)
        assert 1 == len(level_4_second_container.children)
        level_5_first_container = level_4_first_container.children[0]
        level_5_second_container = level_4_second_container.children[0]
        assert level_5_first_container.field_type == "container_promo"
        assert level_5_second_container.field_type == "container_promo"
        # Level 6
        assert 1 == len(level_5_first_container.children)
        assert 1 == len(level_5_second_container.children)

class TestGetColumnsFromReport:
    async def test_get_columns_from_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get columns from report which consist of all field's names grouped by.
        It allows to the front-end to list the columns on which a report may be filtered on.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-columns-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        columns = [ColumnPublic(**column) for column in res.json()]

        # Assert there are all the columns
        assert 7 == len(columns)

        # Assert the first column is "Ambition" containing all the column values
        assert columns[0].field_name == test_cpo_report["1"].name
        assert columns[0].field_type == 'container'
        assert columns[0].column_values == [test_cpo_report["1"].data, test_cpo_report["2"].data]

        # Assert the second column is "Priorité" containing all the column values
        assert columns[1].field_name == test_cpo_report["1_1"].name
        assert columns[1].field_type == 'container'
        assert columns[1].column_values == [test_cpo_report["1_1"].data, test_cpo_report["2_1"].data]

        # Assert the fourth column is "AT" containing all the column values
        assert columns[3].field_name == test_cpo_report["1_1_1_1"].name
        assert columns[3].field_type == 'container'
        assert columns[3].column_values == [
            test_cpo_report["1_1_1_1"].data,
            test_cpo_report["1_1_1_2"].data,
            test_cpo_report["2_1_1_3"].data
        ]

        # Assert the for-last column is "Additional description" containing all the column values.
        assert columns[5].field_name == test_cpo_report["2_1_1_2_1_2"].name
        assert columns[5].field_type == 'qualitative_text'
        assert columns[5].column_values == [
            test_cpo_report["1_1_1_2_1_1"].data,
            ]

        # Assert the last column is "Additional description" containing all the column values.
        # Note that as some fields have the same data, the number of column values is
        # less than the number of fields.
        assert columns[6].field_name == test_cpo_report["1_1_1_1_1_1"].name
        assert columns[6].field_type == 'qualitative_text'
        assert columns[6].column_values == [
            test_cpo_report["1_1_1_2_1_1"].data,
            test_cpo_report["1_1_1_1_1_1"].data,
            ]

    async def test_get_columns_from_report_by_id_filtered(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get columns from report which consist of all field's names grouped by.
        It allows to the front-end to list the columns on which a report may be filtered on.
        Filters should be able to be applied on this route to correspond with the report's fields return.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-columns-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1"].name, # "Ambition 1"
                                "field_data": test_cpo_report["1"].data,
                            }
                        ]
                    }
                )
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        columns = [ColumnPublic(**column) for column in res.json()]

        # Assert there are all the columns
        assert 6 == len(columns)

        # Assert the first column is "Ambition" containing all the column values
        # It should not contain "Ambition 2"
        assert columns[0].field_name == test_cpo_report["1"].name
        assert columns[0].field_type == 'container'
        assert columns[0].column_values == [test_cpo_report["1"].data]

        # Assert the second column is "Priorité" containing all the column values
        # It should not contain "Priorité 2.1"
        assert columns[1].field_name == test_cpo_report["1_1"].name
        assert columns[1].field_type == 'container'
        assert columns[1].column_values == [test_cpo_report["1_1"].data]

        # Assert the last column is "Description" containing all the column values.
        # Note that as some fields have the same data, the number of column values is
        # less than the number of fields.
        assert columns[5].field_name == test_cpo_report["1_1_1_1_1_1"].name
        assert columns[5].field_type == 'qualitative_text'
        assert columns[5].column_values == [
            test_cpo_report["1_1_1_2_1_1"].data,
            test_cpo_report["1_1_1_1_1_1"].data,
            ]

    async def test_get_columns_from_report_by_id_filtered_and_searched(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Get columns from report which consist of all field's names grouped by.
        It allows to the front-end to list the columns on which a report may be filtered on.
        Filters and search string should be able to be applied on this route to correspond with the report's fields return.
        """
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-columns-from-report-by-id", report_id=test_cpo_report["0"].id
            ),
            params={
                "filters": json.dumps(
                    {
                        "filters": [
                            {
                                "field_name": test_cpo_report["1"].name, # "Ambition 1"
                                "field_data": test_cpo_report["1"].data,
                            }
                        ]
                    }
                ),
                "search": "another description",
            },
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        columns = [ColumnPublic(**column) for column in res.json()]

        # Assert there are all the columns
        assert 6 == len(columns)

        # Assert the first column is "Ambition" containing all the column values
        # It should not contain "Ambition 2"
        assert columns[0].field_name == test_cpo_report["1"].name
        assert columns[0].field_type == 'container'
        assert columns[0].column_values == [test_cpo_report["1"].data]

        # Assert the second column is "Priorité" containing all the column values
        # It should not contain "Priorité 2.1"
        assert columns[1].field_name == test_cpo_report["1_1"].name
        assert columns[1].field_type == 'container'
        assert columns[1].column_values == [test_cpo_report["1_1"].data]

        # Assert the last column is "Description" containing all the column values.
        # Note that as some fields have the same data, the number of column values is
        # less than the number of fields.
        assert columns[5].field_name == test_cpo_report["1_1_1_1_1_1"].name
        assert columns[5].field_type == 'qualitative_text'
        assert columns[5].column_values == [
            test_cpo_report["1_1_1_2_1_1"].data,
            ]

class TestImportFieldsIntoReport:
    async def test_import_csv_file_in_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_cpo_report_v2: FieldInDB,
        request,
    ) -> None:
        """
        You should be able to import fields by batch from a CSV file
        """

        # Send the CSV file
        data = {'source_id': 'manual_entry'}
        files = {'file': open('./tests/fixtures/fields_sample.csv', 'rb')}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data=data)
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_cpo_report_v2.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['meta'] == {'job_type': 'csv_file_import'}
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_cpo_report_v2.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        assert data['result']['total_processed'] == 10
        assert len([line for line in data['result']['log'] if line['status'] == 'imported']) == 9
        ignored_lines = [line for line in data['result']['log'] if line['status'] == 'ignored']
        assert len(ignored_lines) == 1

        # Check that the erroneous field has been ignored and not partially created
        erroneous_line = [line for line in ignored_lines if line['id'] == '361873'][0]
        assert "Invalid value 'malformed' for '11. 2022 – 2ⁿᵈ D réalisé'" == erroneous_line['explanation']

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report_v2.id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        fields = sorted(fields, key=lambda x: x.name)

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)

        # Assert exact number of children
        # Level 1
        assert 7 == len(fields)
        assert fields[0].field_type == "template_qualitative_text"
        assert fields[1].field_type == "template_quantitative_addition"
        assert fields[2].field_type == "template_quantitative_addition"
        assert fields[3].field_type == "template_qualitative_bool"
        assert fields[4].field_type == "template_qualitative_bool"
        assert fields[5].field_type == "container"
        # Level 2
        assert 1 == len(fields[5].children)
        level_2_container = fields[5].children[0]
        assert level_2_container.name == "Ambition"
        # Level 3
        assert 2 == len(level_2_container.children)
        level_3_container = level_2_container.children[0]
        assert level_3_container.name == "Priorité"
        assert level_3_container.data == "1.1 L'inclusion scolaire"
        # Level 4
        assert 3 == len(level_3_container.children)
        level_4_container = level_3_container.children[0]
        assert level_4_container.name == "Action"
        assert level_4_container.data == "Pôle ressources handicaps"
        # Level 5
        assert 2 == len(level_4_container.children)
        level_5_container = level_4_container.children[0]
        assert level_5_container.name == "AT"
        assert level_5_container.data == "05. Nouvelle Aquitaine"
        # Level 6
        assert 1 == len(level_5_container.children)
        level_6_container = level_5_container.children[0]
        assert level_6_container.name == "Académie"
        assert level_6_container.data == "Poitiers"
        # Level 7
        assert 1 == len(level_6_container.children)
        level_7_container = level_6_container.children[0]
        assert level_7_container.name == "événement"
        assert level_7_container.data == "Sensibilisation aux handicaps"
        assert level_7_container.field_type == "container_promo"
        assert level_7_container.external_source_id == "manual_entry"
        # Level 8
        assert 5 == len(level_7_container.children)
        level_8_container_1 = level_7_container.children[0]
        assert level_8_container_1.name == "00. Description"
        assert level_8_container_1.data == "Sensibilisation aux handicaps auprès d'élèves du CP au CM2"
        level_8_container_2 = level_7_container.children[1]
        assert level_8_container_2.name == "02. 2022 réalisé – nb bénéficiaires"
        assert level_8_container_2.data == "120"
        level_8_container_3 = level_7_container.children[2]
        assert level_8_container_3.name == "03. 2023 prévu – nb bénéficiaires"
        assert level_8_container_3.data == "180"
        level_8_container_4 = level_7_container.children[3]
        assert level_8_container_4.name == "11. 2022 – 2ⁿᵈ D réalisé"
        assert level_8_container_4.data == 'true'
        level_8_container_5 = level_7_container.children[4]
        assert level_8_container_5.name == "12. 2023 – 2ⁿᵈ D prévu"
        assert level_8_container_5.data == 'false'

        # Check annotated imported CSV file
        ## to avoid to check the variable datetime in the middle, we check only start and end
        assert data['result']['annotated_imported_csv_filename'].startswith(f'annotated_imported_csv_file_')
        assert data['result']['annotated_imported_csv_filename'].endswith(f'_fields_sample.csv')

        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-annotated-imported-csv-file",
                report_id=test_cpo_report_v2.id,
                annotated_imported_csv_file=data['result']['annotated_imported_csv_filename'],
            ),
        )
        parsed_response = csv.reader(io.StringIO(res.text), delimiter= ',')

        first_row = next(parsed_response)
        assert len(first_row) == 60
        assert first_row[0] == "[PR] Import status"
        assert first_row[1] == "[PR] Error code"
        assert first_row[2] == '[PR] Explanation'
        assert first_row[3] == 'id'
        assert first_row[4] == 'Année'
        assert first_row[5] == 'Ambition'
        assert first_row[6] == 'Priorité'
        assert first_row[7] == 'Action'
        assert first_row[8] == 'AT'
        assert first_row[9] == 'Académie'
        assert first_row[10] == 'événement'
        assert first_row[11] == '00. Description'
        assert first_row[12] == '01. Note interne'

    async def test_import_csv_file_in_report_idempotence(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        authorized_csv_client_all_scopes: AsyncClient,
        test_cpo_report_v2: FieldInDB,
        request,
    ) -> None:
        """
        You should not be able to import fields with the same id.
        Lines with unknown columns shoud be ignored.
        """

        # Send the CSV file with specifying source
        data = {'source_id': f"something"}
        files = {'file': open('./tests/fixtures/fields_mini_sample.csv', 'rb')}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data=data)
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_cpo_report_v2.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_cpo_report_v2.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check import summary is okay
        assert data['result']['total_processed'] == 3
        assert len([line for line in data['result']['log'] if line['status'] == 'imported']) == 3

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report_v2.id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        fields = sorted(fields, key=lambda x: x.name)

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)

        # Assert exact number of children
        # Level 7
        level_6_container_1 = (fields[5].children[0].children[0].children[0]
            .children[0].children[0])
        assert 1 == len(level_6_container_1.children)
        level_7_container = level_6_container_1.children[0]
        assert level_7_container.name == "événement"
        assert level_7_container.data == "Sensibilisation aux handicaps"
        assert level_7_container.field_type == "container_promo"
        assert level_7_container.external_source_id == "something"
        # Level 8
        assert 5 == len(level_7_container.children)
        level_8_container_1 = level_7_container.children[0]
        assert level_8_container_1.name == "00. Description"
        assert level_8_container_1.data == "Sensibilisation aux handicaps auprès d'élèves du CP au CM2"
        level_8_container_2 = level_7_container.children[1]
        assert level_8_container_2.name == "02. 2022 réalisé – nb bénéficiaires"
        assert level_8_container_2.data == "120"

        level_6_container_3 = (fields[5].children[0].children[0].children[2]
            .children[0].children[0])
        # Académie container should be empty here
        assert None == level_6_container_3.children

        ###
        ## SECOND PART OF THE TEST
        ###

        # Resend the full CSV file with specifying the source
        data = {'source_id': f"something"}
        files = {'file': ('fields_sample.csv', open('./tests/fixtures/fields_sample.csv', 'rb'))}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data=data)
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_cpo_report_v2.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_cpo_report_v2.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check import summary is okay
        assert data['result']['report_id'] == test_cpo_report_v2.id
        assert data['result']['filename'] == 'fields_sample.csv'
        assert data['result']['total_processed'] == 10
        assert len([line for line in data['result']['log'] if line['status'] == 'imported']) == 6
        assert len([line for line in data['result']['log'] if line['status'] == 'ignored']) == 1

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report_v2.id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        fields = sorted(fields, key=lambda x: x.name)

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)

        # Assert exact number of children
        # Level 7
        level_3_container_1 = (fields[5].children[0].children[0])
        assert 3 == len(level_3_container_1.children)
        level_6_container_1 = (fields[5].children[0].children[0].children[0]
            .children[0].children[0])
        # This container_promo should have been imported only once
        assert 1 == len(level_6_container_1.children)
        level_7_container = level_6_container_1.children[0]
        assert level_7_container.name == "événement"
        assert level_7_container.data == "Sensibilisation aux handicaps"
        assert level_7_container.field_type == "container_promo"
        assert level_7_container.external_source_id == f"something"
        # Level 8
        assert 5 == len(level_7_container.children)
        level_8_container_1 = level_7_container.children[0]
        assert level_8_container_1.name == "00. Description"
        assert level_8_container_1.data == "Sensibilisation aux handicaps auprès d'élèves du CP au CM2"
        level_8_container_2 = level_7_container.children[1]
        assert level_8_container_2.name == "02. 2022 réalisé – nb bénéficiaires"
        assert level_8_container_2.data == "120"

        # Level 8
        level_2_container = fields[5].children[0]
        level_3_container = level_2_container.children[0]
        level_4_container_2 = level_3_container.children[1]
        level_5_container_2 = level_4_container_2.children[0]
        level_6_container_2 = level_5_container_2.children[0]
        level_7_container_2 = level_6_container_2.children[0]
        assert level_7_container_2.children[0].name == "00. Description"
        assert level_7_container_2.children[0].data == "Cinéma \"Le Concorde\" autour de l'histoire de Tosquelles pour exemple"
        assert level_7_container_2.children[1].name == "02. 2022 réalisé – nb bénéficiaires"
        assert level_7_container_2.children[1].data == "60"

        # Académie container should now be imported
        level_6_container_3 = (fields[5].children[0].children[0].children[2]
            .children[0].children[0])
        assert 1 == len(level_6_container_3.children)

        ###
        ## THIRD PART OF THE TEST
        ###

        # Send a badly formed CSV file with specifying source
        data = {'source_id': f"something"}
        files = {'file': open('./tests/fixtures/fields_bad_sample.csv', 'rb')}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data=data)
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_cpo_report_v2.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_cpo_report_v2.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check import summary is okay
        assert data['result']['total_processed'] == 4
        assert len(data['result']['log']) == 3
        assert len([line for line in data['result']['log'] if line['status'] == 'imported']) == 1
        assert len([line for line in data['result']['log'] if line['status'] == 'ignored']) == 2
        assert data['result']['log'][0]['explanation'] == i18n.t(
            'translate.report.container_promo_column_not_found_error',
            column_name='bad_Priorité'
        )
        assert data['result']['log'][1]['explanation'] == i18n.t(
            'translate.report.container_promo_column_not_found_error',
            column_name='bad_Priorité'
        )

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report_v2.id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        fields = sorted(fields, key=lambda x: x.name)

        # There should still have only 2 Priorité containers
        assert len(fields[5].children[0].children) == 2

        # The only valid item of the fixture should have been imported
        # and added to the already imported container_promo
        level_2_container = fields[5].children[0]
        level_3_container = level_2_container.children[0]
        level_4_container = level_3_container.children[0]
        level_5_container = level_4_container.children[0]
        level_6_container = level_5_container.children[0]
        assert 2 == len(level_6_container.children)
        level_7_container = level_6_container.children[0]
        assert 5 == len(level_7_container.children)
        level_8_container_1 = level_7_container.children[0]
        assert level_8_container_1.name == "00. Description"
        assert level_8_container_1.data == "Encore sensibilisation aux handicaps auprès d'élèves du CP au CM2"
        level_8_container_2 = level_7_container.children[1]
        assert level_8_container_2.name == "02. 2022 réalisé – nb bénéficiaires"
        assert level_8_container_2.data == "120"

        ###
        ## FOURTH PART OF THE TEST:
        # Trying to import again the same fields from the report you exported
        # should not be imported
        ###

        # Export report in CSV
        res = await authorized_csv_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report_v2.id
            ),
            params={"export": "true"},
        )
        assert res.status_code == HTTP_200_OK

        # Resend the same CSV file without specifying source
        files = {'file': io.StringIO(res.text)}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data={})
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_cpo_report_v2.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_cpo_report_v2.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # All duplicate lines shouldn't be displayed in logs
        assert data['result']['total_processed'] == 10
        assert len([line for line in data['result']['log'] if line['status'] == 'ignored']) == 0

        ###
        ## FIFTH PART OF THE TEST: resend same exported CSV file with report_id as source
        ###

        # Export report in CSV
        res = await authorized_csv_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_cpo_report_v2.id
            ),
            params={"export": "true"},
        )
        assert res.status_code == HTTP_200_OK

        # Resend the same CSV file
        files = {'file': io.StringIO(res.text)}

        # Specify the own report id as source
        data = {'source_id': f"report_{test_cpo_report_v2.id}"}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data=data)
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_cpo_report_v2.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_cpo_report_v2.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # All duplicate lines shouldn't be displayed in logs
        assert data['result']['total_processed'] == 10
        assert len([line for line in data['result']['log'] if line['status'] == 'ignored']) == 0


    async def test_import_csv_file_in_asymmetrical_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_asymmetric_report: FieldInDB,
        request,
    ) -> None:
        """
        You should be able to import fields in a asymmetrical report with
        different column names.
        Lines with unknown columns shoud be ignored.
        """

        # Send the CSV file with specifying source
        data = {'source_id': f"something"}
        files = {'file': open('./tests/fixtures/fields_asymettrical_sample.csv', 'rb')}

        # python-multipart has a bug with the body parser.
        # We use requests to prepare the body as suggested here as a workaround: https://github.com/encode/starlette/issues/1059#issuecomment-1623624210
        body, content_type = requests.PreparedRequest()._encode_files(files=files, data=data)
        res = await authorized_client_all_scopes.post(
            app.url_path_for(
                "reports:import-csv-fields-into-report", report_id=test_asymmetric_report.id
            ),
            content=body, headers={"Content-Type": content_type}
        )
        assert res.status_code == HTTP_201_CREATED

        assert isinstance(res.json(), dict)

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Fields batch import from CSV file to report "{test_asymmetric_report.id}"')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # Check import summary is okay
        assert data['result']['total_processed'] == 9
        assert len([line for line in data['result']['log'] if line['status'] == 'imported']) == 3
        assert len([line for line in data['result']['log'] if line['status'] == 'ignored']) == 6

        # Get report's fields to check
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_asymmetric_report.id,
            ),
            params={"order_by": "name"},
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        fields = [FieldPublic(**field) for field in res.json()]
        fields = sorted(fields, key=lambda x: x.name)

        # Assert all children are unique
        assert len(list({v.model_dump()["id"]: v for v in fields}.keys())) == len(fields)

        level_1_container = fields[0].children
        assert 4 == len(level_1_container)
        level_2_container = level_1_container[2]
        assert level_2_container.name == "Nom"
        assert level_2_container.data == "Jean Dupont"
        assert level_2_container.children[0].name == "Diplôme"
        assert level_2_container.children[0].data == "DEES"

        # This CSV row should be imported in one column but not the following one
        level_2_container_2 = level_1_container[3]
        assert level_2_container_2.name == "Nom"
        assert level_2_container_2.data == "Julie Martin"
        assert level_2_container_2.children[0].name == "Diplôme"
        assert level_2_container_2.children[0].data == "BAFD"

        level_2_container = fields[1].children[1].children
        assert 1 == len(level_2_container)
        assert level_2_container[0].name == "Outils"
        assert level_2_container[0].data == "Portfolio"
