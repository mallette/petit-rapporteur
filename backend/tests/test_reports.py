from typing import List
import jwt
import pytest
from httpx import AsyncClient
from fastapi import FastAPI
from fastapi.testclient import TestClient
from databases import Database
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_401_UNAUTHORIZED,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.core.config import (
    SECRET_KEY,
    JWT_ALGORITHM,
    JWT_AUDIENCE,
    JWT_TOKEN_PREFIX,
    ACCESS_TOKEN_EXPIRE_MINUTES,
)
from app.models.report import ReportCreate, ReportPublic, ReportInDB
from app.models.field import FieldInDB
from app.models.policy import (
    AnonymousPoliciesGroupPublic,
    PoliciesGroupInDB,
    ReportShareCreate,
)
from app.db.repositories.policies import PoliciesRepository
from app.services import auth_service

# decorate all tests with @pytest.mark.asyncio
pytestmark = pytest.mark.asyncio


@pytest.fixture
def new_report():
    return ReportCreate(
        field_type="container",
        data="Name of the report",
    )


@pytest.fixture
def new_duplicated_report():
    return ReportCreate(
        field_type="container",
        data="Name of the duplicated report",
    )


@pytest.fixture
def new_report_template():
    return ReportCreate(
        name="ReportTemplate",
        field_type="container",
        data="Name of the template report",
    )


@pytest.fixture
async def authorized_client_partial_reports(
    client: AsyncClient,
    db: Database,
    test_report_2: ReportInDB,
    test_cpo_report: List["FieldInDB"],
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Partial reports",
        policies=[{"report_id": str(test_cpo_report["0"].id), "actions": ["report:read"]}],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


class TestReportsRoutes:
    @pytest.mark.asyncio
    async def test_routes_exist(
        self, app: FastAPI, authorized_client_all_scopes: AsyncClient
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"), json={}
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_invalid_input_raises_error(
        self, app: FastAPI, authorized_client_all_scopes: AsyncClient
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"), json={}
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY


class TestGetAllReports:
    async def test_get_all_reports(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportCreate,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for("reports:get-reports")
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        reports = [ReportPublic(**report) for report in res.json()]
        assert ReportPublic(**test_report.model_dump()) in reports

    async def test_get_some_reports(
        self,
        app: FastAPI,
        authorized_client_partial_reports: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_partial_reports.get(
            app.url_path_for("reports:get-reports")
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) == 1

        reports = [ReportPublic(**report) for report in res.json()]
        assert ReportPublic(**test_cpo_report["0"].model_dump()) in reports

    async def test_get_all_report_templates(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportCreate,
        test_report_template: ReportCreate,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for("reports:get-reports"), params={"name": "ReportTemplate"}
        )
        assert res.status_code == HTTP_200_OK
        assert isinstance(res.json(), list)
        assert len(res.json()) > 0

        reports = [ReportPublic(**report) for report in res.json()]
        assert ReportPublic(**test_report_template.model_dump()) in reports
        assert ReportPublic(**test_report.model_dump()) not in reports


class TestCreateReport:
    async def test_valid_input_creates_report(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_report: ReportCreate,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"),
            json={"new_report": new_report.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        created_report = ReportCreate(**res.json())
        assert created_report == new_report

    async def test_valid_input_creates_report_template(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_report_template: ReportCreate,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"),
            json={"new_report": new_report_template.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        created_report = ReportCreate(**res.json())
        assert created_report == new_report_template

    async def test_duplicate_report_data_creation_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        new_duplicated_report: ReportCreate,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"),
            json={"new_report": new_duplicated_report.model_dump()},
        )
        assert res.status_code == HTTP_201_CREATED
        created_report = ReportCreate(**res.json())
        assert created_report == new_duplicated_report

        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"),
            json={"new_report": new_duplicated_report.model_dump()},
        )
        assert res.status_code == HTTP_400_BAD_REQUEST

    @pytest.mark.parametrize(
        "invalid_payload, status_code",
        (
            (None, 422),
            ({}, 422),
            ({"name": "test_name"}, 422),
            ({"field_type": "container"}, 422),
            ({"name": "test_name", "description": "test"}, 422),
            ({"name": "", "data": ""}, 422),
            ({"name": "Report", "field_type": "container", "data": ""}, 422),
        ),
    )
    async def test_invalid_input_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        invalid_payload: dict,
        status_code: int,
    ) -> None:
        res = await authorized_client_all_scopes.post(
            app.url_path_for("reports:create-report"),
            json={"new_report": invalid_payload},
        )
        assert res.status_code == status_code


class TestGetReport:
    async def test_get_report_by_id(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
    ) -> None:
        res = await authorized_client_all_scopes.get(
            app.url_path_for("reports:get-report-by-id", report_id=test_report.id)
        )
        assert res.status_code == HTTP_200_OK
        report = ReportPublic(**res.json())
        assert report == ReportPublic(**test_report.model_dump())


class TestEditReport:
    @pytest.mark.parametrize(
        "attrs_to_change, values",
        ((["data"], ["new description"]),),
    )
    async def test_update_report_with_valid_input(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        attrs_to_change: List[str],
        values: List[str],
    ) -> None:
        report_update = {
            "report_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-report-by-id",
                report_id=test_report.id,
            ),
            json=report_update,
        )
        assert res.status_code == HTTP_200_OK

        updated_report = ReportPublic(**res.json())
        assert updated_report.id == test_report.id  # make sure it's the same report

        # make sure that any attribute we updated has changed to the correct value
        for i in range(len(attrs_to_change)):
            attr_to_change = getattr(updated_report, attrs_to_change[i])
            assert attr_to_change != getattr(test_report, attrs_to_change[i])
            assert attr_to_change == values[i]

        # make sure that no other attributes' values have changed
        for attr, value in updated_report.model_dump().items():
            if attr not in attrs_to_change:
                assert getattr(test_report, attr) == value

    @pytest.mark.parametrize(
        "attrs_to_change, values, status_code",
        (
            (["field_type"], ["info"], 400),
            (["name"], ["Change the field name"], 400),
            (["id"], [0], 400),
            (["name", "field_type"], ["Change the field name", "info"], 400),
        ),
    )
    async def test_update_report_with_invalid_input_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        attrs_to_change: List[str],
        values: List[str],
        status_code: int,
    ) -> None:
        report_update = {
            "report_update": {
                attrs_to_change[i]: values[i] for i in range(len(attrs_to_change))
            }
        }
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-report-by-id",
                report_id=test_report.id,
            ),
            json=report_update,
        )
        assert res.status_code == status_code

    async def test_update_report_with_duplicate_data_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        test_report_template: ReportInDB,
    ) -> None:
        report_update = {"report_update": {"data": test_report_template.data}}
        res = await authorized_client_all_scopes.put(
            app.url_path_for(
                "reports:update-report-by-id",
                report_id=test_report.id,
            ),
            json=report_update,
        )
        assert res.status_code == HTTP_400_BAD_REQUEST


class TestDeleteReport:
    async def test_can_delete_report_successfully(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_report: ReportInDB,
        test_field: FieldInDB,
        request,
    ) -> None:
        # delete the field
        res = await authorized_client_all_scopes.delete(
            app.url_path_for(
                "reports:delete-report-by-id",
                report_id=test_report.id,
            ),
        )
        assert res.status_code == HTTP_200_OK

        # Connect to the Websocket to get job status
        client = TestClient(app)
        with client.websocket_connect(f"/v1/ws/jobs?client_id=test_id_{request.node.name}") as websocket:
            websocket.send_json(
                {'action': 'subscribe', 'job_id': res.json()['id']},
                mode="text"
            )
            data = websocket.receive_json()
            assert data['description'] == (f'Report "{test_report.id}" deletion')
            assert data['status'] in ['started', 'queued', 'finished']
            # Wait to receive a notification that the job has finished before proceeding
            while data['status'] != 'finished':
                data = websocket.receive_json()

        # ensure that the report no longer exists
        res = await authorized_client_all_scopes.get(
            app.url_path_for(
                "reports:get-report-by-id",
                report_id=test_report.id,
            ),
        )
        assert res.status_code == HTTP_404_NOT_FOUND
