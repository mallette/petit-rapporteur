from typing import List
import pytest
from httpx import AsyncClient
from databases import Database
from fastapi import FastAPI
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.db.repositories.utils import UtilsRepository
from app.models.report import ReportInDB
from app.models.field import FieldPublic, FieldCreate, FieldInDB

# decorate all tests with @pytest.mark.asyncio
pytestmark = pytest.mark.asyncio


class TestUtilsRepository:
    async def test_get_leaf_fields_from_root(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        test_report_2: ReportInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        utils_repo = UtilsRepository(db)
        fields = await utils_repo.get_leaf_fields(field_id=test_report_2.id)
        assert 4 == len(fields)
        # Assert the root node is not in the response
        assert FieldInDB(**test_fields_with_children["1"].model_dump()) not in fields

        # Assert children are in the response
        assert FieldInDB(**test_fields_with_children["1_1"].model_dump()) in fields
        assert FieldInDB(**test_fields_with_children["1_2_1_1"].model_dump()) in fields
        assert FieldInDB(**test_fields_with_children["1_2_2"].model_dump()) in fields
        assert FieldInDB(**test_fields_with_children["1_2_3"].model_dump()) in fields

        # Assert fields with children are not in the response
        assert FieldInDB(**test_fields_with_children["1_2_1"].model_dump()) not in fields

    async def test_get_leaf_fields_from_average_field(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        test_report_2: ReportInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        utils_repo = UtilsRepository(db)
        fields = await utils_repo.get_leaf_fields(
            field_id=test_fields_with_children["1"].id
        )
        assert 4 == len(fields)
        # Assert the root node is not in the response
        assert FieldInDB(**test_fields_with_children["1"].model_dump()) not in fields

        # Assert children are in the response
        assert FieldInDB(**test_fields_with_children["1_1"].model_dump()) in fields
        assert FieldInDB(**test_fields_with_children["1_2_1_1"].model_dump()) in fields
        assert FieldInDB(**test_fields_with_children["1_2_2"].model_dump()) in fields
        assert FieldInDB(**test_fields_with_children["1_2_3"].model_dump()) in fields

        # Assert fields with children are not in the response
        assert FieldInDB(**test_fields_with_children["1_2_1"].model_dump()) not in fields

    async def test_get_leaf_fields_from_last_field_should_return_empty(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        test_report_2: ReportInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        utils_repo = UtilsRepository(db)
        fields = await utils_repo.get_leaf_fields(
            field_id=test_fields_with_children["1_1"].id
        )
        assert 0 == len(fields)

    async def test_get_leaf_container_fields_from_average_field(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        test_report_2: ReportInDB,
        test_fields_with_children: List[FieldInDB],
    ) -> None:
        utils_repo = UtilsRepository(db)
        fields = await utils_repo.get_leaf_fields(
            field_id=test_fields_with_children["1"].id,
            filters={"field_type": "container"},
        )
        assert 1 == len(fields)
        # Assert the root node is not in the response
        assert FieldInDB(**test_fields_with_children["1"].model_dump()) not in fields

        # Assert children are in the response
        assert FieldInDB(**test_fields_with_children["1_1"].model_dump()) in fields

        # Assert containers with children are not in the response
        assert FieldInDB(**test_fields_with_children["1_2_1"].model_dump()) not in fields
        assert FieldInDB(**test_fields_with_children["1_2_2"].model_dump()) not in fields
