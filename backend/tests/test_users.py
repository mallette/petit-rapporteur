from typing import List, Union, Type, Optional
from databases import Database

import pytest
import jwt
from httpx import AsyncClient
from fastapi import FastAPI, HTTPException, status
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_401_UNAUTHORIZED,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from pydantic import ValidationError
from starlette.datastructures import Secret

from app.core.config import (
    SECRET_KEY,
    JWT_ALGORITHM,
    JWT_AUDIENCE,
    JWT_TOKEN_PREFIX,
    ACCESS_TOKEN_EXPIRE_MINUTES,
)
from app.db.repositories.policies import PoliciesRepository
from app.models.token import JWTMeta, JWTCreds, JWTPayload

from app.models.user import UserBase, UserPublic
from app.services import auth_service

pytestmark = pytest.mark.asyncio


class TestAuthTokens:
    async def test_can_create_access_token_successfully(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        db: Database,
        test_root_user: UserBase,
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        access_token = auth_service.create_access_token_for_user(
            user=test_root_user,
            policies_group=policies_group,
            secret_key=str(SECRET_KEY),
            audience=JWT_AUDIENCE,
            expires_in=ACCESS_TOKEN_EXPIRE_MINUTES,
        )
        creds = jwt.decode(
            access_token,
            str(SECRET_KEY),
            audience=JWT_AUDIENCE,
            algorithms=[JWT_ALGORITHM],
        )
        assert creds.get("username") is not None
        assert creds["username"] == test_root_user.username
        assert creds["aud"] == JWT_AUDIENCE

    async def test_token_missing_user_is_invalid(
        self, app: FastAPI, authorized_client_all_scopes: AsyncClient, db: Database
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        access_token = auth_service.create_access_token_for_user(
            user=None,
            policies_group=policies_group,
            secret_key=str(SECRET_KEY),
            audience=JWT_AUDIENCE,
            expires_in=ACCESS_TOKEN_EXPIRE_MINUTES,
        )
        with pytest.raises(jwt.PyJWTError):
            jwt.decode(
                access_token,
                str(SECRET_KEY),
                audience=JWT_AUDIENCE,
                algorithms=[JWT_ALGORITHM],
            )

    @pytest.mark.parametrize(
        "secret_key, jwt_audience, exception",
        (
            ("wrong-secret", JWT_AUDIENCE, jwt.InvalidSignatureError),
            (None, JWT_AUDIENCE, jwt.InvalidSignatureError),
            (SECRET_KEY, "othersite:auth", jwt.InvalidAudienceError),
            (SECRET_KEY, None, ValidationError),
        ),
    )
    async def test_invalid_token_content_raises_error(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        db: Database,
        test_root_user: UserBase,
        secret_key: Union[str, Secret],
        jwt_audience: str,
        exception: Type[BaseException],
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        with pytest.raises(exception):
            access_token = auth_service.create_access_token_for_user(
                user=test_root_user,
                policies_group=policies_group,
                secret_key=str(secret_key),
                audience=jwt_audience,
                expires_in=ACCESS_TOKEN_EXPIRE_MINUTES,
            )
            jwt.decode(
                access_token,
                str(SECRET_KEY),
                audience=JWT_AUDIENCE,
                algorithms=[JWT_ALGORITHM],
            )

    async def test_can_retrieve_username_from_token(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        db: Database,
        test_root_user: UserBase,
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        token = auth_service.create_access_token_for_user(
            user=test_root_user,
            policies_group=policies_group,
            secret_key=str(SECRET_KEY),
        )
        username = auth_service.get_username_from_token(
            token=token, secret_key=str(SECRET_KEY)
        )
        assert username == test_root_user.username

    @pytest.mark.parametrize(
        "secret, wrong_token",
        (
            (SECRET_KEY, "asdf"),  # use wrong token
            (SECRET_KEY, ""),  # use wrong token
            (SECRET_KEY, None),  # use wrong token
            ("ABC123", "use correct token"),  # use wrong secret
        ),
    )
    async def test_error_when_token_or_secret_is_wrong(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        db: Database,
        test_root_user: UserBase,
        secret: Union[Secret, str],
        wrong_token: Optional[str],
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        token = auth_service.create_access_token_for_user(
            user=test_root_user,
            policies_group=policies_group,
            secret_key=str(SECRET_KEY),
        )
        if wrong_token == "use correct token":
            wrong_token = token
        with pytest.raises(HTTPException):
            username = auth_service.get_username_from_token(
                token=wrong_token, secret_key=str(secret)
            )


class TestUserLogin:
    async def test_root_user_can_login_successfully_and_receives_valid_token(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_root_user: UserBase,
    ) -> None:
        authorized_client_all_scopes.headers[
            "content-type"
        ] = "application/x-www-form-urlencoded"
        login_data = {
            "username": test_root_user.username,
            "password": "thegoodpassword",  # insert user's plaintext password
        }
        res = await authorized_client_all_scopes.post(
            app.url_path_for("users:login-password"), data=login_data
        )
        assert res.status_code == HTTP_200_OK
        # check that token exists in response and has user encoded within it
        token = res.json().get("access_token")
        creds = jwt.decode(
            token, str(SECRET_KEY), audience=JWT_AUDIENCE, algorithms=[JWT_ALGORITHM]
        )
        assert "username" in creds
        assert creds["username"] == test_root_user.username
        # check that token is proper type
        assert "token_type" in res.json()
        assert res.json().get("token_type") == "bearer"

    @pytest.mark.parametrize(
        "credential, wrong_value, status_code",
        (
            ("password", "wrongpassword", 401),
            ("password", None, 422),
        ),
    )
    async def test_root_user_with_wrong_creds_doesnt_receive_token(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        test_root_user: UserBase,
        credential: str,
        wrong_value: str,
        status_code: int,
    ) -> None:
        authorized_client_all_scopes.headers[
            "content-type"
        ] = "application/x-www-form-urlencoded"
        user_data = test_root_user.model_dump()
        user_data[credential] = wrong_value
        login_data = {
            "username": user_data["username"],
            "password": user_data["password"],
        }
        res = await authorized_client_all_scopes.post(
            app.url_path_for("users:login-password"), data=login_data
        )
        assert res.status_code == status_code
        assert "access_token" not in res.json()


class TestUserMe:
    async def test_authenticated_user_can_retrieve_own_data(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        test_root_user: UserBase,
    ) -> None:
        res = await authorized_client.get(app.url_path_for("users:get-current-user"))
        assert res.status_code == HTTP_200_OK
        user = UserPublic(**res.json())
        assert user.username == test_root_user.username

    async def test_root_user_cannot_access_own_data_if_not_authenticated(
        self,
        app: FastAPI,
        client: AsyncClient,
        test_root_user: UserBase,
    ) -> None:
        res = await client.get(app.url_path_for("users:get-current-user"))
        assert res.status_code == HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize(
        "jwt_prefix",
        (
            ("",),
            ("value",),
            ("Token",),
            ("JWT",),
            ("Swearer",),
        ),
    )
    async def test_root_user_cannot_access_own_data_with_incorrect_jwt_prefix(
        self,
        app: FastAPI,
        authorized_client_all_scopes: AsyncClient,
        db: Database,
        test_root_user: UserBase,
        jwt_prefix: str,
    ) -> None:
        policies_repo = PoliciesRepository(db)
        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=1
        )
        token = auth_service.create_access_token_for_user(
            user=test_root_user,
            policies_group=policies_group,
            secret_key=str(SECRET_KEY),
        )
        res = await authorized_client_all_scopes.get(
            app.url_path_for("users:get-current-user"),
            headers={"Authorization": f"{jwt_prefix} {token}"},
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED
