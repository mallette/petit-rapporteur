from typing import Dict, List
import warnings
import os
import pytest
from asgi_lifespan import LifespanManager
from fastapi import FastAPI
from httpx import AsyncClient
from databases import Database
import alembic
from alembic.config import Config

from app.core.config import SECRET_KEY, JWT_TOKEN_PREFIX
from app.models.report import ReportCreate
from app.db.repositories.reports import ReportsRepository
from app.models.field import FieldCreate, FieldPublic
from app.db.repositories.fields import FieldsRepository
from app.db.repositories.policies import PoliciesRepository
from app.models.user import UserBase
from app.models.policy import PoliciesGroupInDB, ReportShareCreate
from app.services import auth_service

# Apply migrations at beginning and end of testing session
@pytest.fixture(scope="session")
def apply_migrations():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    os.environ["TESTING"] = "1"
    config = Config("alembic.ini")
    alembic.command.upgrade(config, "head")
    yield
    alembic.command.downgrade(config, "base")


# Create a new application for testing
@pytest.fixture
def app(apply_migrations: None) -> FastAPI:
    from app.api.server import get_application

    return get_application()


# Grab a reference to our database when needed
@pytest.fixture
def db(app: FastAPI) -> Database:
    return app.state._db


# Make requests in our tests
@pytest.fixture
async def client(app: FastAPI) -> AsyncClient:
    async with LifespanManager(app):
        async with AsyncClient(
            app=app,
            base_url="http://testserver",
            headers={"Content-Type": "application/json"},
        ) as client:
            yield client


@pytest.fixture
async def authorized_client(
    client: AsyncClient, db: Database, test_root_user: UserBase
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    policies_group = await policies_repo.get_policies_group_by_id(policies_group_id=1)
    access_token = auth_service.create_access_token_for_user(
        user=test_root_user, policies_group=policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def all_scopes_policies_group(client: AsyncClient, db: Database) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="All actions",
        policies=[
            {
                "actions": [
                    "report:list",
                    "report:read",
                    "report:write",
                    "report:import",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ]
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    return created_policies_group


@pytest.fixture
async def authorized_client_all_scopes(
    client: AsyncClient,
    db: Database,
    test_root_user: UserBase,
    test_cpo_report: List["FieldInDB"],
    all_scopes_policies_group: PoliciesGroupInDB,
) -> AsyncClient:
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=all_scopes_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def authorized_csv_client_all_scopes(
    client: AsyncClient,
    db: Database,
    test_root_user: UserBase,
    test_cpo_report: List["FieldInDB"],
    all_scopes_policies_group: PoliciesGroupInDB,
) -> AsyncClient:
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=all_scopes_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "accept": "text/csv",
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def authorized_client_partial_scopes(
    client: AsyncClient,
    db: Database,
    test_root_user: UserBase,
    test_cpo_report: List["FieldInDB"],
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Partial scopes",
        policies=[{"actions": ["report:share", "report:edit", "report:delete"]}],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def test_report(db: Database) -> ReportCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        data="fake report name",
        field_type="container",
    )
    created_report = await report_repo.create_report(new_report=new_report)

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"

    return created_report


@pytest.fixture
async def test_report_template(db: Database) -> ReportCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        name="ReportTemplate",
        data="fake report name",
        field_type="container",
    )
    created_report = await report_repo.create_report(new_report=new_report)

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"
    return created_report


@pytest.fixture
async def test_report_template_field(db: Database, test_report_template) -> FieldCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    field_repo = FieldsRepository(db)
    new_field = FieldCreate(
        name="AT",
        parent_id=test_report_template.id,
        data="01",
        field_type="container",
    )
    created_field = await field_repo.create_field(
        id=test_report_template.id, new_field=new_field
    )

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"
    return created_field


@pytest.fixture
async def test_field(db: Database, test_report) -> FieldCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    field_repo = FieldsRepository(db)
    new_field = FieldCreate(
        name="Some field name",
        parent_id=test_report.id,
        data="Some field description",
        field_type="container",
    )
    created_field = await field_repo.create_field(
        id=test_report.id, new_field=new_field
    )

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"

    return created_field


@pytest.fixture
async def test_field_info(db: Database, test_report) -> FieldCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    field_repo = FieldsRepository(db)
    new_field = FieldCreate(
        name="Some info field name",
        parent_id=test_report.id,
        data="Some info field description",
        field_type="info",
    )

    created_field = await field_repo.create_field(
        id=test_report.id, new_field=new_field
    )

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"

    return created_field


@pytest.fixture
async def test_report_2(db: Database) -> ReportCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        data="other fake report name",
        field_type="container",
    )

    created_report = await report_repo.create_report(new_report=new_report)

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"

    return created_report


@pytest.fixture
async def test_fields_with_children(
    db: Database, test_report_2
) -> Dict[str, FieldPublic]:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    field_repo = FieldsRepository(db)
    field_1 = FieldCreate(
        name="Some other field name",
        parent_id=test_report_2.id,
        data="Some other field description",
        field_type="container",
    )
    field_1 = await field_repo.create_field(id=test_report_2.id, new_field=field_1)

    field_2 = FieldCreate(
        name="Sub-field",
        parent_id=field_1.id,
        data="Some sub-field 1 description",
        field_type="container",
    )
    field_2 = await field_repo.create_field(id=test_report_2.id, new_field=field_2)

    field_3 = FieldCreate(
        name="Sub-field",
        parent_id=field_1.id,
        data="Another sub-field 2 description",
        field_type="container",
    )
    field_3 = await field_repo.create_field(id=test_report_2.id, new_field=field_3)

    field_4 = FieldCreate(
        name="Some other sub-field 2-1 name",
        parent_id=field_3.id,
        data="Some other sub-field 2-1 description",
        field_type="container",
    )
    field_4 = await field_repo.create_field(id=test_report_2.id, new_field=field_4)

    field_5 = FieldCreate(
        name="Some info sub-field 2-2 name",
        parent_id=field_3.id,
        data="Some info sub-field 2-2 description",
        field_type="info",
    )
    field_5 = await field_repo.create_field(id=test_report_2.id, new_field=field_5)

    field_6 = FieldCreate(
        name="Some description template",
        parent_id=field_3.id,
        data="Some description template data",
        field_type="template_qualitative_text",
    )
    field_6 = await field_repo.create_field(id=test_report_2.id, new_field=field_6)

    field_7 = FieldCreate(
        name="Some description template",
        parent_id=field_4.id,
        data="Some description template data",
        field_type="qualitative_text",
        template_id=field_6.id,
    )
    field_7 = await field_repo.create_field(id=test_report_2.id, new_field=field_7)

    fields = {}

    fields["1"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_1.id
    )
    fields["1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_2.id
    )
    fields["1_2"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_3.id
    )
    fields["1_2_1"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_4.id
    )
    fields["1_2_2"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_5.id
    )
    fields["1_2_3"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_6.id
    )
    fields["1_2_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=test_report_2.id, field_id=field_7.id
    )

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"

    return fields


@pytest.fixture
async def test_report_3(db: Database) -> ReportCreate:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        data="Another fake report name",
        field_type="container",
    )
    created_report = await report_repo.create_report(new_report=new_report)

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"

    return created_report


@pytest.fixture
async def test_cpo_report(db: Database) -> Dict[str, FieldPublic]:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        data="My CPO Report",
        field_type="container",
    )
    report = await report_repo.create_report(new_report=new_report)

    # Create the field number 2 before field number 1 only to ensure tests about
    # the fields list ordered by name work as expected.
    field_repo = FieldsRepository(db)
    field_2 = FieldCreate(
        name="Ambition",
        parent_id=report.id,
        data="2. Second ambition",
        field_type="container",
    )
    field_2 = await field_repo.create_field(id=report.id, new_field=field_2)

    field_1 = FieldCreate(
        name="Ambition",
        parent_id=report.id,
        data="1. First ambition",
        field_type="container",
    )
    field_1 = await field_repo.create_field(id=report.id, new_field=field_1)

    field_3 = FieldCreate(
        name="Priorité",
        parent_id=field_1.id,
        data="1.1 First priority",
        field_type="container",
    )
    field_3 = await field_repo.create_field(id=report.id, new_field=field_3)

    field_4 = FieldCreate(
        name="Priorité",
        parent_id=field_2.id,
        data="2.1 First priority",
        field_type="container",
    )
    field_4 = await field_repo.create_field(id=report.id, new_field=field_4)

    field_5 = FieldCreate(
        name="Action",
        parent_id=field_3.id,
        data="One action",
        field_type="container",
    )
    field_5 = await field_repo.create_field(id=report.id, new_field=field_5)

    field_6 = FieldCreate(
        name="Action",
        parent_id=field_4.id,
        data="Another action",
        field_type="container",
    )
    field_6 = await field_repo.create_field(id=report.id, new_field=field_6)

    field_7 = FieldCreate(
        name="AT",
        parent_id=field_5.id,
        data="01",
        field_type="container",
    )
    field_7 = await field_repo.create_field(id=report.id, new_field=field_7)

    field_8 = FieldCreate(
        name="AT",
        parent_id=field_5.id,
        data="02",
        field_type="container",
    )
    field_8 = await field_repo.create_field(id=report.id, new_field=field_8)

    field_9 = FieldCreate(
        name="Promo",
        parent_id=field_7.id,
        data="I did something 1",
        field_type="container_promo",
    )
    field_9 = await field_repo.create_field(id=report.id, new_field=field_9)

    field_10 = FieldCreate(
        name="Promo",
        parent_id=field_8.id,
        data="I also did something 1",
        field_type="container_promo",
    )
    field_10 = await field_repo.create_field(id=report.id, new_field=field_10)

    field_11 = FieldCreate(
        name="Description",
        parent_id=field_9.id,
        data="This is the description of my promo",
        field_type="qualitative_text",
    )
    field_11 = await field_repo.create_field(id=report.id, new_field=field_11)

    field_12 = FieldCreate(
        name="Description",
        parent_id=field_10.id,
        data="This is another description of my other promo",
        field_type="qualitative_text",
    )
    field_12 = await field_repo.create_field(id=report.id, new_field=field_12)

    field_13 = FieldCreate(
        name="AT",
        parent_id=field_6.id,
        data="01",
        field_type="container",
    )
    field_13 = await field_repo.create_field(id=report.id, new_field=field_13)

    field_14 = FieldCreate(
        name="AT",
        parent_id=field_6.id,
        data="02",
        field_type="container",
    )
    field_14 = await field_repo.create_field(id=report.id, new_field=field_14)

    field_15 = FieldCreate(
        name="Promo",
        parent_id=field_13.id,
        data="I did something here 2",
        field_type="container_promo",
    )
    field_15 = await field_repo.create_field(id=report.id, new_field=field_15)

    field_16 = FieldCreate(
        name="Promo",
        parent_id=field_14.id,
        data="I also did something 2",
        field_type="container_promo",
    )
    field_16 = await field_repo.create_field(id=report.id, new_field=field_16)

    field_17 = FieldCreate(
        name="Description",
        parent_id=field_15.id,
        data="This is the description of my promo",
        field_type="qualitative_text",
    )
    field_17 = await field_repo.create_field(id=report.id, new_field=field_17)

    field_18 = FieldCreate(
        name="Description",
        parent_id=field_16.id,
        data="This is another description of my other promo",
        field_type="qualitative_text",
    )
    field_18 = await field_repo.create_field(id=report.id, new_field=field_18)

    field_19 = FieldCreate(
        name="Additional description",
        parent_id=field_16.id,
        data="This is another description of my other promo",
        field_type="qualitative_text",
    )
    field_19 = await field_repo.create_field(id=report.id, new_field=field_19)

    field_20 = FieldCreate(
        name="AT",
        parent_id=field_6.id,
        data="My empty container",
        field_type="container",
    )
    field_20 = await field_repo.create_field(id=report.id, new_field=field_20)

    fields = {}

    fields["0"] = await report_repo.get_report_by_id(id=report.id)
    fields["1"] = await field_repo.get_field_with_children_by_id(report_id=report.id, field_id=field_1.id)
    fields["2"] = await field_repo.get_field_with_children_by_id(report_id=report.id, field_id=field_2.id)
    fields["1_1"] = await field_repo.get_field_with_children_by_id(report_id=report.id, field_id=field_3.id)
    fields["2_1"] = await field_repo.get_field_with_children_by_id(report_id=report.id, field_id=field_4.id)
    fields["1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_5.id
    )
    fields["2_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_6.id
    )
    fields["1_1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_7.id
    )
    fields["1_1_1_2"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_8.id
    )
    fields["1_1_1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_9.id
    )
    fields["1_1_1_2_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_10.id
    )
    fields["1_1_1_1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_11.id
    )
    fields["1_1_1_2_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_12.id
    )
    fields["2_1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_13.id
    )
    fields["2_1_1_2"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_14.id
    )
    fields["2_1_1_3"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_20.id
    )
    fields["2_1_1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_15.id
    )
    fields["2_1_1_2_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_16.id
    )
    fields["2_1_1_1_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_17.id
    )
    fields["2_1_1_2_1_1"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_18.id
    )
    fields["2_1_1_2_1_2"] = await field_repo.get_field_with_children_by_id(
        report_id=report.id, field_id=field_19.id
    )

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"
    return fields


@pytest.fixture
async def test_cpo_report_v2(db: Database) -> Dict[str, FieldPublic]:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        data="My CPO Report v2",
        field_type="container",
    )
    report = await report_repo.create_report(new_report=new_report)

    field_repo = FieldsRepository(db)

    field_0 = FieldCreate(
        name="00. Description",
        parent_id=report.id,
        data="",
        field_type="template_qualitative_text",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)
    field_0 = FieldCreate(
        name="02. 2022 réalisé – nb bénéficiaires",
        parent_id=report.id,
        data="",
        field_type="template_quantitative_addition",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)
    field_0 = FieldCreate(
        name="03. 2023 prévu – nb bénéficiaires",
        parent_id=report.id,
        data="",
        field_type="template_quantitative_addition",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)
    field_0 = FieldCreate(
        name="11. 2022 – 2ⁿᵈ D réalisé",
        parent_id=report.id,
        data="",
        field_type="template_qualitative_bool",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)
    field_0 = FieldCreate(
        name="12. 2023 – 2ⁿᵈ D prévu",
        parent_id=report.id,
        data="",
        field_type="template_qualitative_bool",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)

    field_0 = FieldCreate(
        name="événement",
        parent_id=report.id,
        data="",
        field_type="template_container_promo",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)

    field_1 = FieldCreate(
        name="Année",
        parent_id=report.id,
        data="2022",
        field_type="container",
    )
    field_1 = await field_repo.create_field(id=report.id, new_field=field_1)

    field_2 = FieldCreate(
        name="Ambition",
        parent_id=field_1.id,
        data="1. La réussite scolaire de tous",
        field_type="container",
    )
    field_2 = await field_repo.create_field(id=report.id, new_field=field_2)

    field_3 = FieldCreate(
        name="Priorité",
        parent_id=field_2.id,
        data="1.1 L'inclusion scolaire",
        field_type="container",
    )
    field_3 = await field_repo.create_field(id=report.id, new_field=field_3)

    field_4 = FieldCreate(
        name="Priorité",
        parent_id=field_2.id,
        data="1.2 Orientation et Lutte décrochage",
        field_type="container",
    )
    field_4 = await field_repo.create_field(id=report.id, new_field=field_4)

    field_5 = FieldCreate(
        name="Action",
        parent_id=field_3.id,
        data="Z. Autres",
        field_type="container",
    )
    field_5 = await field_repo.create_field(id=report.id, new_field=field_5)

    field_6 = FieldCreate(
        name="Action",
        parent_id=field_3.id,
        data="Rencontres Vidéo-Santé mentale",
        field_type="container",
    )
    field_6 = await field_repo.create_field(id=report.id, new_field=field_6)

    field_7 = FieldCreate(
        name="Action",
        parent_id=field_3.id,
        data="Pôle ressources handicaps",
        field_type="container",
    )
    field_7 = await field_repo.create_field(id=report.id, new_field=field_7)

    field_8 = FieldCreate(
        name="Action",
        parent_id=field_4.id,
        data="Mener des actions innovantes pour favoriser le raccrochage scolaire",
        field_type="container",
    )
    field_8 = await field_repo.create_field(id=report.id, new_field=field_8)

    field_9 = FieldCreate(
        name="AT",
        parent_id=field_5.id,
        data="42. Martinique",
        field_type="container",
    )
    field_9 = await field_repo.create_field(id=report.id, new_field=field_9)

    field_10 = FieldCreate(
        name="AT",
        parent_id=field_6.id,
        data="16. Pays de la Loire",
        field_type="container",
    )
    field_10 = await field_repo.create_field(id=report.id, new_field=field_10)

    field_11 = FieldCreate(
        name="AT",
        parent_id=field_7.id,
        data="05. Nouvelle Aquitaine",
        field_type="container",
    )
    field_11 = await field_repo.create_field(id=report.id, new_field=field_11)

    field_12 = FieldCreate(
        name="AT",
        parent_id=field_7.id,
        data="42. Martinique",
        field_type="container",
    )
    field_12 = await field_repo.create_field(id=report.id, new_field=field_12)

    field_13 = FieldCreate(
        name="AT",
        parent_id=field_8.id,
        data="02. Picardie",
        field_type="container",
    )
    field_13 = await field_repo.create_field(id=report.id, new_field=field_13)

    field_14 = FieldCreate(
        name="AT",
        parent_id=field_8.id,
        data="11. Nord pas de Calais",
        field_type="container",
    )
    field_14 = await field_repo.create_field(id=report.id, new_field=field_14)

    field_15 = FieldCreate(
        name="AT",
        parent_id=field_8.id,
        data="40. Guadeloupe",
        field_type="container",
    )
    field_15 = await field_repo.create_field(id=report.id, new_field=field_15)

    field_16 = FieldCreate(
        name="AT",
        parent_id=field_8.id,
        data="42. Martinique",
        field_type="container",
    )
    field_16 = await field_repo.create_field(id=report.id, new_field=field_16)

    field_17 = FieldCreate(
        name="AT",
        parent_id=field_8.id,
        data="43. La Réunion",
        field_type="container",
    )
    field_17 = await field_repo.create_field(id=report.id, new_field=field_17)

    field_18 = FieldCreate(
        name="Académie",
        parent_id=field_9.id,
        data="Martinique",
        field_type="container",
    )
    field_18 = await field_repo.create_field(id=report.id, new_field=field_18)

    field_19 = FieldCreate(
        name="Académie",
        parent_id=field_10.id,
        data="Nantes",
        field_type="container",
    )
    field_19 = await field_repo.create_field(id=report.id, new_field=field_19)

    field_20 = FieldCreate(
        name="Académie",
        parent_id=field_11.id,
        data="Poitiers",
        field_type="container",
    )
    field_20 = await field_repo.create_field(id=report.id, new_field=field_20)

    field_21 = FieldCreate(
        name="Académie",
        parent_id=field_16.id,
        data="Martinique",
        field_type="container",
    )
    field_21 = await field_repo.create_field(id=report.id, new_field=field_21)

    field_22 = FieldCreate(
        name="Académie",
        parent_id=field_13.id,
        data="Amiens",
        field_type="container",
    )
    field_22 = await field_repo.create_field(id=report.id, new_field=field_22)

    field_23 = FieldCreate(
        name="Académie",
        parent_id=field_14.id,
        data="Lille",
        field_type="container",
    )
    field_23 = await field_repo.create_field(id=report.id, new_field=field_23)

    field_24 = FieldCreate(
        name="Académie",
        parent_id=field_15.id,
        data="Guadeloupe",
        field_type="container",
    )
    field_24 = await field_repo.create_field(id=report.id, new_field=field_24)

    field_25 = FieldCreate(
        name="Académie",
        parent_id=field_12.id,
        data="Martinique",
        field_type="container",
    )
    field_25 = await field_repo.create_field(id=report.id, new_field=field_25)

    field_26 = FieldCreate(
        name="Académie",
        parent_id=field_17.id,
        data="La Réunion",
        field_type="container",
    )
    field_26 = await field_repo.create_field(id=report.id, new_field=field_26)

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"
    return await report_repo.get_report_by_id(id=report.id)

@pytest.fixture
async def test_asymmetric_report(db: Database) -> Dict[str, FieldPublic]:
    # Enable Fixture mode to disable side effects at creation.
    # IMPORTANT! Needs to be disabled at end of fixture
    os.environ["FIXTURE_MODE"] = "True"

    report_repo = ReportsRepository(db)
    new_report = ReportCreate(
        data="Asymmetric report",
        field_type="container",
    )
    report = await report_repo.create_report(new_report=new_report)

    field_repo = FieldsRepository(db)

    field_0 = FieldCreate(
        name="Formateurs·rices",
        parent_id=report.id,
        data="BAFA-BAFD",
        field_type="container",
    )
    field_0 = await field_repo.create_field(id=report.id, new_field=field_0)

    field_1 = FieldCreate(
        name="Nom",
        parent_id=field_0.id,
        data="",
        field_type="template_container_promo",
    )
    field_1 = await field_repo.create_field(id=report.id, new_field=field_1)

    field_2 = FieldCreate(
        name="Diplôme",
        parent_id=field_0.id,
        data="",
        field_type="template_qualitative_text",
    )
    field_2 = await field_repo.create_field(id=report.id, new_field=field_2)

    field_3 = FieldCreate(
        name="Outils pédagogiques",
        parent_id=report.id,
        data="2023",
        field_type="container",
    )
    field_3 = await field_repo.create_field(id=report.id, new_field=field_3)

    field_4 = FieldCreate(
        name="Outils",
        parent_id=field_3.id,
        data="",
        field_type="template_container_promo",
    )
    field_4 = await field_repo.create_field(id=report.id, new_field=field_4)

    field_5 = FieldCreate(
        name="Stagiaires BAFA",
        parent_id=field_3.id,
        data="2023",
        field_type="container",
    )
    field_5 = await field_repo.create_field(id=report.id, new_field=field_5)

    # Disable Fixture mode to reenable side effects at creation.
    os.environ["FIXTURE_MODE"] = "False"
    return await report_repo.get_report_by_id(id=report.id)

@pytest.fixture
async def test_root_user(db: Database) -> UserBase:
    # Register password in envvar
    password_update = auth_service.create_salt_and_hashed_password(
        plaintext_password="thegoodpassword"
    )

    os.environ["ROOT_PASSWORD_HASH"] = password_update.model_dump()["password"]
    os.environ["ROOT_PASSWORD_SALT"] = password_update.model_dump()["salt"]

    return UserBase(username="root")
