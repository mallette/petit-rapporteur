from typing import List
import pytest
from httpx import AsyncClient
from databases import Database
from fastapi import FastAPI
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_401_UNAUTHORIZED,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.core.config import SECRET_KEY, JWT_TOKEN_PREFIX
from app.db.repositories.policies import PoliciesRepository
from app.models.report import ReportInDB
from app.models.field import FieldPublic, FieldCreate, FieldInDB, FieldPublicNoChildren
from app.models.policy import PoliciesGroupPublic, ReportShareCreate
from app.services import auth_service

# decorate all tests with @pytest.mark.asyncio
pytestmark = pytest.mark.asyncio


@pytest.fixture
async def authorized_client_limited_report(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Limited Report",
        policies=[
            {
                "report_id": "1",
                "actions": [
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def authorized_client_limited_field(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Limited field",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "field_id": str(test_cpo_report["1"].id),
                "actions": [
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def authorized_client_filtered_fields(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Filtered fields",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "actions": [
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ],
                "filters": [{"field_name": "AT", "field_data": "01"}],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def authorized_client_double_filtered_fields(
    client: AsyncClient, db: Database, test_cpo_report: List["FieldInDB"]
) -> AsyncClient:
    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Double filtered fields",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "actions": [
                    "report:read",
                    "report:write",
                    "report:create",
                    "report:share",
                    "report:edit",
                    "report:delete",
                ],
                "filters": [
                    {
                        "field_name": test_cpo_report["1_1_1_1_1"].name,
                        "field_data": test_cpo_report["1_1_1_1_1"].data,
                    },
                    {
                        "field_name": test_cpo_report["2_1_1_2_1"].name,
                        "field_data": test_cpo_report["2_1_1_2_1"].data,
                    },
                ],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )
    access_token = auth_service.create_access_token_for_report_share(
        policies_group=created_policies_group, secret_key=str(SECRET_KEY)
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest.fixture
async def test_policies_group(db: Database, test_cpo_report) -> PoliciesGroupPublic:

    policies_repo = PoliciesRepository(db)
    new_policy_group = ReportShareCreate(
        comment="Some policies group",
        policies=[
            {
                "report_id": str(test_cpo_report["0"].id),
                "actions": ["report:read", "report:write"],
                "filters": [
                    {
                        "field_name": test_cpo_report["1_1_1_1"].name,
                        "field_data": "L'inclusion scolaire",
                    }
                ],
            }
        ],
    )
    created_policies_group = await policies_repo.create_policies_group(
        upsert=True, policies_group_params=new_policy_group
    )

    return created_policies_group


class TestPoliciesRepository:
    async def test_get_policies_group_by_id(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        test_policies_group: PoliciesGroupPublic,
    ) -> None:
        policies_repo = PoliciesRepository(db)

        policies_group = await policies_repo.get_policies_group_by_id(
            policies_group_id=test_policies_group.id
        )

        assert policies_group.id == test_policies_group.id
        assert len(policies_group.policies) == 1


class TestPoliciesAuthorization:
    async def test_get_fields_without_required_scope_fails(
        self,
        app: FastAPI,
        authorized_client_partial_scopes: AsyncClient,
        test_report: ReportInDB,
    ) -> None:
        res = await authorized_client_partial_scopes.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id", report_id=test_report.id
            )
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED

    async def test_get_fields_outside_authorized_report_id_fails(
        self,
        app: FastAPI,
        authorized_client_limited_report: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_limited_report.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            )
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED

    async def test_get_fields_with_limited_access_fails(
        self,
        app: FastAPI,
        authorized_client_limited_field: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        As the limited access only allows for a specified field_id, getting the whole report is forbidden.
        """
        res = await authorized_client_limited_field.get(
            app.url_path_for(
                "reports:get-fields-from-report-by-id",
                report_id=test_cpo_report["0"].id,
            )
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED


    async def test_get_subfield_with_filtered_access_succeeds(
        self,
        app: FastAPI,
        authorized_client_filtered_fields: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Access subfields of fields on which filters has been set
        """
        res = await authorized_client_filtered_fields.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["2_1_1_1_1_1"].id,
            )
        )
        assert res.status_code == HTTP_200_OK

    async def test_get_subfield_with_double_filtered_access_succeeds(
        self,
        app: FastAPI,
        authorized_client_double_filtered_fields: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Access subfields of fields on which filters has been set
        """
        res = await authorized_client_double_filtered_fields.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1_1_1_1_1_1"].id,
            )
        )
        assert res.status_code == HTTP_200_OK

        res = await authorized_client_double_filtered_fields.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["2_1_1_2_1_1"].id,
            )
        )
        assert res.status_code == HTTP_200_OK

    async def test_get_neighbour_field_with_filtered_access_fails(
        self,
        app: FastAPI,
        authorized_client_filtered_fields: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        """
        Deny access on fields which have been filtered out.
        """
        res = await authorized_client_filtered_fields.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["2_1_1_2_1_1"].id,
            )
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED

    async def test_get_field_by_id_outside_authorized_field_id_fail(
        self,
        app: FastAPI,
        authorized_client_limited_field: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_limited_field.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["2"].id,
            )
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED

    async def test_get_field_by_id_with_specific_authorized_field_id_succeeds(
        self,
        app: FastAPI,
        authorized_client_limited_field: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_limited_field.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1"].id,
            )
        )
        assert res.status_code == HTTP_200_OK

    async def test_get_field_by_id_subfield_from_field_id_fails(
        self,
        app: FastAPI,
        authorized_client_limited_field: AsyncClient,
        test_cpo_report: List[FieldInDB],
    ) -> None:
        res = await authorized_client_limited_field.get(
            app.url_path_for(
                "reports:get-field-by-id",
                report_id=test_cpo_report["0"].id,
                field_id=test_cpo_report["1_1"].id,
            )
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED
