FROM python:3.10-slim-buster
WORKDIR /backend
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1

EXPOSE 8000

# install system dependencies
RUN apt-get update \
  && apt-get -y install netcat gcc postgresql curl \
  && apt-get clean

ARG DOCKER_UID
ARG DOCKER_GID

RUN groupadd -g ${DOCKER_GID} app_user && useradd -u ${DOCKER_UID} -s /bin/bash -g app_user -m app_user

ADD --chown=app_user:app_user . /backend/
RUN mkdir -p /backend/exports \
 && chown -R app_user:app_user /backend/exports

# install python dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install -r requirements.dev.txt

COPY user-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["user-entrypoint.sh"]
