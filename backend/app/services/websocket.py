import logging

from fastapi import (
    APIRouter,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    Query,
    status,
)


logger = logging.getLogger(__name__)


class ConnectionManager:

    subscriptions = {}

    def __init__(self):
        self.active_connections: dict[str: WebSocket] = {}

    async def connect(self, client_id: str, websocket: WebSocket):
        await websocket.accept()
        logger.info(f'Websocket with client {client_id} connected')
        self.active_connections[client_id] = websocket

    def disconnect(self, client_id: str):
        self.active_connections.pop(client_id, None)
        logger.info(f'Websocket with client {client_id} disconnected')

    async def send_message(self, client_id: str, message: str):
        await self.active_connections[client_id].send_text(message)


ws_manager = ConnectionManager()
