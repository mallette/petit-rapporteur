import os
import glob
import logging
import i18n
from app.core import config, tasks

logger = logging.getLogger(__name__)
logger.setLevel(config.APP_LOGGING_LEVEL)

c_handler = logging.StreamHandler()
c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
c_handler.setFormatter(c_format)
logger.addHandler(c_handler)

## I18N
# Setup languages configuration
lang_folder_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'lang')
i18n.load_path.append(lang_folder_path)

# Parse available languages in folder
language_list = glob.glob("{}/*.json".format(lang_folder_path))

def convert_file_paths_to_lang(file_path):
    filename  = os.path.basename(file_path)
    lang, ext = os.path.splitext(filename)
    # Split to remove namespace from file name
    return lang.split('.')[1]

global AVAILABLE_LANGUAGES
AVAILABLE_LANGUAGES = list(map(convert_file_paths_to_lang, language_list))

i18n.set('fallback', config.APP_DEFAULT_LANG)
i18n.set('file_format', 'json')
