import json
from typing import Optional, List
from pydantic import field_validator
from enum import Enum
from app.models.core import IDModelMixin, CoreModel


class ActionType(str, Enum):
    report_read = "report:read"
    report_list = "report:list"
    report_write = "report:write"
    report_import = "report:import"
    report_edit = "report:edit"
    report_share = "report:share"
    report_create = "report:create"
    report_delete = "report:delete"
    container_promo_delete = "container_promo:delete"


class Filter(CoreModel):
    field_name: Optional[str] = None
    field_data: Optional[str] = None


class PolicyBase(CoreModel):
    """
    All common characteristics of our Policy resource
    """

    report_id: str = "*"
    field_id: str = "*"
    actions: List[ActionType]
    filters: Optional[List[Filter]] = None

    @field_validator("filters", mode="before")
    @classmethod
    def parse_filters_json_to_dict(cls, v):
        if v is None:
            return v
        elif v is not None and len(v) == 0:
            return None
        else:
            parsed_v = []
            for item in v:
                if isinstance(item, str):
                    parsed_v.append(json.loads(item))
                else:
                    parsed_v.append(item)
            return parsed_v


class PolicyCreate(PolicyBase):
    pass


class PolicyInDB(IDModelMixin, PolicyBase):
    pass


class PolicyPublic(PolicyInDB):
    pass


class AnonymousPolicyPublic(PolicyBase):
    pass


class PoliciesGroupBase(CoreModel):
    """
    All common characteristics of our PolicyGroup resource
    """

    comment: str


class PoliciesGroupInDB(IDModelMixin, PoliciesGroupBase):
    pass


class PoliciesGroupPublic(PoliciesGroupInDB):
    policies: List[PolicyPublic]


class AnonymousPoliciesGroupPublic(PoliciesGroupBase):
    """
    Class for test purposes to make comparisons without id.
    """

    policies: List[AnonymousPolicyPublic]


class ReportShareCreate(CoreModel):
    """
    Class for creating a new report share link
    """

    comment: str
    policies: List[PolicyCreate]
