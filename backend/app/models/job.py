from typing import Optional
from pydantic import Field
from uuid import UUID

from app.models.core import CoreModel


class JobDetails(CoreModel):
    """
    Job details when run by a task runner
    """

    id: UUID = Field(..., validation_alias='_id')
    status: str = Field(..., validation_alias='_status')
    description: str
    meta: Optional[dict] = None
    result: Optional[dict] = None
