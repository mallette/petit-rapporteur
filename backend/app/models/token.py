from typing import List, Optional
from datetime import datetime, timedelta
from app.core.config import JWT_AUDIENCE, ACCESS_TOKEN_EXPIRE_MINUTES
from app.models.core import CoreModel
from app.models.policy import PoliciesGroupPublic


class JWTMeta(CoreModel):
    iss: str = "petit-rapporteur"
    aud: str = JWT_AUDIENCE
    iat: float = datetime.timestamp(datetime.utcnow())
    exp: Optional[float] = None


class JWTCreds(CoreModel):
    """How we'll identify users"""

    username: Optional[str] = None
    policies_group: Optional[PoliciesGroupPublic] = None


class JWTPayload(JWTMeta, JWTCreds):
    """
    JWT Payload right before it's encoded - combine meta and username
    """

    pass


class AccessToken(CoreModel):
    access_token: str
    token_type: str


class PoliciesGroupPublicWithAccessToken(CoreModel):
    policies_group: PoliciesGroupPublic
    access_token: AccessToken
