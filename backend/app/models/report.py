from typing import Optional, List
from enum import Enum

from pydantic import StringConstraints

from app.models.core import IDModelMixin, CoreModel
from app.models.field import FieldInDB
from typing_extensions import Annotated


class Name(str, Enum):
    report = "Report"
    report_template = "ReportTemplate"


class FieldType(str, Enum):
    container = "container"


class ReportBase(CoreModel):
    """
    All common characteristics of our Report resource
    """

    name: Name = "Report"
    parent_id: None = None
    field_type: FieldType = "container"
    data: Annotated[str, StringConstraints(min_length=1)]


class ReportCreate(ReportBase):
    name: Name = "Report"


class ReportUpdate(CoreModel):
    data: Optional[str] = None


class ReportInDB(IDModelMixin, ReportBase):
    name: Name
    data_id: int
    children: Optional[List["FieldInDB"]] = None


class ReportPublic(IDModelMixin, ReportBase):
    pass
