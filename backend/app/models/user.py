from typing import Optional
from pydantic import StringConstraints

from app.models.core import IDModelMixin, CoreModel
from app.models.token import AccessToken
from typing_extensions import Annotated


class UserBase(CoreModel):
    """
    Leaving off password and salt from base model
    """

    username: str


class UserPublic(UserBase):
    access_token: Optional[AccessToken] = None


class UserPasswordUpdate(CoreModel):
    """
    Users can change their password
    """

    password: Annotated[str, StringConstraints(min_length=7, max_length=100)]
    salt: str
