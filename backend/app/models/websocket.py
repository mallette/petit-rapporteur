from pydantic import Field
from uuid import UUID

from app.models.core import CoreModel
from app.models.job import JobDetails


class JobSubscriptionMessage(CoreModel):
    """
    Message to subscribe to a job status
    """

    job_id: UUID
    action: str = 'subscribe'


class JobStatusMessage(CoreModel):
    """
    Message containing a job status
    """

    job: JobDetails
    action: str = 'status'
