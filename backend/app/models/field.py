import i18n
from typing import Optional, List
from pydantic import field_validator, validator, computed_field
from enum import Enum
from app.models.core import IDModelMixin, CoreModel


class FieldType(str, Enum):
    container = "container"
    container_promo = "container_promo"
    template_container_promo = "template_container_promo"
    info = "info"
    qualitative_text = "qualitative_text"
    template_qualitative_text = "template_qualitative_text"
    quantitative_addition = "quantitative_addition"
    template_quantitative_addition = "template_quantitative_addition"
    quantitative_average = "quantitative_average"
    template_quantitative_average = "template_quantitative_average"
    qualitative_bool = "qualitative_bool"
    template_qualitative_bool = "template_qualitative_bool"


class FieldBase(CoreModel):
    """
    All common characteristics of our Field resource
    """

    name: str
    parent_id: int
    field_type: FieldType
    data: str
    template_id: Optional[int] = None
    external_source_id: Optional[str] = None
    external_source_field_id: Optional[str] = None

    @field_validator("name", "data")
    @classmethod
    def name_must_not_contain_asterisk(cls, v):
        if v == "*":
            raise ValueError(i18n.t('translate.validators.asterisk_character_forbidden_value'))
        return v

    @field_validator("data")
    def data_must_match_field_type(cls, v, info):
        if 'field_type' in info.data and info.data['field_type']:
            if info.data['field_type'].find('quantitative') != -1:
                if v != "":
                    try:
                        float(v)
                    except ValueError:
                        raise ValueError(i18n.t('translate.validators.quantitative_field_numeric_values_error_message'))
            elif info.data['field_type'].find('qualitative_bool') != -1:
                if v != "" and v != "False" and v != "True" and v != "false" and v != "true":
                    raise ValueError(i18n.t('translate.validators.boolean_value_validation_error_message'))
        return v

class FieldCreate(FieldBase):
    template_id: Optional[int] = None


class FieldCopyType(str, Enum):
    content_only = "content_only"
    whole_container = "whole_container"


class FieldCopy(CoreModel):
    source_id: int
    recursive: bool = True
    loop: bool = False
    field_name: Optional[str] = None
    run_side_effects: bool = False
    copy_type: FieldCopyType = 'content_only'

    @computed_field
    @property
    def identifier_type(self) -> str:
        if self.copy_type == 'content_only':
            return 'parent_id'
        elif self.copy_type == 'whole_container':
            return 'id'


class FieldUpdate(FieldBase):
    name: Optional[str] = None
    parent_id: Optional[int] = None
    field_type: Optional[FieldType] = None
    data: Optional[str] = None


class FieldTemplateUpdate(FieldBase):
    name: Optional[str] = None
    parent_id: Optional[int] = None
    field_type: None
    data: None
    template_id: None


class FieldInDB(IDModelMixin, FieldBase):
    data_id: int
    children: Optional[List["FieldInDB"]] = None


class FieldPublic(IDModelMixin, FieldBase):
    children: Optional[List["FieldPublic"]] = None


class FieldPublicJSON(CoreModel):
    exported_fields: List["FieldPublic"]
    exported_report_id: int


class FieldPublicNoChildren(IDModelMixin, FieldBase):
    pass


class ColumnPublic(CoreModel):
    """
    Field representing a column in a report and containing the values
    of fields with the same name
    """

    field_name: str
    field_type: FieldType
    column_values: List[str]


# Only used for field comparisons in tests
class FieldAnonymized(CoreModel):
    name: str
    field_type: FieldType
    data: str
    children: Optional[List["FieldAnonymized"]] = None


# Only used for field comparisons in tests
class FieldAnonymizedNoChildren(CoreModel):
    name: str
    field_type: FieldType
    data: str
