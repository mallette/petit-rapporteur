import os
from fastapi import FastAPI
from databases import Database
from app.core.config import DATABASE_URL
import logging
import i18n

from app.db.repositories.fields import FieldsRepository
from app.db.repositories.reports import ReportsRepository


logger = logging.getLogger(__name__)


async def connect_to_db(app: FastAPI) -> None:
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(
        DB_URL, min_size=2, max_size=10
    )  # these can be configured in config as well

    try:
        await database.connect()
        app.state._db = database
    except Exception as e:
        logger.warn("--- DB CONNECTION ERROR ---")
        logger.warn(e)
        logger.warn("--- DB CONNECTION ERROR ---")


async def close_db_connection(app: FastAPI) -> None:
    try:
        await app.state._db.disconnect()
    except Exception as e:
        logger.warn("--- DB DISCONNECT ERROR ---")
        logger.warn(e)
        logger.warn("--- DB DISCONNECT ERROR ---")


# Wrapper to be used by a task queue runner
async def run_copy_fields_task(report_id, field_id, field_copy):
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(
        DB_URL, min_size=2, max_size=10
    )  # these can be configured in config as well

    try:
        await database.connect()
    except Exception as e:
        logger.warn("--- DB CONNECTION ERROR ---")
        logger.warn(e)
        logger.warn("--- DB CONNECTION ERROR ---")

    fields_repo = FieldsRepository(database)
    await fields_repo.copy_field(
        id=report_id, target_field_id=field_id, field_copy=field_copy
    )

async def run_add_fields_task(report_id, exported_report_id, exported_fields):
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(
        DB_URL, min_size=2, max_size=10
    )  # these can be configured in config as well

    try:
        await database.connect()
    except Exception as e:
        logger.warn("--- DB CONNECTION ERROR ---")
        logger.warn(e)
        logger.warn("--- DB CONNECTION ERROR ---")

    fields_repo = FieldsRepository(database)
    return await fields_repo.create_fields_by_batch(
        report_id=report_id,
        exported_report_id=exported_report_id,
        exported_fields=exported_fields,
    )

async def run_delete_field_task(report_id, field_id):
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(
        DB_URL, min_size=2, max_size=10
    )  # these can be configured in config as well

    try:
        await database.connect()
    except Exception as e:
        logger.warn("--- DB CONNECTION ERROR ---")
        logger.warn(e)
        logger.warn("--- DB CONNECTION ERROR ---")

    fields_repo = FieldsRepository(database)
    await fields_repo.delete_field_by_id(id=report_id, field_id=field_id)

async def run_delete_report_task(report_id):
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(
        DB_URL, min_size=2, max_size=10
    )  # these can be configured in config as well

    try:
        await database.connect()
    except Exception as e:
        logger.warn("--- DB CONNECTION ERROR ---")
        logger.warn(e)
        logger.warn("--- DB CONNECTION ERROR ---")

    reports_repo = ReportsRepository(database)
    await reports_repo.delete_report_by_id(
        id=report_id,
    )

async def run_bulk_import_task(
    lang_code,
    report_id,
    source_id,
    local_filename,
    filename,
    annotated_imported_csv_filename
):
    DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(
        DB_URL, min_size=2, max_size=10
    )  # these can be configured in config as well

    try:
        await database.connect()
    except Exception as e:
        logger.warn("--- DB CONNECTION ERROR ---")
        logger.warn(e)
        logger.warn("--- DB CONNECTION ERROR ---")

    # Set locale for inside the worker
    i18n.set('locale', lang_code)

    fields_repo = FieldsRepository(database)
    return await fields_repo.bulk_import_fields(
        report_id=report_id,
        source_id=source_id,
        local_filename=local_filename,
        filename=filename,
        annotated_imported_csv_filename=annotated_imported_csv_filename
    )
