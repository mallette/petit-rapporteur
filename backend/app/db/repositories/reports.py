import os
import i18n
import logging
from string import Template

from fastapi import HTTPException
from asyncpg import ForeignKeyViolationError
from starlette.status import HTTP_400_BAD_REQUEST

from app.db.repositories.base import BaseRepository
from app.models.report import ReportCreate, ReportInDB, ReportUpdate
from app.models.policy import PoliciesGroupInDB

logger = logging.getLogger(__name__)

DEFER_FOREIGN_KEY_CONSTRAINTS = """
    SET CONSTRAINTS "data_versions_field_id_fkey" DEFERRED;
"""

CREATE_DATA_VERSION_WITHOUT_FIELD_REFERENCE_QUERY = """
    INSERT INTO data_versions (value, author)
    VALUES (:data, 'Unknown')
    RETURNING id data_id;
"""

CREATE_FIELD_QUERY = """
    INSERT INTO fields (name, parent_id, data_id, field_type)
    VALUES (:name, :parent_id, :data_id, :field_type)
    RETURNING id, name, parent_id, field_type, data_id;
"""

CREATE_REPORT_VIEW_QUERY = """
    CREATE OR REPLACE RECURSIVE VIEW view_report_$report_id(id, name, parent_id, data_id, field_type, template_id, generation_number) AS (
        SELECT id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            0 AS generation_number
        FROM fields
        WHERE id = $report_id

    UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            generation_number+1 AS generation_number
        FROM fields child
        JOIN view_report_$report_id v
          ON v.id = child.parent_id
    );
"""

DELETE_REPORT_VIEW_QUERY = """
    DROP VIEW view_report_$report_id CASCADE;
"""

UPDATE_DATA_VERSION_WITH_FIELD_REFERENCE_QUERY = """
    UPDATE data_versions SET field_id = :field_id WHERE id = :data_id;
"""

GET_DUPLICATE_REPORTS_QUERY = """
    SELECT fields.id, fields.name, fields.parent_id, fields.field_type,
           fields.data_id, data_versions.value AS data
    FROM fields
    INNER JOIN data_versions ON fields.data_id = data_versions.id
    WHERE fields.parent_id IS NULL AND data_versions.value = :data
"""

GET_REPORTS_QUERY = """
    SELECT fields.id, fields.name, fields.parent_id, fields.field_type,
           fields.data_id, data_versions.value AS data
    FROM fields
    INNER JOIN data_versions ON fields.data_id = data_versions.id
    WHERE fields.parent_id IS NULL
    $filters
    $limits
    AND fields.field_type = 'container';
"""

GET_REPORT_BY_ID_QUERY = """
    SELECT fields.id, fields.name, fields.parent_id, fields.field_type,
           fields.data_id, data_versions.value AS data
    FROM fields
    INNER JOIN data_versions ON fields.data_id = data_versions.id
    WHERE fields.id = :id AND fields.parent_id IS NULL;
"""

UPDATE_REPORT_BY_ID_QUERY = """
    UPDATE data_versions
    SET value = :data
    FROM fields
    WHERE fields.id = :id AND fields.data_id = data_versions.id
    RETURNING fields.id, fields.name, fields.parent_id, fields.field_type,
        data_versions.value AS data, fields.data_id;
"""

DELETE_REPORT_BY_ID_QUERY = """
    DELETE FROM fields
    WHERE id = :id
    RETURNING id;
"""


class ReportsRepository(BaseRepository):
    """ "
    All database actions associated with the Report resource
    """

    async def create_report_view(self, report_id: int):
        """
        Create a SQL View for the report. It helps making requests easier to read.
        """

        query = Template(CREATE_REPORT_VIEW_QUERY)
        query = query.substitute(report_id=report_id)
        await self.db.execute(query=query)

    async def recreate_all_report_views(self):
        """
        MAINTENANCE TASK.
        Create or recreate a report view for all reports.
        """

        logger.info('MAINTENANCE: Recreating all report\'s views')

        query = Template(GET_REPORTS_QUERY)
        query = query.substitute(filters="", limits="")

        reports = await self.db.fetch_all(query=query)

        for report in reports:
            await self.create_report_view(report_id=report.id)

    async def create_report(self, *, new_report: ReportCreate) -> ReportInDB:
        """
        Create a new report in database.
        """

        # Check if there is already a report with this data
        if not "FIXTURE_MODE" in os.environ or not os.environ["FIXTURE_MODE"] == "True":
            duplicate_reports = await self.db.fetch_all(
                query=GET_DUPLICATE_REPORTS_QUERY,
                values=new_report.model_dump(include={"data"}),
            )
            if len(duplicate_reports) > 0:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.report.report_already_same_name_error'),
                )

        async with self.db.transaction():
            # Defer constraint on foreign key
            await self.db.execute(query=DEFER_FOREIGN_KEY_CONSTRAINTS)

            # Insert data in data_versions table
            data_id = await self.db.fetch_one(
                query=CREATE_DATA_VERSION_WITHOUT_FIELD_REFERENCE_QUERY,
                values=new_report.model_dump(include={"data"}),
            )

            # Create field with associated data
            field = await self.db.fetch_one(
                query=CREATE_FIELD_QUERY,
                values={**data_id, **new_report.model_dump(exclude={"data"})},
            )

            new_report = ReportInDB(**new_report.model_dump(include={"data"}), **dict(field))
            # Update previous data row with linked field_id
            await self.db.fetch_one(
                query=UPDATE_DATA_VERSION_WITH_FIELD_REFERENCE_QUERY,
                values=new_report.model_dump(include={"data_id", "field_id"}),
            )

            # Create dedicated SQL View for the report
            await self.create_report_view(report_id=new_report.id)

        # Return created full report
        return new_report

    async def get_reports(
        self, policies_group: PoliciesGroupInDB, filters: dict = None
    ) -> ReportInDB:
        """
        List reports according to policies group.

        If report:list scope has been set, authorize to list everything.
        """
        filters_str = ""

        authorize_list_everything = False
        authorized_reports = []

        # Authorize to list all reports if the permission has been set.
        for policy in policies_group.policies:
            if "report:list" in policy.actions:
                authorize_list_everything = True
                break
            elif "report:read" in policy.actions:
                authorized_reports.append(policy.report_id)

        if filters:
            if "name" in filters:
                filters_str += "AND name = '{}'".format(filters["name"])

        limits_str = ""
        if not authorize_list_everything:
            limits_str = "AND fields.id IN ({})".format(
                ",".join(map(str, authorized_reports))
            )

        query = Template(GET_REPORTS_QUERY)
        query = query.substitute(filters=filters_str, limits=limits_str)

        reports = await self.db.fetch_all(query=query)

        return [ReportInDB(**report) for report in reports]

    async def get_report_by_id(self, *, id: int) -> ReportInDB:
        report = await self.db.fetch_one(
            query=GET_REPORT_BY_ID_QUERY, values={"id": id}
        )
        if not report:
            return None
        return ReportInDB(**report)

    async def update_report(
        self,
        *,
        id: int,
        report_update: ReportUpdate,
    ) -> ReportInDB:
        report = await self.get_report_by_id(id=id)

        if not report:
            return None

        # Raise error if there are no valid params left
        if not report_update.model_dump(exclude_unset=True):
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.invalid_update_params'),
            )

        # Prevent from using the same name as an existing report
        duplicate_reports = await self.db.fetch_all(
            query=GET_DUPLICATE_REPORTS_QUERY,
            values=report_update.model_dump(include={"data"}),
        )
        if len(duplicate_reports) > 0:
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.report_already_same_name_error'),
            )

        report_update_params = report.model_copy(
            update=report_update.model_dump(exclude_unset=True),
        )

        try:
            updated_report = await self.db.fetch_one(
                query=UPDATE_REPORT_BY_ID_QUERY,
                values=report_update_params.model_dump(include={"id", "data"}),
            )
            return ReportInDB(**updated_report)
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.invalid_update_params'),
            )

    async def delete_report_by_id(self, *, id: int) -> int:
        report = await self.get_report_by_id(id=id)
        if not report:
            return None

        try:
            deleted_id = await self.db.execute(
                query=DELETE_REPORT_BY_ID_QUERY,
                values={"id": id},
            )
        # Should never happen as now the Foreign Key constraint has CASCADE ON DELETE
        except ForeignKeyViolationError as e:
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.fields_still_linked_to_report_to_delete_error'),
            )

        # Delete dedicated SQL View for the report
        query = Template(DELETE_REPORT_VIEW_QUERY)
        query = query.substitute(report_id=report.id)
        await self.db.execute(query=query)

        return deleted_id
