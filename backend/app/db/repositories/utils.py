from string import Template
from typing import List
from fastapi import HTTPException
from asyncpg import ForeignKeyViolationError
from starlette.status import HTTP_400_BAD_REQUEST
from pydantic import ValidationError

from app.db.repositories.base import BaseRepository
from app.db.repositories.reports import ReportsRepository
from app.models.field import FieldCreate, FieldInDB, FieldPublic, FieldUpdate


GET_FIELDS_FROM_PARENT_BY_ID = """
    WITH RECURSIVE generation AS (
        SELECT id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            0 AS generation_number
        FROM fields
        WHERE id = :id

    UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            generation_number+1 AS generation_number
        FROM fields child
        JOIN generation g
          ON g.id = child.parent_id
    )

    SELECT
         g1.id,
         g1.name,
         g1.parent_id,
         g1.data_id,
         g1.field_type,
         g1.template_id,
         data_versions.value AS data,
         g1.generation_number
    FROM generation g1
    INNER JOIN data_versions ON g1.data_id = data_versions.id
    WHERE NOT g1.id = :id AND
          g1.id NOT IN (SELECT g1.parent_id FROM generation g1 WHERE g1.parent_id IS NOT NULL)
          $filters;
"""


class UtilsRepository(BaseRepository):
    """ "
    All database actions to be used by other resources
    """

    async def get_leaf_fields(
        self, *, field_id: int, filters: dict = None
    ) -> List[FieldInDB]:
        """
        Return fields which have no children
        """

        # Take into account filters for query
        filters_str = ""
        if filters:
            if "field_type" in filters:
                filters_str = filters_str + " AND g1.field_type = :field_type"

        query = Template(GET_FIELDS_FROM_PARENT_BY_ID)
        query = query.substitute(filters=filters_str)

        fields = await self.db.fetch_all(
            query=query, values={"id": field_id, **(filters or {})}
        )

        if not fields:
            return []

        fields = [FieldInDB(**field) for field in fields]

        return fields
