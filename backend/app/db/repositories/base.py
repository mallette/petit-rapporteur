from starlette.config import Config
from databases import Database

config = Config(".env")


class BaseRepository:
    def __init__(self, db: Database) -> None:
        self.db = db
        self.root_password = config("ROOT_PASSWORD_HASH", cast=str)
        self.root_password_salt = config("ROOT_PASSWORD_SALT", cast=str)
