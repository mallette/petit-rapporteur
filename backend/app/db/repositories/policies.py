import i18n
import json
import logging
from databases import Database
from typing import Optional, List
from string import Template

from fastapi import HTTPException, status
from asyncpg import UniqueViolationError
from pydantic import ValidationError
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR

from app.db.repositories.base import BaseRepository
from app.db.repositories.fields import FieldsRepository
from app.db.repositories.reports import ReportsRepository
from app.models.policy import (
    PoliciesGroupInDB,
    PolicyCreate,
    PoliciesGroupPublic,
    PolicyInDB,
    PolicyPublic,
    ReportShareCreate,
)
from app.models.report import ReportInDB
from app.models.field import FieldInDB
from app.services import auth_service


logger = logging.getLogger(__name__)


CREATE_POLICIES_GROUP_INSERT_QUERY = """
    INSERT INTO policies_groups (comment)
    VALUES (:comment)
    RETURNING id, comment;
"""

CREATE_POLICIES_GROUP_UPSERT_QUERY = """
    WITH ins AS (
        INSERT INTO policies_groups (comment)
        VALUES (:comment)
        ON     CONFLICT (comment) DO UPDATE
        SET    comment = NULL
        WHERE  FALSE      -- never executed, but locks the row
        RETURNING id, comment
    )
    SELECT id, comment FROM ins
    UNION  ALL
    SELECT id, comment FROM policies_groups
    WHERE  -- only executed if no INSERT
       comment IS NOT DISTINCT FROM :comment
    LIMIT  1;
"""

CREATE_POLICY_QUERY = """
    WITH ins AS (
       INSERT INTO policies (report_id, field_id, actions, filters)
       VALUES (:report_id, :field_id, CAST(:actions AS text[]), CAST(:filters AS text[]))
       ON     CONFLICT (report_id, field_id, actions, filters) DO UPDATE
       SET    actions = NULL
       WHERE  FALSE      -- never executed, but locks the row
       RETURNING id, report_id, field_id, actions, filters
       )
    SELECT id, report_id, field_id, actions, filters FROM ins
    UNION  ALL
    SELECT id, report_id, field_id, actions, filters FROM policies
    WHERE  -- only executed if no INSERT
       report_id IS NOT DISTINCT FROM :report_id AND
       field_id IS NOT DISTINCT FROM :field_id AND
       actions = CAST(:actions AS text[]) AND
       filters = CAST(:filters AS text[])
    LIMIT  1;
"""

CREATE_REPORT_POLICY_VIEW_QUERY = """
    CREATE OR REPLACE RECURSIVE VIEW view_report_policy_$policy_id(id, name, parent_id, data_id, field_type, template_id, data, generation_number) AS (
        SELECT v.id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            data_versions.value AS data,
            -1 AS generation_number
        FROM $report_table v
        INNER JOIN data_versions ON v.data_id = data_versions.id
        $filters

    UNION

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            data_versions.value AS data,
            -1 AS generation_number
        FROM $report_table child
        JOIN view_report_policy_$policy_id v
          ON v.id = child.parent_id
        INNER JOIN data_versions ON child.data_id = data_versions.id
    );
"""

# Set -1 as generation_number to show it is erroneous. We keep this column for legacy reasons as it already existed in previous views
# and PostgreSQL doesn't allow to remove columns from already existing views.
CREATE_ALL_ACCESS_POLICY_VIEW_QUERY = """
    CREATE OR REPLACE VIEW view_report_policy_$policy_id(id, name, parent_id, data_id, field_type, template_id, data, generation_number) AS (
        SELECT v.id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            data_versions.value AS data,
            -1 AS generation_number
        FROM $report_table v
        INNER JOIN data_versions ON v.data_id = data_versions.id
        $filters
    );
"""

CREATE_POLICY_FIELD_VIEW_QUERY = """
    CREATE OR REPLACE RECURSIVE VIEW view_report_policy_$policy_id(id, name, parent_id, data_id, field_type, template_id, generation_number) AS (
        SELECT id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            0 AS generation_number
        FROM fields
        WHERE id = $field_id

    UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            generation_number+1 AS generation_number
        FROM fields child
        JOIN view_report_policy_$policy_id v
          ON v.id = child.parent_id
    );
"""

CREATE_POLICY_POLICIES_GROUP_LINK_QUERY = """
    WITH ins AS (
        INSERT INTO policies_groups_policies (policy_id, policies_group_id)
        VALUES (:policy_id, :policies_group_id)
        ON     CONFLICT (policy_id, policies_group_id) DO UPDATE
        SET    policies_group_id = NULL
        WHERE  FALSE      -- never executed, but locks the row
        RETURNING policy_id, policies_group_id
    )
    SELECT policy_id, policies_group_id FROM ins
    UNION  ALL
    SELECT policy_id, policies_group_id FROM policies_groups_policies
    WHERE  -- only executed if no INSERT
       policy_id IS NOT DISTINCT FROM :policy_id AND
       policies_group_id IS NOT DISTINCT FROM :policies_group_id
    LIMIT  1;
"""

GET_POLICIES_GROUPS_QUERY = """
   SELECT * FROM policies_groups;
"""

GET_POLICIES_GROUP_BY_ID_QUERY = """
   SELECT * FROM policies_groups WHERE id = :id;
"""

GET_POLICIES_GROUP_POLICIES_QUERY = """
   SELECT * FROM policies
   INNER JOIN policies_groups_policies pgp ON pgp.policy_id = policies.id
   INNER JOIN policies_groups pg ON pgp.policies_group_id = pg.id;
"""

GET_POLICIES_GROUP_POLICIES_BY_ID_QUERY = """
    SELECT policies.id,
           policies.field_id,
           policies.report_id,
           policies.actions,
           policies.filters
   FROM policies
   INNER JOIN policies_groups_policies pgp ON pgp.policy_id = policies.id
   INNER JOIN policies_groups pg ON pgp.policies_group_id = pg.id
   WHERE pg.id = :id;
"""

GET_AUTHORIZING_POLICIES_QUERY = """
    SELECT policies.id,
           policies.field_id,
           policies.report_id,
           policies.actions,
           policies.filters
    FROM policies_groups
    INNER JOIN policies_groups_policies ON policies_groups_policies.policies_group_id = policies_groups.id
    INNER JOIN policies ON policies.id = policies_groups_policies.policy_id
    WHERE policies_groups.id = $policies_group_id
    AND (policies.report_id = '*' $report_id_filter_str)
    AND (policies.field_id = '*' $field_id_filter_str)
    AND ('*' = ANY (actions) $actions_filter_str)
    ORDER BY field_id DESC, report_id DESC, actions DESC;
"""

GET_NUMBER_MATCHING_FIELDS_FROM_POLICY_VIEW_QUERY = """
    SELECT COUNT(id) nb_fields FROM $policy_view
    WHERE id = $field_id;
"""

UPDATE_POLICIES_GROUP_BY_ID_QUERY = """
    UPDATE policies_groups
    SET comment = :comment
    WHERE id = :id
    RETURNING id, comment;
"""

DELETE_LINKS_FROM_POLICIES_GROUP_BY_ID_QUERY = """
    DELETE FROM policies_groups_policies
    WHERE policies_groups_policies.policies_group_id = :id;
"""

GET_ALL_POLICIES_QUERY = """
    SELECT * FROM policies;
"""

GET_ALL_LONELY_POLICIES_QUERY = """
    SELECT * FROM policies
    FULL JOIN policies_groups_policies pgp ON policies.id = pgp.policy_id
    WHERE pgp.policy_id = NULL;
"""

DELETE_POLICY_BY_ID_QUERY = """
    DELETE FROM policies
    WHERE policies.id = :id;
"""

DELETE_POLICY_VIEW_BY_ID_QUERY = """
    DROP VIEW view_report_policy_$policy_id;
"""


class PoliciesRepository(BaseRepository):
    def __init__(self, db: Database) -> None:
        super().__init__(db)
        self.auth_service = auth_service

    def check_unique_reports_in_policies_group(
        self, *, policies_group_params: ReportShareCreate
    ) -> bool:
        report_ids = []
        for policy in policies_group_params.model_dump()["policies"]:
            report_ids.append(policy["report_id"])

        if len(report_ids) != len(set(report_ids)):
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.policies.policies_group_unicity_error'),
            )

    async def create_policy_view(self, *,
        policy: PolicyInDB,
    ) -> None:
        filters_str = ""
        if "filters" in policy.model_dump() and policy.model_dump()["filters"] is not None:
            for filter in policy.model_dump()["filters"]:
                if filter["field_name"] and filter["field_data"]:
                    if filters_str == "":
                        filters_str += "WHERE "
                    else:
                        filters_str += " OR "

                    filters_str += (
                        "(v.name = e'{}' AND data_versions.value = e'{}')".format(
                            filter["field_name"].replace("'",'\\' + "'"), filter["field_data"].replace("'",'\\' + "'")
                        )
                    )

        if policy.model_dump()["field_id"] and policy.model_dump()["field_id"] != "*":
            query = Template(CREATE_POLICY_FIELD_VIEW_QUERY)
            query = query.substitute(
                field_id=policy.model_dump()["field_id"],
                policy_id=policy.id,
                filters=filters_str,
            )
        else:
            if policy.model_dump()["report_id"] and policy.model_dump()["report_id"] != "*":
                report_table = "view_report_" + str(policy.model_dump()["report_id"])
                query = Template(CREATE_REPORT_POLICY_VIEW_QUERY)
            else:
                report_table = "fields"
                query = Template(CREATE_ALL_ACCESS_POLICY_VIEW_QUERY)

            query = query.substitute(
                report_table=report_table,
                policy_id=policy.id,
                filters=filters_str,
            )
        await self.db.fetch_one(query=query)

    async def attach_policies_to_policies_group(
        self, *, policies_group_params: ReportShareCreate, policies_group_id: int
    ) -> List[PolicyInDB]:
        created_policies = []

        # Check policies validity
        self.check_unique_reports_in_policies_group(
            policies_group_params=policies_group_params
        )

        report_fields = None
        # Create each policy whith their SQL view if not already created
        for policy in policies_group_params.model_dump()["policies"]:
            # Check that the report exists if specified
            if policy["report_id"] and policy["report_id"] != "*":
                field_repo = FieldsRepository(self.db)
                report_fields = (
                    await field_repo.get_flatten_recursive_children_from_parent_by_id(
                        report_id=int(policy["report_id"]),
                        field_id=int(policy["report_id"]),
                    )
                )
                if not report_fields:
                    raise HTTPException(
                        status_code=HTTP_404_NOT_FOUND,
                        detail=i18n.t('translate.policies.report_does_not_exist', report_id=policy["report_id"]),
                    )

            # Check that field_id is present in report with report_id
            if policy["field_id"] and policy["field_id"] != "*":
                if policy["report_id"] == "*":
                    raise HTTPException(
                        status_code=HTTP_400_BAD_REQUEST,
                        detail=i18n.t('translate.policies.report_id_needs_to_be_defined'),
                    )
                if int(policy["field_id"]) not in [
                    FieldInDB(**field).id for field in report_fields
                ]:
                    raise HTTPException(
                        status_code=HTTP_400_BAD_REQUEST,
                        detail=i18n.t('translate.policies.field_id_is_not_present_in_report_error', field_id=policy["field_id"], report_id=policy["report_id"])
                    )

            # Format actions as strings
            actions = [action.value for action in policy["actions"]]

            # Format filters as strings
            filters = (
                [json.dumps(filter) for filter in policy["filters"]]
                if policy["filters"]
                else []
            )

            # Create policy in database
            policy = PolicyCreate(**policy)
            created_policy = await self.db.fetch_one(
                query=CREATE_POLICY_QUERY,
                values={
                    "actions": actions,
                    "filters": filters,
                    **policy.model_dump(exclude={"actions", "filters"}),
                },
            )
            created_policy = PolicyInDB(**created_policy)
            created_policies.append(created_policy)

            # Create policy SQL view
            await self.create_policy_view(
                policy=created_policy,
            )

            # Attach policy to policies group
            await self.db.fetch_one(
                query=CREATE_POLICY_POLICIES_GROUP_LINK_QUERY,
                values={
                    "policy_id": created_policy.id,
                    "policies_group_id": policies_group_id,
                },
            )

        return created_policies

    async def create_policies_group(
        self, *, upsert=False, policies_group_params: ReportShareCreate
    ) -> PoliciesGroupInDB:
        """
        Create a policies group and its related policies.
        Upsert parameter allows to avoid errors when automating creation like at server start
        or for unit tests. It must be used carefully as it can create inconsistencies.
        """
        # Check policies validity
        self.check_unique_reports_in_policies_group(
            policies_group_params=policies_group_params
        )

        # Create policies group
        if upsert:
            policies_group = await self.db.fetch_one(
                query=CREATE_POLICIES_GROUP_UPSERT_QUERY,
                values=policies_group_params.model_dump(include={"comment"}),
            )
        else:
            try:
                policies_group = await self.db.fetch_one(
                    query=CREATE_POLICIES_GROUP_INSERT_QUERY,
                    values=policies_group_params.model_dump(include={"comment"}),
                )
            except UniqueViolationError as e:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.policies.policies_group_same_comment_error'),
                )

        policies_group = PoliciesGroupInDB(**policies_group)

        created_policies = await self.attach_policies_to_policies_group(
            policies_group_params=policies_group_params,
            policies_group_id=policies_group.model_dump()["id"],
        )

        created_policies = [PolicyPublic(**policy.model_dump()) for policy in created_policies]
        return PoliciesGroupPublic(policies=created_policies, **policies_group.model_dump())

    async def get_policies_group_by_id(
        self, *, policies_group_id: int
    ) -> PoliciesGroupPublic:
        policies_group = await self.db.fetch_one(
            query=GET_POLICIES_GROUP_BY_ID_QUERY, values={"id": policies_group_id}
        )
        if not policies_group:
            return None

        policies = await self.db.fetch_all(
            query=GET_POLICIES_GROUP_POLICIES_BY_ID_QUERY,
            values={"id": policies_group_id},
        )

        policies = [PolicyPublic(**policy) for policy in policies]
        return PoliciesGroupPublic(policies=policies, **policies_group)

    async def get_policies_groups(self) -> List[PoliciesGroupPublic]:
        policies_groups = await self.db.fetch_all(
            query=GET_POLICIES_GROUPS_QUERY,
        )
        if not policies_groups:
            return None

        all_policies_groups = []
        for policies_group in policies_groups:
            policies = await self.db.fetch_all(
                query=GET_POLICIES_GROUP_POLICIES_BY_ID_QUERY,
                values={"id": policies_group.id},
            )
            policies = [PolicyPublic(**policy) for policy in policies]
            all_policies_groups.append(
                PoliciesGroupPublic(policies=policies, **policies_group)
            )

        return all_policies_groups

    async def update_policies_group_by_id(
        self, *, policies_group_id: int, policies_group_params: ReportShareCreate
    ) -> PoliciesGroupPublic:
        """
        Update a policies group by requesting it by its ID.
        """
        # Update policies group comment
        policies_group = await self.db.fetch_one(
            query=UPDATE_POLICIES_GROUP_BY_ID_QUERY,
            values={
                "id": policies_group_id,
                "comment": policies_group_params.model_dump()["comment"],
            },
        )

        # Delete all policies links
        await self.db.fetch_one(
            query=DELETE_LINKS_FROM_POLICIES_GROUP_BY_ID_QUERY,
            values={"id": policies_group_id},
        )

        # Delete lonely policies and their views for cleanup
        lonely_policies = await self.db.fetch_all(query=GET_ALL_LONELY_POLICIES_QUERY)
        for lonely_policy in lonely_policies:
            await self.db.fetch_one(
                query=DELETE_POLICY_BY_ID_QUERY, values={"id": lonely_policy.id}
            )
            query = Template(DELETE_POLICY_VIEW_BY_ID_QUERY)
            query = query.substitute(policy_id=lonely_policy["id"])
            await self.db.fetch_one(query=query)

        # Recreate policies and policy views
        created_policies = await self.attach_policies_to_policies_group(
            policies_group_params=policies_group_params,
            policies_group_id=policies_group._mapping["id"],
        )

        created_policies = [PolicyPublic(**policy.model_dump()) for policy in created_policies]
        return PoliciesGroupPublic(policies=created_policies, **policies_group)

    async def recreate_policies_views(
        self
    ):
        """
        MAINTENANCE TASK.
        Recreate existing policies SQL views in order to update their format.
        Since commit 5fd79f0de364fa9e857ca4534eedec5eb790cdd8 from 2023-07-19,
        Policy SQL views creation has been changed and existing ones need to be updated.
        """
        logger.info("MAINTENANCE: Recreating policies' SQL views")

        # Delete existing policies' SQL views for cleanup
        policies = await self.db.fetch_all(query=GET_ALL_POLICIES_QUERY)
        for policy in policies:
            policy = PolicyInDB(**policy)

            # Delete the policy's view
            query = Template(DELETE_POLICY_VIEW_BY_ID_QUERY)
            query = query.substitute(policy_id=policy.id)
            await self.db.fetch_one(query=query)

            # Recreate the policy's SQL view
            await self.create_policy_view(
                policy=policy,
            )

    async def check_if_authorized_by_policies(
        self,
        *,
        policies_group_id: int,
        report_id: int = None,
        field_id: int = None,
        actions: List[str]
    ) -> bool:
        """
        Check if the policies group authorizes the current request following this procedure:
             1.  Does the request have the permission for report X ?
                 1.1  No :  Does the request have the permission for reports * ?
                     1.1.1  No : End and not authorized
                     1.1.2  Yes : See 1.2.
                 1.2  Yes : Does the request have the permission for action X for report X or * ?
                     1.2.1  No : Does the request have the permission for actions * for report X or * ?
                         1.2.1.1  No : End and not authorized
                         1.2.1.2  Yes : See 1.2.2
                     1.2.2 Yes : The field_id, or if not specified, the report_id of the request is it present in the list of ids of the DB view filtered with the permission parameters?
                         1.2.2.1  Yes : Authorized
                         1.2.2.2  No : End and not authorized
        """

        # First check if field_id exist if specified or otherwise report_id
        if field_id is not None:
            field_repo = FieldsRepository(self.db)

            try:
                field = await field_repo.get_field_by_id(
                    id=report_id, field_id=field_id
                )
                if field is None:
                    raise HTTPException(
                        status_code=HTTP_404_NOT_FOUND,
                        detail=i18n.t('translate.report.field_not_found_error'),
                    )
            except HTTPException as e:
                if (
                    e.status_code == HTTP_400_BAD_REQUEST
                    and e.detail == i18n.t('translate.policies.report_ids_not_allowed_only_fields_error')
                ):
                    # Try if the target is a report
                    report_repo = ReportsRepository(self.db)
                    report = await report_repo.get_report_by_id(id=field_id)
                    if report is None:
                        raise HTTPException(
                            status_code=HTTP_404_NOT_FOUND,
                            detail=i18n.t('translate.report.report_not_found_error'),
                        )
                else:
                    raise e

        elif report_id is not None:
            report_repo = ReportsRepository(self.db)
            report = await report_repo.get_report_by_id(id=report_id)
            if report is None:
                raise HTTPException(
                    status_code=HTTP_404_NOT_FOUND,
                    detail=i18n.t('translate.report.report_not_found_error'),
                )

        report_id_filter_str = ""
        if report_id:
            report_id_filter_str = " OR policies.report_id = '{}'".format(report_id)

        field_id_filter_str = ""
        if field_id:
            field_id_filter_str = " OR policies.field_id = '{}'".format(field_id)

        actions_filter_str = ""
        for action in actions:
            actions_filter_str += " OR '{}' = ANY (actions)".format(action)

        query = Template(GET_AUTHORIZING_POLICIES_QUERY)
        query = query.substitute(
            policies_group_id=policies_group_id,
            report_id_filter_str=report_id_filter_str,
            field_id_filter_str=field_id_filter_str,
            actions_filter_str=actions_filter_str,
        )
        authorizing_policies = await self.db.fetch_all(query=query)
        # Forbid access if there is no potential authorizing policy
        if len(authorizing_policies) == 0:
            return None

        # Bypass ids check when they are none.
        if field_id is None and report_id is None:
            return authorizing_policies[0]
        elif report_id is not None:
            # For each authorizing policy, check that the field_id or the report_id
            # is present in the DB view
            for policy in authorizing_policies:
                policy = PolicyInDB(**policy)

                # No need to check fields if field_id is None. Simply return first match.
                if field_id is None:
                    return policy
                else:
                    query = Template(GET_NUMBER_MATCHING_FIELDS_FROM_POLICY_VIEW_QUERY)
                    query = query.substitute(
                        policy_view="view_report_policy_{}".format(policy.id),
                        field_id = field_id
                    )
                    nb_matched_fields = await self.db.fetch_one(query=query)

                    # Special container_promo:delete permission:
                    # Check that the target is really a container_promo
                    if 'container_promo:delete' in actions and \
                       'container_promo:delete' in policy.actions and not \
                       'report:delete' in policy.actions:
                        fields_repo = FieldsRepository(self.db)
                        field = await fields_repo.get_field_by_id(
                                id=report_id,
                                field_id=field_id,
                        )
                        if field.field_type != 'container_promo':
                            raise HTTPException(
                                status_code=status.HTTP_401_UNAUTHORIZED,
                                detail=i18n.t('translate.authentication.not_enough_permissions_error'),
                            )

                    # Check if the field_id is authorized
                    if nb_matched_fields._mapping['nb_fields'] == 1:
                        return policy
                    # This should never happen as ids are unique
                    elif nb_matched_fields._mapping['nb_fields'] > 1:
                        raise HTTPException(
                            status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                        )

        return None
