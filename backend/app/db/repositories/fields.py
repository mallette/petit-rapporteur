import os
from pathlib import Path
import i18n
import hashlib
from string import Template
import logging
import csv
import json
from typing import List
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
import asyncpg
from asyncpg import ForeignKeyViolationError, DuplicateTableError, UniqueViolationError
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED
from pydantic import ValidationError

from app.core.config import DATABASE_URL, APP_EXPORTS_FOLDER, APP_IMPORTS_FOLDER
from app.db.repositories.base import BaseRepository
from app.db.repositories.reports import ReportsRepository
from app.db.repositories.utils import UtilsRepository
from app.models.field import (
    FieldCreate,
    FieldInDB,
    FieldPublic,
    FieldUpdate,
    FieldCopy,
    FieldTemplateUpdate,
    ColumnPublic,
)

logger = logging.getLogger(__name__)

DEFER_FOREIGN_KEY_CONSTRAINTS = """
    SET CONSTRAINTS "data_versions_field_id_fkey" DEFERRED;
"""

CHECK_INFO_FIELD_SAME_PARENT_QUERY = """
    SELECT f.id, f.name, f.parent_id, f.field_type, f.data_id, d.value AS data
    FROM fields f
    INNER JOIN data_versions d ON f.data_id = d.id
    WHERE field_type = 'info' AND parent_id = :parent_id;
"""

CREATE_FIELD_QUERY = """
    WITH created_data AS (
        INSERT INTO data_versions (value, author, field_id)
        VALUES (:data, 'Unknown', nextval('fields_id_seq'))
        RETURNING id AS data_id, field_id, value
    ),
    created_field AS (
        INSERT INTO fields (id, name, parent_id, data_id, field_type, template_id, external_source_id, external_source_field_id)
        SELECT d.field_id, :name, :parent_id, d.data_id, :field_type, :template_id, :external_source_id, :external_source_field_id
        FROM created_data d
        RETURNING id, name, parent_id, field_type, data_id, template_id, external_source_id, external_source_field_id
    )

    SELECT f.id, f.name, f.parent_id, f.field_type, f.data_id, f.external_source_id, f.external_source_field_id, d.value AS data FROM created_field f
    INNER JOIN created_data d ON f.id = d.field_id;
"""

DEEP_COPY_CHILDREN_FIELDS_QUERY = """
    WITH new_fields_ids AS (
        WITH RECURSIVE cte AS (
            SELECT *, nextval('fields_id_seq') new_id FROM fields WHERE $identifier_type = $source_field_id
            UNION ALL
            SELECT fields.*, nextval('fields_id_seq') new_id FROM cte JOIN fields ON cte.id = fields.parent_id
        )
        SELECT C1.new_id, C1.name, C1.field_type, C3.new_id new_template_id, C1.data_id, C2.new_id new_parent_id
        FROM cte C1 LEFT JOIN cte C2 ON C1.parent_id = C2.id LEFT JOIN cte C3 ON C1.template_id = C3.id
    ),
    cloned_data AS (
        INSERT INTO data_versions (value, author, field_id)
        SELECT d.value, d.author, c.new_id
        FROM new_fields_ids c
        INNER JOIN data_versions d ON c.data_id = d.id
        RETURNING id AS data_id, field_id, value
    ),
    created_data AS (
        SELECT new_fields_ids.new_id, new_fields_ids.name, new_fields_ids.field_type, new_fields_ids.new_parent_id, new_fields_ids.new_template_id, cloned_data.data_id
        FROM new_fields_ids
        INNER JOIN cloned_data ON new_fields_ids.new_id = cloned_data.field_id
    ),
    cloned_fields AS (
        INSERT INTO fields (id, name, parent_id, field_type, template_id, data_id)
        SELECT new_id, name, COALESCE(new_parent_id, $target_field_id), field_type, new_template_id, data_id FROM created_data
        RETURNING id, name, parent_id, field_type, template_id, data_id
    )


    SELECT f.id, f.name, f.parent_id, f.field_type, f.template_id, f.data_id, d.value AS data FROM cloned_fields f
    INNER JOIN cloned_data d ON f.id = d.field_id;
"""

# XXX This request should be used for better performance instead of using a recursive code
# in the function get_field_all_children_by_id()
GET_FLATTENED_FIELDS_FROM_PARENT_BY_ID = """
    WITH RECURSIVE generation AS (
        SELECT id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            0 AS generation_number
        FROM fields
        WHERE id = :id

    UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            generation_number+1 AS generation_number
        FROM fields child
        JOIN generation g
          ON g.id = child.parent_id
    )

    SELECT
         generation.id,
         generation.name,
         generation.parent_id,
         generation.data_id,
         generation.field_type,
         generation.template_id,
         data_versions.value AS data,
         generation.generation_number
    FROM generation
    INNER JOIN data_versions ON generation.data_id = data_versions.id
    WHERE parent_id IS NOT NULL $filters
    $order_by;
"""

INSTALL_TABLEFUNC_FUNCTION_QUERY = """
CREATE EXTENSION IF NOT EXISTS tablefunc;
"""

CREATE_TREE_FUNCTION_QUERY = """
CREATE OR REPLACE FUNCTION nest_json_field_tree_#function_name(_table varchar, _field_id int, _depth int)
RETURNS jsonb
AS $$
DECLARE
userid jsonb;
BEGIN
    EXECUTE format(E'SELECT jsonb_agg(sub)
FROM  (
   SELECT
          v.id,
          v.name,
          v.parent_id,
          v.data_id,
          v.field_type,
          v.template_id,
          v.generation_number,
          v.external_source_id,
          v.external_source_field_id,
          data_versions.value AS data,
          nest_json_field_tree_#function_name(\\'%1$s\\', v.id, %3$s+1) AS children
   FROM   %1$s v
   INNER JOIN data_versions ON v.data_id = data_versions.id
   WHERE  v.parent_id = %2$s
   #order_by_str
) sub
WHERE CASE
    WHEN #depth_limit
    THEN TRUE
    ELSE FALSE
    END

', _table, _field_id, _depth) INTO userid;
    RETURN userid;
END;
$$ LANGUAGE plpgsql;
"""

CREATE_FIELD_TREE_VIEW_QUERY= """
CREATE OR REPLACE RECURSIVE VIEW view_field_tree_$field_id(id, name, parent_id, data_id, field_type, template_id, external_source_id, external_source_field_id, generation_number) AS (
    SELECT id,
        name,
        parent_id,
        data_id,
        field_type,
        template_id,
        external_source_id,
        external_source_field_id,
        0 AS generation_number
    FROM fields
    WHERE id = $field_id

UNION ALL

    SELECT child.id,
        child.name,
        child.parent_id,
        child.data_id,
        child.field_type,
        child.template_id,
        child.external_source_id,
        child.external_source_field_id,
        generation_number+1 AS generation_number
    FROM fields child
    JOIN view_field_tree_$field_id v
      ON v.id = child.parent_id
);
"""

CREATE_PREPROCESSED_VIEW_QUERY= """
CREATE MATERIALIZED VIEW IF NOT EXISTS view_preprocessed_$view_name AS (
    SELECT v.id,
        v.name,
        v.parent_id,
        v.data_id,
        v.field_type,
        v.template_id,
        v.generation_number,
        v.external_source_id,
        v.external_source_field_id,
        data_versions.value AS data
    FROM       view_field_tree_$field_id v
    INNER JOIN data_versions ON v.data_id = data_versions.id
    WHERE      True
    $order_by
);
"""

CREATE_PREPROCESSED_BY_LEAF_CONTAINER_ROWS_VIEW_QUERY= """
CREATE MATERIALIZED VIEW IF NOT EXISTS view_preprocessed_$view_name AS (
    WITH RECURSIVE processed_leaf_containers AS (
        SELECT  v.id,
                 v.name,
                 v.parent_id,
                 v.data_id,
                 v.field_type,
                 v.template_id,
                 v.generation_number,
                 v.external_source_id,
                 v.external_source_field_id,
                 data_versions.value AS data
        FROM view_field_tree_$field_id v
        INNER JOIN data_versions ON v.data_id = data_versions.id
        WHERE v.id IN (
            SELECT id FROM crosstab(
                  E'WITH RECURSIVE leaf_containers AS (
                        SELECT v.id, v.name, v.parent_id, v.data_id,
                               v.field_type, v.template_id,
                               data_versions.value AS data, v.generation_number
                        FROM view_field_tree_$field_id v
                        INNER JOIN data_versions ON v.data_id = data_versions.id
                        WHERE v.id NOT IN (
                            SELECT v.parent_id
                            FROM view_field_tree_$field_id v
                            WHERE v.parent_id IS NOT NULL
                            AND v.field_type = \\'container\\')
                        AND v.field_type = \\'container\\'
                        AND v.parent_id IS NOT NULL
                    ), children AS (
                        SELECT leaf_containers.id,
                            name,
                            parent_id,
                            data_id,
                            field_type,
                            template_id,
                            leaf_containers.generation_number,
                            leaf_containers.id AS leaf_container_id,
                            leaf_containers.data
                        FROM leaf_containers

                        UNION ALL

                        SELECT child.id,
                            child.name,
                            child.parent_id,
                            child.data_id,
                            child.field_type,
                            child.template_id,
                            child.generation_number,
                            c.leaf_container_id AS leaf_container_id,
                            data_versions.value AS data
                        FROM view_field_tree_$field_id child
                        INNER JOIN data_versions ON child.data_id = data_versions.id
                        JOIN children c
                          ON c.id = child.parent_id
                    ), parents AS (
                        SELECT leaf_containers.id,
                            name,
                            parent_id,
                            data_id,
                            field_type,
                            template_id,
                            0 AS generation_number,
                            leaf_containers.id AS leaf_container_id,
                            leaf_containers.data
                        FROM leaf_containers

                        UNION ALL

                        SELECT child.id,
                            child.name,
                            child.parent_id,
                            child.data_id,
                            child.field_type,
                            child.template_id,
                            p.generation_number+1 AS generation_number,
                            p.leaf_container_id AS leaf_container_id,
                            data_versions.value AS data
                        FROM view_field_tree_$field_id child
                        INNER JOIN data_versions ON child.data_id = data_versions.id
                        JOIN parents p
                          ON p.parent_id = child.id
                        WHERE child.parent_id IS NOT NULL
                    ), columns AS (
                        SELECT DISTINCT parents.leaf_container_id, parents.name, parents.data
                        FROM parents
                        UNION
                        SELECT DISTINCT children.leaf_container_id, children.name, children.data
                        FROM children
                        UNION
                        SELECT DISTINCT leaf_containers.id, leaf_containers.name, leaf_containers.data
                        FROM leaf_containers
                        ORDER BY 1,2
                    )


                    SELECT columns.leaf_container_id, columns.name, columns.data
                    FROM columns;',
                    E'$category_sql'
                ) AS ct(id INT $crosstab_columns_str)
            WHERE True $filters $search_str
        )
    ), children AS (
        SELECT processed_leaf_containers.id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            external_source_id,
            external_source_field_id,
            processed_leaf_containers.generation_number,
            processed_leaf_containers.id AS leaf_container_id,
            processed_leaf_containers.data
        FROM processed_leaf_containers

        UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            child.external_source_id,
            child.external_source_field_id,
            child.generation_number,
            c.leaf_container_id AS leaf_container_id,
            data_versions.value AS data
        FROM view_field_tree_$field_id child
        INNER JOIN data_versions ON child.data_id = data_versions.id
        JOIN children c
          ON c.id = child.parent_id
    ), parents AS (
        SELECT processed_leaf_containers.id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            external_source_id,
            external_source_field_id,
            processed_leaf_containers.generation_number,
            processed_leaf_containers.id AS leaf_container_id,
            processed_leaf_containers.data
        FROM processed_leaf_containers

        UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            child.external_source_id,
            child.external_source_field_id,
            child.generation_number,
            p.leaf_container_id AS leaf_container_id,
            data_versions.value AS data
        FROM view_field_tree_$field_id child
        INNER JOIN data_versions ON child.data_id = data_versions.id
        JOIN parents p
          ON p.parent_id = child.id
    )

    SELECT DISTINCT ON (id) id, name, parent_id, data_id, field_type, template_id, data, external_source_id, external_source_field_id, generation_number
    FROM parents
    UNION
    SELECT DISTINCT ON (id) id, name, parent_id, data_id, field_type, template_id, data, external_source_id, external_source_field_id, generation_number
    FROM children
    UNION
    SELECT DISTINCT ON (id) id, name, parent_id, data_id, field_type, template_id, data, external_source_id, external_source_field_id, generation_number
    FROM processed_leaf_containers
    $order_by
);
"""

GET_FLAT_FIELDS_FROM_PARENT_BY_ID = """
    WITH RECURSIVE generation AS (
        SELECT id,
            name,
            parent_id,
            data_id,
            field_type,
            template_id,
            0 AS generation_number
        FROM fields
        WHERE id = $field_id

    UNION ALL

        SELECT child.id,
            child.name,
            child.parent_id,
            child.data_id,
            child.field_type,
            child.template_id,
            generation_number+1 AS generation_number
        FROM fields child
        JOIN generation g
          ON g.id = child.parent_id
    )

    SELECT
         generation.id,
         generation.name,
         generation.parent_id,
         generation.data_id,
         generation.field_type,
         generation.template_id,
         data_versions.value AS data,
         generation.generation_number
    FROM generation
    INNER JOIN data_versions ON generation.data_id = data_versions.id
    $order_by;
"""

# Used to check if a field from same external source has already been imported
SEARCH_SAME_EXTERNAL_SOURCE_FROM_PARENT_BY_ID = """
    WITH RECURSIVE generation AS (
        SELECT id,
            parent_id,
            external_source_id,
            external_source_field_id,
            0 AS generation_number
        FROM fields
        WHERE id = :id

    UNION ALL

        SELECT child.id,
            child.parent_id,
            child.external_source_id,
            child.external_source_field_id,
            generation_number+1 AS generation_number
        FROM fields child
        JOIN generation g
          ON g.id = child.parent_id
    )

    SELECT
         generation.id,
         generation.parent_id,
         generation.external_source_id,
         generation.external_source_field_id,
         generation.generation_number
    FROM generation
    WHERE parent_id IS NOT NULL
      AND external_source_id = :external_source_id
      AND external_source_field_id = :external_source_field_id;
"""

GET_FLAT_PROCESSED_FIELDS_FROM_PARENT_BY_ID = """
SELECT
      v.id,
      v.name,
      v.parent_id,
      v.data_id,
      v.field_type,
      v.template_id,
      v.data
FROM   view_preprocessed_$view_name v
"""

GET_NESTED_FIELDS_FROM_PARENT_BY_ID = """
SELECT to_jsonb(sub) AS tree
FROM  (
   SELECT
          v.id,
          v.name,
          v.parent_id,
          v.data_id,
          v.field_type,
          v.template_id,
          data_versions.value AS data,
          nest_json_field_tree_$function_name('view_field_tree_$field_id', v.id, 0) AS children
   FROM   view_field_tree_$field_id v
   INNER JOIN data_versions ON v.data_id = data_versions.id
   WHERE  v.id = $field_id
) sub;
"""

GET_NESTED_PROCESSED_FIELDS_FROM_PARENT_BY_ID = """
SELECT to_jsonb(sub) AS tree
FROM  (
   SELECT
          v.id,
          v.name,
          v.parent_id,
          v.data_id,
          v.field_type,
          v.template_id,
          v.generation_number,
          v.data,
          nest_json_field_tree_$function_name('view_preprocessed_$view_name', v.id, 0) AS children
   FROM   view_preprocessed_$view_name v
   WHERE  v.id = $field_id
) sub;
"""

GET_FIELDS_CSV_FROM_PARENT_BY_ID = """
SELECT * FROM crosstab(
      E'WITH RECURSIVE promos AS (
            SELECT
                 v.id,
                 v.name,
                 v.parent_id,
                 v.data_id,
                 v.field_type,
                 v.template_id,
                 v.generation_number,
                 data_versions.value AS data
            FROM view_field_tree_$field_id v
            INNER JOIN data_versions ON v.data_id = data_versions.id
            WHERE field_type = \\'container_promo\\'
        ), children AS (
            SELECT
                 v.id,
                 v.name,
                 v.parent_id,
                 v.data_id,
                 v.field_type,
                 v.template_id,
                 data_versions.value AS data,
                 v.generation_number,
                 promos.id AS promo_id
            FROM view_field_tree_$field_id v
            JOIN promos on promos.id = v.parent_id
            INNER JOIN data_versions ON v.data_id = data_versions.id
            ORDER BY v.name ASC
        ), parents AS (
            SELECT promos.id,
                name,
                parent_id,
                data_id,
                field_type,
                template_id,
                0 AS generation_number,
                promos.id AS promo_id,
                promos.data
            FROM promos

            UNION ALL

            SELECT child.id,
                child.name,
                child.parent_id,
                child.data_id,
                child.field_type,
                child.template_id,
                p.generation_number+1 AS generation_number,
                p.promo_id AS promo_id,
                data_versions.value AS data
            FROM view_field_tree_$field_id child
            INNER JOIN data_versions ON child.data_id = data_versions.id
            JOIN parents p
              ON p.parent_id = child.id
            WHERE child.parent_id IS NOT NULL
        ), columns AS (
            SELECT DISTINCT parents.promo_id, parents.name, parents.generation_number, parents.data
            FROM parents
            UNION
            SELECT DISTINCT children.promo_id, children.name, children.generation_number, children.data
            FROM children
            UNION
            SELECT DISTINCT promos.id, promos.name, promos.generation_number, promos.data
            FROM promos
            ORDER BY 1,3,2
        )


        SELECT columns.promo_id, columns.name, columns.data
        FROM columns;',
        E'$category_sql'
    ) AS ct(id INT $crosstab_columns_str)
WHERE True $filters $search_str
"""

GET_COLUMNS_FROM_REPORT_BY_ID = """
SELECT *
FROM (
    SELECT DISTINCT ON (name) name, field_type, generation_number
    FROM view_report_$report_id
    WHERE NOT field_type LIKE 'template%' AND
          NOT field_type = 'info' AND
          parent_id IS NOT NULL
    ) c
ORDER BY generation_number, name, field_type;
"""

CREATE_MATERIALIZED_VIEW_TO_GET_COLUMN_VALUES_FILTERED_BY_LEAF_CONTAINER_ROWS_QUERY= """
CREATE MATERIALIZED VIEW IF NOT EXISTS view_columns_$view_name AS (
    SELECT * FROM crosstab(
          E'WITH RECURSIVE leaf_containers AS (
                SELECT
                     v.id,
                     v.name,
                     v.parent_id,
                     v.data_id,
                     v.field_type,
                     v.template_id,
                     data_versions.value AS data
                FROM view_report_$report_id v
                INNER JOIN data_versions ON v.data_id = data_versions.id
                WHERE v.id NOT IN (
                    SELECT v.parent_id
                    FROM view_report_$report_id v
                    WHERE v.parent_id IS NOT NULL
                    AND v.field_type = \\'container\\')
                AND v.field_type = \\'container\\'
                AND v.parent_id IS NOT NULL
            ), children AS (
                SELECT leaf_containers.id,
                    name,
                    parent_id,
                    data_id,
                    field_type,
                    template_id,
                    leaf_containers.id AS leaf_container_id,
                    leaf_containers.data
                FROM leaf_containers

                UNION ALL

                SELECT child.id,
                    child.name,
                    child.parent_id,
                    child.data_id,
                    child.field_type,
                    child.template_id,
                    c.leaf_container_id AS leaf_container_id,
                    data_versions.value AS data
                FROM view_report_$report_id child
                INNER JOIN data_versions ON child.data_id = data_versions.id
                JOIN children c
                  ON c.id = child.parent_id
            ), parents AS (
                SELECT leaf_containers.id,
                    name,
                    parent_id,
                    data_id,
                    field_type,
                    template_id,
                    0 AS generation_number,
                    leaf_containers.id AS leaf_container_id,
                    leaf_containers.data
                FROM leaf_containers

                UNION ALL

                SELECT child.id,
                    child.name,
                    child.parent_id,
                    child.data_id,
                    child.field_type,
                    child.template_id,
                    p.generation_number+1 AS generation_number,
                    p.leaf_container_id AS leaf_container_id,
                    data_versions.value AS data
                FROM view_report_$report_id child
                INNER JOIN data_versions ON child.data_id = data_versions.id
                JOIN parents p
                  ON p.parent_id = child.id
                WHERE child.parent_id IS NOT NULL
            ), columns AS (
                SELECT DISTINCT parents.leaf_container_id, parents.name, parents.data
                FROM parents
                UNION
                SELECT DISTINCT children.leaf_container_id, children.name, children.data
                FROM children
                UNION
                SELECT DISTINCT leaf_containers.id, leaf_containers.name, leaf_containers.data
                FROM leaf_containers
                ORDER BY 1,2
            )


            SELECT columns.leaf_container_id, columns.name, columns.data
            FROM columns;',
            E'$category_sql'
        ) AS ct(id INT $crosstab_columns_str)
    WHERE True $filters $search_str
);
"""

GET_COLUMN_VALUES_FILTERED_BY_LEAF_CONTAINER_ROWS_QUERY= """
SELECT * FROM view_columns_$view_name;
"""

GET_PARENT_FIELDS_COLUMNS_FROM_PARENT_BY_ID_WITH_VIEW = """
SELECT name FROM (
    SELECT name, generation_number FROM (
        SELECT DISTINCT ON (name) name, generation_number
        FROM $from_table
        WHERE NOT field_type LIKE 'template%' AND
              NOT field_type = 'info' AND
              parent_id IS NOT NULL
    ) sub1
    $order_by
) sub2;
"""

GET_PARENT_FIELDS_COLUMNS_FROM_PARENT_BY_ID = """
WITH RECURSIVE generation AS (
    SELECT id,
        name,
        parent_id,
        data_id,
        field_type,
        template_id,
        0 AS generation_number
    FROM fields
    WHERE id = $field_id

UNION ALL

    SELECT child.id,
        child.name,
        child.parent_id,
        child.data_id,
        child.field_type,
        child.template_id,
        generation_number+1 AS generation_number
    FROM fields child
    JOIN generation g
      ON g.id = child.parent_id
)

SELECT name FROM (
    SELECT name, generation_number FROM (
        SELECT DISTINCT ON (name) name, generation_number
        FROM generation
        WHERE NOT field_type LIKE 'template%' AND
              NOT field_type = 'info' AND
              parent_id IS NOT NULL
    ) sub1
    $order_by
) sub2;
"""

GET_FIELD_BY_ID_QUERY = """
    SELECT fields.id, fields.name, fields.parent_id, fields.field_type,
           fields.data_id, fields.template_id, data_versions.value AS data
    FROM fields
    INNER JOIN data_versions ON fields.data_id = data_versions.id
    WHERE fields.id = :id
"""

GET_FIELD_DIRECT_CHILDREN_BY_ID_QUERY = """
    SELECT fields.id, fields.name, fields.parent_id, fields.field_type,
           fields.data_id, fields.template_id, data_versions.value AS data
    FROM fields
    INNER JOIN data_versions ON fields.data_id = data_versions.id
    WHERE parent_id = $field_id $filters
    $order_by;
"""

GET_FIELDS_BY_TEMPLATE_ID_QUERY = """
    SELECT fields.id, fields.name, fields.parent_id, fields.field_type,
           fields.data_id, fields.template_id, data_versions.value AS data
    FROM fields
    INNER JOIN data_versions ON fields.data_id = data_versions.id
    WHERE fields.template_id = :template_id
"""

GET_PARENT_TEMPLATES_BY_ID_QUERY = """
    WITH RECURSIVE parents AS (
            SELECT fields.id,
                   name,
                   parent_id,
                   data_id,
                   field_type,
                   template_id,
                   0 AS parents_number
            FROM fields
            WHERE id = :id

            UNION ALL

            SELECT child.id,
                   child.name,
                   child.parent_id,
                   child.data_id,
                   child.field_type,
                   child.template_id,
                   parents_number+1 AS parents_number
            FROM fields child
            JOIN parents g
              ON g.parent_id = child.id
    ), children AS (
        SELECT child.id,
               child.name,
               child.parent_id,
               child.data_id,
               child.field_type,
               child.template_id,
               parents.parents_number
        FROM fields child
        JOIN parents ON parents.id = child.parent_id
    )

    SELECT children.id, children.name, children.parent_id, children.data_id,
           children.field_type, children.template_id, data_versions.value AS data
    FROM children
    INNER JOIN data_versions ON children.data_id = data_versions.id
    WHERE children.id != :id AND children.field_type LIKE 'template%'
    ORDER BY children.parents_number;
"""

GET_ALL_LEAF_CONTAINER_FIELDS = """
    SELECT v.id, v.name, v.parent_id, v.data_id,
           v.field_type, v.template_id, data_versions.value AS data
    FROM view_report_$report_id v
    INNER JOIN data_versions ON v.data_id = data_versions.id
    WHERE v.id NOT IN (SELECT v.parent_id FROM view_report_$report_id v WHERE v.parent_id IS NOT NULL AND (v.field_type = 'container' OR v.field_type = 'container_promo'))
    AND v.field_type = 'container'
    AND v.parent_id IS NOT NULL;
"""

UPDATE_FIELD_BY_ID_QUERY = """
    WITH data_table AS (
        UPDATE data_versions
        SET value = :data
        FROM fields
        WHERE fields.id = :id AND fields.data_id = data_versions.id
        RETURNING data_versions.id, data_versions.value AS data
        )
    UPDATE fields
    SET parent_id = :parent_id,
        field_type = :field_type,
        template_id = :template_id,
        name = :name
    FROM data_table
    WHERE fields.id = :id AND fields.data_id = data_table.id
    RETURNING fields.id, fields.name, fields.parent_id, fields.field_type,
        fields.template_id, data_table.data AS data, fields.data_id;
"""

DELETE_FIELD_BY_ID_QUERY = """
    DELETE FROM fields
    WHERE id = :id
    RETURNING id;
"""


class ContainerPromoColumnNotFoundError(Exception):
    """
    Raised when trying to import CSV fields and the corresponding container_promo
    column to insert into is not found.
    """
    pass


class FieldsRepository(BaseRepository):
    """ "
    All database actions associated with the Field resource
    """

    async def get_nearest_template_container_promo(self, parent_id: int):
        """
        Get first bottom-up template_container_promo if it exists
        """
        parent_templates = await self.db.fetch_all(
            query=GET_PARENT_TEMPLATES_BY_ID_QUERY,
            values={"id": parent_id},
        )
        promo_template = None
        for template in parent_templates:
            if template.field_type == 'template_container_promo':
                promo_template = template
                break;

        return promo_template

    async def create_promo_field(
        self,
        report_id: int,
        parent_id: int,
        name=i18n.t('translate.report.promo'),
        data="",
        external_source_id=None,
        external_source_field_id=None,
        side_effects=True,
        run_refresh=True
    ):
        """
        Create a promo field with default params according to a template if it exists.
        """

        # Get first bottom-up template_container_promo if it exists
        promo_template = await self.get_nearest_template_container_promo(parent_id=parent_id)

        # Create new promo field
        promo_field = FieldCreate(
            name=name,
            data=data,
            external_source_id=external_source_id,
            external_source_field_id=external_source_field_id,
            field_type="container_promo",
            parent_id=parent_id,
        )

        # Use template's name for the new promo field and set the promo
        # field's template_id to the template's id
        if promo_template:
            promo_field.name = promo_template.name
            promo_field.template_id = promo_template.id
            if data == '':
                promo_field.data = promo_template.data


        # Create promo
        new_field = await self.create_field(
            id=report_id,
            new_field=promo_field,
            side_effects=side_effects,
            run_refresh=run_refresh
        )

        return new_field

    async def run_container_side_effects(self, report_id: int, new_field: FieldInDB, run_refresh=True):
        """
        Run side effects for a newly created container field.
        Move parent's fields to the new field.
        """
        if new_field.parent_id == report_id:
            report_repo = ReportsRepository(self.db)
            parent_field = await report_repo.get_report_by_id(id=report_id)
            parent_field_children = parent_field.children
        else:
            query = Template(GET_FIELD_DIRECT_CHILDREN_BY_ID_QUERY)
            query = query.substitute(field_id=new_field.parent_id, filters="", order_by="")
            parent_field_children = await self.db.fetch_all(
                query=query,
            )

        if parent_field_children and "container_promo" in [
            field.field_type for field in parent_field_children
        ]:
            # Move parent's promo fields to the newly created container field
            for field in parent_field_children:
                if field.field_type == "container_promo":
                    field_update = FieldUpdate(parent_id=new_field.id)
                    await self.update_field(
                        report_id=report_id, field_id=field.id, field_update=field_update
                    )

    async def run_container_promo_side_effects(self, report_id: int, field_id: int):
        """
        Run side effects for a newly created container_promo field.
        Populate this field with all linked template fields.
        """
        # Get parent templates
        parent_templates = await self.db.fetch_all(
            query=GET_PARENT_TEMPLATES_BY_ID_QUERY,
            values={"id": field_id},
        )

        # For each template, copy template to promo
        for template in parent_templates:
            if template["field_type"] != "template_container_promo":
                # Copy template to child
                child_field = FieldCreate(**template)
                # Change child's template_id and parent_id
                child_field.template_id = template["id"]
                child_field.field_type = template["field_type"].removeprefix("template_")
                child_field.parent_id = field_id

                # Create child field
                await self.create_field(id=report_id, new_field=child_field)

    async def create_fields_by_batch(self, report_id: int, exported_fields: List[FieldCreate], exported_report_id: int):
        """
        Loop through fields to be imported. Create them by transposing their ids into the current system
        and keep the children and the template_id's connected.
        """

        created_fields = []
        fields_to_retry = []

        def deep_recurse_update_fields(fields, old_id, new_id):
            for field in fields:
                if field.parent_id == old_id:
                    field.parent_id = new_id
                if field.template_id == old_id:
                    field.template_id = new_id

        async def deep_recurse_create_fields(fields, fields_to_retry):
            fields_to_create = fields

            async def process_field(idx, import_field):
                logger.debug('IMPORT: Processing field {}/{}...'.format(idx + 1, len(fields_to_create)))
                # Delete the field's old id
                importing_field = FieldCreate(**import_field.model_dump())

                # Replace field's parent_id to point to the current report_id.
                if importing_field.parent_id == exported_report_id:
                    importing_field.parent_id = report_id

                # Create the field
                created_field = await self.create_field(
                    id=report_id,
                    new_field=importing_field,
                    side_effects=False,
                )
                # If the field creation has failed
                # Put it on the side to retry after other fields have been created
                if created_field is None:
                    fields_to_retry.append(importing_field)
                    return

                created_fields.append(created_field)

                # Update the original field's ids in the importing fields
                try:
                    deep_recurse_update_fields(exported_fields, import_field.id, created_field.id)
                except Exception as e:
                    logger.warn(f"IMPORT: Failed to update created field: {created_field}")
                    raise e

            continue_retry = True
            first_time_retry = True
            number_of_fields_to_retry = 0

            # Process fields to import.
            # Then, run again fields processing on fields which failed.
            # Loop through the failed fields while their number diminishes.
            # Stop the loop if no more fields are imported.
            while continue_retry:

                for idx, import_field in enumerate(fields_to_create):
                    await process_field(idx, import_field)

                # Retry in any case at least once if some
                # fields have failed
                if first_time_retry and len(fields_to_retry) > 0:
                    continue_retry = True
                    first_time_retry = False
                    number_of_fields_to_retry = len(fields_to_retry)
                    fields_to_create = fields_to_retry
                    fields_to_retry = []
                # Retry only if the previous trial has succeded to import
                # some field
                elif len(fields_to_retry) < number_of_fields_to_retry:
                    continue_retry = True
                    number_of_fields_to_retry = len(fields_to_retry)
                    fields_to_create = fields_to_retry
                    fields_to_retry = []
                else:
                    continue_retry = False

            return fields_to_retry


        logger.info('IMPORT: Starting importing {} fields to report {}...'.format(len(exported_fields), report_id))

        fields_to_retry = await deep_recurse_create_fields(
            fields=exported_fields,
            fields_to_retry=fields_to_retry
        )

        # Refresh materialized views of the report
        await self.drop_report_materialized_views(report_id=id)

        status = 'succeeded'
        if len(created_fields) < len(exported_fields):
            nb_missing_fields = len(exported_fields) - len(created_fields)
            logger.error('IMPORT: Failed to import {} fields to report {}.'.format(nb_missing_fields, report_id))
            status = 'partially_failed'

        logger.info('IMPORT: Finished importing {} fields to report {}.'.format(len(created_fields), report_id))

        return {
            'status': status,
            'failed_imported_fields': [jsonable_encoder(item) for item in fields_to_retry]
        }

    async def create_field(
        self,
        *,
        id: int,
        new_field: FieldCreate,
        side_effects=True,
        run_refresh=False,
    ) -> FieldInDB:
        """
        Create a new field in a report.
        1. Check the parameters validity
        2. Create the new field
        3. Run side-effects according to the created field
        """
        report_repo = ReportsRepository(self.db)
        report = await report_repo.get_report_by_id(id=id)

        if not report:
            return None

        # Check that the parent field is a container if it's not the report
        parent_field = report
        if new_field.parent_id != id:
            parent_field = await self.get_field_by_id(
                id=id, field_id=new_field.parent_id
            )

            if not parent_field:
                return None

            if not parent_field.field_type.startswith("container"):
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.report.parent_field_should_be_container_error'),
                )

        # If the new field is an info field, check that there is no other one
        # which already exist with the same parent_id.
        if new_field.field_type == 'info':
            info_fields = await self.db.fetch_all(
                query=CHECK_INFO_FIELD_SAME_PARENT_QUERY,
                values={'parent_id': new_field.parent_id},
            )
            if len(info_fields) >= 1:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.report.only_one_info_field_per_parent_can_be_created'),
                )

        # Create the new field
        async with self.db.transaction():
            # Defer constraint on foreign key
            await self.db.execute(query=DEFER_FOREIGN_KEY_CONSTRAINTS)

            # Create field with associated data
            new_field = await self.db.fetch_one(
                query=CREATE_FIELD_QUERY,
                values={**new_field.model_dump()},
            )
            new_field = FieldInDB(**new_field)

        # Manage side effects according to fixture mode
        if (
            not "FIXTURE_MODE" in os.environ or not os.environ["FIXTURE_MODE"] == "True"
        ) and side_effects == True:

            # Create new fields based on a template
            if new_field.field_type.startswith("template_") and new_field.field_type != "template_container_promo":
                utils_repo = UtilsRepository(self.db)

                sub_promo_fields = (
                    await self.get_flatten_recursive_children_from_parent_by_id(
                        report_id=id,
                        field_id=new_field.parent_id,
                        filters={"field_type": "container_promo"},
                    )
                )

                # For each sub_promo copy the template
                for sub_promo in [FieldInDB(**promo) for promo in sub_promo_fields]:
                    # Avoid to create a child for the template itself and for other fields than containers
                    if sub_promo.id != new_field.id:
                        # Copy template to child
                        child_field = FieldCreate(**new_field.model_copy().model_dump())
                        # Change child's template_id and parent_id
                        child_field.template_id = new_field.id
                        child_field.field_type = new_field.field_type.removeprefix(
                            "template_"
                        )
                        child_field.parent_id = sub_promo.id
                        # Create child field
                        await self.create_field(id=id, new_field=child_field)
            # Move parent's fields to the new container
            elif (
                new_field.field_type == "container" and report.name != "ReportTemplate"
            ):
                await self.run_container_side_effects(report_id=id, new_field=new_field, run_refresh=run_refresh)
            # Inherit fields from parent template fields
            elif new_field.field_type == "container_promo":
                await self.run_container_promo_side_effects(
                    report_id=id, field_id=new_field.id
                )

        # Run only once, at the end of the field creation process
        if run_refresh:
            # Refresh materialized views of the report
            await self.drop_report_materialized_views(report_id=id)

        # Return created full field
        return new_field

    async def search_container_promo_column(
        self,
        field_id,
        columns,
        previous_column=None
    ):
        """
        Search recursively each column until the last found and check if it is
        a column for container_promo
        """
        column = next(columns)

        # Ignore id column
        if column[0] == 'id':
            column = next(columns)

        # Ignore column with empty data
        while column[1] in (None, '') or not column[1].strip():
            try:
                column = next(columns)
            except StopIteration as e:
                raise Exception(i18n.t(
                    'translate.report.no_column_after_having_removed_blank_error'
                ))

        query = Template(GET_FIELD_DIRECT_CHILDREN_BY_ID_QUERY)
        field_name = column[0].replace("'","\\'")
        field_value = column[1].replace("'","\\'")
        query = query.substitute(
            field_id=field_id,
            filters=f"AND fields.name = E'{field_name}' AND data_versions.value = E'{field_value}'",
            order_by=""
        )
        parent_field_children = await self.db.fetch_all(
            query=query,
        )

        # This case should normally never run
        if len(parent_field_children) > 1 and parent_field_children[0].field_type == 'container':
            raise Exception('Error while searching for columns children at import: some children are duplicated under same parent.')
        # If there is no existing parent column to search for anymore,
        # or if its children are container_promo fields, then we
        # consider the previous column as the container to import into.
        elif len(parent_field_children) == 0 or parent_field_children[0].field_type == 'container_promo':
            # Get info about template_container_promo
            promo_template = await self.get_nearest_template_container_promo(parent_id=field_id)
            if promo_template is not None:
                container_promo_name = promo_template.name
            else:
                container_promo_name = i18n.t('translate.report.promo')

            # Check the column is named as a container_promo
            if column[0] == container_promo_name:
                return previous_column, column
            else:
                raise ContainerPromoColumnNotFoundError(i18n.t(
                    'translate.report.container_promo_column_not_found_error',
                    column_name=column[0]
                ))
        else:
            previous_column = parent_field_children[0]
            return await self.search_container_promo_column(parent_field_children[0].id, columns, previous_column)

    async def bulk_import_fields(
        self,
        *,
        report_id: int,
        source_id: str,
        local_filename: str,
        filename: str,
        annotated_imported_csv_filename: str,
    ) -> {}:
        """
        Load a CSV file and import all lines as fields in a report.
        """

        import_summary = {
            'filename': filename,
            'report_id': report_id,
            'annotated_imported_csv_filename': annotated_imported_csv_filename,
            'log': [],
            'total_processed': 0
        }

        annotated_imported_csv_file_localpath = '{}/{}'.format(
            APP_EXPORTS_FOLDER,
            os.path.basename(annotated_imported_csv_filename)
        )

        with (open(local_filename, 'r') as csv_file_to_import,
            open(annotated_imported_csv_file_localpath, 'w') as annotated_imported_csv_file):
            # Get imported file headers to add to summary
            header_fields = [
                i18n.t('translate.report.pr_import_status'),
                i18n.t('translate.report.pr_import_error_code'),
                i18n.t('translate.report.pr_import_explanation'),
                'id'
            ]
            local_file = open(local_filename, 'r')
            csv_reader = csv.reader(local_file)
            header_row = next(csv_reader)
            header_fields.extend(header_row)
            local_file.close()
            # Remove duplicates
            header_fields = list(dict.fromkeys(header_fields))
            # Set up CSV writer
            annotated_csv_writer = csv.DictWriter(
                annotated_imported_csv_file,
                fieldnames=header_fields
            )
            annotated_csv_writer.writeheader()

            # Reinitiate reader to start reading from start again
            csv_reader = csv.DictReader(csv_file_to_import)

            def write_import_log(row, import_id, import_status, import_error_code, import_reason):
                row['id'] = import_id
                row[i18n.t('translate.report.pr_import_status')] = import_status
                row[i18n.t('translate.report.pr_import_error_code')] = import_error_code
                row[i18n.t('translate.report.pr_import_explanation')] = import_reason
                annotated_csv_writer.writerow(row)

                import_summary['log'].append({
                    'id': row['id'],
                    'status': row[i18n.t('translate.report.pr_import_status')],
                    'error_code': row[i18n.t('translate.report.pr_import_error_code')],
                    'explanation': row[i18n.t('translate.report.pr_import_explanation')]
                })

            for row in csv_reader:
                import_summary['total_processed'] += 1

                row_id = row.get('id', None)
                # 1. If no id defined, avoid existing_field check
                if 'id' not in row or row['id'] == '':
                    existing_field = False
                # 2. Check that source + id don't exist yet in this report
                # If source_id is the report itself, check the internal ids
                elif source_id == f"report_{report_id}":
                    existing_field = await self.db.fetch_one(
                        query=GET_FIELD_BY_ID_QUERY, values={"id": int(row['id'])}
                    )
                # Otherwise check the external_source_field_id
                else:
                    existing_field = await self.db.fetch_one(
                        query=SEARCH_SAME_EXTERNAL_SOURCE_FROM_PARENT_BY_ID,
                        values={
                            'id': report_id,
                            'external_source_id': source_id,
                            'external_source_field_id': row_id
                        },
                    )

                # We ignore and don't try to import the field if it already exists
                if existing_field:
                    continue

                # 2. Retrieve recursively each column one per one starting with the first with the report as parent
                # until we get the last existing container. We consider this container to receive the new container_promo.
                columns = iter(row.items())
                try:
                    last_container_column, column_to_create = await self.search_container_promo_column(report_id, columns)
                except Exception as e:
                    write_import_log(
                        row,
                        row_id,
                        i18n.t('translate.report.ignored'),
                        'search_container_promo_column_error',
                        str(e)
                    )
                    continue

                # 3. Create a container_promo with last_container_column as parent and with column_to_create data
                # If it is own source_id, don't set the external source_id
                if source_id == f"report_{report_id}":
                    new_field = await self.create_promo_field(
                        report_id=report_id,
                        parent_id=last_container_column.id,
                        name=column_to_create[0],
                        data=column_to_create[1]
                    )
                else:
                    new_field = await self.create_promo_field(
                        report_id=report_id,
                        parent_id=last_container_column.id,
                        name=column_to_create[0],
                        data=column_to_create[1],
                        external_source_id=source_id,
                        external_source_field_id=row_id
                    )

                # Declare new id of created field if there was none
                if 'id' not in row or row['id'] == '':
                    row_id = new_field.id

                # 4. Retrieve children fields of the newly created container_promo
                query = Template(GET_FIELD_DIRECT_CHILDREN_BY_ID_QUERY)
                query = query.substitute(
                    field_id=new_field.id,
                    filters="",
                    order_by=""
                )
                new_field_children = await self.db.fetch_all(
                    query=query,
                )

                # 5. Search for the children's name in the CSV columns and complete with the data if it exists
                for child in new_field_children:
                    if child['name'] in row:
                        try:
                            await self.update_field(
                                report_id=report_id,
                                field_id=child['id'],
                                field_update=FieldUpdate(data=row[child['name']])
                            )
                        except Exception as e:
                            # Delete the container_promo field (with its children)
                            # to allow to retry later
                            await self.delete_field_by_id(
                                id=report_id,
                                field_id=new_field.id
                            )
                            write_import_log(
                                row,
                                row_id,
                                i18n.t('translate.report.ignored'),
                                'invalid_value_error',
                                f"Invalid value '{row[child['name']]}' for '{child['name']}'"
                            )
                            break
                # else clause is called if the previous loop ran ok.
                else:
                    # Log the processing result in the summary
                    write_import_log(
                        row,
                        row_id,
                        i18n.t('translate.report.imported'),
                        None,
                        ''
                    )
                    continue


            logger.info(f"Finished importing fields into report {report_id}. Processed {import_summary['total_processed']} lines.")

        # Finally delete the file after having finished to import it
        os.remove(local_filename)

        return import_summary

    async def create_database_functions(
        self,
    ):
        """
        SETUP TASK.
        Setup reusable database functions:
        - Native Postgresql tablefunc extension for using the crosstab function
        - Function to be run recursively and get nested fields
        - Function to be run recursively and get nested fields with a limited depth
        """

        logger.info("SETUP: Ensure needed PostgreSQL functions are loaded")

        # Create tree function for Postgresql
        class CustomTemplate(Template):
            delimiter = '#'

        query = INSTALL_TABLEFUNC_FUNCTION_QUERY
        await self.db.execute(query=query)

        for depth_name, depth_limit_str in [("unlimited", "True"), ("lazy_loading", "%3$s < 2")]:
            for order_by, order_by_str in [("order_by_id", "ORDER BY id ASC"), ("order_by_name", "ORDER BY generation_number ASC, name ASC, data ASC")]:
                function_name="{}_{}".format(depth_name, order_by)
                query = CustomTemplate(CREATE_TREE_FUNCTION_QUERY)
                query = query.substitute(depth_limit=depth_limit_str, order_by_str=order_by_str, function_name=function_name)
                await self.db.execute(query=query)

    async def fix_database_default_names(
        self,
    ):
        """
        MAINTENANCE TASK.
        Set default name to container_promo fields with empty name.
        Allows to fix legacy errors when a container field name was empty
        """

        UPDATE_DEFAULT_PROMO_NAME_QUERY = """
        UPDATE fields
        SET name = '$name'
        WHERE field_type = 'container_promo' AND name = '';
        """

        logger.info("MAINTENANCE: Updating container_promo fields default names")
        query = Template(UPDATE_DEFAULT_PROMO_NAME_QUERY)
        query = query.substitute(name=i18n.t('translate.report.promo'))
        await self.db.execute(query=query)

    async def clean_database_views(
        self,
    ):
        """
        MAINTENANCE TASK.
        Drop PostgreSQL views which are usually automatically recreated.
        Allows to clean sometimes the database and to avoid bugs when the views columns are changing.
        """

        DROPPABLE_VIEWS_QUERY = """
        SELECT views.name FROM (SELECT
          c.relname as "name"
        FROM pg_catalog.pg_class c
             LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE c.relkind IN ('r','p','v','m','S','f','')
              AND n.nspname <> 'pg_catalog'
              AND n.nspname <> 'information_schema'
              AND n.nspname !~ '^pg_toast'
          AND pg_catalog.pg_table_is_visible(c.oid)
          AND c.relname LIKE 'view_field_tree_%') as views;
        """

        DROP_VIEW = "DROP VIEW $name CASCADE;"

        DROPPABLE_MATERIALIZED_VIEWS_QUERY = """
        SELECT views.name FROM (SELECT
          c.relname as "name"
        FROM pg_catalog.pg_class c
             LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE c.relkind IN ('r','p','v','m','S','f','')
              AND n.nspname <> 'pg_catalog'
              AND n.nspname <> 'information_schema'
              AND n.nspname !~ '^pg_toast'
          AND pg_catalog.pg_table_is_visible(c.oid)
          AND c.relname LIKE 'view_columns_%') as views;
        """

        DROP_MATERIALIZED_VIEW = "DROP MATERIALIZED VIEW $name CASCADE;"

        views = await self.db.fetch_all(DROPPABLE_VIEWS_QUERY)
        views = [view.name for view in views]

        logger.info("MAINTENANCE: Preparing to drop following views: {}".format(views))
        for view in views:
            query = Template(DROP_VIEW)
            query = query.substitute(name=view)
            logger.info(query)
            await self.db.execute(query=query)

        materialized_views = await self.db.fetch_all(DROPPABLE_MATERIALIZED_VIEWS_QUERY)
        materialized_views = [view.name for view in materialized_views]

        logger.info("MAINTENANCE: Preparing to drop following materialized views: {}".format(materialized_views))
        for view in materialized_views:
            query = Template(DROP_MATERIALIZED_VIEW)
            query = query.substitute(name=view)
            logger.info(query)
            await self.db.execute(query=query)

    async def clean_export_files(
        self,
    ):
        """
        MAINTENANCE TASK.
        Empty the export files folder to keep a low footprint on disk.
        """

        logger.info("MAINTENANCE: Cleaning content of exports folder")
        for path in Path(APP_EXPORTS_FOLDER).glob("*"):
            if path.is_file():
                path.unlink()

    async def drop_report_materialized_views(
        self,
        report_id: int
    ):
        """
        POST-PROCESSING.
        Drop all materialized views for a report.

        Allows to avoid conflicts which may happen at view refresh. For example when it contains
        a dynamic query to update columns, but the crosstab columns are hard-written.
        """

        MATERIALIZED_VIEWS_QUERY = """
        SELECT views.name FROM (SELECT
          c.relname as "name"
        FROM pg_catalog.pg_class c
             LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE c.relkind IN ('r','p','v','m','S','f','')
              AND n.nspname <> 'pg_catalog'
              AND n.nspname <> 'information_schema'
              AND n.nspname !~ '^pg_toast'
          AND pg_catalog.pg_table_is_visible(c.oid)
          AND (c.relname LIKE 'view_preprocessed_$report_id%' OR c.relname LIKE 'view_columns_$report_id%')) as views;
        """

        DROP_MATERIALIZED_VIEW_QUERY= """
        DROP MATERIALIZED VIEW $name;
        """

        query = Template(MATERIALIZED_VIEWS_QUERY)
        query = query.substitute(report_id=report_id)
        views = await self.db.fetch_all(query)
        views = [view.name for view in views]

        logger.info("POST-PROCESSING: Dropping following materialized views: {}".format(views))
        for view in views:
            query = Template(DROP_MATERIALIZED_VIEW_QUERY)
            query = query.substitute(name=view)
            await self.db.execute(query=query)

    async def prepare_field_tree_processing_variables(
        self,
        *,
        report_id: int,
        field_id: int,
        params: dict = None,
        filters: list = None,
        no_cache: bool = False,
    ) -> dict:
        """
        Prepare the parameter's strings for processing a field_tree which can be reused
        in several queries.
        Create the field tree PostgreSQL view.

        Parameters:
            - no_cache: Don't create SQL materialized views which are only useful if requested enough times.
        """

        # Take into account params for query
        order_by_str = "ORDER BY id ASC"
        order_by = "order_by_id"
        if params and "order_by" in params and params["order_by"] == "name":
            order_by_str = "ORDER BY generation_number ASC, name ASC, data ASC"
            order_by = "order_by_name"

        # Setup lazy loading parameters
        depth_limit_str = "True"
        function_name="{}_{}".format("unlimited", order_by)
        if params and "lazy_loading" in params and params["lazy_loading"] is True:
            depth_limit_str = "_depth < 2"
            function_name="{}_{}".format("lazy_loading", order_by)

        fetching_id = field_id
        if params and "lazy_loading_from" in params and params["lazy_loading_from"] is not None:
            fetching_id = params["lazy_loading_from"]

        if not no_cache:
            # Run query to create the field's tree view
            query = Template(CREATE_FIELD_TREE_VIEW_QUERY)
            query = query.substitute(field_id=field_id)
            try:
                await self.db.execute(query=query)
            # Exceptions may happen with race conditions. We ignore it as it has no impact.
            except UniqueViolationError:
                pass

            # Get parent column names for all rows.
            # They will be used to be filtered on in the crosstab function.
            # Use the created view to make the request
            query = Template(GET_PARENT_FIELDS_COLUMNS_FROM_PARENT_BY_ID_WITH_VIEW)
            query = query.substitute(from_table='view_field_tree_{}'.format(field_id), order_by='ORDER BY generation_number, name')
            parent_columns = await self.db.fetch_all(query=query)
        else:
            # Get parent column names for all rows.
            # They will be used to be filtered on in the crosstab function.
            query = Template(GET_PARENT_FIELDS_COLUMNS_FROM_PARENT_BY_ID)
            query = query.substitute(field_id=field_id, order_by='ORDER BY generation_number, name')
            parent_columns = await self.db.fetch_all(query=query)

        # Set search string
        search_str = ""
        if params and "search" in params and params["search"] is not None:
            concat_columns_str = ""
            for column in parent_columns:
                if concat_columns_str != "":
                    concat_columns_str += ","
                concat_columns_str += "\"{}\"".format(column.name)
            search_str = "AND CONCAT_WS(' ', {}) ILIKE '%{}%'".format(concat_columns_str, params["search"].replace("'",'\\' + "'"))

        # Take into account filters for query
        filters_str = ""
        if filters:
            # Sort filters to merge on field's names
            sorted_filters = {}
            for filter in filters:
                filter = filter.model_dump()
                # Only allow to filter on columns which exist
                if filter['field_name'] in [column.name for column in parent_columns]:
                    if filter['field_name'] not in sorted_filters:
                        sorted_filters[filter['field_name']] = []
                    sorted_filters[filter['field_name']].append(filter)

            # Format filter string only they are some left
            if len(sorted_filters.keys()) > 0:
                # Open all complementary filters expression
                filters_str = " AND ("
                for idx, filter_group in enumerate(sorted_filters):
                    if idx != 0:
                        filters_str += " AND"

                    filter_criteria_str = ""
                    for filter_criteria in sorted_filters[filter_group]:

                        if filter_criteria_str != "":
                            filter_criteria_str += " OR"

                        filter_criteria_str += " \"{}\" = e'{}'".format(
                            filter_criteria["field_name"].replace("'",'\\' + "'"),
                            filter_criteria["field_data"].replace("'",'\\' + "'")
                        )

                    filters_str += filter_criteria_str

                # Close all filters expression
                filters_str += ")"

        crosstab_columns_str = ""
        for column in parent_columns:
            crosstab_columns_str += ", \"{}\" TEXT".format(column.name)

        category_sql = Template(GET_PARENT_FIELDS_COLUMNS_FROM_PARENT_BY_ID_WITH_VIEW).substitute(from_table='view_field_tree_{}'.format(field_id), order_by='ORDER BY generation_number, name').replace("'",'\\' + "'")

        hashed_params=hashlib.md5(filters_str.encode()+search_str.encode()+order_by_str.encode()).hexdigest()

        view_name = str(report_id) + '_' + str(field_id) + '_' + hashed_params

        return {
            'category_sql': category_sql,
            'crosstab_columns_str': crosstab_columns_str,
            'depth_limit_str': depth_limit_str,
            'fetching_id': fetching_id,
            'field_id': field_id,
            'filters_str': filters_str,
            'function_name': function_name,
            'hashed_params': hashed_params,
            'order_by_str': order_by_str,
            'parent_columns': parent_columns,
            'search_str': search_str,
            'view_name': view_name
        }

    async def get_csv_file_containing_children_from_parent_by_id(
        self,
        *,
        report_id: int,
        field_id: int,
        params: dict = None,
        filters: list = None,
    ) -> str:
        """
        Get the full list of children with headers and export it to a CSV file.
        Each container_promo = one line in the CSV file instead of
        each leaf container = one line in the JSON export.
        Returns the path to the CSV file.
        """

        prepared_params = await self.prepare_field_tree_processing_variables(
            report_id=report_id,
            field_id=field_id,
            params=params,
            filters=filters
        )

        # Run query to get list of fields
        query = Template(GET_FIELDS_CSV_FROM_PARENT_BY_ID)
        query = query.substitute(
            field_id=prepared_params['field_id'],
            view_name=prepared_params['view_name'],
            category_sql=prepared_params['category_sql'],
            order_by=prepared_params['order_by_str'],
            crosstab_columns_str=prepared_params['crosstab_columns_str'],
            search_str=prepared_params['search_str'],
            filters=prepared_params['filters_str']
        )

        DB_URL = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else str(DATABASE_URL)
        con = await asyncpg.connect(dsn=DB_URL)

        exports_folder = APP_EXPORTS_FOLDER
        filename = '{}/export_{}.csv'.format(exports_folder, prepared_params['hashed_params'])
        await con.copy_from_query(
            query,
            output=filename,
            format='csv',
            header=True,
        )

        return filename

    async def prepare_preprocessed_fields_view_from_parent_by_id(
        self,
        *,
        report_id: int,
        field_id: int,
        params: dict = None,
        filters: list = None,
    ) -> FieldInDB:
        """
        Create SQL views with processed fields.
        If a search query or filters are given, rearrange the fields as rows with `container_promo` fields and process them through filters.
        """

        prepared_params = await self.prepare_field_tree_processing_variables(
            report_id=report_id,
            field_id=field_id,
            params=params,
            filters=filters
        )

        # Run query to create a preprocessed view
        if prepared_params['search_str'] != '' or prepared_params['filters_str'] != '':
            query = Template(CREATE_PREPROCESSED_BY_LEAF_CONTAINER_ROWS_VIEW_QUERY)
            query = query.substitute(
                field_id=prepared_params['field_id'],
                view_name=prepared_params['view_name'],
                category_sql=prepared_params['category_sql'],
                order_by=prepared_params['order_by_str'],
                crosstab_columns_str=prepared_params['crosstab_columns_str'],
                search_str=prepared_params['search_str'],
                filters=prepared_params['filters_str']
            )
        else:
            query = Template(CREATE_PREPROCESSED_VIEW_QUERY)
            query = query.substitute(
                field_id=prepared_params['field_id'],
                view_name=prepared_params['view_name'],
                order_by=prepared_params['order_by_str']
            )
        try:
            await self.db.execute(query=query)
        # Exceptions may happen with race conditions. We ignore it as it has no impact.
        except UniqueViolationError:
            pass

        return {
            'fetching_id': prepared_params['fetching_id'],
            'function_name': prepared_params['function_name'],
            'view_name': prepared_params['view_name']
        }


    async def get_fields_from_parent_by_id(
        self,
        *,
        report_id: int,
        field_id: int,
        params: dict = None,
        filters: list = None,
        nested: bool = False,
        no_cache: bool = False,
    ) -> FieldInDB:
        """
        If a search query or filters are given, rearrange the fields as rows with `container_promo` fields and process them through filters.
        Finally, get parent and all its fields as a nested JSON object.

        Parameters:
            - no_cache: Don't create SQL materialized views which are only useful if requested enough times.
        """

        if not nested and no_cache:
            preprocessed_params = await self.prepare_field_tree_processing_variables(
                report_id=report_id,
                field_id=field_id,
                params=params,
                filters=filters,
                no_cache=True
            )
        else:
            preprocessed_params= await self.prepare_preprocessed_fields_view_from_parent_by_id(
                report_id=report_id,
                field_id=field_id,
                params=params,
                filters=filters
            )

        # Run query to get nested fields
        if nested:
            if no_cache:
                query = Template(GET_NESTED_FIELDS_FROM_PARENT_BY_ID)
                query = query.substitute(
                    function_name=preprocessed_params['function_name'],
                    field_id=preprocessed_params['fetching_id']
                )
            else:
                query = Template(GET_NESTED_PROCESSED_FIELDS_FROM_PARENT_BY_ID)
                query = query.substitute(
                    view_name=preprocessed_params['view_name'],
                    function_name=preprocessed_params['function_name'],
                    field_id=preprocessed_params['fetching_id']
                )

            field = await self.db.fetch_one(query=query)

            if not field:
                return None

            # Load JSON result
            fields = json.loads(field._mapping['tree'])
        else:
            if no_cache:
                query = Template(GET_FLAT_FIELDS_FROM_PARENT_BY_ID)
                query = query.substitute(
                    field_id=preprocessed_params['field_id'],
                    order_by=preprocessed_params['order_by_str'],
                )
            else:
                query = Template(GET_FLAT_PROCESSED_FIELDS_FROM_PARENT_BY_ID)
                query = query.substitute(
                    view_name=preprocessed_params['view_name'],
                )

            fields = await self.db.fetch_all(query=query)

        return fields

    async def get_nested_children_from_parent_by_id(
        self,
        *,
        report_id: int,
        field_id: int,
        params: dict = None,
        filters: list = None,
    ) -> FieldInDB:
        """
        Get all children fields from parent as a nested JSON object.
        """
        field = await self.get_fields_from_parent_by_id(
            report_id=report_id,
            field_id=field_id,
            params=params,
            filters=filters,
            nested=True
        )

        if not field or not 'children' in field or field['children'] is None:
            return []

        return field['children']

    async def get_flatten_recursive_children_from_parent_by_id(
        self,
        *,
        report_id: int,
        field_id: int,
        params: dict = None,
        filters: dict = None,
    ) -> FieldInDB:
        report_repo = ReportsRepository(self.db)
        report = await report_repo.get_report_by_id(id=report_id)

        if not report:
            return None

        # Take into account filters for query
        filters_str = ""
        if filters:
            # Remove unset filters
            filters = {
                key: value for key, value in filters.items() if value is not None
            }
            if "field_type" in filters:
                filters_str = filters_str + " AND generation.field_type = :field_type"
            if "name" in filters:
                filters_str = filters_str + " AND generation.name = :name"
            if "data" in filters:
                filters_str = filters_str + " AND data_versions.value = :data"

        # Take into account params for query
        order_by_str = "ORDER BY generation.id ASC"
        if params and "order_by" in params and params["order_by"] == "name":
            order_by_str = "ORDER BY generation.name ASC, data ASC"

        # Apply filters on query string
        query = Template(GET_FLATTENED_FIELDS_FROM_PARENT_BY_ID)
        query = query.substitute(filters=filters_str, order_by=order_by_str)

        fields = await self.db.fetch_all(
            query=query, values={"id": field_id, **(filters or {})}
        )
        if not fields:
            return []

        if (
            params
            and "include_children" in params
            and params["include_children"] is True
        ):
            all_children = []
            for index, field in enumerate(fields):
                field = FieldInDB(**field)
                if field.id == field_id:
                    fields.pop(index)
                else:
                    children = (
                        await self.get_flatten_recursive_children_from_parent_by_id(
                            report_id=report_id, field_id=field.id, params=params
                        )
                    )
                    all_children += children
            fields += all_children

        return fields

    async def get_columns_from_report_by_id(
        self,
        *,
        report_id: int,
        filters: list,
        params: dict,
    ) -> List[ColumnPublic]:
        """
        Return list of columns of a report.
        It allows to the front-end to list the columns on which a report may be filtered on.
        Filters should be able to be applied on this function to correspond with the report's fields return.
        """
        report_repo = ReportsRepository(self.db)
        report = await report_repo.get_report_by_id(id=report_id)

        if not report:
            return None

        # Get columns on which filters can be applied
        # They are sorted by their depth in the report
        query = Template(GET_COLUMNS_FROM_REPORT_BY_ID)
        query = query.substitute(report_id=report_id)
        columns_list = await self.db.fetch_all(query=query)

        # Set search string
        search_str = ""
        if params and "search" in params and params["search"] is not None:
            concat_columns_str = ""
            for column in columns_list:
                if concat_columns_str != "":
                    concat_columns_str += ","
                concat_columns_str += "\"{}\"".format(column.name)
            search_str = "AND CONCAT_WS(' ', {}) ILIKE '%{}%'".format(concat_columns_str, params["search"].replace("'",'\\' + "'"))

        # Take into account filters for query
        filters_str = ""
        if filters:
            # Sort filters to merge on field's names
            sorted_filters = {}
            for filter in filters:
                filter = filter.model_dump()
                # Only allow to filter on columns which exist
                if filter['field_name'] in [column.name for column in columns_list]:
                    if filter['field_name'] not in sorted_filters:
                        sorted_filters[filter['field_name']] = []
                    sorted_filters[filter['field_name']].append(filter)

            # Format filter string only they are some left
            if len(sorted_filters.keys()) > 0:
                # Open all complementary filters expression
                filters_str = " AND ("
                for idx, filter_group in enumerate(sorted_filters):
                    if idx != 0:
                        filters_str += " AND"

                    filter_criteria_str = ""
                    for filter_criteria in sorted_filters[filter_group]:

                        if filter_criteria_str != "":
                            filter_criteria_str += " OR"

                        filter_criteria_str += " \"{}\" = e'{}'".format(
                            filter_criteria["field_name"].replace("'",'\\' + "'"),
                            filter_criteria["field_data"].replace("'",'\\' + "'")
                        )

                    filters_str += filter_criteria_str

                # Close all filters expression
                filters_str += ")"

        # Create a Crosstab view from the report view and apply filters on it
        crosstab_columns_str = ""
        crosstab_columns = sorted([column.name for column in columns_list])
        for column in crosstab_columns:
            crosstab_columns_str += ", \"{}\" TEXT".format(column)

        # Run query to create a preprocessed view
        hashed_params=hashlib.md5(filters_str.encode()+search_str.encode()).hexdigest()
        view_name = str(report_id) + '_' + hashed_params
        query = Template(CREATE_MATERIALIZED_VIEW_TO_GET_COLUMN_VALUES_FILTERED_BY_LEAF_CONTAINER_ROWS_QUERY)
        query = query.substitute(
            report_id=report_id,
            category_sql=Template(GET_PARENT_FIELDS_COLUMNS_FROM_PARENT_BY_ID_WITH_VIEW).substitute(from_table='view_report_{}'.format(report_id), order_by='ORDER BY name').replace("'",'\\' + "'"),
            crosstab_columns_str=crosstab_columns_str,
            filters=filters_str,
            search_str=search_str,
            view_name=view_name
        )
        try:
            await self.db.execute(query=query)
        # Exceptions may happen with race conditions. We ignore it as it has no impact.
        except UniqueViolationError:
            pass

        query = Template(GET_COLUMN_VALUES_FILTERED_BY_LEAF_CONTAINER_ROWS_QUERY)
        query = query.substitute(
            view_name=view_name
        )

        filtered_columns = await self.db.fetch_all(query=query)

        # Create the columns objects with the values in each column
        # Avoid duplicates by using a dict to store column values.
        columns = []
        for column in columns_list:
            completed_column = {
                "field_name": column.name,
                "field_type": column.field_type,
                "column_values" : []
            }
            column_values = {}

            for row in filtered_columns:
                if column.name in row and row[column.name] is not None and row[column.name] != '':
                    column_values[row[column.name]] = True

            completed_column["column_values"] = sorted(list(column_values.keys()))

            if len(completed_column["column_values"]) > 0:
                columns.append(completed_column)

        return columns

    async def get_field_by_id(
        self, *, id: int, field_id: int, params: dict = None, filters: dict = None
    ) -> FieldInDB:
        """
        Get only a field, without its children, by its id. Reports are forbidden.
        """
        report_repo = ReportsRepository(self.db)
        report = await report_repo.get_report_by_id(id=id)

        if not report:
            raise HTTPException(
                status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
            )

        field = await self.db.fetch_one(
            query=GET_FIELD_BY_ID_QUERY, values={"id": field_id}
        )
        if not field:
            return None

        try:
            field_object = FieldInDB(**field)
        except ValidationError as e:
            if (
                e.errors()[0]["loc"][0] == "parent_id"
                and e.errors()[0]["type"] == "int_type"
            ):
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.policies.report_ids_not_allowed_only_fields_error'),
                )
            else:
                raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=e.json())

        return field_object


    async def get_field_with_children_by_id(
        self, *, report_id:int, field_id: int, params: dict = None, filters: list = None, no_cache: bool = False
    ) -> FieldInDB:
        """
        Get field and all its nested children by id. Reports are forbidden.
        """
        report_repo = ReportsRepository(self.db)
        report = await report_repo.get_report_by_id(id=report_id)

        if not report:
            raise HTTPException(
                status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
            )

        field = await self.get_fields_from_parent_by_id(
            report_id=report_id,
            field_id=field_id,
            params=params,
            filters=filters,
            nested=True,
            no_cache=no_cache
        )

        if not field:
            return None

        try:
            field_object = FieldInDB(**field)
        except ValidationError as e:
            if (
                e.errors()[0]["loc"][0] == "parent_id"
                and e.errors()[0]["type"] == "type_error.none.not_allowed"
            ):
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.policies.report_ids_not_allowed_only_fields_error'),
                )
            else:
                raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=e.json())

        return field_object

    async def get_field_all_children_by_id(
        self, *, id: int, field_id: int, params: dict = None, filters: dict = None
    ) -> List[FieldInDB]:
        report_repo = ReportsRepository(self.db)
        report = await report_repo.get_report_by_id(id=id)

        if not report:
            raise HTTPException(
                status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
            )

        # Take into account filters for query
        filters_str = ""
        if filters:
            if "field_type" in filters:
                filters_str = filters_str + " AND fields.field_type = :field_type"

        # Take into account params for query
        order_by_str = "ORDER BY fields.id ASC"
        if params and "order_by" in params and params["order_by"] == "name":
            order_by_str = "ORDER BY fields.name ASC, data ASC"

        # Apply filters on query string
        query = Template(GET_FIELD_DIRECT_CHILDREN_BY_ID_QUERY)
        query = query.substitute(field_id=field_id, filters=filters_str, order_by=order_by_str)

        children = await self.db.fetch_all(
            query=query, values={**(filters or {})}
        )
        if not children:
            return []

        all_fields = []
        for field in children:
            try:
                field_object = FieldInDB(**field)
            except ValidationError as e:
                if (
                    e.errors()[0]["loc"][0] == "parent_id"
                    and e.errors()[0]["type"] == "type_error.none.not_allowed"
                ):
                    raise HTTPException(
                        status_code=HTTP_400_BAD_REQUEST,
                        detail=i18n.t('translate.policies.report_ids_not_allowed_only_fields_error'),
                    )
                else:
                    raise HTTPException(
                        status_code=HTTP_400_BAD_REQUEST, detail=e.json()
                    )

            field_children = await self.get_field_all_children_by_id(
                id=id, field_id=field_object.id, params=params, filters=filters
            )

            if field_children:
                field_object.children = field_children

            all_fields.append(field_object)

        return all_fields

    async def update_field(
        self,
        *,
        report_id: int,
        field_id: int,
        field_update: FieldUpdate,
        run_refresh=False,
    ) -> FieldInDB:
        """
        report_id: ID of the report containing the field going to be updated
        field_id: ID of the field going to be updated
        field_update: object containing the attributes to be update
        run_refresh: whether to drop the database materialized views after the update

        Update the attributes of the target field.
        """
        field = await self.get_field_by_id(id=report_id, field_id=field_id)

        if not field:
            return None

        # Raise error if there are no valid params left
        if not field_update.model_dump(exclude_unset=True):
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.invalid_update_params'),
            )

        # If the field is a template type, check only allowed properties are changed
        if field.field_type.startswith("template_"):
            try:
                FieldTemplateUpdate(**field_update.model_dump())
            except:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.report.invalid_update_params_for_a_template'),
                )

        # If there is an update on the parent_id, prevent that
        # it is not containing a child id.
        if field_update.model_dump()["parent_id"] is not None:
            children = (
                await self.get_flatten_recursive_children_from_parent_by_id(
                    report_id=report_id, field_id=field_id
                )
            )
            children_ids = [FieldInDB(**field).id for field in children]
            if field_update.model_dump()["parent_id"] in children_ids:
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=i18n.t('translate.report.field_cant_have_child_as_parent_error'),
                )

        field_update_params = field.model_copy(
            update=field_update.model_dump(exclude_none=True),
        )

        # Raise error if the update params merged with the existent field are invalid
        try:
            FieldInDB(**field_update_params.model_dump())
        except Exception as e:
            logger.error(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.invalid_update_params'),
            )

        try:
            updated_field = await self.db.fetch_one(
                query=UPDATE_FIELD_BY_ID_QUERY,
                values=field_update_params.model_dump(exclude={
                    "data_id",
                    "children",
                    "external_source_id",
                    "external_source_field_id"
                }),
            )
            field_object = FieldInDB(**updated_field)
        except Exception as e:
            logger.error(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.invalid_update_params'),
            )

        # If the updated field was a template, update also its linked fields
        if field.field_type.startswith("template_"):
            # Get all fields which have field.id as template_id
            linked_fields = await self.db.fetch_all(
                query=GET_FIELDS_BY_TEMPLATE_ID_QUERY, values={"template_id": field.id}
            )

            # Run update_field on each field
            for linked_field in linked_fields:
                await self.update_field(
                    report_id=report_id, field_id=linked_field["id"], field_update=FieldUpdate(**field_update.model_dump(include={'name'}))
                )

        # Add children to current field for response
        children = await self.get_field_all_children_by_id(
            id=report_id, field_id=field_object.id
        )
        if children:
            field_object.children = children

        # Run only once, at the end of the field update process
        if run_refresh:
            # Refresh materialized views of the report
            await self.drop_report_materialized_views(report_id=report_id)

        return field_object

    async def copy_field(
        self,
        *,
        id: int,
        target_field_id: int,
        field_copy: FieldCopy,
    ) -> FieldInDB:
        """
        id: ID of the target report
        target_field_id: ID of the target field or report (can be the same as `id`) where to copy the fields
        field_copy: Configuration of fields to copy from

        Copy fields from one report to another report or list of fields if field_copy.loop is true.
        """

        logger.info('Starting copying fields from {} to {}...'.format(field_copy.source_id, target_field_id))

        if field_copy.recursive == False:
            raise HTTPException(
                status_code=HTTP_405_METHOD_NOT_ALLOWED,
                detail=i18n.t('translate.report.not_recursive_copy_not_implemented_error'),
            )

        # Check target field exist
        report_repo = ReportsRepository(self.db)
        try:
            target_field = await self.get_field_by_id(id=id, field_id=target_field_id)
        except HTTPException as e:
            if (
                e.status_code == HTTP_400_BAD_REQUEST
                and e.detail == i18n.t('translate.policies.report_ids_not_allowed_only_fields_error')
            ):
                # Try if the target is a report
                target_field = await report_repo.get_report_by_id(id=target_field_id)
                if not target_field:
                    return None

        # Get source field if its a field or a report
        try:
            source_field = await self.get_field_by_id(
                id=id,
                field_id=field_copy.source_id
            )
        except HTTPException as e:
            if (
                e.status_code == HTTP_400_BAD_REQUEST
                and e.detail == i18n.t('translate.policies.report_ids_not_allowed_only_fields_error')
            ):
                # Try if the source is a report
                source_field = await report_repo.get_report_by_id(id=field_copy.source_id)
                if not source_field:
                    return None

        fields_to_copy_to = [target_field]

        # Check if loop is enabled
        if field_copy.loop is True and field_copy.field_name is not None:
            logger.debug('Fields copy [from {} to {}]: getting fields to copy to from the loop...'.format(field_copy.source_id, target_field_id))
            # Get all fields filtered by field_name
            fields_to_copy_to = (
                await self.get_flatten_recursive_children_from_parent_by_id(
                    report_id=id,
                    field_id=target_field_id,
                    filters={"name": field_copy.field_name},
                )
            )
            fields_to_copy_to = [FieldInDB(**field) for field in fields_to_copy_to]

        async def deep_clone_field(source_field_id, target_field_id, identifier_type):
            async with self.db.transaction():
                # Defer constraint on foreign key
                await self.db.execute(query=DEFER_FOREIGN_KEY_CONSTRAINTS)
                # Create shallow copy of every children fields
                query = Template(DEEP_COPY_CHILDREN_FIELDS_QUERY).substitute(
                    identifier_type=identifier_type,
                    source_field_id=source_field_id,
                    target_field_id=target_field_id
                )
                created_fields = await self.db.fetch_all(query=query)

            # For every created field, create new data to make a real deep copy
            return created_fields

        logger.debug('Fields copy [from {} to {}]: running deep clone on all fields...'.format(field_copy.source_id, target_field_id))
        created_fields = []
        for idx, target_field in enumerate(fields_to_copy_to):
            logger.debug('Fields copy [from {} to {}]: running deep clone [{}/{}]...'.format(field_copy.source_id, target_field_id, idx, len(fields_to_copy_to)))
            created_fields += await deep_clone_field(
                field_copy.source_id, target_field.id, field_copy.identifier_type
            )

        if field_copy.run_side_effects:
            logger.debug('Fields copy [from {} to {}]: running fields side effects...'.format(field_copy.source_id, target_field_id))
            # Run side effects for every created field if they are promo fields
            for idx, created_field in enumerate(created_fields):
                logger.debug('Fields copy [from {} to {}]: running fields side effects [{}/{}]...'.format(field_copy.source_id, target_field_id, idx, len(created_fields)))
                if created_field.field_type == "container_promo":
                    logger.debug('Fields copy [from {} to {}]: running container_promo side effect [{}/{}]...'.format(field_copy.source_id, target_field_id, idx, len(created_fields)))
                    await self.run_container_promo_side_effects(
                        report_id=id, field_id=created_field.id
                    )
                elif created_field.field_type == "container":
                    logger.debug('Fields copy [from {} to {}]: running container side effect [{}/{}]...'.format(field_copy.source_id, target_field_id, idx, len(created_fields)))
                    await self.run_container_side_effects(
                        report_id=id, new_field=created_field, run_refresh=False
                    )

        # Refresh materialized views of the report
        await self.drop_report_materialized_views(report_id=id)

        logger.info('Finished copying fields from {} to {}.'.format(field_copy.source_id, target_field_id))

        return created_fields

    async def delete_field_by_id(self, *, id: int, field_id: int) -> int:
        """
        id: ID of the target report where the field has to be deleted
        field_id: ID of the field to be deleted

        Delete a field and its linked fields if it is a template given its id.
        """
        field = await self.get_field_by_id(id=id, field_id=field_id)
        if not field:
            return None

        try:
            deleted_id = await self.db.execute(
                query=DELETE_FIELD_BY_ID_QUERY,
                values={"id": field_id},
            )
        # Should never happen as now the Foreign Key constraint has CASCADE ON DELETE
        except ForeignKeyViolationError as e:
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=i18n.t('translate.report.fields_still_linked_to_report_to_delete_error'),
            )

        # If the deleted field was a template, delete also its linked fields
        if field.field_type.startswith("template_"):
            # Get all fields which have field.id as template_id
            linked_fields = await self.db.fetch_all(
                query=GET_FIELDS_BY_TEMPLATE_ID_QUERY, values={"template_id": field.id}
            )

            # Run delete_field_by_id on each field
            for linked_field in linked_fields:
                await self.delete_field_by_id(id=id, field_id=linked_field["id"])

        # Refresh materialized views of the report
        await self.drop_report_materialized_views(report_id=id)

        return deleted_id
