from databases import Database
from typing import Optional

from app.db.repositories.base import BaseRepository
from app.models.user import UserBase
from app.services import auth_service


class UsersRepository(BaseRepository):
    def __init__(self, db: Database) -> None:
        super().__init__(db)
        self.auth_service = auth_service

    async def authenticate_user(
        self, *, username: str, password: str
    ) -> Optional[UserBase]:
        # Only allow authentication for root user
        if not username == "root":
            return None

        # Retrieve root credentials from environment variables
        user_password = self.root_password
        user_salt = self.root_password_salt

        # if submitted password doesn't match
        if not self.auth_service.verify_password(
            password=password, salt=user_salt, hashed_pw=user_password
        ):
            return None

        return UserBase(username=username)

    async def get_user_by_username(self, *, username: str) -> UserBase:
        # Only allow to get root user
        if not username == "root":
            return None

        return UserBase(username=username)
