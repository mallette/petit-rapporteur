"""Add index on parent_id

Revision ID: e139aed2b87a
Revises: 51e149e906a5
Create Date: 2023-01-25 11:02:53.742620

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = 'e139aed2b87a'
down_revision = '51e149e906a5'
branch_labels = None
depends_on = None

def upgrade() -> None:
    with op.batch_alter_table("fields") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.create_index(
            "fields_parent_id_index", ["parent_id"]
        )

def downgrade() -> None:
    with op.batch_alter_table("fields") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.drop_index(
            "fields_parent_id_index"
        )
