"""create_main_tables

Revision ID: d6a67c66f006
Revises: 
Create Date: 2022-08-17 08:38:58.873121

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "d6a67c66f006"
down_revision = None
branch_labels = None
depends_on = None


def create_reports_table() -> None:
    op.create_table(
        "fields",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text, nullable=False, index=True),
        sa.Column("parent_id", sa.Integer, sa.ForeignKey("fields.id"), nullable=True),
        sa.Column("template_id", sa.Integer, nullable=True),
        sa.Column("data_id", sa.Integer, nullable=False),
        sa.Column("external_source_id", sa.String, nullable=True),
        sa.Column("external_source_field_id", sa.String, nullable=True),
        sa.Column("field_type", sa.String, nullable=False),
        sa.Column("comparator_id", sa.Integer, nullable=True),
    )

    op.create_table(
        "data_versions",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("author", sa.Text, nullable=False, index=True),
        sa.Column(
            "field_id",
            sa.Integer,
            sa.ForeignKey("fields.id", ondelete="CASCADE", deferrable=True),
            nullable=True,
        ),
        sa.Column("value", sa.Text, nullable=False),
        sa.Column("date", sa.DateTime, server_default=sa.func.now()),
    )

    op.create_foreign_key(
        "field_id_data_versions_fkey", "fields", "data_versions", ["data_id"], ["id"]
    )


def upgrade() -> None:
    create_reports_table()


def downgrade() -> None:
    op.drop_constraint("field_id_data_versions_fkey", "fields", type_="foreignkey")
    op.drop_table("data_versions")
    op.drop_table("fields")
