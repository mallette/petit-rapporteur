"""Create policies tables

Revision ID: 51e149e906a5
Revises: 6bae4b36768d
Create Date: 2022-10-27 15:49:50.995398

"""
from typing import Tuple
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "51e149e906a5"
down_revision = "6bae4b36768d"
branch_labels = None
depends_on = None


def create_updated_at_trigger() -> None:
    op.execute(
        """
        CREATE OR REPLACE FUNCTION update_updated_at_column()
            RETURNS TRIGGER AS
        $$
        BEGIN
            NEW.updated_at = now();
            RETURN NEW;
        END;
        $$ language 'plpgsql';
        """
    )


def timestamps(indexed: bool = False) -> Tuple[sa.Column, sa.Column]:
    return (
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=indexed,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=indexed,
        ),
    )


def create_tables() -> None:
    op.create_table(
        "policies",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("report_id", sa.Text, nullable=False),
        sa.Column("field_id", sa.Text, nullable=False),
        sa.Column("actions", sa.ARRAY(sa.Text), nullable=False),
        sa.Column("filters", sa.ARRAY(sa.Text), nullable=True),
        sa.Index(
            "unique_policies_index",
            "report_id",
            "field_id",
            "actions",
            "filters",
            unique=True,
        ),
        *timestamps(),
    )

    op.execute(
        """
        CREATE TRIGGER update_policies_modtime
            BEFORE UPDATE
            ON policies
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )

    op.create_table(
        "policies_groups",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("comment", sa.Text, nullable=False, unique=True),
        *timestamps(),
    )

    op.execute(
        """
        CREATE TRIGGER update_policies_groups_modtime
            BEFORE UPDATE
            ON policies_groups
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )

    op.create_table(
        "policies_groups_policies",
        sa.Column(
            "policy_id",
            sa.Integer,
            sa.ForeignKey("policies.id", ondelete="CASCADE"),
            nullable=False,
        ),
        sa.Column(
            "policies_group_id",
            sa.Integer,
            sa.ForeignKey("policies_groups.id", ondelete="CASCADE"),
            nullable=False,
        ),
        sa.Index(
            "unique_policies_groups_policies_index",
            "policy_id",
            "policies_group_id",
            unique=True,
        ),
        *timestamps(),
    )

    op.execute(
        """
        CREATE TRIGGER update_policies_groups_policies_modtime
            BEFORE UPDATE
            ON policies_groups_policies
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def upgrade() -> None:
    create_updated_at_trigger()
    create_tables()


def downgrade() -> None:
    op.drop_constraint(
        "policies_groups_policies_policy_id_fkey",
        "policies_groups_policies",
        type_="foreignkey",
    )
    op.drop_constraint(
        "policies_groups_policies_policies_group_id_fkey",
        "policies_groups_policies",
        type_="foreignkey",
    )
    op.drop_table("policies")
    op.drop_table("policies_groups")
    op.drop_table("policies_groups_policies")
