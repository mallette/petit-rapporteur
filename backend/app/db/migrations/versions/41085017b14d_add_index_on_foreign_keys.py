"""Add index on foreign keys

Revision ID: 41085017b14d
Revises: e139aed2b87a
Create Date: 2023-11-07 13:10:10.499450

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = '41085017b14d'
down_revision = 'e139aed2b87a'
branch_labels = None
depends_on = None

def upgrade() -> None:
    with op.batch_alter_table("fields") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.create_index(
            "fields_data_id_index", ["data_id"]
        )
    with op.batch_alter_table("data_versions") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.create_index(
            "data_versions_field_id_index", ["field_id"]
        )

def downgrade() -> None:
    with op.batch_alter_table("fields") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.drop_index(
            "field_id_data_id_index"
        )
    with op.batch_alter_table("data_versions") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.drop_index(
            "data_versions_field_id_index"
        )
