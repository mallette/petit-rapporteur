"""Add DELETE ON CASCADE for fields

Revision ID: 6bae4b36768d
Revises: d6a67c66f006
Create Date: 2022-10-05 09:04:49.619419

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "6bae4b36768d"
down_revision = "d6a67c66f006"
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table("fields") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.drop_constraint("fields_parent_id_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            "fields_parent_id_fkey",
            "fields",
            ["parent_id"],
            ["id"],
            ondelete="CASCADE",
            deferrable=True,
        )


def downgrade() -> None:
    with op.batch_alter_table("fields") as batch_op:
        # Source table argument is not specified as implied by batch_op
        batch_op.drop_constraint("fields_parent_id_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            "fields_parent_id_fkey", "fields", ["parent_id"], ["id"]
        )
