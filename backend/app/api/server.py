import i18n
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware

from app import AVAILABLE_LANGUAGES
from app.core import config, tasks

from app.api.routes import router as api_router
from app.db.repositories.fields import FieldsRepository
from app.db.repositories.policies import PoliciesRepository
from app.db.repositories.reports import ReportsRepository
from app.models.policy import ReportShareCreate


def get_application():
    app = FastAPI(title=config.PROJECT_NAME, version=config.VERSION)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.add_event_handler("startup", tasks.create_start_app_handler(app))
    app.add_event_handler("shutdown", tasks.create_stop_app_handler(app))
    app.include_router(api_router, prefix="/v1")

    @app.on_event("startup")
    async def startup_event():
        """
        Initialize database with unrestricted policy for root user.
        """
        policies_repo = PoliciesRepository(app.state._db)
        new_policy_group = ReportShareCreate(
            comment="All actions",
            policies=[
                {
                    "actions": [
                        "report:list",
                        "report:read",
                        "report:write",
                        "report:import",
                        "report:create",
                        "report:share",
                        "report:edit",
                        "report:delete",
                    ]
                }
            ],
        )

        # Allow upsert to avoid errors at server restart
        await policies_repo.create_policies_group(
            upsert=True, policies_group_params=new_policy_group
        )

        # Setup database functions
        fields_repo = FieldsRepository(app.state._db)
        await fields_repo.create_database_functions()

        # Drop views which will be recreated on the fly
        await fields_repo.clean_database_views()

        # Clean exports files folder
        await fields_repo.clean_export_files()

        # XXX Set default name to legacy container_promos which have an empty name
        await fields_repo.fix_database_default_names()

        # XXX Create or recreate report's views to ensure they exist.
        reports_repo = ReportsRepository(app.state._db)
        await reports_repo.recreate_all_report_views()

        # XXX Recreate policies' views to ensure they are up-to-date.
        await policies_repo.recreate_policies_views()


    return app


app = get_application()


@app.middleware("http")
async def read_accept_language_header(request: Request, call_next):
    """
    Read Accept-Language HTTP Header to translate response text.
    """
    # Parse client's accepted languages
    if 'accept-language' in request.headers:
        accepted_languages = request.headers['accept-language']
        for accepted_lang in accepted_languages.split(','):
            lang = accepted_lang.split(';')
            lang_code = lang[0]
            # If the language is available, set it up
            if lang_code in AVAILABLE_LANGUAGES:
                i18n.set('locale', lang_code)
                break

    response = await call_next(request)
    return response
