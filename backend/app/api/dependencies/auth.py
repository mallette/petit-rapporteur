import i18n
from typing import Optional
from fastapi import Depends, HTTPException, status, Path
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from app.core.config import SECRET_KEY, API_PREFIX
from app.models.user import UserPublic
from app.models.policy import PoliciesGroupPublic, PoliciesGroupInDB
from app.api.dependencies.database import get_repository
from app.db.repositories.users import UsersRepository
from app.db.repositories.policies import PoliciesRepository
from app.services import auth_service


oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl=f"{API_PREFIX}/users/login/token/",
    scopes={
        "report:read": "Read report.",
        "report:write": "Write to report.",
        "report:list": "List reports. Works only if report_id is set to '*'.",
        "report:create": "Create a new report. Works only if report_id is set to '*'.",
        "report:edit": "Edit report's structure.",
        "report:delete": "Delete report or its fields.",
        "report:share": "Share report.",
    },
)


async def get_user_from_token(
    *,
    token: str = Depends(oauth2_scheme),
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
) -> Optional[UserPublic]:
    try:
        username = auth_service.get_username_from_token(
            token=token, secret_key=str(SECRET_KEY)
        )
        user = await user_repo.get_user_by_username(username=username)
    except Exception as e:
        raise e
    return user


async def get_policies_group_linked_to_token(
    *,
    token: str = Depends(oauth2_scheme),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> Optional[UserPublic]:
    try:
        policies_group_from_token = auth_service.get_policies_group_from_token(
            token=token, secret_key=str(SECRET_KEY)
        )
        if policies_group_from_token:
            policies_group_from_db = await policies_repo.get_policies_group_by_id(
                policies_group_id=policies_group_from_token.id
            )
        else:
            return None
    except Exception as e:
        raise e
    return policies_group_from_db


def get_current_active_user(
    security_scopes: SecurityScopes,
    current_user: UserPublic = Depends(get_user_from_token),
    token: str = Depends(oauth2_scheme),
) -> Optional[UserPublic]:
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f"Bearer"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail=i18n.t('translate.authentication.credentials_validation_error'),
        headers={"WWW-Authenticate": authenticate_value},
    )

    if not current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=i18n.t('translate.authentication.absent_user_or_policy_group_error'),
            headers={"WWW-Authenticate": "Bearer"},
        )

    return current_user


async def authorize_request(
    security_scopes: SecurityScopes,
    report_id: int = None,
    field_id: int = None,
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
    current_policies_group: PoliciesGroupPublic = Depends(
        get_policies_group_linked_to_token
    ),
    token: str = Depends(oauth2_scheme),
) -> bool:
    """
    Authorize the request according to the policies group linked to the token and the targeted report.
    """
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f"Bearer"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail=i18n.t('translate.authentication.credentials_validation_error'),
        headers={"WWW-Authenticate": authenticate_value},
    )

    if not current_policies_group:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=i18n.t('translate.authentication.absent_policy_group_error'),
            headers={"WWW-Authenticate": "Bearer"},
        )

    authorization = await policies_repo.check_if_authorized_by_policies(
        policies_group_id=current_policies_group.id,
        report_id=report_id,
        field_id=field_id,
        actions=security_scopes.scopes,
    )

    if authorization is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=i18n.t('translate.authentication.not_enough_permissions_error'),
            headers={"WWW-Authenticate": authenticate_value},
        )

    return authorization


# XXX Ideally, we should mutualize with authorize_request if it is possible to have an optional Path.
# For the moment it doesn't seem possible.
async def authorize_request_report_id(
    security_scopes: SecurityScopes,
    report_id: Optional[int] = Path(ge=0, title="The ID of the report."),
    field_id: Optional[int] = None,
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
    current_policies_group: PoliciesGroupPublic = Depends(
        get_policies_group_linked_to_token
    ),
    token: str = Depends(oauth2_scheme),
) -> bool:
    """
    Interface authorize_request with report_id path parameters
    """
    return await authorize_request(
        security_scopes=security_scopes,
        report_id=report_id,
        field_id=field_id,
        policies_repo=policies_repo,
        current_policies_group=current_policies_group,
        token=token,
    )


# XXX Ideally, we should mutualize with authorize_request if it is possible to have an optional Path.
# For the moment it doesn't seem possible.
async def authorize_request_report_id_field_id(
    security_scopes: SecurityScopes,
    report_id: Optional[int] = Path(ge=0, title="The ID of the report."),
    field_id: Optional[int] = Path(ge=0, title="The ID of the field."),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
    current_policies_group: PoliciesGroupPublic = Depends(
        get_policies_group_linked_to_token
    ),
    token: str = Depends(oauth2_scheme),
) -> bool:
    """
    Interface authorize_request with report_id and field_id path parameters
    """
    return await authorize_request(
        security_scopes=security_scopes,
        report_id=report_id,
        field_id=field_id,
        policies_repo=policies_repo,
        current_policies_group=current_policies_group,
        token=token,
    )


async def get_current_policies_group(
    current_policies_group: PoliciesGroupPublic = Depends(
        get_policies_group_linked_to_token
    ),
) -> PoliciesGroupInDB:
    return current_policies_group
