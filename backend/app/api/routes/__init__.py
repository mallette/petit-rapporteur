from fastapi import APIRouter
from app.api.routes.reports import router as reports_router
from app.api.routes.users import router as users_router
from app.api.routes.shares import router as shares_router
from app.api.routes.jobs import ws_router as jobs_ws_router, router as jobs_router

router = APIRouter()
router.include_router(reports_router, prefix="/reports", tags=["reports"])
router.include_router(users_router, prefix="/users", tags=["users"])
router.include_router(shares_router, prefix="/shares", tags=["shares"])
router.include_router(jobs_router, prefix="/jobs", tags=["jobs"])
router.include_router(jobs_ws_router, prefix="/ws", tags=["jobs"])
