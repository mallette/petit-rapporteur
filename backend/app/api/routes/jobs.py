import i18n
import json
import logging
import asyncio
from typing import Annotated
from pydantic import TypeAdapter, ValidationError
from rq.exceptions import NoSuchJobError
from rq.job import Job
from redis import Redis
from uuid import UUID

from starlette.status import (
    HTTP_404_NOT_FOUND,
)
from fastapi import (
    APIRouter,
    WebSocket,
    WebSocketException,
    WebSocketDisconnect,
    Query,
    Path,
    HTTPException
)

from app.models.websocket import JobSubscriptionMessage
from app.models.job import JobDetails
from app.services.websocket import ws_manager


logger = logging.getLogger(__name__)
router = APIRouter()
ws_router = APIRouter()
redis_connection = Redis(host='redis')

@router.get(
    "/{job_id}/", response_model=JobDetails, name="jobs:get-job-by-id"
)
async def get_job_by_id(
    job_id: UUID = Path(..., title="The UUID of the job to get."),
) -> JobDetails:
    try:
        job = Job.fetch(id=str(job_id), connection=redis_connection)
        job_status = job.get_status()
    except NoSuchJobError:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.job.job_not_found_error')
        )
    return {
        '_id': job.id,
        'description': job.description,
        '_status': job_status,
        'meta': job.get_meta(),
        'result': job.return_value()
    }


@ws_router.websocket("/jobs")
async def ws_get_jobs_status(
    websocket: WebSocket,
    client_id: Annotated[str, Query()],
):
    await ws_manager.connect(client_id, websocket)
    try:
        while True:
            # Check reception every second
            try:
                data = await asyncio.wait_for(websocket.receive_text(), timeout=1)

                try:
                    message = TypeAdapter(JobSubscriptionMessage).validate_json(data)

                    if message.action == 'subscribe':
                        if str(message.job_id) not in ws_manager.subscriptions:
                            ws_manager.subscriptions[str(message.job_id)] = {}
                        ws_manager.subscriptions[str(message.job_id)]['client_id'] = client_id
                except ValidationError as e:
                    await ws_manager.send_message(client_id, f"Unsupported payload: {data}")
            except asyncio.exceptions.TimeoutError:
                # Stopped listening from websocket after timeout
                pass

            # Process jobs subscriptions
            kept_subscriptions = {}
            for job_id, data in ws_manager.subscriptions.items():
                job = Job.fetch(id=job_id, connection=redis_connection)
                job_status = job.get_status()
                if 'status' not in ws_manager.subscriptions[str(job_id)]:
                    ws_manager.subscriptions[str(job_id)]['status'] = ''
                # Send notification if the status has changed
                if ws_manager.subscriptions[str(job_id)]['status'] != job_status:
                    ws_manager.subscriptions[str(job_id)]['status'] = job_status
                    await ws_manager.send_message(data['client_id'], json.dumps(
                        {
                            'id': job.id,
                            'description': job.description,
                            'status': job_status,
                            'meta': job.get_meta(),
                            'result': job.return_value()
                        }
                    ))
                # Keep job subscriptions if the jobs are not finished
                if job_status != 'finished':
                    kept_subscriptions[str(job.id)] = ws_manager.subscriptions[str(job.id)]

            ws_manager.subscriptions = kept_subscriptions
    except WebSocketDisconnect:
        ws_manager.disconnect(client_id)
