import i18n
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends, APIRouter, HTTPException, Path, Body, Security
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_401_UNAUTHORIZED,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.api.dependencies.auth import get_current_active_user
from app.api.dependencies.database import get_repository
from app.models.user import UserBase, UserPublic
from app.db.repositories.policies import PoliciesRepository
from app.db.repositories.users import UsersRepository
from app.models.token import AccessToken
from app.services import auth_service

router = APIRouter()


@router.post("/login/token/", response_model=AccessToken, name="users:login-password")
async def user_login_with_password(
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
    form_data: OAuth2PasswordRequestForm = Depends(OAuth2PasswordRequestForm),
) -> AccessToken:
    user = await user_repo.authenticate_user(
        username=form_data.username, password=form_data.password
    )

    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail=i18n.t('translate.authentication.authentication_unsuccessful'),
            headers={"WWW-Authenticate": "Bearer"},
        )

    # Use root policy group which has been created at startup for root user
    policies_group = await policies_repo.get_policies_group_by_id(policies_group_id=1)

    # Generate and return access token containing id of the policies group
    access_token = AccessToken(
        access_token=auth_service.create_access_token_for_user(
            user=user, policies_group=policies_group
        ),
        token_type="bearer",
    )

    return access_token


@router.get("/me/", response_model=UserPublic, name="users:get-current-user")
async def get_currently_authenticated_user(
    current_user: UserBase = Security(get_current_active_user, scopes=[])
) -> UserPublic:
    return current_user
