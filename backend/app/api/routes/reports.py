import i18n
import io
import json
from typing import List, Annotated
from asyncpg import ForeignKeyViolationError
from fastapi import (
    APIRouter,
    Body,
    Depends,
    File,
    Form,
    HTTPException,
    Header,
    Path,
    Query,
    Security,
    UploadFile,
    Request
)
from fastapi.responses import JSONResponse, FileResponse
import datetime
from pydantic import TypeAdapter
from starlette.status import (
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_400_BAD_REQUEST,
)
from redis import Redis
from rq import Queue, Callback
from rq.job import Job
from urllib.parse import urlparse


from app.core.config import APP_IMPORTS_FOLDER, APP_EXPORTS_FOLDER
from app.db.tasks import (
    run_copy_fields_task,
    run_add_fields_task,
    run_delete_field_task,
    run_delete_report_task,
    run_bulk_import_task
)
from app.api.dependencies.auth import (
    authorize_request,
    authorize_request_report_id,
    authorize_request_report_id_field_id,
    get_current_policies_group,
)
from app.models.report import ReportCreate, ReportPublic, ReportUpdate
from app.models.field import (
    FieldCreate,
    FieldPublic,
    FieldPublicJSON,
    FieldUpdate,
    FieldCopy,
    ColumnPublic,
)
from app.models.user import UserBase
from app.models.token import AccessToken
from app.models.job import JobDetails
from app.models.policy import PoliciesGroupPublic, Filter, PoliciesGroupInDB
from app.db.repositories.reports import ReportsRepository
from app.db.repositories.fields import FieldsRepository
from app.db.repositories.utils import UtilsRepository
from app.db.repositories.policies import PoliciesRepository
from app.api.dependencies.database import get_repository
from app.services import auth_service
from app.services.websocket import ws_manager


router = APIRouter()
redis_connection = Redis(host='redis')
queue = Queue(connection=redis_connection)


@router.get("/", response_model=List[ReportPublic], name="reports:get-reports")
async def get_all_reports(
    current_policies_group: PoliciesGroupInDB = Depends(get_current_policies_group),
    reports_repo: ReportsRepository = Depends(get_repository(ReportsRepository)),
    name: str = None,
) -> List[ReportPublic]:
    filters = {"name": name} if name is not None else None

    reports = await reports_repo.get_reports(
        filters=filters, policies_group=current_policies_group
    )
    return reports


@router.post(
    "/",
    response_model=ReportCreate,
    name="reports:create-report",
    status_code=HTTP_201_CREATED,
)
async def create_new_report(
    authorization: bool = Security(authorize_request, scopes=["report:create"]),
    new_report: ReportCreate = Body(..., embed=True),
    reports_repo: ReportsRepository = Depends(get_repository(ReportsRepository)),
) -> ReportPublic:
    created_report = await reports_repo.create_report(new_report=new_report)
    return created_report


@router.get(
    "/{report_id}/", response_model=ReportPublic, name="reports:get-report-by-id"
)
async def get_report_by_id(
    authorization: bool = Security(authorize_request_report_id, scopes=["report:read"]),
    report_id: int = Path(..., ge=0, title="The ID of the report to get."),
    reports_repo: ReportsRepository = Depends(get_repository(ReportsRepository)),
) -> ReportPublic:
    report = await reports_repo.get_report_by_id(id=report_id)
    if not report:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
        )
    return report


@router.put(
    "/{report_id}/",
    response_model=ReportPublic,
    name="reports:update-report-by-id",
)
async def update_report_by_id(
    authorization: bool = Security(authorize_request_report_id, scopes=["report:edit"]),
    report_id: int = Path(..., ge=0, title="The ID of the report to update."),
    report_update: ReportUpdate = Body(..., embed=True),
    reports_repo: ReportsRepository = Depends(get_repository(ReportsRepository)),
) -> ReportPublic:
    updated_report = await reports_repo.update_report(
        id=report_id,
        report_update=report_update,
    )
    if not updated_report:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail=i18n.t('translate.report.report_not_found_error'),
        )
    return updated_report


@router.delete("/{report_id}/", response_model=JobDetails, name="reports:delete-report-by-id")
async def delete_report_by_id(
    authorization: bool = Security(
        authorize_request_report_id, scopes=["report:delete"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to get."),
    reports_repo: ReportsRepository = Depends(get_repository(ReportsRepository)),
) -> JobDetails:
    report = await reports_repo.get_report_by_id(id=report_id)
    if not report:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
        )

    job = Job.create(
        run_delete_report_task,
        args=(report_id,),
        meta={'job_type': 'delete_report_by_id'},
        description=i18n.t(
            'translate.report.report_deletion_operation_message',
            target_id=report_id
        ),
        timeout='1h',
        connection=redis_connection
    )

    job_details = queue.enqueue_job(job)

    job_details = JobDetails(**job_details.__dict__)

    return job_details


@router.get(
    "/{report_id}/fields",
    response_model=List[FieldPublic],
    responses={
        200: {
            "content": {"application/json": {}},
            "model": FieldPublicJSON,
            "description": "Return the fields as a proper JSON item.",
        },
        200: {
            "content": {"text/csv": {}},
            "model": str,
            "description": "Return the fields as a CSV.",
        }
    },
    name="reports:get-fields-from-report-by-id",
)
async def get_fields_from_report_by_id(
    authorization: bool = Security(authorize_request_report_id, scopes=["report:read"]),
    report_id: int = Path(..., ge=0, title="The ID of the report to get from."),
    flatten: bool = False,
    export: bool = False,
    leaf_containers: bool = False,
    lazy_loading: bool = False,
    lazy_loading_from: int = None,
    include_children: bool = False,
    order_by: str = None,
    field_type: str | None = None,
    name: str | None = None,
    data: str | None = None,
    filters: str
    | None = Query(
        default=None, title="JSON object containing list of additive filters"
    ),
    search: str | None = None,
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
    utils_repo: UtilsRepository = Depends(get_repository(UtilsRepository)),
    accept: str | None = Header(default='application/json')
) -> List[FieldPublic]:
    # Parse JSON filters manually as FastAPI doesn't manage JSON objects in query location.
    if filters is not None:
        filters_list = TypeAdapter(List[Filter]).validate_python(json.loads(filters)["filters"])
    else:
        filters_list = []

    # Override filters with the authorization policy
    if authorization.filters is not None:
        # Remove filters which are already set by the policy
        for idx, filter in enumerate(filters_list):
            filter = filter.model_dump()
            if filter['field_name'] in [policy_filter.model_dump()['field_name'] for policy_filter in authorization.filters]:
                del filters_list[idx]
        # Add filters from policy
        filters_list += authorization.filters

    # The leaf_containers param is not used anymore by the front-end as of today,
    # but it is kept in case it may be useful in the future.
    if leaf_containers:
        fields = await utils_repo.get_leaf_fields(
            field_id=report_id, filters={"field_type": "container"}
        )
    # The flatten param is not used anymore by the front-end as of today, but it
    # is kept in case it may be useful in the future.
    elif flatten:
        fields = await fields_repo.get_flatten_recursive_children_from_parent_by_id(
            report_id=report_id,
            field_id=report_id,
            params={"order_by": order_by, "include_children": include_children},
            filters={"field_type": field_type, "name": name, "data": data},
        )
    elif export and accept == 'text/csv':
        filename = await fields_repo.get_csv_file_containing_children_from_parent_by_id(
            report_id=report_id,
            field_id=report_id,
            params={"order_by": order_by, "lazy_loading": lazy_loading, "lazy_loading_from": lazy_loading_from, "search": search},
            filters=filters_list,
        )

        response = FileResponse(
            filename,
            media_type="text/csv",
            status_code=200
        )

        return response
    elif export:
        fields = await fields_repo.get_fields_from_parent_by_id(
            report_id=report_id,
            field_id=report_id,
            params={"order_by": order_by, "lazy_loading": lazy_loading, "lazy_loading_from": lazy_loading_from, "search": search},
            filters=filters_list,
        )
        response = JSONResponse(status_code=200, content={
            'exported_report_id': report_id,
            'exported_fields': [dict(field._mapping) for field in fields if field.id != report_id]
        })
        return response
    else:
        fields = await fields_repo.get_nested_children_from_parent_by_id(
            report_id=report_id,
            field_id=report_id,
            params={"order_by": order_by, "lazy_loading": lazy_loading, "lazy_loading_from": lazy_loading_from, "search": search},
            filters=filters_list,
        )
    if fields is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
        )

    else:
        return fields


@router.get(
    "/{report_id}/columns",
    response_model=List[ColumnPublic],
    name="reports:get-columns-from-report-by-id",
)
async def get_columns_from_report_by_id(
    authorization: bool = Security(authorize_request_report_id, scopes=["report:read"]),
    report_id: int = Path(..., ge=0, title="The ID of the report to get from."),
    search: str | None = None,
    filters: str
    | None = Query(
        default=None, title="JSON object containing list of additive filters"
    ),
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
) -> List[ColumnPublic]:
    """
    Return what corresponds to the report's columns which can be used to apply filters.
    This same route takes into account filters too in order to filter the column's values.
    """
    # Parse JSON filters manually as FastAPI doesn't manage JSON objects in query location.
    if filters is not None:
        filters_list = TypeAdapter(List[Filter]).validate_python(json.loads(filters)["filters"])
    else:
        filters_list = []

    # Override filters with the authorization policy
    if authorization.filters is not None:
        # Remove filters which are already set by the policy
        for idx, filter in enumerate(filters_list):
            filter = filter.model_dump()
            if filter['field_name'] in [policy_filter.model_dump()['field_name'] for policy_filter in authorization.filters]:
                del filters_list[idx]
        # Add filters from policy
        filters_list += authorization.filters

    fields = await fields_repo.get_columns_from_report_by_id(
        report_id=report_id,
        filters=filters_list,
        params={"search": search}
    )

    if fields is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
        )
    return fields

@router.post(
    "/{report_id}/fields",
    response_model=List[FieldPublic]|JobDetails,
    name="reports:add-field-to-report",
    status_code=HTTP_201_CREATED,
)
async def add_field_to_report(
    authorization: bool = Security(
        authorize_request_report_id, scopes=["report:write"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to add to."),
    new_field: FieldCreate = Body(default=None, embed=True),
    exported_fields: List[FieldPublic] = Body(default=None, embed=True),
    exported_report_id: int = Body(default=None, embed=True),
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
) -> FieldPublic|JobDetails:
    if new_field is None and exported_fields is None:
        raise HTTPException(status_code=HTTP_422_UNPROCESSABLE_ENTITY, detail=i18n.t('translate.report.define_either_new_field_or_exported_fields'))

    try:
        if exported_fields is not None:
            job = Job.create(
                run_add_fields_task,
                args=(report_id, exported_report_id, exported_fields,),
                meta={'job_type': 'add_fields_by_batch_to_report'},
                description=i18n.t(
                    'translate.report.batch_creation_operation_message',
                    target_id=report_id
                ),
                timeout='1h',
                connection=redis_connection
            )

            job_details = queue.enqueue_job(job)

            job_details = JobDetails(**job_details.__dict__)

            return job_details
        # Redirect promo field creation to its specific function
        elif new_field is not None and new_field.field_type == 'container_promo':
            created_field =  await fields_repo.create_promo_field(
                report_id=report_id,
                name=new_field.name,
                data=new_field.data,
                parent_id=new_field.parent_id
            )
            created_fields = [created_field]
        else:
            created_field = await fields_repo.create_field(
                id=report_id,
                new_field=new_field,
                run_refresh=True
            )
            created_fields = [created_field]
        if len(created_fields) == 0 or created_fields[0] is None:
            raise HTTPException(
                status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.report_not_found_error')
            )
    except ForeignKeyViolationError as e:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=e.message)
    return created_fields


@router.get(
    "/{report_id}/bulk_import/{annotated_imported_csv_file}",
    name="reports:get-annotated-imported-csv-file",
    status_code=HTTP_201_CREATED,
)
async def get_annotated_imported_csv_file(
    annotated_imported_csv_file: Annotated[str, Path(
        title="Annotated imported CSV file name")],
    authorization: bool = Security(
        authorize_request_report_id, scopes=["report:import"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to add to."),
) -> FileResponse:

    annotated_imported_csv_file_path = '{}/{}'.format(
        APP_EXPORTS_FOLDER,
        annotated_imported_csv_file
    )

    try:
        response = FileResponse(
            annotated_imported_csv_file_path,
            media_type="text/csv",
            status_code=200
        )
    except RuntimeError as e:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=e.message)

    return response

@router.post(
    "/{report_id}/bulk_import",
    name="reports:import-csv-fields-into-report",
    status_code=HTTP_201_CREATED,
)
async def import_fields_to_report(
    request: Request,
    authorization: bool = Security(
        authorize_request_report_id, scopes=["report:import"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to add to."),
    source_id: Annotated[str, Form()] = None,
    file: UploadFile = File(...),
) -> JobDetails:
    # Save file onto disk
    local_filename = f"{APP_IMPORTS_FOLDER}/import_report-{report_id}_{datetime.datetime.utcnow().isoformat()}.csv"
    with open(local_filename, 'wb') as uploaded_file:
        uploaded_file.write(file.file.read())

    # Define annotated imported CSV file name
    annotated_imported_csv_filename=f'annotated_imported_csv_file_{datetime.datetime.utcnow().isoformat()}_{file.filename}'

    # Normalize source_id
    if source_id is not None:
        source_id = source_id.lower()
    else:
        source_id = f"report_{report_id}"

    lang_code = i18n.get('locale')
    # Create job to load file
    job = Job.create(
        run_bulk_import_task,
        args=(
            lang_code,
            report_id,
            source_id,
            local_filename,
            file.filename,
            annotated_imported_csv_filename,
        ),
        meta={'job_type': 'csv_file_import'},
        description=i18n.t(
            'translate.report.bulk_import_operation_message',
            report_id=report_id,
        ),
        timeout='1h',
        connection=redis_connection
    )

    job_details = queue.enqueue_job(job)

    job_details = JobDetails(**job_details.__dict__)

    return job_details


@router.get(
    "/{report_id}/fields/{field_id}",
    response_model=FieldPublic,
    name="reports:get-field-by-id",
)
async def get_field_by_id(
    authorization: bool = Security(
        authorize_request_report_id_field_id, scopes=["report:read"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to get."),
    field_id: int = Path(..., ge=0, title="The ID of the field to get."),
    order_by: str = None,
    lazy_loading: bool = False,
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
    filters: str
    | None = Query(
        default=None, title="JSON object containing list of additive filters"
    ),
) -> FieldPublic:
    # Parse JSON filters manually as FastAPI doesn't manage JSON objects in query location.
    if filters is not None:
        filters_list = TypeAdapter(List[Filter]).validate_python(json.loads(filters)["filters"])
    else:
        filters_list = []

    # Override filters with the authorization policy
    if authorization.filters is not None:
        # Remove filters which are already set by the policy
        for idx, filter in enumerate(filters_list):
            filter = filter.model_dump()
            if filter['field_name'] in [policy_filter.model_dump()['field_name'] for policy_filter in authorization.filters]:
                del filters_list[idx]
        # Add filters from policy
        filters_list += authorization.filters

    field = await fields_repo.get_field_with_children_by_id(
            report_id=report_id,
            field_id=field_id,
            params={"order_by": order_by, "lazy_loading": lazy_loading},
            filters=filters_list
    )
    if not field:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND, detail=i18n.t('translate.report.field_not_found_error')
        )
    return field


@router.post(
    "/{report_id}/fields/{field_id}",
    name="reports:copy-fields-to-report",
    status_code=HTTP_201_CREATED,
)
async def copy_fields_to_report(
    authorization: bool = Security(
        authorize_request_report_id_field_id, scopes=["report:write"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to add to."),
    field_id: int = Path(..., ge=0, title="The ID of the field to copy to."),
    field_copy: FieldCopy = Body(..., embed=True),
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
    report_repo: ReportsRepository = Depends(get_repository(ReportsRepository)),
) -> JobDetails:
    # Get target field details
    try:
        target_field = await fields_repo.get_field_by_id(
            id=report_id,
            field_id=field_id
        )
    except HTTPException as e:
        if (
            e.status_code == HTTP_400_BAD_REQUEST
            and e.detail == i18n.t('translate.policies.report_ids_not_allowed_only_fields_error')
        ):
            # Try if the target is a report
            target_field = await report_repo.get_report_by_id(id=field_id)
            if target_field is None:
                raise HTTPException(
                    status_code=HTTP_404_NOT_FOUND,
                    detail=i18n.t('translate.report.report_not_found_error')
                )
    # Get source field details
    try:
        source_field = await fields_repo.get_field_by_id(
            id=report_id,
            field_id=field_copy.source_id
        )
    except HTTPException as e:
        if (
            e.status_code == HTTP_400_BAD_REQUEST
            and e.detail == i18n.t('translate.policies.report_ids_not_allowed_only_fields_error')
        ):
            # Try if the source is a report
            source_field = await report_repo.get_report_by_id(id=field_copy.source_id)
            if not source_field:
                raise HTTPException(
                    status_code=HTTP_404_NOT_FOUND,
                    detail=i18n.t('translate.report.report_not_found_error')
                )

    job = Job.create(
        run_copy_fields_task,
        args=(report_id, field_id, field_copy,),
        meta={'job_type': 'copy_fields_into_report'},
        description=i18n.t(
            'translate.report.copy_operation_message',
            source_field_name=source_field.data,
            target_field_name=target_field.data
        ),
        timeout='1h',
        connection=redis_connection
    )

    job_details = queue.enqueue_job(job)

    job_details = JobDetails(**job_details.__dict__)

    return job_details


@router.put(
    "/{report_id}/fields/{field_id}",
    response_model=FieldPublic,
    name="reports:update-field-by-id",
)
async def update_field_by_id(
    authorization: bool = Security(
        authorize_request_report_id_field_id, scopes=["report:write"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to update."),
    field_id: int = Path(..., ge=0, title="The ID of the field to update."),
    field_update: FieldUpdate = Body(..., embed=True),
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
) -> FieldPublic:
    updated_field = await fields_repo.update_field(
        report_id=report_id,
        field_id=field_id,
        field_update=field_update,
        run_refresh=True,
    )

    if not updated_field:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail=i18n.t('translate.report.field_not_found_error'),
        )
    return updated_field


@router.delete(
    "/{report_id}/fields/{field_id}",
    response_model=JobDetails,
    name="reports:delete-field-by-id",
)
async def delete_field_by_id(
    authorization: bool = Security(
        authorize_request_report_id_field_id, scopes=["report:delete", "container_promo:delete"]
    ),
    report_id: int = Path(..., ge=0, title="The ID of the report to update."),
    field_id: int = Path(..., ge=0, title="The ID of the field to update."),
    fields_repo: FieldsRepository = Depends(get_repository(FieldsRepository)),
) -> JobDetails:
    field = await fields_repo.get_field_by_id(
            id=report_id,
            field_id=field_id,
    )
    if not field:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail=i18n.t('translate.report.field_not_found_error'),
        )

    job = Job.create(
        run_delete_field_task,
        args=(report_id, field.id,),
        meta={'job_type': 'delete_field_by_id'},
        description=i18n.t(
            'translate.report.field_deletion_operation_message',
            target_id=field.id,
            target_name=field.name
        ),
        timeout='1h',
        connection=redis_connection
    )

    job_details = queue.enqueue_job(job)

    job_details = JobDetails(**job_details.__dict__)

    return job_details
