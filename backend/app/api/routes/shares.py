from typing import List
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends, APIRouter, HTTPException, Path, Body, Security
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_401_UNAUTHORIZED,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.api.dependencies.auth import (
    authorize_request,
    authorize_request_report_id,
    authorize_request_report_id_field_id,
    get_policies_group_linked_to_token
)
from app.api.dependencies.database import get_repository
from app.db.repositories.policies import PoliciesRepository
from app.models.token import AccessToken, PoliciesGroupPublicWithAccessToken
from app.models.policy import PoliciesGroupPublic, ReportShareCreate
from app.services import auth_service

router = APIRouter()


@router.get(
    "/",
    response_model=List[PoliciesGroupPublic],
    name="shares:get-shares",
)
async def get_shares(
    authorization: bool = Security(authorize_request, scopes=["report:share"]),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> List[PoliciesGroupPublic]:
    policies_groups = await policies_repo.get_policies_groups()
    return policies_groups


@router.post(
    "/",
    response_model=PoliciesGroupPublicWithAccessToken,
    name="shares:create-share",
    status_code=HTTP_201_CREATED,
)
async def create_share(
    authorization: bool = Security(authorize_request, scopes=["report:share"]),
    report_share_create: ReportShareCreate = Body(...),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> AccessToken:
    policies_group = await policies_repo.create_policies_group(
        policies_group_params=report_share_create
    )

    # Generate and return access token containing id of the policies group
    access_token = AccessToken(
        access_token=auth_service.create_access_token_for_report_share(
            policies_group=policies_group
        ),
        token_type="bearer",
    )
    return {
        "access_token": access_token,
        "policies_group": policies_group
    }


@router.get(
    "/{share_id}",
    response_model=PoliciesGroupPublic,
    name="shares:get-share-by-id",
)
async def get_share_by_id(
    authorization: bool = Security(authorize_request, scopes=["report:share"]),
    share_id: int = Path(..., ge=0, title="The ID of the share to get."),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> PoliciesGroupPublic:
    policies_group = await policies_repo.get_policies_group_by_id(
        policies_group_id=share_id
    )
    return policies_group


@router.put(
    "/{share_id}",
    response_model=PoliciesGroupPublic,
    name="shares:update-share-by-id",
)
async def update_share_by_id(
    authorization: bool = Security(authorize_request, scopes=["report:share"]),
    share_id: int = Path(..., ge=0, title="The ID of the share to get."),
    report_share_create: ReportShareCreate = Body(...),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> PoliciesGroupPublic:
    policies_group = await policies_repo.update_policies_group_by_id(
        policies_group_id=share_id, policies_group_params=report_share_create
    )
    return policies_group


@router.get(
    "/{share_id}/token",
    response_model=AccessToken,
    name="shares:get-share-token-by-id",
)
async def get_share_token_by_id(
    authorization: bool = Security(authorize_request, scopes=["report:share"]),
    share_id: int = Path(..., ge=0, title="The ID of the share to get."),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> AccessToken:
    policies_group = await policies_repo.get_policies_group_by_id(
        policies_group_id=share_id
    )

    # Generate and return access token containing id of the policies group
    access_token = AccessToken(
        access_token=auth_service.create_access_token_for_report_share(
            policies_group=policies_group
        ),
        token_type="bearer",
    )
    return access_token


@router.get("/me/", response_model=AccessToken, name="shares:get-current-share")
async def get_currently_authenticated_share(
    current_share = Depends(get_policies_group_linked_to_token),
    policies_repo: PoliciesRepository = Depends(get_repository(PoliciesRepository)),
) -> AccessToken:
    policies_group = await policies_repo.get_policies_group_by_id(
        policies_group_id=current_share.id
    )

    # Generate and return access token containing id of the policies group
    access_token = AccessToken(
        access_token=auth_service.create_access_token_for_report_share(
            policies_group=policies_group
        ),
        token_type="bearer",
    )
    return access_token
