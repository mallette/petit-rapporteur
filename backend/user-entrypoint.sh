#!/bin/sh

# Get uid and guid from user running inside the container
CUR_DOCKER_GID=`id -g`
CUR_DOCKER_UID=`id -u`
CUR_USERNAME='app_user'

# If they don't match with the id from the host outside, adjust them
if [ ! -z "$DOCKER_GID" -a "$DOCKER_GID" != "$CUR_DOCKER_GID" ]; then
  groupmod -g ${DOCKER_GID} ${CUR_USERNAME}
fi
if [ ! -z "$DOCKER_UID" -a "$DOCKER_UID" != "$CUR_DOCKER_UID" ]; then
  usermod -g ${DOCKER_GID} -u ${DOCKER_UID} ${CUR_USERNAME}
fi

chown -R ${DOCKER_UID}:${DOCKER_GID} /backend/

# Run automatically database migrations
alembic upgrade head

su ${CUR_USERNAME} --command "${*}"
